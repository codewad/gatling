use crate::guarded;
use crate::types;
use crate::util::get_vk_handle;

use super::vkAllocateMemory;
use super::vkFreeMemory;

#[derive(Debug)]
pub struct MemoryAllocationPool
{
    handle: types::VkDeviceMemory,
    device: guarded::LogicalDeviceHandle,
    size: types::VkDeviceSize,
    type_index: u32
}

impl MemoryAllocationPool
{
    fn new(handle: types::VkDeviceMemory, device: guarded::LogicalDeviceHandle, size: types::VkDeviceSize, type_index: u32) -> Self
    {
        Self
        {
            handle: handle,
            device: device,
            size: size,
            type_index: type_index
        }
    }

    pub fn create(device: guarded::LogicalDeviceHandle, memory_type_index: u32, size: types::VkDeviceSize) -> Result<Self, types::VkResult>
    {
        Ok(Self::new(
            Self::create_handle(device.get_handle(), memory_type_index, size)?,
            device,
            size,
            memory_type_index
        ))
    }

    pub fn len(&self) -> types::VkDeviceSize
    {
        self.size
    }

    fn create_handle(device: types::VkDevice, memory_type_index: u32, device_size: types::VkDeviceSize) -> Result<types::VkDeviceMemory, types::VkResult>
    {
        let allocate_info = types::VkMemoryAllocateInfo {
            sType : types::VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
            pNext : std::ptr::null(),
            allocationSize : device_size,
            memoryTypeIndex : memory_type_index
        };

        get_vk_handle(|handle: &mut types::VkDeviceMemory| {
            unsafe { vkAllocateMemory(device, &allocate_info, std::ptr::null(), handle) }
        })
    }

    pub fn get_handle(&self) -> types::VkDeviceMemory
    {
        self.handle
    }

    pub fn get_device(&self) -> guarded::LogicalDeviceHandle
    {
        self.device.clone()
    }
}

impl Drop for MemoryAllocationPool
{
    fn drop(&mut self)
    {
        unsafe { vkFreeMemory(self.get_device().get_handle(), self.get_handle(), std::ptr::null()) };
    }
}
