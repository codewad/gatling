use std::ffi::CString;

use super::types;
use super::wrappertypes::ShaderModule;

impl types::VkPipelineShaderStageCreateInfo
{
    fn define_shader_stage(module: &ShaderModule, entrypoint: &CString, stage: types::VkShaderStageFlagBits) -> Self
    {
        Self
        {
            sType : types::VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            pNext : std::ptr::null(),
            flags : 0,
            stage : stage,
            module : module.get_handle(),
            pName : entrypoint.as_ptr(),
            pSpecializationInfo : std::ptr::null()
        }
    }

    pub fn define_vertex_shader_stage(entrypoint: &CString, module: &ShaderModule) -> Self
    {
        Self::define_shader_stage(module, entrypoint, types::VK_SHADER_STAGE_VERTEX_BIT)
    }

    pub fn define_fragment_shader_stage(entrypoint: &CString, module: &ShaderModule) -> Self
    {
        Self::define_shader_stage(module, entrypoint, types::VK_SHADER_STAGE_FRAGMENT_BIT)
    }

    pub fn define_compute_shader_stage(entrypoint: &CString, module: &ShaderModule) -> Self
    {
        Self::define_shader_stage(module, entrypoint, types::VK_SHADER_STAGE_COMPUTE_BIT)
    }
}
