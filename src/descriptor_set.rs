use std::sync::Arc;
use std::cmp::PartialEq;

use super::{
    LogicalDevice,
    vkFreeDescriptorSets,
    vkUpdateDescriptorSets,
};
use super::sampler::Sampler;
use super::types;

use crate::guarded;
use crate::Image;
use crate::util::map_vk_result;

#[derive(Clone, Debug)]
enum DescriptorBinding {
    Buffer(guarded::BufferHandle, types::VkDescriptorBufferInfo),
    StorageBuffer(guarded::BufferHandle, types::VkDescriptorBufferInfo),
    DynamicStorageBuffer(guarded::BufferHandle, types::VkDescriptorBufferInfo),
    Image(guarded::ImageViewHandle<Image>, types::VkDescriptorImageInfo)
}

pub struct DescriptorSet
{
    handle: types::VkDescriptorSet,
    pool: types::VkDescriptorPool,
    device: Arc<LogicalDevice>,
    dynamic_offsets: Vec<u32>,
    bindings: Vec<Option<DescriptorBinding>>,
}

impl std::fmt::Debug for DescriptorSet
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        f.debug_struct("DescriptorSet")
            .field("handle", &self.handle)
            .field("bindings", &self.bindings)
            .finish()
    }
}

impl DescriptorSet
{
    pub fn get_handle(&self) -> types::VkDescriptorSet
    {
        self.handle
    }

    pub fn get_device(&self) -> Arc<LogicalDevice>
    {
        self.device.clone()
    }

    pub fn reset(&mut self) {
        self.bindings.clear();
        self.dynamic_offsets.clear();
    }

    pub fn update(&mut self)
    {
        let device = self.get_device().get_handle();

        let writes : Vec<types::VkWriteDescriptorSet> = self.bindings.iter()
            .enumerate()
            .filter(|(_, x)| x.is_some())
            .map(|(i, x)| (i, x.as_ref().unwrap()))
            .map(|(binding_index, descriptor_binding)| {
                match descriptor_binding {

                    DescriptorBinding::Buffer(_, info) => {
                        types::VkWriteDescriptorSet
                        {
                            sType: types::VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                            pNext: std::ptr::null(),
                            dstSet: self.get_handle(),
                            dstBinding: binding_index as u32,
                            dstArrayElement: 0,
                            descriptorType: types::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                            descriptorCount: 1,
                            pBufferInfo: info,
                            pImageInfo: std::ptr::null(),
                            pTexelBufferView: std::ptr::null()
                        }
                    },

                    DescriptorBinding::StorageBuffer(_, info) => {
                        types::VkWriteDescriptorSet {
                            sType: types::VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                            pNext: std::ptr::null(),
                            dstSet: self.get_handle(),
                            dstBinding: binding_index as u32,
                            dstArrayElement: 0,
                            descriptorType: types::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                            descriptorCount: 1,
                            pBufferInfo: info,
                            pImageInfo: std::ptr::null(),
                            pTexelBufferView: std::ptr::null()
                        }
                    },

                    DescriptorBinding::DynamicStorageBuffer(_, info) => {
                        types::VkWriteDescriptorSet {
                            sType: types::VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                            pNext: std::ptr::null(),
                            dstSet: self.get_handle(),
                            dstBinding: binding_index as u32,
                            dstArrayElement: 0,
                            descriptorType: types::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC,
                            descriptorCount: 1,
                            pBufferInfo: info,
                            pImageInfo: std::ptr::null(),
                            pTexelBufferView: std::ptr::null()
                        }
                    },

                    DescriptorBinding::Image(_, info) => {
                        types::VkWriteDescriptorSet
                        {
                            sType: types::VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                            pNext: std::ptr::null(),
                            dstSet: self.get_handle(),
                            dstBinding: binding_index as u32,
                            dstArrayElement: 0,
                            descriptorType: types::VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                            descriptorCount: 1,
                            pBufferInfo: std::ptr::null(),
                            pImageInfo: info,
                            pTexelBufferView: std::ptr::null()
                        }
                    }
                }
            })
            .collect();

        let len = writes.len() as u32;
        let ptr = writes.as_ptr();

        unsafe { vkUpdateDescriptorSets(
            device,
            len,
            ptr,
            0,
            std::ptr::null()
        ) };
    }

    pub fn get_dynamic_offsets(&self) -> Vec<u32>
    {
        self.dynamic_offsets.clone()
    }

    pub fn buffer(&mut self, binding: u32, buffer: guarded::BufferHandle, size: usize)
    {
        let index = binding as usize;
        if self.bindings.len() <= index {
            self.bindings.resize(index + 1, None);
        }

        let info = types::VkDescriptorBufferInfo {
            buffer: buffer.get_handle(),
            offset: 0,
            range: size as types::VkDeviceSize
        };

        self.bindings[index] = Some(DescriptorBinding::Buffer(buffer, info));
    }

    pub fn sampler(&mut self, binding: u32, sampler: &Sampler, texture_image_view: guarded::ImageViewHandle<Image>)
    {
        let index = binding as usize;
        if self.bindings.len() <= index {
            self.bindings.resize(index + 1, None);
        }

        let info = types::VkDescriptorImageInfo {
            imageLayout: types::VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
            imageView: texture_image_view.get_handle(),
            sampler: sampler.get_handle()
        };

        self.bindings[index] = Some(DescriptorBinding::Image(texture_image_view, info));
    }

    pub fn storage_buffer(&mut self, binding: u32, buffer: guarded::BufferHandle, size: usize)
    {
        let index = binding as usize;
        if self.bindings.len() <= index {
            self.bindings.resize(index + 1, None);
        }

        let info = types::VkDescriptorBufferInfo {
            buffer: buffer.get_handle(),
            offset: 0,
            range: size as types::VkDeviceSize
        };

        self.bindings[index] = Some(DescriptorBinding::StorageBuffer(buffer, info));
    }

    pub fn dynamic_storage_buffer(&mut self, binding: u32, buffer: guarded::BufferHandle, size: usize)
    {
        let index = binding as usize;
        if self.bindings.len() <= index {
            self.bindings.resize(index + 1, None);
        }

        let info = types::VkDescriptorBufferInfo {
            buffer: buffer.get_handle(),
            offset: 0,
            range: size as types::VkDeviceSize
        };

        self.bindings[index] = Some(DescriptorBinding::DynamicStorageBuffer(buffer, info));

        self.dynamic_offsets.push(0);
    }
}

impl DescriptorSet
{
    pub fn new(handle: types::VkDescriptorSet, pool: types::VkDescriptorPool, device: Arc<LogicalDevice>) -> Self
    {
        Self
        {
            handle: handle,
            pool: pool,
            device: device,
            dynamic_offsets: Vec::new(),
            bindings: Vec::new()
        }
    }
}

impl Drop for DescriptorSet
{
    fn drop(&mut self) {
        let handles = [self.get_handle()];

        map_vk_result(unsafe { vkFreeDescriptorSets(
            self.get_device().get_handle(),
            self.pool,
            1,
            handles.as_ptr())
        }).unwrap();
    }
}

impl PartialEq for DescriptorSet
{
    fn eq(&self, other: &Self) -> bool
    {
        self.get_handle() == other.get_handle()
    }
}
