use std::convert::TryInto;
use super::types;

pub fn map_vk_result(result: types::VkResult) -> Result<(), types::VkResult>
{
    if result == types::VK_SUCCESS
    {
        Ok(())
    }
    else
    {
        Err(result)
    }
}

pub fn get_from_vk<T, F>(get: F) -> T
where T: Default,
      F: FnOnce(&mut T) -> ()
{
    let mut item = T::default();
    get(&mut item);
    item
}

pub fn get_ptr_from_vk<T, F>(get: F) -> *const T
    where F: FnOnce(&mut *const T) -> ()
{
    let mut ptr : *const T = std::ptr::null();
    get(&mut ptr);
    ptr
}

pub fn get_from_vk_with_result<T, F>(get: F) -> Result<T, types::VkResult>
where T: Default,
      F: FnOnce(&mut T) -> types::VkResult
{
    let mut item = T::default();
    match map_vk_result(get(&mut item))
    {
        Ok(()) => Ok(item),
        Err(code) => Err(code)
    }
}

pub fn get_vk_handle<T, F>(get: F) -> Result<*const T, types::VkResult>
where F: FnOnce(&mut *const T) -> types::VkResult
{
    let mut handle : *const T = std::ptr::null();
    match map_vk_result(get(&mut handle))
    {
        Ok(()) => Ok(handle),
        Err(code) => Err(code)
    }
}

pub fn make_vec_from_raw<T, U>(count: u32, get: T) -> Vec<U>
where T: Fn(&mut u32, &mut Vec<U>) -> ()
{
    let mut c = count;
    let ucount: usize = count.try_into().unwrap();
    let mut collection = Vec::with_capacity(ucount);

    get(&mut c, &mut collection);

    unsafe { collection.set_len(ucount) };

    collection
}

pub fn get_raw_collection<T, F, G>(count_fn: F, get_fn: G) -> Vec<T>
where F: Fn(&mut u32) -> (),
      G: Fn(&mut u32, &mut Vec<T>) -> ()
{
    make_vec_from_raw(get_from_vk(count_fn), get_fn)
}

#[cfg(any(test,feature = "test"))]
mod tests
{
    use super::*;

    #[test]
    fn test_map_vk_result()
    {
        assert_eq!(map_vk_result(types::VK_SUCCESS), Ok(()));
        assert_eq!(map_vk_result(types::VK_TIMEOUT), Err(types::VK_TIMEOUT));
        assert_eq!(map_vk_result(types::VK_ERROR_OUT_OF_HOST_MEMORY), Err(types::VK_ERROR_OUT_OF_HOST_MEMORY));
    }

    #[test]
    fn test_get_from_vk_works()
    {
        assert_eq!(get_from_vk(|x| { *x = 0 }), 0);
        assert_eq!(get_from_vk(|x| { *x = 5 }), 5);
    }

    #[test]
    fn test_get_from_vk_with_result_works()
    {
        assert_eq!(get_from_vk_with_result(|x| { *x = 0; return types::VK_SUCCESS; }), Ok(0));
        assert_eq!(get_from_vk_with_result(|x| { *x = 0; return types::VK_ERROR_OUT_OF_HOST_MEMORY; }), Err(types::VK_ERROR_OUT_OF_HOST_MEMORY));
    }

    #[test]
    fn test_get_vk_handle_works()
    {
        assert_eq!(get_from_vk_with_result(|x| { *x = 1; return types::VK_SUCCESS; }), Ok(1));
        assert_eq!(get_from_vk_with_result(|x| { *x = 1; return types::VK_ERROR_OUT_OF_HOST_MEMORY; }), Err(types::VK_ERROR_OUT_OF_HOST_MEMORY));
    }
}
