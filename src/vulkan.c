#include <vulkan/vulkan.h>

uint32_t RW_VK_MAKE_VERSION(uint32_t major, uint32_t minor, uint32_t patch)
{
    return VK_MAKE_VERSION(major, minor, patch);
}
