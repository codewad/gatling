use super::types;

impl types::VkDescriptorSetLayoutBinding
{
    fn create_for_type(binding: u32, descriptor_type: types::VkDescriptorType) -> Self
    {
        types::VkDescriptorSetLayoutBinding
        {
            binding: binding,
            descriptorType: descriptor_type,
            descriptorCount: 1,
            stageFlags: types::VK_SHADER_STAGE_ALL,
            pImmutableSamplers: std::ptr::null()
        }
    }
}

#[derive(Debug)]
pub enum DescriptorType {
    UniformBuffer(u32, u32, u32), // binding, count, size
    Sampler(u32, u32) // binding, count
}

#[derive(Debug)]
pub struct DescriptorSetDefinition
{
    bindings: Vec<types::VkDescriptorSetLayoutBinding>,
}

impl DescriptorSetDefinition
{
    pub fn map<T, F>(&self, f: F) -> Vec<T>
    where F: Fn(&types::VkDescriptorSetLayoutBinding) -> T
    {
        self.bindings.iter().map(f).collect()
    }
}

impl From<&Vec<DescriptorType>> for DescriptorSetDefinition
{
    fn from(o: &Vec<DescriptorType>) -> Self {
        let mut tmp = Self::new();

        for t in o {
            tmp.add_descriptor_type(t);
        }

        tmp
    }
}

impl DescriptorSetDefinition
{
    pub fn new() -> Self
    {
        Self
        {
            bindings: Vec::new(),
        }
    }

    pub fn len(&self) -> usize
    {
        self.bindings.len()
    }

    pub fn as_ptr(&self) -> *const types::VkDescriptorSetLayoutBinding
    {
        self.bindings.as_ptr()
    }

    pub fn add_descriptor_type(&mut self, t: &DescriptorType) {
        match t {
            DescriptorType::UniformBuffer(binding, count, _size) => self.add_uniform_buffer(*binding, *count),
            DescriptorType::Sampler(binding, count) => self.add_image_sampler(*binding, *count)
        }
    }

    pub fn add_uniform_buffer(&mut self, binding: u32, count: u32)
    {
        self.bindings.push(
            types::VkDescriptorSetLayoutBinding
            {
                binding: binding,
                descriptorType: types::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                descriptorCount: count,
                stageFlags: types::VK_SHADER_STAGE_VERTEX_BIT | types::VK_SHADER_STAGE_FRAGMENT_BIT,
                pImmutableSamplers: std::ptr::null()
            }
        );
    }

    pub fn add_image_sampler(&mut self, binding: u32, count: u32)
    {
        self.bindings.push(
            types::VkDescriptorSetLayoutBinding
            {
                binding: binding,
                descriptorType: types::VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                descriptorCount: count,
                stageFlags: types::VK_SHADER_STAGE_FRAGMENT_BIT,
                pImmutableSamplers: std::ptr::null()
            }
        );
    }

    pub fn add_storage_buffer(&mut self)
    {
        self.bindings.push(
            types::VkDescriptorSetLayoutBinding::create_for_type(
                self.bindings.len() as u32,
                types::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER
            )
        );
    }

    pub fn add_dynamic_storage_buffer(&mut self)
    {
        self.bindings.push(
            types::VkDescriptorSetLayoutBinding::create_for_type(
                self.bindings.len() as u32,
                types::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC
            )
        );
    }
}
