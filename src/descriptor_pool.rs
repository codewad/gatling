use std::sync::Arc;

use super::types;

use super::{
    vkDestroyDescriptorPool
};

use super::{
    LogicalDevice,
    DescriptorSetLayout,
    DescriptorSetDefinition
};

use super::descriptor_set::DescriptorSet;
use crate::guarded::DescriptorSetLayoutHandle;

#[derive(Debug)]
pub struct DescriptorPool
{
    handle: types::VkDescriptorPool,
    device: Arc<LogicalDevice>
}

impl DescriptorPool
{
    fn create_descriptor_pool(device: &LogicalDevice, definition: &DescriptorSetDefinition) -> Result<types::VkDescriptorPool, types::VkResult>
    {
        let pool_sizes : Vec<types::VkDescriptorPoolSize> = definition.map(|binding| {
            types::VkDescriptorPoolSize
            {
                r#type : binding.descriptorType,
                descriptorCount : 1
            }
        });

        let create_info = types::VkDescriptorPoolCreateInfo
        {
            sType : types::VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
            pNext : std::ptr::null(),
            flags : types::VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT,
            maxSets : 10000 as u32,
            poolSizeCount : pool_sizes.len() as u32,
            pPoolSizes : pool_sizes.as_ptr()
        };

        DescriptorPool::create_handle(device, create_info)
    }

    pub fn new(device: Arc<LogicalDevice>, definition: &DescriptorSetDefinition) -> Result<Self, types::VkResult>
    {
        match Self::create_descriptor_pool(&device, definition)
        {
            Ok(handle) => Ok(Self {
                handle: handle,
                device: device
            }),
            Err(e) => Err(e)
        }

    }

    pub fn get_handle(&self) -> types::VkDescriptorPool
    {
        self.handle
    }

    pub fn get_device(&self) -> Arc<LogicalDevice>
    {
        self.device.clone()
    }

    pub fn create_descriptor_set(&self, layout: &DescriptorSetLayoutHandle) -> Result<DescriptorSet, types::VkResult>
    {
        let handles = [layout.get_handle()];

        let allocate_info = types::VkDescriptorSetAllocateInfo
        {
            sType : types::VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
            pNext : std::ptr::null(),
            descriptorPool : self.get_handle(),
            descriptorSetCount : 1,
            pSetLayouts : handles.as_ptr()
        };

        Ok(DescriptorSet::new(
            (DescriptorSet::create_handles(&self.get_device(), 1, allocate_info)?)[0],
            self.get_handle(),
            self.get_device()
        ))
    }

    pub fn create_descriptor_sets(&self, layouts: &Vec<&DescriptorSetLayout>) -> Vec<DescriptorSet>
    {
        let layout_handles : Vec<types::VkDescriptorSetLayout> = layouts.iter().map(
            |l| {
                l.get_handle()
            }
        ).collect();

        let count = layouts.len();
        let allocate_info = types::VkDescriptorSetAllocateInfo
        {
            sType : types::VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
            pNext : std::ptr::null(),
            descriptorPool : self.get_handle(),
            descriptorSetCount : count as u32,
            pSetLayouts : layout_handles.as_ptr()
        };

        DescriptorSet::create_handles(&self.get_device(), count, allocate_info).unwrap()
            .iter()
            .map(
                |handle| {
                    DescriptorSet::new(*handle, self.get_handle(), self.get_device())
                }
            ).collect()
    }
}

impl Drop for DescriptorPool
{
    fn drop(&mut self)
    {
        unsafe { vkDestroyDescriptorPool(self.get_device().get_handle(), self.get_handle(), std::ptr::null()) };
    }
}
