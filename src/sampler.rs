use std::sync::Arc;

use super::{
    vkDestroySampler
};
use super::wrappertypes::LogicalDevice;
use super::types;

#[derive(Debug)]
pub struct Sampler
{
    handle: types::VkSampler,
    device: Arc<LogicalDevice>
}

impl Sampler
{
    fn create_sampler(device: &LogicalDevice) -> Result<types::VkSampler, types::VkResult>
    {
        let create_info = types::VkSamplerCreateInfo
        {
            sType : types::VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
            pNext : std::ptr::null(),
            flags : 0,
            magFilter : types::VK_FILTER_NEAREST,
            minFilter : types::VK_FILTER_NEAREST,
            mipmapMode : types::VK_SAMPLER_MIPMAP_MODE_LINEAR,
            addressModeU : types::VK_SAMPLER_ADDRESS_MODE_REPEAT,
            addressModeV : types::VK_SAMPLER_ADDRESS_MODE_REPEAT,
            addressModeW : types::VK_SAMPLER_ADDRESS_MODE_REPEAT,
            mipLodBias : 0.0,
            anisotropyEnable : types::VK_TRUE,
            maxAnisotropy : 16.0,
            compareEnable : types::VK_FALSE,
            compareOp : types::VK_COMPARE_OP_ALWAYS,
            minLod : 0.0,
            maxLod : 0.0,
            borderColor : types::VK_BORDER_COLOR_INT_OPAQUE_BLACK,
            unnormalizedCoordinates : types::VK_FALSE
        };

        Self::create_handle(device, create_info)
    }

    pub fn new(device: Arc<LogicalDevice>) -> Result<Self, types::VkResult>
    {
        match Self::create_sampler(&device) {
            Ok(handle) => Ok(Self {
                handle: handle,
                device: device
            }),
            Err(e) => Err(e)
        }
    }

    pub fn get_handle(&self) -> types::VkSampler
    {
        self.handle
    }

    pub fn get_device(&self) -> Arc<LogicalDevice>
    {
        self.device.clone()
    }
}

impl Drop for Sampler
{
    fn drop(&mut self)
    {
        unsafe { vkDestroySampler(self.get_device().get_handle(), self.get_handle(), std::ptr::null()) };
    }
}
