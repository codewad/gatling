use std::collections::HashMap;
use std::os::raw::c_char;
use std::sync::Arc;
use std::sync::Mutex;
use std::sync::RwLock;
use raw_window_handle::HasRawWindowHandle;
use allocation_tracker::AllocationBlock;

use crate::types;
use crate::Buffer;
use crate::CommandBuffer;
use crate::CommandBufferRecording;
use crate::CommandPool;
use crate::DescriptorPool;
use crate::DescriptorSet;
use crate::DescriptorSetDefinition;
use crate::DescriptorSetLayout;
use crate::Fence;
use crate::FrameBuffer;
use crate::GraphicsPipeline;
use crate::Image;
use crate::ImageView;
use crate::IntoVertexAttributeDescriptions;
use crate::LogicalDevice;
use crate::MemoryAllocation;
use crate::MemoryMap;
use crate::PipelineLayout;
use crate::PhysicalDevice;
use crate::Queue;
use crate::RenderPass;
use crate::Sampler;
use crate::Semaphore;
use crate::Surface;
use crate::SwapChain;
use crate::VulkanInstance;
use crate::VulkanLibContext;
use crate::image::ImageTrait;
use crate::wrappertypes::AllocatorHandle;

fn string_from_raw(raw: *const c_char) -> String
{
    unsafe { std::ffi::CStr::from_ptr(raw) }.to_str().unwrap().trim().to_string()
}

#[derive(Clone)]
pub struct LibContextHandle
{
    pub context: Arc<VulkanLibContext>
}

impl LibContextHandle
{
    pub fn new() -> Self
    {
        Self
        {
            context: Arc::new(crate::get_library())
        }
    }

    pub fn create_instance(
        &self,
        preferred_extensions: Vec<String>,
        preferred_layers: Vec<String>
    ) -> Result<InstanceHandle, types::VkResult>
    {
        let context = self.context.clone();

        let available_instance_extensions = context.get_available_extensions();
        let available_instance_layers = context.get_available_layers();

        let selected_properties : Vec<*const c_char> = available_instance_extensions.iter()
            .filter(|p| {
                let name_string = string_from_raw(p.extensionName.as_ptr() as *const c_char);
                preferred_extensions.contains(&name_string)
            })
            .map(|p| { p.extensionName.as_ptr() as *const c_char })
            .collect();

        let selected_layers : Vec<*const c_char> = available_instance_layers.iter()
            .filter(|l| {
                let name_string = string_from_raw(l.layerName.as_ptr() as *const c_char);
                preferred_layers.contains(&name_string)
            })
            .map(|l| { l.layerName.as_ptr() as *const c_char })
            .collect();

        let instance = Arc::new(
            VulkanInstance::new(
                context.get_instance(selected_properties, selected_layers)?,
                context
            )
        );

        instance.load_functions();

        Ok(InstanceHandle {
            instance: instance
        })
    }
}

#[derive(Clone, Debug)]
pub struct InstanceHandle
{
    pub instance: Arc<VulkanInstance>
}

impl InstanceHandle
{
    pub fn get_physical_devices(&self) -> Vec<PhysicalDeviceHandle>
    {
        self.instance.get_physical_devices().into_iter()
            .map(|handle| PhysicalDeviceHandle {
                device: Arc::new(PhysicalDevice::new(handle)),
                _instance: self.clone()
            })
            .collect()
    }

    pub fn create_surface<T>(&self, window: &T) -> Result<SurfaceHandle, types::VkResult>
    where T: HasRawWindowHandle
    {
        Ok(SurfaceHandle {
            surface: Arc::new(Surface::new(
                self.instance.clone(),
                window
            )?)
        })
    }
}

#[derive(Clone, Debug)]
pub struct PhysicalDeviceHandle
{
    pub device: Arc<PhysicalDevice>,
    _instance: InstanceHandle
}

impl PhysicalDeviceHandle
{
    pub fn is_discrete_gpu(&self) -> bool
    {
        let properties = self.device.get_properties();
        properties.deviceType == types::VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU
    }

    pub fn get_queue_families(&self) -> Vec<types::VkQueueFamilyProperties>
    {
        self.device.get_queue_families()
    }

    pub fn get_memory_properties(&self) -> types::VkPhysicalDeviceMemoryProperties
    {
        self.device.get_memory_properties()
    }

    pub fn get_memory_allocator(&self, memory_type_bits: u32, properties: types::VkMemoryPropertyFlags) -> AllocatorHandle {
        self.device.get_memory_allocator(memory_type_bits, properties)
    }

    pub fn find_depth_format(&self) -> Option<types::VkFormat>
    {
        self.device.find_depth_format()
    }

    pub fn make_logical_device(
        &self,
        selected_extensions: Vec<Vec<u8>>,
        queue_family_indices: Vec<u32>
    ) -> Result<LogicalDeviceHandle, types::VkResult>
    {
        let device = self.device.make_logical_device(selected_extensions, queue_family_indices)?;

        Ok(LogicalDeviceHandle::new(
            LogicalDevice::new(device, self.device.clone()),
            self.clone()
        ))
    }
}

#[derive(Clone)]
pub struct SurfaceHandle
{
    pub surface: Arc<Surface>
}

#[derive(Clone, Debug)]
pub struct LogicalDeviceHandle
{
    pub handle: Arc<LogicalDevice>,
    device: PhysicalDeviceHandle
}

impl LogicalDeviceHandle
{
    pub fn new(handle: LogicalDevice, device: PhysicalDeviceHandle) -> Self {
        Self {
            handle: Arc::new(handle),
            device: device
        }
    }

    pub fn get_handle(&self) -> types::VkDevice {
        self.handle.get_handle()
    }

    pub fn get_device(&self) -> PhysicalDeviceHandle {
        self.device.clone()
    }

    pub fn get_memory_properties(&self) -> types::VkPhysicalDeviceMemoryProperties
    {
        self.handle.get_memory_properties()
    }

    pub fn get_queue(&self, queue_index: u32) -> QueueHandle
    {
        QueueHandle::new(
            Queue::new(self.handle.get_queue(queue_index)),
            self.clone()
        )
    }

    pub fn create_fence(&self) -> Result<FenceHandle, types::VkResult>
    {
        Ok(FenceHandle
        {
            fence: Arc::new(
                Fence::new(
                    self.handle.create_fence()?,
                    self.handle.clone()
                )
            )
        })
    }

    pub fn create_semaphore(&self) -> Result<SemaphoreHandle, types::VkResult>
    {
        Ok(SemaphoreHandle
        {
            handle: Arc::new(
                Semaphore::new(
                    self.handle.create_semaphore()?,
                    self.handle.clone()
                )
            )
        })
    }

    pub fn create_command_pool(&self, queue_index: u32) -> Result<CommandPoolHandle, types::VkResult>
    {
        Ok(CommandPoolHandle::new(
            CommandPool::new(
                self.handle.create_command_pool(queue_index)?,
                self.handle.clone()
            ),
            self.clone()
        ))
    }

    pub fn create_render_pass(&self, surface_format: types::VkSurfaceFormatKHR) -> Result<RenderPassHandle, types::VkResult>
    {
        Ok(RenderPassHandle {
            render_pass: Arc::new(RenderPass::new(
                self.handle.create_render_pass(surface_format)?,
                self.handle.clone()
            ))
        })
    }

    pub fn create_swap_chain(
        &self,
        surface: SurfaceHandle,
        surface_format: types::VkSurfaceFormatKHR,
        queue_family_indices: &Vec<u32>,
        surface_capabilities: types::VkSurfaceCapabilitiesKHR,
        old_swap_chain: Option<SwapChainHandle>
    ) -> Result<SwapChainHandle, types::VkResult>
    {
        Ok(SwapChainHandle {
            handle: Arc::new(SwapChain::new(
                self.clone(),
                surface.surface.clone(),
                surface_format,
                types::VK_PRESENT_MODE_MAILBOX_KHR,
                queue_family_indices,
                surface_capabilities,
                match old_swap_chain {
                    Some(context) => Some(context),
                    None => None
                }
            )?)
        })
    }

    pub fn create_depth_image(&self, width: usize, height: usize) -> Result<ImageHandle<Image>, types::VkResult>
    {
        Ok(self.create_image_handle(
            Image::new_depth_image(self.clone(), width, height)?
        ))
    }

    pub fn create_texture_image(&self, width: usize, height: usize) -> Result<ImageHandle<Image>, types::VkResult>
    {
        Ok(self.create_image_handle(
            Image::new_texture_image(self.clone(), width, height)?
        ))
    }

    pub fn create_texture_image_with_format(&self, width: usize, height: usize, format: types::VkFormat) -> Result<ImageHandle<Image>, types::VkResult>
    {
        Ok(self.create_image_handle(
            Image::new_texture_image_with_format(self.clone(), width, height, format)?
        ))
    }

    fn create_image_handle<T: ImageTrait>(&self, image: T) -> ImageHandle<T> {
        ImageHandle::new(image, self.clone())
    }

    pub fn create_vertex_buffer(&self, size: usize) -> BufferHandle
    {
        BufferHandle::new(
            Buffer::new_vertex_buffer(self.handle.clone(), size),
            self.clone()
        )
    }

    pub fn create_index_buffer(&self, size: usize) -> BufferHandle
    {
        BufferHandle::new(
            Buffer::create_index_buffer(self.handle.clone(), size),
            self.clone()
        )
    }

    pub fn create_staging_buffer(&self, size: usize) -> BufferHandle
    {
        BufferHandle::new(
            Buffer::create_staging_buffer(self.handle.clone(), size),
            self.clone()
        )
    }

    pub fn create_uniform_buffer(&self, size: usize) -> BufferHandle
    {
        BufferHandle::new(
            Buffer::create_uniform_buffer(self.handle.clone(), size),
            self.clone()
        )
    }

    pub fn create_descriptor_pool(&self, descriptor_set_definition: &DescriptorSetDefinition) -> DescriptorPoolHandle
    {
        DescriptorPoolHandle::new(
            DescriptorPool::new(
                self.handle.clone(),
                descriptor_set_definition
            ).unwrap()
        )
    }

    pub fn create_descriptor_set_layout(&self, definition: &DescriptorSetDefinition) -> DescriptorSetLayoutHandle {
        DescriptorSetLayoutHandle::new(
            DescriptorSetLayout::new(self.clone(), definition).unwrap()
        )
    }

    pub fn create_pipeline_layout(
        &self,
        layouts: Vec<&DescriptorSetLayoutHandle>,
        push_constants: Vec<types::VkPushConstantRange>
    ) -> PipelineLayoutHandle {
        PipelineLayoutHandle::new(
            PipelineLayout::new(
                self.handle.clone(),
                layouts,
                push_constants
            ).unwrap()
        )
    }

    pub fn create_graphics_pipeline<T: IntoVertexAttributeDescriptions>(
        &self,
        shader_stages : &Vec<types::VkPipelineShaderStageCreateInfo>,
        swap_chain_extent: &types::VkExtent2D,
        pipeline_layout: &PipelineLayoutHandle,
        render_pass: &RenderPass
    ) -> Result<GraphicsPipelineHandle, types::VkResult> {
        Ok(GraphicsPipelineHandle {
            handle: Arc::new(GraphicsPipeline::new(
                self.handle.create_graphics_pipeline::<T>(
                    shader_stages,
                    swap_chain_extent,
                    pipeline_layout,
                    render_pass
                )?,
                self.handle.clone()
            ))
        })
    }
}

#[derive(Clone, Debug)]
pub struct GraphicsPipelineHandle {
    handle: Arc<GraphicsPipeline>
}

impl GraphicsPipelineHandle {
    pub fn get_handle(&self) -> types::VkPipeline {
        self.handle.get_handle()
    }
}

#[derive(Clone)]
pub struct QueueHandle
{
    pub queue: Arc<Queue>,
    device: LogicalDeviceHandle,
    in_flight_command_buffers: Vec<CommandBufferHandle>
}

impl QueueHandle {
    pub fn new(queue: Queue, device: LogicalDeviceHandle) -> Self {
        Self {
            queue: Arc::new(queue),
            device: device,
            in_flight_command_buffers: Vec::new()
        }
    }

    pub fn submit(
        &mut self,
        command_buffer: &mut CommandBufferHandle,
        wait_semaphores: Vec<&Semaphore>,
        signal_semaphores: Vec<&Semaphore>,
    ) {
        //println!("Command Buffer ({:?}) submit", command_buffer.get_handle());
        command_buffer.in_flight = true;

        if !self.in_flight_command_buffers.contains(command_buffer) {
            self.in_flight_command_buffers.push(command_buffer.clone());
        }

        self.queue.submit(
            command_buffer,
            wait_semaphores,
            signal_semaphores,
            Some(&command_buffer.fence.fence)
        );

        //println!("Fence ({:?}) Claimed via CommandBuffer ({:?})", command_buffer.fence.fence.get_handle(), command_buffer.get_handle());
    }

    pub fn submit_and_wait(
        &mut self,
        command_buffer: &mut CommandBufferHandle,
        wait_semaphores: Vec<&Semaphore>,
        signal_semaphores: Vec<&Semaphore>,
    ) {
        //println!("Command Buffer ({:?}) submit_and_wait", command_buffer.get_handle());
        command_buffer.in_flight = true;

        self.queue.submit(
            command_buffer,
            wait_semaphores,
            signal_semaphores,
            Some(&command_buffer.fence.fence)
        );

        command_buffer.wait();
        //println!("Command Buffer ({:?}) Complete", command_buffer.get_handle());
        command_buffer.reset();
        command_buffer.release();
    }

    pub fn get_completed_command_buffers(&mut self) -> Vec<CommandBufferHandle> {
        let mut i = 0;
        let mut completed : Vec<CommandBufferHandle> = Vec::new();

        while i < self.in_flight_command_buffers.len() {
            if self.in_flight_command_buffers[i].is_complete() {
                let command_buffer = self.in_flight_command_buffers.swap_remove(i);
                //println!("Command Buffer ({:?}) Complete", command_buffer.get_handle());
                completed.push(command_buffer);
            } else {
                i += 1;
            }
        }

        completed
    }
}

#[derive(Clone, Debug)]
pub struct CommandPoolHandle
{
    pub handle: Arc<CommandPool>,
    device: LogicalDeviceHandle,
    available_buffers: Arc<RwLock<Vec<CommandBufferHandle>>>
}

impl CommandPoolHandle
{
    pub fn new(pool: CommandPool, device: LogicalDeviceHandle) -> Self {
        Self {
            handle: Arc::new(pool),
            device: device,
            available_buffers: Arc::new(RwLock::new(Vec::new()))
        }
    }

    pub fn create_command_buffer(&self) -> CommandBufferHandle
    {
        match self.available_buffers.write().unwrap().pop() {
            Some(buffer) => {
                //println!("Command Buffer ({:?}) Reusing", buffer.get_handle());
                buffer
            },
            None => {
                let buffer = CommandBuffer::new(self.clone());
                //println!("Command Buffer ({:?}) Creating", buffer.get_handle());
                CommandBufferHandle::new(
                    buffer,
                    self.device.create_fence().unwrap(),
                    self.clone()
                )
            }
        }
    }

    pub fn release_command_buffer(&self, buffer: CommandBufferHandle) {
        let mut available_buffers = self.available_buffers.write().unwrap();
        if !available_buffers.contains(&buffer) {
            //println!("Command Buffer ({:?}) Releasing", buffer.get_handle());
            available_buffers.push(buffer);
        }
    }
}

#[derive(Clone)]
pub struct BufferHandle
{
    pub buffer: Arc<Mutex<Buffer>>,
    device: LogicalDeviceHandle,
    allocation: Option<MemoryAllocationHandle>
}

impl std::fmt::Debug for BufferHandle {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("BufferHandle")
            .field("handle", &self.get_handle())
            .field("memory", &self.get_memory_info())
            .finish()
    }
}

impl PartialEq for BufferHandle {
    fn eq(&self, rhs: &Self) -> bool {
        self.get_handle() == rhs.get_handle()
    }
}

impl BufferHandle
{
    pub fn new(buffer: Buffer, device: LogicalDeviceHandle) -> Self {
        Self {
            buffer: Arc::new(Mutex::new(buffer)),
            device: device,
            allocation: None
        }
    }

    pub fn get_offset(&self) -> types::VkDeviceSize {
        match &self.allocation {
            Some(allocation) => { allocation.get_offset() },
            None => 0
        }
    }

    pub fn get_memory_info(&self) -> Option<(types::VkDeviceMemory, allocation_tracker::AllocationBlock)> {
        match &self.allocation {
            Some(allocation) => { Some((
                allocation.get_handle(),
                allocation.get_block()
            )) },
            None => None
        }
    }

    pub fn get_handle(&self) -> types::VkBuffer {
        self.buffer.lock().unwrap().get_handle()
    }

    pub fn get_device(&self) -> LogicalDeviceHandle {
        self.device.clone()
    }

    pub fn get_memory_requirements(&self) -> types::VkMemoryRequirements {
        self.buffer.lock().unwrap().get_memory_requirements()
    }

    pub fn copy_to_allocation<T: std::fmt::Debug>(&mut self, data: &[T])
    {
        self.copy_to_allocation_sized(
            data.as_ptr() as *const std::ffi::c_void,
            std::mem::size_of::<T>() * data.len()
        );
    }

    pub fn copy_to_allocation_sized(&mut self, data: *const std::ffi::c_void, size: usize)
    {
        if let Some(allocation) = &mut self.allocation {
            allocation.get_map(size).copy_to(data, size);
        }
    }

    pub fn bind_memory_allocation(&mut self, allocation: MemoryAllocationHandle, offset: usize)
    {
        self.allocation = Some(allocation.clone());
        self.buffer.lock().unwrap().bind_memory(allocation, offset as types::VkDeviceSize).unwrap();
    }

    pub fn get_and_bind_memory_allocation(&mut self, properties: types::VkMemoryPropertyFlags)
    {
        if let None = self.allocation {
            let device = self.get_device();
            let physical_device = device.get_device();
            let requirements = self.get_memory_requirements();
            let allocator = physical_device.get_memory_allocator(requirements.memoryTypeBits, properties);
            let allocation = allocator.allocate(device, requirements.size).unwrap();
            allocation.bind_buffer(&self).unwrap();
            self.allocation = Some(allocation);
        }
    }
}

#[derive(Debug)]
pub struct ImageHandle<T>
{
    pub image: Arc<Mutex<T>>,
    device: LogicalDeviceHandle,
    allocation: Option<MemoryAllocationHandle>
}

impl<T> Clone for ImageHandle<T>
{
    fn clone(&self) -> Self {
        Self {
            image: self.image.clone(),
            device: self.device.clone(),
            allocation: self.allocation.clone()
        }
    }
}

impl<T: ImageTrait> ImageHandle<T>
{
    pub fn new(image: T, device: LogicalDeviceHandle) -> Self
    {
        Self {
            image: Arc::new(Mutex::new(image)),
            device: device,
            allocation: None
        }
    }

    pub fn get_and_bind_memory_allocation(&mut self, properties: types::VkMemoryPropertyFlags)
    {
        if let None = self.allocation {
            let device = self.get_device();
            let physical_device = device.get_device();
            let requirements = self.image.lock().unwrap().get_memory_requirements();
            let allocator = physical_device.get_memory_allocator(requirements.memoryTypeBits, properties);
            let allocation = allocator.allocate(device, requirements.size).unwrap();
            allocation.bind_image(&self).unwrap();
            self.allocation = Some(allocation);
        }
    }

    pub fn get_handle(&self) -> types::VkImage {
        self.image.lock().unwrap().get_handle()
    }

    pub fn get_device(&self) -> LogicalDeviceHandle {
        self.device.clone()
    }

    pub fn new_image_view(&self, aspect: types::VkImageAspectFlags) -> ImageViewHandle<T>
    {
        ImageViewHandle::new(
            self.clone(),
            ImageView::new_from_handle(self, aspect).unwrap()
        )
    }

    pub fn get_format(&self) -> types::VkFormat
    {
        self.image.lock().unwrap().get_format()
    }
}

#[derive(Debug, Clone)]
pub struct ImageViewHandle<T>
{
    pub handle: Arc<ImageView>,
    image: ImageHandle<T>
}

impl<T: ImageTrait> ImageViewHandle<T> {
    pub fn new(image: ImageHandle<T>, view: ImageView) -> Self
    {
        //println!("ImageView ({:?}) Created for image ({:?})", view.get_handle(), image.get_handle());
        Self {
            handle: Arc::new(view),
            image: image
        }
    }

    pub fn get_handle(&self) -> types::VkImageView {
        self.handle.get_handle()
    }
}

#[derive(Clone)]
pub struct RenderPassHandle
{
    pub render_pass: Arc<RenderPass>
}

#[derive(Clone, Debug)]
pub struct MemoryAllocationHandle
{
    pub allocation: Arc<MemoryAllocation>,
    map: Option<MemoryMapHandle>
}

impl MemoryAllocationHandle
{
    pub fn new(allocation: MemoryAllocation) -> Self {
        Self {
            allocation: Arc::new(allocation),
            map: None
        }
    }

    pub fn get_handle(&self) -> types::VkDeviceMemory {
        self.allocation.get_handle()
    }

    pub fn get_device(&self) -> LogicalDeviceHandle {
        self.allocation.get_device()
    }

    pub fn get_offset(&self) -> types::VkDeviceSize {
        self.allocation.get_offset()
    }

    pub fn get_block(&self) -> AllocationBlock {
        self.allocation.get_block()
    }

    pub fn bind_buffer(&self, buffer: &BufferHandle) -> Result<(), types::VkResult> {
        self.allocation.bind_buffer(buffer)
    }

    pub fn bind_image<T: ImageTrait>(&self, image: &ImageHandle<T>) -> Result<(), types::VkResult> {
        self.allocation.bind_image(image)
    }

    pub fn get_map(&mut self, size: usize) -> MemoryMapHandle {
        MemoryMapHandle {
            handle: Arc::new(self.allocation.get_map(size))
        }
    }

    pub fn copy_to<T>(&mut self, data: &[T])
    {
        let size = std::mem::size_of::<T>() * data.len();
        self.get_map(size).copy_to(
            data.as_ptr() as *const std::ffi::c_void,
            size
        );
    }
}

#[derive(Clone, Debug)]
pub struct MemoryMapHandle {
    handle: Arc<MemoryMap>
}

impl MemoryMapHandle {
    pub fn copy_to(&self, data: *const std::ffi::c_void, size: usize) {
        self.handle.copy_to(data, size)
    }
}

#[derive(Clone, Debug)]
pub struct FenceHandle
{
    pub fence: Arc<Fence>
}

impl FenceHandle
{
    pub fn wait(&self)
    {
        self.fence.wait()
    }

    pub fn reset(&self)
    {
        self.fence.reset()
    }

    pub fn is_signaled(&self) -> bool {
        self.fence.check() == types::VK_SUCCESS
    }
}

#[derive(Clone)]
pub struct SemaphoreHandle
{
    pub handle: Arc<Semaphore>
}

impl SemaphoreHandle {
    pub fn get_counter_value(&self) -> u64 {
        self.handle.get_counter_value()
    }
}

#[derive(Clone, Debug)]
pub struct CommandBufferHandle
{
    pub handle: Arc<RwLock<CommandBuffer>>,
    fence: FenceHandle,
    pool: CommandPoolHandle,
    in_flight: bool,
}

impl PartialEq for CommandBufferHandle
{
    fn eq(&self, other: &Self) -> bool
    {
        *self.handle.read().unwrap() == *other.handle.read().unwrap()
    }
}

impl CommandBufferHandle
{
    fn new(buffer: CommandBuffer, fence: FenceHandle, pool: CommandPoolHandle) -> Self {
        //println!("Fence ({:?}) New for CommandBuffer ({:?})", fence.fence.get_handle(), buffer.get_handle());

        fence.reset();

        Self {
            handle: Arc::new(RwLock::new(buffer)),
            fence: fence,
            pool: pool,
            in_flight: false
        }
    }

    pub fn get_handle(&self) -> types::VkCommandBuffer {
        self.handle.read().unwrap().get_handle()
    }

    pub fn is_complete(&self) -> bool {
        self.fence.is_signaled() || !self.in_flight
    }

    pub fn wait(&self) {
        self.fence.wait();
    }

    pub fn reset(&mut self) {
        self.handle.write().unwrap().reset();
        self.fence.reset();
        self.in_flight = false;
    }

    pub fn release(&self) {
        self.pool.release_command_buffer(self.clone());
    }

    pub fn get_fence(&self) -> FenceHandle {
        self.fence.clone()
    }

    pub fn unclaim_descriptor_sets(&self) -> Vec<DescriptorSetHandle> {
        self.handle.write().unwrap().unclaim_descriptor_sets()
    }

    pub fn begin_recording(&self) -> CommandBufferRecording
    {
        CommandBufferRecording::begin_recording(self.handle.write().unwrap())
    }

    pub fn begin_one_time_recording(&self) -> CommandBufferRecording
    {
        CommandBufferRecording::begin_one_time_recording(self.handle.write().unwrap())
    }
}

#[derive(Clone)]
pub struct SwapChainHandle
{
    pub handle: Arc<SwapChain>
}

impl SwapChainHandle
{
    pub fn get_width(&self) -> usize
    {
        self.handle.get_width()
    }

    pub fn get_height(&self) -> usize
    {
        self.handle.get_height()
    }

    pub fn create_framebuffers(&self, depth_view: &ImageViewHandle<Image>, render_pass: &RenderPass) -> Vec<FrameBuffer>
    {
        self.handle.create_framebuffers(depth_view, render_pass)
    }

    pub fn acquire_next_image(&self, semaphore: &Semaphore) -> Result<u32, types::VkResult>
    {
        self.handle.acquire_next_image(semaphore)
    }
}

#[derive(Clone, Debug)]
pub struct DescriptorPoolHandle
{
    pub handle: Arc<DescriptorPool>,
    available_sets: Arc<RwLock<HashMap<DescriptorSetLayoutHandle, Vec<DescriptorSetHandle>>>>
}

impl DescriptorPoolHandle
{
    pub fn new(handle: DescriptorPool) -> Self {
        Self {
            handle: Arc::new(handle),
            available_sets: Arc::new(RwLock::new(HashMap::new()))
        }
    }

    pub fn create_descriptor_set(&self, layout: &DescriptorSetLayoutHandle) -> DescriptorSetHandle
    {
        let mut available_sets = self.available_sets.write().unwrap();

        match available_sets.get_mut(&layout) {
            Some(sets) => match sets.pop() {
                Some(set) => set,
                None => DescriptorSetHandle::new(
                    self.handle.create_descriptor_set(layout).unwrap(),
                    self.clone(),
                    layout.clone()
                )
            },
            None => DescriptorSetHandle::new(
                self.handle.create_descriptor_set(layout).unwrap(),
                self.clone(),
                layout.clone()
            )
        }
    }

    pub fn create_descriptor_sets(&self, layouts: &Vec<&DescriptorSetLayout>) -> Vec<DescriptorSet>
    {
        self.handle.create_descriptor_sets(layouts)
    }

    fn release_set(&self, set: &DescriptorSetHandle) {
        let mut available_sets = self.available_sets.write().unwrap();

        if !available_sets.contains_key(&set.layout) {
            available_sets.insert(set.layout.clone(), Vec::new());
        }

        available_sets.get_mut(&set.layout).unwrap().push(set.clone());
    }
}

#[derive(Clone, Debug)]
pub struct DescriptorSetLayoutHandle {
    handle: Arc<DescriptorSetLayout>
}

impl PartialEq for DescriptorSetLayoutHandle {
    fn eq(&self, rhs: &Self) -> bool {
        self.get_handle() == rhs.get_handle()
    }
}
impl Eq for DescriptorSetLayoutHandle {}

impl std::hash::Hash for DescriptorSetLayoutHandle {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.get_handle().hash(state);
    }
}

impl DescriptorSetLayoutHandle {
    pub fn new(handle: DescriptorSetLayout) -> Self {
        Self {
            handle: Arc::new(handle)
        }
    }

    pub fn get_handle(&self) -> types::VkDescriptorSetLayout {
        self.handle.get_handle()
    }
}

#[derive(Clone)]
pub struct DescriptorSetHandle {
    handle: Arc<RwLock<DescriptorSet>>,
    pool: DescriptorPoolHandle,
    layout: DescriptorSetLayoutHandle
}

impl std::fmt::Debug for DescriptorSetHandle {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", *self.handle.read().unwrap())
    }
}

impl PartialEq for DescriptorSetHandle {
    fn eq(&self, rhs: &Self) -> bool {
        self.get_handle() == rhs.get_handle()
    }
}
impl Eq for DescriptorSetHandle {}

impl DescriptorSetHandle {
    pub fn new(
        handle: DescriptorSet,
        pool: DescriptorPoolHandle,
        layout: DescriptorSetLayoutHandle
    ) -> Self {
        Self {
            handle: Arc::new(RwLock::new(handle)),
            pool: pool,
            layout: layout
        }
    }

    pub fn get_handle(&self) -> types::VkDescriptorSet {
        self.handle.read().unwrap().get_handle()
    }

    pub fn get_dynamic_offsets(&self) -> Vec<u32> {
        self.handle.read().unwrap().get_dynamic_offsets()
    }

    pub fn sampler(&self, binding: u32, sampler: &Sampler, image_view: ImageViewHandle<Image>) {
        self.handle.write().unwrap().sampler(binding, sampler, image_view);
    }

    pub fn buffer(&self, binding: u32, buffer: BufferHandle, size: usize) {
        self.handle.write().unwrap().buffer(binding, buffer, size);
    }

    pub fn update(&self) {
        self.handle.write().unwrap().update()
    }

    pub fn release(&self) {
        self.handle.write().unwrap().reset();
        self.pool.release_set(self);
    }
}

#[derive(Clone)]
pub struct PipelineLayoutHandle {
    handle: Arc<PipelineLayout>
}

impl PipelineLayoutHandle {
    pub fn new(handle: PipelineLayout) -> Self {
        Self {
            handle: Arc::new(handle)
        }
    }

    pub fn get_handle(&self) -> types::VkPipelineLayout {
        self.handle.get_handle()
    }
}
