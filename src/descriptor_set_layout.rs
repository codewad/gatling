use super::types;
use super::vkDestroyDescriptorSetLayout;
use super::DescriptorSetDefinition;
use crate::LogicalDeviceHandle;

#[derive(Debug)]
pub struct DescriptorSetLayout
{
    handle: types::VkDescriptorSetLayout,
    device: LogicalDeviceHandle
}

impl DescriptorSetLayout
{
    fn create_descriptor_set_layout(
        device: &LogicalDeviceHandle,
        definition: &DescriptorSetDefinition
    ) -> Result<types::VkDescriptorSetLayout, types::VkResult> {
        let create_info = types::VkDescriptorSetLayoutCreateInfo
        {
            sType: types::VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
            pNext: std::ptr::null(),
            flags: 0,
            bindingCount: definition.len() as u32,
            pBindings: definition.as_ptr()
        };

        DescriptorSetLayout::create_handle(device.get_handle(), &create_info)
    }

    pub fn new(
        device: LogicalDeviceHandle,
        definition: &DescriptorSetDefinition
    ) -> Result<Self, types::VkResult> {
        match Self::create_descriptor_set_layout(&device, definition)
        {
            Ok(handle) => Ok(Self {
                handle: handle,
                device: device
            }),
            Err(e) => Err(e)
        }
    }

    pub fn get_handle(&self) -> types::VkDescriptorSetLayout
    {
        self.handle
    }

    pub fn get_device(&self) -> LogicalDeviceHandle
    {
        self.device.clone()
    }
}

impl Drop for DescriptorSetLayout
{
    fn drop(&mut self)
    {
        unsafe { vkDestroyDescriptorSetLayout(self.get_device().get_handle(), self.get_handle(), std::ptr::null()) };
    }
}
