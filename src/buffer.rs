use std::sync::Arc;

use crate::types;
use crate::util::map_vk_result;
use crate::vkBindBufferMemory;
use crate::vkDestroyBuffer;
use crate::vkGetBufferMemoryRequirements;
use crate::LogicalDevice;
use crate::MemoryAllocationHandle;

#[derive(Debug)]
pub struct Buffer
{
    handle: types::VkBuffer,
    device_handle: types::VkDevice,
}

impl Buffer
{
    pub fn new_vertex_buffer(device: Arc<LogicalDevice>, size: usize) -> Self
    {
        Self::new(
            device,
            size as u64,
            types::VK_BUFFER_USAGE_VERTEX_BUFFER_BIT |
            types::VK_BUFFER_USAGE_TRANSFER_DST_BIT
        )
    }

    pub fn create_vertex_storage_buffer(device: Arc<LogicalDevice>, size: usize) -> Self
    {
        Self::new(
            device,
            size as u64,
            types::VK_BUFFER_USAGE_VERTEX_BUFFER_BIT |
            types::VK_BUFFER_USAGE_STORAGE_BUFFER_BIT |
            types::VK_BUFFER_USAGE_TRANSFER_DST_BIT
        )
    }

    pub fn create_index_buffer(device: Arc<LogicalDevice>, size: usize) -> Self
    {
        Self::new(
            device,
            size as u64,
            types::VK_BUFFER_USAGE_INDEX_BUFFER_BIT |
            types::VK_BUFFER_USAGE_TRANSFER_DST_BIT
        )
    }

    pub fn create_staging_buffer(device: Arc<LogicalDevice>, size: usize) -> Self
    {
        Self::new(
            device,
            size as u64,
            types::VK_BUFFER_USAGE_TRANSFER_SRC_BIT
        )
    }

    pub fn create_uniform_buffer(device: Arc<LogicalDevice>, size: usize) -> Self
    {
        Self::new(
            device,
            size as u64,
            types::VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT
        )
    }

    pub fn create_storage_buffer(device: Arc<LogicalDevice>, size: usize) -> Self
    {
        Self::new(
            device,
            size as u64,
            types::VK_BUFFER_USAGE_STORAGE_BUFFER_BIT
        )
    }

    pub fn create_src_storage_buffer(device: Arc<LogicalDevice>, size: usize) -> Self
    {
        Self::new(
            device,
            size as u64,
            types::VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | types::VK_BUFFER_USAGE_TRANSFER_SRC_BIT
        )
    }

    pub fn create_dest_storage_buffer(device: Arc<LogicalDevice>, size: usize) -> Self
    {
        Self::new(
            device,
            size as u64,
            types::VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | types::VK_BUFFER_USAGE_TRANSFER_DST_BIT
        )
    }

    pub fn new(device: Arc<LogicalDevice>, size: u64, usage_flags: types::VkBufferUsageFlags) -> Self
    {
        let create_info = types::VkBufferCreateInfo {
            sType : types::VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
            pNext : std::ptr::null(),
            flags : 0,
            size : size,
            usage : usage_flags,
            sharingMode : types::VK_SHARING_MODE_EXCLUSIVE,
            queueFamilyIndexCount : 0,
            pQueueFamilyIndices : std::ptr::null()
        };

        let handle = Buffer::create_handle(&device, create_info).unwrap();

        Buffer
        {
            handle: handle,
            device_handle: device.get_handle(),
        }
    }

    pub fn get_handle(&self) -> types::VkBuffer
    {
        self.handle
    }

    pub fn get_memory_requirements(&self) -> types::VkMemoryRequirements
    {
        let mut requirements = types::VkMemoryRequirements::default();
        unsafe { vkGetBufferMemoryRequirements(self.device_handle, self.handle, &mut requirements) };
        requirements
    }

    pub fn bind_memory(&self, memory: MemoryAllocationHandle, offset: types::VkDeviceSize) -> Result<(), types::VkResult> {
        map_vk_result(unsafe {
            vkBindBufferMemory(
                self.device_handle,
                self.get_handle(),
                memory.get_handle(),
                (memory.get_offset() + offset) as types::VkDeviceSize
            )
        })
    }
}

impl Drop for Buffer
{
    fn drop(&mut self)
    {
        unsafe { vkDestroyBuffer(self.device_handle, self.handle, std::ptr::null()) };
    }
}
