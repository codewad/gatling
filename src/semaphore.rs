use std::sync::Arc;

use crate::util::get_from_vk_with_result;
use super::types;

use super::wrappertypes::{
    LogicalDevice
};

use super::{
    vkDestroySemaphore,
    vkGetSemaphoreCounterValue
};

#[derive(Debug)]
pub struct Semaphore
{
    handle : types::VkSemaphore,
    device : Arc<LogicalDevice>
}

impl Semaphore
{
    pub fn new(handle: types::VkSemaphore, device: Arc<LogicalDevice>) -> Self
    {
        Self
        {
            handle: handle,
            device: device
        }
    }
}

impl Semaphore
{
    pub fn get_handle(&self) -> types::VkSemaphore
    {
        self.handle
    }

    pub fn get_counter_value(&self) -> u64 {
        get_from_vk_with_result(|value: &mut u64| {
            unsafe { vkGetSemaphoreCounterValue(self.device.get_handle(), self.get_handle(), value) }
        }).unwrap()
    }
}

impl Drop for Semaphore
{
    fn drop(&mut self)
    {
        unsafe { vkDestroySemaphore(self.device.get_handle(), self.get_handle(), std::ptr::null()) };
    }
}
