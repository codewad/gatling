use itertools::Itertools;
use super::types;
use super::vkGetPhysicalDeviceSurfaceSupportKHR;
use super::wrappertypes::{
    PhysicalDevice,
    Surface
};

pub struct LogicalDeviceBuilder
{
    queue_families: Vec<types::VkQueueFamilyProperties>,
    extensions: Vec<Vec<u8>>,
    selected_queue_family_indices: Vec<u32>,
    graphics_queue_index: Option<u32>,
    compute_queue_index: Option<u32>
}

impl LogicalDeviceBuilder
{
    pub fn new(queue_families: Vec<types::VkQueueFamilyProperties>) -> Self
    {
        Self
        {
            queue_families: queue_families,
            extensions: Vec::new(),
            selected_queue_family_indices: Vec::new(),
            graphics_queue_index: None,
            compute_queue_index: None
        }
    }

    pub fn with_extension(mut self, name: Vec<u8>) -> Self
    {
        self.extensions.push(name);

        self
    }

    pub fn with_graphics_queue(mut self) -> Self
    {
        let graphics_queue_index = find_queue_family_index_by_flag(
            &self.queue_families,
            types::VK_QUEUE_GRAPHICS_BIT,
            types::VK_QUEUE_COMPUTE_BIT
        ).unwrap() as u32;

        self.graphics_queue_index = Some(graphics_queue_index);
        self.selected_queue_family_indices.push(graphics_queue_index);

        self
    }

    pub fn with_compute_queue(mut self) -> Self
    {
        let compute_queue_index = find_queue_family_index_by_flag(
            &self.queue_families,
            types::VK_QUEUE_COMPUTE_BIT,
            types::VK_QUEUE_GRAPHICS_BIT
        ).unwrap() as u32;

        self.compute_queue_index = Some(compute_queue_index);
        self.selected_queue_family_indices.push(compute_queue_index);

        self
    }

    pub fn with_graphics_and_compute_queue(mut self) -> Self
    {
        let queue_index = find_queue_family_index_by_flag(
            &self.queue_families,
            types::VK_QUEUE_COMPUTE_BIT | types::VK_QUEUE_GRAPHICS_BIT,
            0
        ).unwrap() as u32;

        self.compute_queue_index = Some(queue_index);
        self.graphics_queue_index = Some(queue_index);
        self.selected_queue_family_indices.push(queue_index);

        self
    }

    pub fn with_surface_support(mut self, physical_device: &PhysicalDevice, surface: &Surface) -> Self
    {
        let surface_support_index = (&self.queue_families)
            .iter()
            .enumerate()
            .position(|(i, _family)| {
                let mut supported : types::VkBool32 = 0;
                unsafe { vkGetPhysicalDeviceSurfaceSupportKHR(physical_device.get_handle(), i as u32, surface.get_handle(), &mut supported) };
                supported != 0
            }).unwrap();

        //self.surface_support_index = Some(surface_support_index);
        self.selected_queue_family_indices.push(surface_support_index as u32);

        self
    }


    pub fn get_extensions(&self) -> &Vec<Vec<u8>>
    {
        &self.extensions
    }

    pub fn get_queue_families(&self) -> Vec<u32>
    {
        self.selected_queue_family_indices
            .iter()
            .unique()
            .map(|&item| item)
            .collect()
    }

    pub fn get_graphics_queue_index(&self) -> Option<u32>
    {
        self.graphics_queue_index
    }

    pub fn get_compute_queue_index(&self) -> Option<u32>
    {
        self.compute_queue_index
    }
}

fn find_queue_family_index<F>(families: &Vec<types::VkQueueFamilyProperties>, condition: F) -> Option<usize>
where F: FnMut(&types::VkQueueFamilyProperties) -> bool
{
    families.iter()
        .position(condition)
}

fn find_queue_family_index_by_flag(families: &Vec<types::VkQueueFamilyProperties>, flag: types::VkQueueFlagBits, without_flag: types::VkQueueFlagBits) -> Option<usize>
{
    if let Some(index) = find_queue_family_index(
        families,
        |family| { family.queueFlags == flag }
    ) {
        Some(index)
    }
    else if let Some(index) = find_queue_family_index(
        families,
        |family| {
            family.queueFlags & flag != 0
                && family.queueFlags & without_flag == 0
        }
    ) {
        Some(index)
    }
    else
    {
        find_queue_family_index(
            families,
            |family| {
                family.queueFlags & flag != 0
            }
        )
    }
}
