use std::collections::HashMap;
use std::mem;
use std::mem::size_of;
use std::ffi::{CStr, CString, c_void};
use std::fmt;
use std::os::raw;
use std::sync::Arc;
use std::sync::Mutex;
use std::sync::RwLock;
use std::sync::RwLockWriteGuard;
use libloading::Library;
use raw_window_handle::{HasRawWindowHandle, RawWindowHandle};

use super::{
    vkAcquireNextImageKHR,
    vkAllocateCommandBuffers,
    vkBeginCommandBuffer,
    vkCmdBeginRenderPass,
    vkCmdBindDescriptorSets,
    vkCmdBindIndexBuffer,
    vkCmdBindPipeline,
    vkCmdBindVertexBuffers,
    vkCmdCopyBuffer,
    vkCmdCopyBufferToImage,
    vkCmdDispatch,
    vkCmdDraw,
    vkCmdDrawIndexed,
    vkCmdEndRenderPass,
    vkCmdPipelineBarrier,
    vkCmdPushConstants,
    vkCreateDebugUtilsMessengerEXT,
    vkCreateDevice,
    vkCreateFence,
    vkCreateGraphicsPipelines,
    vkCreateInstance,
    vkCreateRenderPass,
    vkCreateShaderModule,
    vkCreateSwapchainKHR,
    vkCreateWin32SurfaceKHR,
    vkDestroyCommandPool,
    vkDestroyDebugUtilsMessengerEXT,
    vkDestroyDevice,
    vkDestroyFence,
    vkDestroyFramebuffer,
    vkDestroyImageView,
    vkDestroyInstance,
    vkDestroyPipeline,
    vkDestroyPipelineLayout,
    vkDestroyRenderPass,
    vkDestroyShaderModule,
    vkDestroySurfaceKHR,
    vkDestroySwapchainKHR,
    vkDeviceWaitIdle,
    vkEndCommandBuffer,
    vkEnumerateDeviceExtensionProperties,
    vkEnumerateInstanceExtensionProperties,
    vkEnumerateInstanceLayerProperties,
    vkEnumeratePhysicalDevices,
    vkGetDeviceQueue,
    vkGetFenceStatus,
    vkGetPhysicalDeviceFeatures,
    vkGetPhysicalDeviceFormatProperties,
    vkGetPhysicalDeviceMemoryProperties,
    vkGetPhysicalDeviceProperties,
    vkGetPhysicalDeviceQueueFamilyProperties,
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR,
    vkGetPhysicalDeviceSurfaceFormatsKHR,
    vkGetPhysicalDeviceSurfacePresentModesKHR,
    vkGetSwapchainImagesKHR,
    vkMapMemory,
    vkQueuePresentKHR,
    vkQueueSubmit,
    vkQueueWaitIdle,
    vkResetFences,
    vkUnmapMemory,
    vkWaitForFences,
};

use super::types;
use super::semaphore::Semaphore;
use super::image::Image;
use super::ComputePipeline;
use super::MemoryAllocation;
use super::util::{
    map_vk_result,
    get_vk_handle,
    get_from_vk,
    get_ptr_from_vk,
    get_from_vk_with_result,
    get_raw_collection
};

use crate::guarded;
use crate::Buffer;
use crate::BufferHandle;
use crate::guarded::DescriptorSetHandle;
use crate::guarded::DescriptorSetLayoutHandle;
use crate::MemoryTypeAllocator;
use crate::image::ImageTrait;
use crate::image::UnownedImage;
use crate::ImageHandle;
use crate::LogicalDeviceHandle;
use crate::MemoryAllocationHandle;

macro_rules! load_vkfn
{
    ($instance:expr, $name:ident) => {
        unsafe {
            super::storage::$name = match super::load_proc($instance, stringify!($name))
            {
                Some(func) => Some(mem::transmute::<types::PFN_vkVoidFunction,_>(func)),
                None => None
            }
        }
    }
}


trait ExtensionName
{
    fn to_raw(&self) -> *const u8;
}

impl ExtensionName for Vec<u8>
{
    fn to_raw(&self) -> *const u8
    {
        self.as_ptr() as *const u8
    }
}

trait ExtensionNameCollection
{
    fn to_raws(&self) -> Vec<*const u8>;
}

impl<T> ExtensionNameCollection for Vec<T>
where T: ExtensionName
{
    fn to_raws(&self) -> Vec<*const u8>
    {
        self.iter()
            .map(|extension| extension.to_raw())
            .collect()
    }
}

trait IntoRawString
{
    fn into_raw(self) -> *const i8;
}

impl IntoRawString for *const u8
{
    fn into_raw(self) -> *const i8
    {
        self as *const i8
    }
}

pub trait IntoVertexAttributeDescriptions
{
    fn into() -> Vec<types::VkVertexInputAttributeDescription>;
}

fn string_from_raw(raw: *const i8) -> String
{
    unsafe { CStr::from_ptr(raw) }.to_str().unwrap().trim().to_string()
}

impl fmt::Debug for types::VkExtensionProperties
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
    {
        f.debug_struct("VkExtensionProperties")
            .field("extensionName", &string_from_raw(self.extensionName.as_ptr() as *const i8))
            .field("specVersion", &self.specVersion)
            .finish()
    }
}

impl fmt::Debug for types::VkLayerProperties
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
    {
        f.debug_struct("VkLayerProperties")
            .field("layerName", &string_from_raw(self.layerName.as_ptr() as *const i8))
            .field("specVersion", &self.specVersion)
            .finish()
    }
}

impl types::VkDeviceQueueCreateInfo
{
    fn new(queue_priority: &f32, queue_family_index: u32) -> Self
    {
        Self
        {
            sType : types::VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            pNext : std::ptr::null_mut(),
            flags : 0,
            queueFamilyIndex : queue_family_index,
            queueCount : 1,
            pQueuePriorities : queue_priority
        }
    }
}

impl types::VkRenderPassCreateInfo
{
    fn new(attachments: Vec<types::VkAttachmentDescription>, subpasses: &Vec<types::VkSubpassDescription>, subpass_dependencies: &Vec<types::VkSubpassDependency>) -> Self
    {
        Self
        {
            sType : types::VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
            flags : 0,
            pNext : std::ptr::null(),
            attachmentCount : attachments.len() as u32,
            pAttachments : attachments.as_ptr(),
            subpassCount : subpasses.len() as u32,
            pSubpasses : subpasses.as_ptr(),
            dependencyCount : subpass_dependencies.len() as u32,
            pDependencies : subpass_dependencies.as_ptr()
        }
    }
}

impl types::VkFramebufferCreateInfo
{
    fn new(
        attachment_handles: &Vec<types::VkImageView>,
        render_pass: &RenderPass,
        extent: &types::VkExtent2D
    ) -> Self
    {
        types::VkFramebufferCreateInfo
        {
            sType : types::VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
            flags : 0,
            pNext : std::ptr::null(),
            renderPass : render_pass.get_handle(),
            attachmentCount : attachment_handles.len() as u32,
            pAttachments : attachment_handles.as_ptr(),
            width : extent.width,
            height : extent.height,
            layers : 1,
        }
    }
}

#[cfg(all(windows,not(any(test,feature = "test"))))]
#[link(name = "cvulk", kind = "static")]
extern
{
    pub fn RW_VK_MAKE_VERSION(major: u32, minor: u32, patch: u32) -> u32;
}

// This struct owning the library object indicates that the member operations are only valid as long as the
// library object is valid
#[derive(Debug)]
pub struct VulkanLibContext
{
    lib: Library
}

impl VulkanLibContext
{
    pub fn new(lib: Library) -> Self
    {
        let context = Self
        {
            lib: lib
        };

        context.init();

        context
    }

    fn init(&self)
    {
        let raw_load_fn : libloading::Symbol<crate::vkGetInstanceProcAddr_type> = unsafe { self.lib.get(b"vkGetInstanceProcAddr") }.unwrap();

        unsafe { super::storage::vkGetInstanceProcAddr = Some(mem::transmute::<_, _>(raw_load_fn.into_raw())) };
        load_vkfn!(0 as types::VkInstance, vkEnumerateInstanceExtensionProperties);
        load_vkfn!(0 as types::VkInstance, vkEnumerateInstanceLayerProperties);
        load_vkfn!(0 as types::VkInstance, vkCreateInstance);
    }

    pub fn get_available_extensions(&self) -> Vec<types::VkExtensionProperties>
    {
        get_raw_collection(
            |c: &mut u32| {
                map_vk_result(unsafe { vkEnumerateInstanceExtensionProperties(std::ptr::null(), c, std::ptr::null_mut::<types::VkExtensionProperties>()) }).unwrap();
            },
            |c: &mut u32, v: &mut Vec<types::VkExtensionProperties>| -> () {
                map_vk_result(unsafe { vkEnumerateInstanceExtensionProperties(std::ptr::null(), c, v.as_mut_ptr()) }).unwrap();
            }
        )
    }

    pub fn get_available_layers(&self) -> Vec<types::VkLayerProperties>
    {
        get_raw_collection(
            |c: &mut u32| {
                map_vk_result(unsafe { vkEnumerateInstanceLayerProperties(c, std::ptr::null_mut()) }).unwrap();
            },
            |c: &mut u32, v: &mut Vec<types::VkLayerProperties>| -> () {
                map_vk_result(unsafe { vkEnumerateInstanceLayerProperties(c, v.as_mut_ptr()) }).unwrap();
            }
        )
    }

    pub fn get_instance(
        &self,
        selected_extensions: Vec<*const raw::c_char>,
        selected_layers: Vec<*const raw::c_char>,
    ) -> Result<types::VkInstance, types::VkResult>
    {
        let app_name = CString::new("Hello?").unwrap();
        let engine_name = CString::new("No Engine").unwrap();

        #[allow(unused_unsafe)]
        let app_info = types::VkApplicationInfo {
            sType: types::VK_STRUCTURE_TYPE_APPLICATION_INFO,
            pApplicationName: app_name.as_ptr() as *const u8,
            applicationVersion: unsafe { RW_VK_MAKE_VERSION(1, 0, 0) },
            pEngineName: engine_name.as_ptr() as *const u8,
            engineVersion: unsafe { RW_VK_MAKE_VERSION(1, 0, 0) },
            apiVersion: unsafe { RW_VK_MAKE_VERSION(1, 2, 0) },
            pNext: std::ptr::null()
        };

        let create_info = types::VkInstanceCreateInfo {
            sType: types::VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
            pApplicationInfo: &app_info,
            ppEnabledExtensionNames: selected_extensions.as_ptr(),
            enabledExtensionCount: selected_extensions.len() as u32,
            ppEnabledLayerNames: selected_layers.as_ptr(),
            flags: 0,
            enabledLayerCount: selected_layers.len() as u32,
            pNext: std::ptr::null()
        };

        match get_vk_handle(
            |handle : &mut types::VkInstance| {
                unsafe { vkCreateInstance(&create_info, std::ptr::null() as *const types::VkAllocationCallbacks, handle) }
            }
        )
        {
            Ok(handle) => {
                //println!("Successfully created instance: {:?}", handle);

                Ok(handle)
            },
            Err(error_code) => {
                let error_string = match error_code
                {
                    types::VK_ERROR_OUT_OF_HOST_MEMORY => "VK_ERROR_OUT_OF_HOST_MEMORY",
                    types::VK_ERROR_OUT_OF_DEVICE_MEMORY => "VK_ERROR_OUT_OF_DEVICE_MEMORY",
                    types::VK_ERROR_INITIALIZATION_FAILED => "VK_ERROR_INITIALIZATION_FAILED",
                    types::VK_ERROR_LAYER_NOT_PRESENT => "VK_ERROR_LAYER_NOT_PRESENT",
                    types::VK_ERROR_EXTENSION_NOT_PRESENT => "VK_ERROR_EXTENSION_NOT_PRESENT",
                    types::VK_ERROR_INCOMPATIBLE_DRIVER => "VK_ERROR_INCOMPATIBLE_DRIVER",
                    _ => "unknown"
                };

                println!("Failed to create instance: {:?}", error_string);

                Err(error_code)
            }
        }
    }
}

#[no_mangle]
#[allow(unused_variables, non_snake_case)]
fn debug_callback(flags: types::VkDebugUtilsMessageSeverityFlagBitsEXT,
                  objectType: types::VkDebugUtilsMessageTypeFlagsEXT,
                  pCallbackData: *const types::VkDebugUtilsMessengerCallbackDataEXT,
                  pUserData: *mut std::os::raw::c_void
) -> types::VkBool32
{
    println!("Debug utils callback:");

    let callback_data = unsafe { &*pCallbackData };

    if flags & types::VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT != 0
    {
        println!(" - severity: INFO");
    }
    if flags & types::VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT != 0
    {
        println!(" - severity: WARNING");
    }
    if flags & types::VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT != 0
    {
        println!(" - severity: ERROR");
    }

    println!(" - flags: {:?}", callback_data.flags);
    println!(" - pMessageIdName: {:?}", string_from_raw(callback_data.pMessageIdName.into_raw()));
    println!(" - messageIdNumber: {:?}", callback_data.messageIdNumber);
    println!(" - pMessage: {:?}", string_from_raw(callback_data.pMessage.into_raw()));
    println!(" - queueLabelCount: {:?}", callback_data.queueLabelCount);
    println!(" - cmdBufLabelCount: {:?}", callback_data.cmdBufLabelCount);
    println!(" - pObjects: {:?}", callback_data.pObjects);
    println!("Debug utils callback end.");

    return types::VK_FALSE;
}

#[derive(Debug)]
pub struct VulkanInstance
{
    pub handle: types::VkInstance,
    _lib: Arc<VulkanLibContext>
}

impl<'a, 'b> VulkanInstance
{
    pub fn new(handle: types::VkInstance, lib: Arc<VulkanLibContext>) -> Self
    {
        VulkanInstance {
            handle: handle,
            _lib: lib
        }
    }

    pub fn get_handle(&self) -> types::VkInstance
    {
        self.handle
    }

    pub fn load_functions(&self)
    {
        let handle = self.get_handle();

        load_vkfn!(handle, vkGetPhysicalDeviceFormatProperties);
        load_vkfn!(handle, vkBindImageMemory);
        load_vkfn!(handle, vkGetImageMemoryRequirements);
        load_vkfn!(handle, vkCmdBindDescriptorSets);
        load_vkfn!(handle, vkUpdateDescriptorSets);
        load_vkfn!(handle, vkAllocateDescriptorSets);
        load_vkfn!(handle, vkCmdDrawIndexed);
        load_vkfn!(handle, vkCmdBindIndexBuffer);
        load_vkfn!(handle, vkCmdCopyBuffer);
        load_vkfn!(handle, vkCmdBindVertexBuffers);
        load_vkfn!(handle, vkMapMemory);
        load_vkfn!(handle, vkUnmapMemory);
        load_vkfn!(handle, vkBindBufferMemory);
        load_vkfn!(handle, vkFreeMemory);
        load_vkfn!(handle, vkAllocateMemory);
        load_vkfn!(handle, vkAcquireNextImageKHR);
        load_vkfn!(handle, vkAllocateCommandBuffers);
        load_vkfn!(handle, vkBeginCommandBuffer);
        load_vkfn!(handle, vkCmdBeginRenderPass);
        load_vkfn!(handle, vkCmdBindPipeline);
        load_vkfn!(handle, vkCmdBindPipeline);
        load_vkfn!(handle, vkCmdDraw);
        load_vkfn!(handle, vkCmdEndRenderPass);
        load_vkfn!(handle, vkCreateBuffer);
        load_vkfn!(handle, vkCreateBufferView);
        load_vkfn!(handle, vkCreateCommandPool);
        load_vkfn!(handle, vkCreateDevice);
        load_vkfn!(handle, vkCreateFence);
        load_vkfn!(handle, vkCreateFramebuffer);
        load_vkfn!(handle, vkCreateGraphicsPipelines);
        load_vkfn!(handle, vkCreateImageView);
        load_vkfn!(handle, vkCreatePipelineLayout);
        load_vkfn!(handle, vkCreateRenderPass);
        load_vkfn!(handle, vkCreateSemaphore);
        load_vkfn!(handle, vkCreateShaderModule);
        load_vkfn!(handle, vkCreateSwapchainKHR);
        load_vkfn!(handle, vkDestroyBuffer);
        load_vkfn!(handle, vkDestroyCommandPool);
        load_vkfn!(handle, vkDestroyDevice);
        load_vkfn!(handle, vkDestroyFence);
        load_vkfn!(handle, vkDestroyFramebuffer);
        load_vkfn!(handle, vkDestroyImageView);
        load_vkfn!(handle, vkDestroyInstance);
        load_vkfn!(handle, vkDestroyPipeline);
        load_vkfn!(handle, vkDestroyPipelineLayout);
        load_vkfn!(handle, vkDestroyRenderPass);
        load_vkfn!(handle, vkDestroySemaphore);
        load_vkfn!(handle, vkDestroyShaderModule);
        load_vkfn!(handle, vkDestroySurfaceKHR);
        load_vkfn!(handle, vkDestroySwapchainKHR);
        load_vkfn!(handle, vkDeviceWaitIdle);
        load_vkfn!(handle, vkEndCommandBuffer);
        load_vkfn!(handle, vkEnumerateDeviceExtensionProperties);
        load_vkfn!(handle, vkEnumeratePhysicalDevices);
        load_vkfn!(handle, vkGetDeviceQueue);
        load_vkfn!(handle, vkGetPhysicalDeviceFeatures);
        load_vkfn!(handle, vkGetPhysicalDeviceProperties);
        load_vkfn!(handle, vkGetPhysicalDeviceQueueFamilyProperties);
        load_vkfn!(handle, vkGetPhysicalDeviceSurfaceCapabilitiesKHR);
        load_vkfn!(handle, vkGetPhysicalDeviceSurfaceFormatsKHR);
        load_vkfn!(handle, vkGetPhysicalDeviceSurfacePresentModesKHR);
        load_vkfn!(handle, vkGetPhysicalDeviceSurfaceSupportKHR);
        load_vkfn!(handle, vkGetSwapchainImagesKHR);
        load_vkfn!(handle, vkQueuePresentKHR);
        load_vkfn!(handle, vkQueueSubmit);
        load_vkfn!(handle, vkQueueWaitIdle);
        load_vkfn!(handle, vkResetFences);
        load_vkfn!(handle, vkWaitForFences);
        load_vkfn!(handle, vkGetBufferMemoryRequirements);
        load_vkfn!(handle, vkGetPhysicalDeviceMemoryProperties);
        load_vkfn!(handle, vkCreateDescriptorSetLayout);
        load_vkfn!(handle, vkDestroyDescriptorSetLayout);
        load_vkfn!(handle, vkCreateDescriptorPool);
        load_vkfn!(handle, vkDestroyDescriptorPool);
        load_vkfn!(handle, vkCreateImage);
        load_vkfn!(handle, vkCmdPipelineBarrier);
        load_vkfn!(handle, vkDestroyImage);
        load_vkfn!(handle, vkCmdCopyBufferToImage);
        load_vkfn!(handle, vkCreateSampler);
        load_vkfn!(handle, vkDestroySampler);
        load_vkfn!(handle, vkCmdDispatch);
        load_vkfn!(handle, vkCreateComputePipelines);
        load_vkfn!(handle, vkCmdPushConstants);
        load_vkfn!(handle, vkGetFenceStatus);
        load_vkfn!(handle, vkGetSemaphoreCounterValue);
        load_vkfn!(handle, vkFreeDescriptorSets);

        // VK_KHR_win32_surface extension
        load_vkfn!(handle, vkCreateWin32SurfaceKHR);

        // VK_EXT_debug_report extension
        // load_vkfn!(handle, vkCreateDebugReportCallbackEXT);
        // load_vkfn!(handle, vkDestroyDebugReportCallbackEXT);

        // VK_EXT_debug_utils extension
        load_vkfn!(handle, vkCreateDebugUtilsMessengerEXT);
        load_vkfn!(handle, vkDestroyDebugUtilsMessengerEXT);
        load_vkfn!(handle, vkSubmitDebugUtilsMessageEXT);
    }

    pub fn get_physical_devices(&'a self) -> Vec<types::VkPhysicalDevice>
    {
        get_raw_collection(
            |c: &mut u32| {
                map_vk_result(unsafe { vkEnumeratePhysicalDevices(self.handle, c, std::ptr::null_mut()) }).unwrap();
            },
            |c: &mut u32, v: &mut Vec<types::VkPhysicalDevice>| {
                map_vk_result(unsafe { vkEnumeratePhysicalDevices(self.handle, c, v.as_mut_ptr()) }).unwrap();
            }
        )
    }

    pub fn get_debug_utils_callback_instance(&self) -> DebugUtilsCallback
    {
        let create_info = types::VkDebugUtilsMessengerCreateInfoEXT {
            sType : types::VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
            pNext : std::ptr::null(),
            flags : 0,
            messageSeverity : types::VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT |
            types::VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT,
            messageType : types::VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
            types::VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT,
            pfnUserCallback : debug_callback,
            pUserData : std::ptr::null_mut::<std::os::raw::c_void>()
        };

        DebugUtilsCallback
        {
            handle: get_vk_handle(
                |handle : &mut types::VkDebugUtilsMessengerEXT| {
                    unsafe { vkCreateDebugUtilsMessengerEXT(self.get_handle(), &create_info, std::ptr::null(), handle) }
                }
            ).unwrap(),
            instance: self
        }
    }
}

impl Drop for VulkanInstance
{
    fn drop(&mut self)
    {
        unsafe { vkDestroyInstance(self.handle, std::ptr::null()) };
    }
}

#[derive(Debug, Clone)]
pub struct AllocatorHandle
{
    handle: Arc<Mutex<MemoryTypeAllocator>>
}

impl AllocatorHandle {
    fn new(allocator: MemoryTypeAllocator) -> Self {
        Self {
            handle: Arc::new(Mutex::new(allocator))
        }
    }

    pub fn allocate(&self, device: guarded::LogicalDeviceHandle, size: types::VkDeviceSize) -> Result<MemoryAllocationHandle, types::VkResult> {
        self.handle.lock().unwrap().allocate(device, size)
    }
}

pub struct PhysicalDevice
{
    handle: types::VkPhysicalDevice,
    extension_properties: Vec<types::VkExtensionProperties>,
    // TODO: The references to MemoryTypeAllocator returned need to be thread safe
    memory_allocators: Arc<RwLock<HashMap<usize, AllocatorHandle>>>
}

impl std::fmt::Debug for PhysicalDevice
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        f.write_fmt(format_args!("{:?}", self.handle))
    }
}

impl PhysicalDevice
{
    pub fn new(handle: types::VkPhysicalDevice) -> Self
    {
        PhysicalDevice
        {
            handle: handle,
            extension_properties: get_raw_collection(
                |c: &mut u32| {
                    map_vk_result(unsafe { vkEnumerateDeviceExtensionProperties(handle, std::ptr::null(), c, std::ptr::null_mut()) }).unwrap();
                },
                |c: &mut u32, v: &mut Vec<types::VkExtensionProperties>| {
                    map_vk_result(unsafe { vkEnumerateDeviceExtensionProperties(handle, std::ptr::null(), c, v.as_mut_ptr()) }).unwrap();
                }
            ),
            memory_allocators: Arc::new(RwLock::new(HashMap::new()))
        }
    }

    // TODO: This eventually needs to be independent of logical device
    pub fn get_memory_allocator(&self, memory_type_bits: u32, properties: types::VkMemoryPropertyFlags) -> AllocatorHandle
    {
        let all_memory_properties = self.get_memory_properties();
        let memory_type_index = self.find_memory_type_index(memory_type_bits, properties).unwrap();

        let mut memory_allocators = self.memory_allocators.write().unwrap();

        if (*memory_allocators).contains_key(&memory_type_index) == false {
            let memory_type = &all_memory_properties.memoryTypes[memory_type_index];

            // TODO: A heap can be shared among multiple types, so we need a better way of
            //       tracking usage than this.
            let memory_heap = &all_memory_properties.memoryHeaps[memory_type.heapIndex as usize];

            (*memory_allocators).insert(memory_type_index, AllocatorHandle::new(MemoryTypeAllocator::new(
                memory_heap.size,
                memory_type_index as u32
            )));
        }

        let allocator = (*memory_allocators).get(&memory_type_index).unwrap();

        allocator.clone()
    }

    // There is a lot of potential to use the vulkan API wrong surrounding these
    // queue family indices at the moment.
    pub fn make_logical_device(
        &self,
        extension_names: Vec<Vec<u8>>,
        queue_family_indices: Vec<u32>
    ) -> Result<types::VkDevice, types::VkResult>
    {
        let queue_priority : f32 = 1.0 / (queue_family_indices.len() as f32);
        let queue_create_infos : Vec<types::VkDeviceQueueCreateInfo> = queue_family_indices.iter().map(
            |index| { types::VkDeviceQueueCreateInfo::new(&queue_priority, *index) }
        ).collect();

        let device_features = (|| {
            let mut tmp = types::VkPhysicalDeviceFeatures::default();
            tmp.samplerAnisotropy = types::VK_TRUE;
            tmp.vertexPipelineStoresAndAtomics = types::VK_TRUE;
            tmp
        })();

        let raw_extension_names = extension_names.to_raws();

        let device_create_info = types::VkDeviceCreateInfo
        {
            sType : types::VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
            pNext : std::ptr::null_mut(),
            flags : 0,
            queueCreateInfoCount : queue_create_infos.len() as u32,
            pQueueCreateInfos : queue_create_infos.as_ptr(),
            enabledLayerCount : 0,
            ppEnabledLayerNames :  std::ptr::null(),
            enabledExtensionCount : extension_names.len() as u32,
            ppEnabledExtensionNames : raw_extension_names.as_ptr(),
            pEnabledFeatures : &device_features
        };

        get_vk_handle(
            |handle : &mut types::VkDevice| {
                unsafe { vkCreateDevice(self.get_handle(), &device_create_info, std::ptr::null() as *const types::VkAllocationCallbacks, handle) }
            }
        )
    }

    pub fn get_handle(&self) -> types::VkPhysicalDevice
    {
        self.handle
    }

    pub fn get_physical_device_features(&self) -> types::VkPhysicalDeviceFeatures
    {
        get_from_vk(|features| unsafe { vkGetPhysicalDeviceFeatures(self.get_handle(), features) })
    }

    pub fn get_properties(&self) -> types::VkPhysicalDeviceProperties
    {
        get_from_vk(|properties| unsafe { vkGetPhysicalDeviceProperties(self.get_handle(), properties) })
    }

    pub fn get_queue_families(&self) -> Vec<types::VkQueueFamilyProperties>
    {
        get_raw_collection(
            |c: &mut u32| {
                unsafe { vkGetPhysicalDeviceQueueFamilyProperties(self.get_handle(), c, std::ptr::null_mut()) }
            },
            |c: &mut u32, v: &mut Vec<types::VkQueueFamilyProperties>| {
                unsafe { vkGetPhysicalDeviceQueueFamilyProperties(self.get_handle(), c, v.as_mut_ptr()) };
            }
        )
    }

    pub fn get_memory_properties(&self) -> types::VkPhysicalDeviceMemoryProperties
    {
        get_from_vk(|properties| unsafe { vkGetPhysicalDeviceMemoryProperties(self.get_handle(), properties) })
    }

    pub fn get_format_properties(&self, format: types::VkFormat) -> types::VkFormatProperties
    {
        get_from_vk(|properties| unsafe { vkGetPhysicalDeviceFormatProperties(self.get_handle(), format, properties) })
    }

    pub fn is_depth_format(&self, format: types::VkFormat) -> bool
    {
        let properties = self.get_format_properties(format);

        (properties.optimalTilingFeatures & types::VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) == types::VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
    }

    pub fn find_depth_format(&self) -> Option<types::VkFormat>
    {
        [
            types::VK_FORMAT_D32_SFLOAT,
            types::VK_FORMAT_D32_SFLOAT_S8_UINT,
            types::VK_FORMAT_D24_UNORM_S8_UINT
        ]
            .iter()
            .filter(|&format| self.is_depth_format(*format) )
            .take(1)
            .map(|&format| format)
            .next()
    }

    pub fn find_memory_type_index(&self, type_filter: u32, property_flags: types::VkMemoryPropertyFlags) -> Option<usize>
    {
        let properties = self.get_memory_properties();

        for i in 0..properties.memoryTypeCount as usize
        {
            if (type_filter & (1 << i) != 0) && (property_flags ^ properties.memoryTypes[i].propertyFlags == 0)
            {
                return Some(i);
            }
        }

        None
    }
}

#[derive(Debug)]
pub struct Queue {
    handle : types::VkQueue,
}

impl Queue {
    pub fn new(handle: types::VkQueue) -> Self {
        Self {
            handle: handle,
        }
    }

    pub fn get_handle(&self) -> types::VkQueue
    {
        self.handle
    }

    pub fn wait_idle(&self)
    {
        unsafe { vkQueueWaitIdle(self.get_handle()) };
    }

    pub fn simple_submit(&self, command_buffer: &guarded::CommandBufferHandle)
    {
        let info = types::VkSubmitInfo
        {
            sType : types::VK_STRUCTURE_TYPE_SUBMIT_INFO,
            pNext : std::ptr::null(),
            waitSemaphoreCount : 0,
            pWaitSemaphores : std::ptr::null(),
            pWaitDstStageMask : std::ptr::null(),
            commandBufferCount : 1,
            pCommandBuffers : &command_buffer.handle.read().unwrap().get_handle(),
            signalSemaphoreCount : 0,
            pSignalSemaphores : std::ptr::null()
        };

        map_vk_result(unsafe { vkQueueSubmit(self.get_handle(), 1, &info, std::ptr::null()) }).unwrap();
    }

    pub fn submit(&self,
                  command_buffer: &guarded::CommandBufferHandle,
                  wait_semaphores: Vec<&Semaphore>,
                  signal_semaphores: Vec<&Semaphore>,
                  fence: Option<&Fence>
    ) {
        let flags : types::VkPipelineStageFlags = types::VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;

        let tmp_wait_semaphores : Vec<types::VkSemaphore> = wait_semaphores.iter()
            .map(|s| s.get_handle())
            .collect();

        let tmp_signal_semaphores : Vec<types::VkSemaphore> = signal_semaphores.iter()
            .map(|s| s.get_handle())
            .collect();

        let info = types::VkSubmitInfo
        {
            sType : types::VK_STRUCTURE_TYPE_SUBMIT_INFO,
            pNext : std::ptr::null(),
            waitSemaphoreCount : tmp_wait_semaphores.len() as u32,
            pWaitSemaphores : tmp_wait_semaphores.as_ptr() as *const types::VkSemaphore,
            pWaitDstStageMask : &flags,
            commandBufferCount : 1,
            pCommandBuffers : &command_buffer.handle.read().unwrap().get_handle(),
            signalSemaphoreCount : tmp_signal_semaphores.len() as u32,
            pSignalSemaphores : tmp_signal_semaphores.as_ptr() as *const types::VkSemaphore
        };

        match fence
        {
            Some(f) => map_vk_result(unsafe { vkQueueSubmit(self.get_handle(), 1, &info, f.get_handle()) }).unwrap(),
            None => map_vk_result(unsafe { vkQueueSubmit(self.get_handle(), 1, &info, std::ptr::null()) }).unwrap()
        };
    }

    pub fn present(&self, swap_chain: &SwapChain, wait_semaphore: &Semaphore, image_index: u32) -> Result<(), types::VkResult>
    {
        let present_info = types::VkPresentInfoKHR
        {
            sType : types::VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
            pNext : std::ptr::null(),
            waitSemaphoreCount : 1,
            pWaitSemaphores : &wait_semaphore.get_handle(),
            swapchainCount : 1,
            pSwapchains : &swap_chain.get_handle(),
            pImageIndices : &image_index,
            pResults : std::ptr::null_mut()
        };

        map_vk_result(unsafe { vkQueuePresentKHR(self.get_handle(), &present_info) })
    }
}

#[derive(Debug)]
pub struct Surface
{
    context: Arc<VulkanInstance>,
    handle: types::VkSurfaceKHR
}

impl Drop for Surface
{
    fn drop(&mut self)
    {
        unsafe { vkDestroySurfaceKHR(self.context.get_handle(), self.get_handle(), std::ptr::null_mut()) };
    }
}

impl Surface
{
    fn create_handle<T>(context: &Arc<VulkanInstance>, window: &T) -> Result<types::VkSurfaceKHR, types::VkResult>
    where T: HasRawWindowHandle
    {
        match window.raw_window_handle() {
            #[cfg(target_os = "windows")]
            RawWindowHandle::Windows(handle) => {
                let surface_create_info = types::win32::VkWin32SurfaceCreateInfoKHR
                {
                    sType : types::VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR,
                    pNext : std::ptr::null(),
                    flags : 0,
                    hinstance : handle.hinstance,
                    hwnd : handle.hwnd
                };

                get_vk_handle(
                    |handle : &mut types::VkSurfaceKHR| {
                        #[allow(unused_unsafe)]
                        unsafe { vkCreateWin32SurfaceKHR(context.get_handle(), &surface_create_info, std::ptr::null(), handle) }
                    }
                )
            },

            _ => Err(types::VK_ERROR_UNKNOWN)
        }
    }

    pub fn new<T>(context: Arc<VulkanInstance>, window: &T) -> Result<Self, types::VkResult>
    where T: HasRawWindowHandle
    {
        match Self::create_handle(&context, window)
        {
            Ok(handle) => Ok(Self {
                handle: handle,
                context: context
            }),
            Err(e) => Err(e)
        }
    }

    pub fn get_handle(&self) -> types::VkSurfaceKHR
    {
        self.handle
    }

    pub fn get_capabilities(&self, device: &PhysicalDevice) -> types::VkSurfaceCapabilitiesKHR
    {
        let mut capabilities = types::VkSurfaceCapabilitiesKHR::default();
        unsafe { vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device.get_handle(), self.get_handle(), &mut capabilities) };
        capabilities
    }

    pub fn get_formats(&self, device: &PhysicalDevice) -> Vec<types::VkSurfaceFormatKHR>
    {
        get_raw_collection(
            |c: &mut u32| {
                map_vk_result(unsafe { vkGetPhysicalDeviceSurfaceFormatsKHR(device.get_handle(), self.get_handle(), c, std::ptr::null_mut()) }).unwrap();
            },
            |c: &mut u32, v: &mut Vec<types::VkSurfaceFormatKHR>| {
                map_vk_result(unsafe { vkGetPhysicalDeviceSurfaceFormatsKHR(device.get_handle(), self.get_handle(), c, v.as_mut_ptr()) }).unwrap();
            }
        )
    }

    pub fn get_present_modes(&self, device: &PhysicalDevice) -> Vec<types::VkPresentModeKHR>
    {
        get_raw_collection(
            |c: &mut u32| {
                map_vk_result(unsafe { vkGetPhysicalDeviceSurfacePresentModesKHR(device.get_handle(), self.get_handle(), c, std::ptr::null_mut()) }).unwrap();
            },
            |c: &mut u32, v: &mut Vec<types::VkPresentModeKHR>| {
                map_vk_result(unsafe { vkGetPhysicalDeviceSurfacePresentModesKHR(device.get_handle(), self.get_handle(), c, v.as_mut_ptr()) }).unwrap();
            }
        )
    }
}

pub struct LogicalDevice
{
    device: Arc<PhysicalDevice>,
    handle: types::VkDevice
}

impl std::fmt::Debug for LogicalDevice
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        f.write_fmt(format_args!("{:?}", self.handle))
    }
}

impl LogicalDevice
{
    pub fn new(handle: types::VkDevice, device: Arc<PhysicalDevice>) -> Self
    {
        Self {
            device: device,
            handle: handle
        }
    }

    pub fn get_memory_properties(&self) -> types::VkPhysicalDeviceMemoryProperties
    {
        self.device.get_memory_properties()
    }

    pub fn get_handle(&self) -> types::VkDevice
    {
        self.handle
    }

    pub fn get_device(&self) -> Arc<PhysicalDevice>
    {
        self.device.clone()
    }

    pub fn wait_idle(&self)
    {
        unsafe { vkDeviceWaitIdle(self.get_handle()) };
    }

    // There is a lot of potential to use the vulkan API wrong surrounding these
    // queue family indices at the moment.
    pub fn get_queue(&self, queue_family_index: u32) -> types::VkQueue
    {
        get_ptr_from_vk(|handle| unsafe { vkGetDeviceQueue(self.get_handle(), queue_family_index, 0, handle) })
    }


    pub fn create_render_pass(&self, surface_format: types::VkSurfaceFormatKHR) -> Result<types::VkRenderPass, types::VkResult>
    {
        let attachments = vec![
            types::VkAttachmentDescription
            {
                format : surface_format.format,
                flags : 0,
                samples : types::VK_SAMPLE_COUNT_1_BIT,
                loadOp : types::VK_ATTACHMENT_LOAD_OP_CLEAR,
                storeOp : types::VK_ATTACHMENT_STORE_OP_STORE,
                stencilLoadOp : types::VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                stencilStoreOp : types::VK_ATTACHMENT_STORE_OP_DONT_CARE,
                initialLayout : types::VK_IMAGE_LAYOUT_UNDEFINED,
                finalLayout : types::VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
            },
            types::VkAttachmentDescription
            {
                format : self.device.find_depth_format().unwrap(),
                flags : 0,
                samples : types::VK_SAMPLE_COUNT_1_BIT,
                loadOp : types::VK_ATTACHMENT_LOAD_OP_CLEAR,
                storeOp : types::VK_ATTACHMENT_STORE_OP_DONT_CARE,
                stencilLoadOp : types::VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                stencilStoreOp : types::VK_ATTACHMENT_STORE_OP_DONT_CARE,
                initialLayout : types::VK_IMAGE_LAYOUT_UNDEFINED,
                finalLayout : types::VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
            }
        ];

        let color_attachment_ref = types::VkAttachmentReference
        {
            attachment : 0,
            layout : types::VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
        };

        let depth_attachment_ref = types::VkAttachmentReference
        {
            attachment : 1,
            layout : types::VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
        };

        let subpass = types::VkSubpassDescription
        {
            flags : 0,
            pipelineBindPoint : types::VK_PIPELINE_BIND_POINT_GRAPHICS,
            inputAttachmentCount : 0,
            pInputAttachments : std::ptr::null(),
            colorAttachmentCount : 1,
            pColorAttachments : &color_attachment_ref,
            pResolveAttachments : std::ptr::null(),
            pDepthStencilAttachment : &depth_attachment_ref,
            preserveAttachmentCount : 0,
            pPreserveAttachments : std::ptr::null()
        };

        let subpass_dependency = types::VkSubpassDependency
        {
            srcSubpass : types::VK_SUBPASS_EXTERNAL,
            dstSubpass : 0,
            srcStageMask : types::VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
            srcAccessMask : 0,
            dstStageMask : types::VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
            dstAccessMask : types::VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
            dependencyFlags : 0
        };

        let subpasses = vec![subpass];
        let subpass_dependencies = vec![subpass_dependency];

        let create_info = types::VkRenderPassCreateInfo::new(
            attachments,
            &subpasses,
            &subpass_dependencies
        );

        get_vk_handle(
            |handle: &mut types::VkRenderPass| {
                unsafe { vkCreateRenderPass(self.get_handle(), &create_info, std::ptr::null_mut(), handle) }
            }
        )
    }

    pub fn create_compute_pipeline(&self, module: &ShaderModule, pipeline_layout: &PipelineLayout) -> ComputePipeline
    {
        let main_str = CString::new("main").unwrap();
        let stage = types::VkPipelineShaderStageCreateInfo::define_compute_shader_stage(&main_str, module);

        let create_info = types::VkComputePipelineCreateInfo
        {
            sType: types::VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
            pNext: std::ptr::null(),
            flags: 0,
            stage: stage,
            layout: pipeline_layout.get_handle(),
            basePipelineHandle: std::ptr::null(),
            basePipelineIndex: -1
        };

        ComputePipeline::new(
            ComputePipeline::create_handle(self, create_info).unwrap(),
            self
        )
    }

    pub fn create_graphics_pipeline<T>(
        &self,
        shader_stages : &Vec<types::VkPipelineShaderStageCreateInfo>,
        swap_chain_extent: &types::VkExtent2D,
        pipeline_layout: &guarded::PipelineLayoutHandle,
        render_pass: &RenderPass
    ) -> Result<types::VkPipeline, types::VkResult>
    where T: IntoVertexAttributeDescriptions
    {
        let binding_description = types::VkVertexInputBindingDescription {
            binding : 0,
            stride : size_of::<T>() as u32,
            inputRate : types::VK_VERTEX_INPUT_RATE_VERTEX
        };

        let vertex_attribute_descriptions = T::into();

        let vertex_input_info = types::VkPipelineVertexInputStateCreateInfo
        {
            sType : types::VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
            pNext : std::ptr::null(),
            flags : 0,
            vertexBindingDescriptionCount : 1,
            pVertexBindingDescriptions : &binding_description,
            vertexAttributeDescriptionCount : vertex_attribute_descriptions.len() as u32,
            pVertexAttributeDescriptions : vertex_attribute_descriptions.as_ptr()
        };

        let input_assembly = types::VkPipelineInputAssemblyStateCreateInfo
        {
            sType : types::VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
            flags : 0,
            pNext : std::ptr::null(),
            topology : types::VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
            primitiveRestartEnable : types::VK_FALSE
        };

        let viewport = types::VkViewport
        {
            x : 0.0,
            y : 0.0,
            width : swap_chain_extent.width as f32,
            height : swap_chain_extent.height as f32,
            minDepth : 0.0,
            maxDepth : 1.0,
        };

        let scissor = types::VkRect2D
        {
            offset : types::VkOffset2D
            {
                x : 0,
                y : 0
            },
            extent : *swap_chain_extent,
        };

        let viewport_state = types::VkPipelineViewportStateCreateInfo
        {
            sType : types::VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
            flags : 0,
            pNext : std::ptr::null(),
            viewportCount : 1,
            pViewports : &viewport,
            scissorCount : 1,
            pScissors : &scissor
        };

        let rasterizer = types::VkPipelineRasterizationStateCreateInfo
        {
            sType : types::VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
            flags : 0,
            pNext : std::ptr::null(),
            depthClampEnable : types::VK_FALSE,
            rasterizerDiscardEnable : types::VK_FALSE,
            polygonMode : types::VK_POLYGON_MODE_FILL,
            lineWidth : 1.0,
            cullMode : types::VK_CULL_MODE_NONE,
            frontFace : types::VK_FRONT_FACE_COUNTER_CLOCKWISE,
            depthBiasEnable : types::VK_FALSE,
            depthBiasSlopeFactor : 0.0,
            depthBiasConstantFactor : 0.0,
            depthBiasClamp : 0.0
        };

        let multisampling = types::VkPipelineMultisampleStateCreateInfo
        {
            sType : types::VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
            flags : 0,
            pNext : std::ptr::null(),
            alphaToOneEnable : types::VK_FALSE,
            sampleShadingEnable : types::VK_FALSE,
            alphaToCoverageEnable : types::VK_FALSE,
            minSampleShading : 0.0,
            rasterizationSamples : types::VK_SAMPLE_COUNT_1_BIT,
            pSampleMask : std::ptr::null()
        };

        let color_blend_attachment = types::VkPipelineColorBlendAttachmentState
        {
            colorWriteMask : types::VK_COLOR_COMPONENT_R_BIT | types::VK_COLOR_COMPONENT_G_BIT | types::VK_COLOR_COMPONENT_B_BIT | types::VK_COLOR_COMPONENT_A_BIT,

            blendEnable : types::VK_TRUE,
            srcColorBlendFactor : types::VK_BLEND_FACTOR_SRC_ALPHA,
            dstColorBlendFactor : types::VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
            colorBlendOp : types::VK_BLEND_OP_ADD,
            srcAlphaBlendFactor : types::VK_BLEND_FACTOR_SRC_ALPHA,
            dstAlphaBlendFactor : types::VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
            alphaBlendOp : types::VK_BLEND_OP_ADD,
        };

        let color_blending = types::VkPipelineColorBlendStateCreateInfo
        {
            sType : types::VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
            flags : 0,
            pNext : std::ptr::null(),
            logicOpEnable : types::VK_FALSE,
            logicOp : types::VK_LOGIC_OP_AND,
            attachmentCount : 1,
            pAttachments : &color_blend_attachment,
            blendConstants : [1.0, 1.0, 1.0, 1.0]
        };

        let depth_stencil_info = types::VkPipelineDepthStencilStateCreateInfo
        {
            sType : types::VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
            pNext : std::ptr::null(),
            flags : 0,
            depthTestEnable : types::VK_TRUE,
            depthWriteEnable : types::VK_TRUE,
            depthCompareOp : types::VK_COMPARE_OP_LESS,
            depthBoundsTestEnable : types::VK_FALSE,
            minDepthBounds : 0.0,
            maxDepthBounds : 1.0,
            stencilTestEnable : types::VK_FALSE,
            front: types::VkStencilOpState::default(),
            back: types::VkStencilOpState::default()
        };

        let pipeline_info = types::VkGraphicsPipelineCreateInfo
        {
            sType : types::VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
            pNext : std::ptr::null(),
            flags : 0,
            stageCount : shader_stages.len() as u32,
            pStages : shader_stages.as_ptr(),
            basePipelineIndex : 0,
            pDepthStencilState : &depth_stencil_info,
            pDynamicState : std::ptr::null(),
            pTessellationState : std::ptr::null(),
            pVertexInputState : &vertex_input_info,
            pInputAssemblyState : &input_assembly,
            pViewportState : &viewport_state,
            pRasterizationState : &rasterizer,
            pMultisampleState : &multisampling,
            pColorBlendState : &color_blending,
            layout : pipeline_layout.get_handle(),
            renderPass : render_pass.get_handle(),
            subpass : 0,
            basePipelineHandle : std::ptr::null()
        };

        get_vk_handle(
            |handle: &mut types::VkPipeline| {
                unsafe { vkCreateGraphicsPipelines(self.get_handle(), std::ptr::null(), 1, &pipeline_info, std::ptr::null(), handle) }
            }
        )
    }

    pub fn create_command_pool(&self, queue_family_index: u32) -> Result<types::VkCommandPool, types::VkResult> {
        let create_info = types::VkCommandPoolCreateInfo {
            sType : types::VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
            pNext : std::ptr::null(),
            queueFamilyIndex : queue_family_index,
            flags : types::VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT
        };

        CommandPool::create_handle(self, create_info)
    }

    pub fn create_semaphore(&self) -> Result<types::VkSemaphore, types::VkResult>
    {
        let create_info = types::VkSemaphoreCreateInfo
        {
            sType : types::VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
            pNext : std::ptr::null(),
            flags : 0
        };

        Semaphore::create_handle(self, create_info)
    }

    pub fn create_multiple_semaphores(&self, count: usize) -> Vec<types::VkSemaphore>
    {
        let mut semaphores = Vec::new();

        for _i in 0..count
        {
            semaphores.push(self.create_semaphore().unwrap());
        }

        semaphores
    }

    pub fn create_fence(&self) -> Result<types::VkFence, types::VkResult>
    {
        let create_info = types::VkFenceCreateInfo
        {
            sType : types::VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
            pNext : std::ptr::null(),
            flags : types::VK_FENCE_CREATE_SIGNALED_BIT
        };

        get_vk_handle(
            |handle: &mut types::VkFence| {
                unsafe { vkCreateFence(self.get_handle(), &create_info, std::ptr::null(), handle) }
            }
        )
    }

    pub fn create_multiple_fences(&self, count: usize) -> Vec<types::VkFence>
    {
        let mut fences = Vec::new();

        for _i in 0..count
        {
            fences.push(self.create_fence().unwrap());
        }

        fences
    }
}

impl Drop for LogicalDevice
{
    fn drop(&mut self)
    {
        println!("LogicalDevice drop ({:?})", self.get_handle());
        unsafe { vkDestroyDevice(self.get_handle(), std::ptr::null_mut()) };
    }
}

#[derive(Debug)]
pub struct FrameBuffer
{
    handle: types::VkFramebuffer,
    device: guarded::LogicalDeviceHandle
}

impl FrameBuffer
{
    fn create_framebuffer(
        device: &guarded::LogicalDeviceHandle,
        attachment_handles: Vec<types::VkImageView>,
        render_pass: &RenderPass,
        extent: &types::VkExtent2D
    ) -> Result<types::VkFramebuffer, types::VkResult>
    {
        let create_info = types::VkFramebufferCreateInfo::new(&attachment_handles, render_pass, extent);

        Self::create_handle(device.get_handle(), create_info)
    }

    pub fn new(
        device: guarded::LogicalDeviceHandle,
        image_view_attachment: &guarded::ImageViewHandle<UnownedImage>,
        attachments: Vec<&guarded::ImageViewHandle<Image>>,
        render_pass: &RenderPass,
        extent: &types::VkExtent2D
    ) -> Result<Self, types::VkResult> {
        let mut attachment_handles = vec![image_view_attachment.get_handle()];
        let extra_handles : Vec<types::VkImageView> = attachments.iter().map(|img| { img.get_handle() }).collect();
        attachment_handles.extend(extra_handles);

        match Self::create_framebuffer(&device, attachment_handles, render_pass, extent)
        {
            Ok(handle) => Ok(Self {
                handle: handle,
                device: device
            }),
            Err(e) => Err(e)
        }
    }

    pub fn get_handle(&self) -> types::VkFramebuffer
    {
        self.handle
    }
}

impl Drop for FrameBuffer
{
    fn drop(&mut self)
    {
        unsafe { vkDestroyFramebuffer(self.device.get_handle(), self.get_handle(), std::ptr::null()) };
    }
}

#[derive(Debug)]
pub struct GraphicsPipeline
{
    handle : types::VkPipeline,
    device : Arc<LogicalDevice>
}

impl GraphicsPipeline
{
    pub fn new(handle: types::VkPipeline, device: Arc<LogicalDevice>) -> Self {
        Self {
            handle: handle,
            device: device
        }
    }

    pub fn get_handle(&self) -> types::VkPipeline
    {
        self.handle
    }
}

impl Drop for GraphicsPipeline
{
    fn drop(&mut self)
    {
        unsafe { vkDestroyPipeline(self.device.get_handle(), self.get_handle(), std::ptr::null()); };
    }
}

#[derive(Debug)]
pub struct RenderPass
{
    handle : types::VkRenderPass,
    device : Arc<LogicalDevice>
}

impl RenderPass
{
    pub fn new(handle: types::VkRenderPass, device: Arc<LogicalDevice>) -> Self {
        Self {
            handle: handle,
            device: device
        }
    }

    pub fn get_handle(&self) -> types::VkRenderPass
    {
        self.handle
    }
}

impl Drop for RenderPass
{
    fn drop(&mut self)
    {
        unsafe { vkDestroyRenderPass(self.device.get_handle(), self.get_handle(), std::ptr::null()); };
    }
}

#[derive(Debug)]
pub struct PipelineLayout
{
    handle : types::VkPipelineLayout,
    device : Arc<LogicalDevice>
}

impl PipelineLayout
{
    fn create_pipeline_layout(
        device: &LogicalDevice,
        descriptor_set_layouts: Vec<&DescriptorSetLayoutHandle>,
        push_constants: Vec<types::VkPushConstantRange>
    ) -> Result<types::VkPipelineLayout, types::VkResult>
    {
        let raw_handles : Vec<types::VkDescriptorSetLayout> = descriptor_set_layouts.iter()
            .map(|layout| layout.get_handle())
            .collect();

        let create_info = types::VkPipelineLayoutCreateInfo {
            sType : types::VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
            pNext : std::ptr::null(),
            flags : 0,
            setLayoutCount : raw_handles.len() as u32,
            pSetLayouts : raw_handles.as_ptr(),
            pushConstantRangeCount : push_constants.len() as u32,
            pPushConstantRanges : push_constants.as_ptr()
        };

        PipelineLayout::create_handle(device, create_info)
    }

    pub fn new(
        device: Arc<LogicalDevice>,
        descriptor_set_layouts: Vec<&DescriptorSetLayoutHandle>,
        push_constants: Vec<types::VkPushConstantRange>
    ) -> Result<Self, types::VkResult>
    {
        match Self::create_pipeline_layout(&device, descriptor_set_layouts, push_constants)
        {
            Ok(handle) => Ok(Self {
                handle: handle,
                device: device
            }),
            Err(e) => Err(e)
        }
    }

    pub fn get_handle(&self) -> types::VkPipelineLayout
    {
        self.handle
    }
}

impl Drop for PipelineLayout
{
    fn drop(&mut self)
    {
        unsafe { vkDestroyPipelineLayout(self.device.get_handle(), self.get_handle(), std::ptr::null()) };
    }
}

#[derive(Debug)]
pub struct SwapChain
{
    handle: types::VkSwapchainKHR,
    device: LogicalDeviceHandle,
    surface: Arc<Surface>,
    images: Vec<types::VkImage>,
    image_views: Vec<guarded::ImageViewHandle<UnownedImage>>,
    extent: types::VkExtent2D
}

impl SwapChain
{
    pub fn new(
        device: LogicalDeviceHandle,
        surface: Arc<Surface>,
        surface_format: types::VkSurfaceFormatKHR,
        present_mode: types::VkPresentModeKHR,
        queue_family_indices: &Vec<u32>,
        surface_capabilities: types::VkSurfaceCapabilitiesKHR,
        old_swap_chain: Option<guarded::SwapChainHandle>
    ) -> Result<Self, types::VkResult>
    {
        match Self::create_swap_chain_handle(
            &device,
            &surface,
            surface_format,
            present_mode,
            queue_family_indices,
            surface_capabilities,
            match old_swap_chain {
                Some(swap_chain) => Some(swap_chain.handle.get_handle()),
                None => None
            }
        )
        {
            Ok(handle) => {
                let images : Vec<types::VkImage> = get_raw_collection(
                    |c: &mut u32| {
                        map_vk_result(unsafe { vkGetSwapchainImagesKHR(device.get_handle(), handle, c, std::ptr::null_mut()) }).unwrap();
                    },
                    |c: &mut u32, v: &mut Vec<types::VkImage>| -> () {
                        map_vk_result(unsafe { vkGetSwapchainImagesKHR(device.get_handle(), handle, c, v.as_mut_ptr()) }).unwrap();
                    }
                );

                let image_views : Vec<guarded::ImageViewHandle<UnownedImage>> = (&images).iter()
                    .map(|image| {
                        UnownedImage::from_raw(device.clone(), *image, surface_format.format)
                    })
                    .map(|img| ImageHandle::new(img, device.clone()))
                    .map(|image| {
                        image.new_image_view(types::VK_IMAGE_ASPECT_COLOR_BIT)
                    })
                    .collect();

                Ok(Self
                {
                    handle: handle,
                    device: device,
                    images: images,
                    image_views: image_views,
                    surface: surface,
                    extent: surface_capabilities.currentExtent
                })
            },
            Err(e) => Err(e)
        }
    }
}

impl SwapChain
{
    fn create_swap_chain_handle(
        device: &LogicalDeviceHandle,
        surface: &Surface,
        surface_format: types::VkSurfaceFormatKHR,
        present_mode: types::VkPresentModeKHR,
        queue_family_indices: &Vec<u32>,
        surface_capabilities: types::VkSurfaceCapabilitiesKHR,
        old_swap_chain: Option<types::VkSwapchainKHR>
    ) -> Result<types::VkSwapchainKHR, types::VkResult> {
        let create_info = types::VkSwapchainCreateInfoKHR
        {
            sType : types::VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
            pNext : std::ptr::null_mut(),
            flags : 0,
            surface : surface.get_handle(),
            minImageCount : surface_capabilities.minImageCount,
            imageFormat : surface_format.format,
            imageColorSpace : surface_format.colorSpace,
            imageExtent : surface_capabilities.currentExtent,
            imageArrayLayers : 1,
            imageUsage : types::VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
            imageSharingMode : if queue_family_indices.len() == 1 { types::VK_SHARING_MODE_EXCLUSIVE } else { types::VK_SHARING_MODE_CONCURRENT },
            queueFamilyIndexCount : queue_family_indices.len() as u32,
            pQueueFamilyIndices : queue_family_indices.as_ptr(),
            preTransform : surface_capabilities.currentTransform,
            compositeAlpha : types::VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
            presentMode : present_mode,
            clipped : types::VK_TRUE,
            oldSwapchain : match old_swap_chain {
                Some(handle) => handle,
                None => std::ptr::null()
            }
        };

        get_vk_handle(
            |handle : &mut types::VkSwapchainKHR| {
                unsafe { vkCreateSwapchainKHR(device.get_handle(), &create_info, std::ptr::null_mut(), handle) }
            }
        )
    }

    pub fn get_handle(&self) -> types::VkSwapchainKHR
    {
        self.handle
    }

    pub fn get_device(&self) -> LogicalDeviceHandle
    {
        self.device.clone()
    }

    pub fn get_width(&self) -> usize
    {
        self.extent.width as usize
    }

    pub fn get_height(&self) -> usize
    {
        self.extent.height as usize
    }

    pub fn create_framebuffers(&self, depth_view: &guarded::ImageViewHandle<Image>, render_pass: &RenderPass) -> Vec<FrameBuffer>
    {
        self.image_views.iter().map(
            |image_view : &guarded::ImageViewHandle<UnownedImage>| {
                FrameBuffer::new(
                    self.device.clone(),
                    image_view,
                    vec![depth_view],
                    render_pass,
                    &self.extent
                ).unwrap()
            }
        ).collect()
    }

    pub fn acquire_next_image(&self, semaphore: &Semaphore) -> Result<u32, types::VkResult>
    {
        get_from_vk_with_result(|image_index| unsafe { vkAcquireNextImageKHR(self.device.get_handle(), self.get_handle(), u64::max_value(), semaphore.get_handle(), std::ptr::null(), image_index) })
    }
}

impl Drop for SwapChain
{
    fn drop(&mut self)
    {
        unsafe { vkDestroySwapchainKHR(self.device.get_handle(), self.get_handle(), std::ptr::null()) };
    }
}

#[derive(Debug)]
pub struct ImageView
{
    handle: types::VkImageView,
    device: LogicalDeviceHandle,
    image: types::VkImage
}

impl ImageView
{
    fn create_image_view(device: &LogicalDeviceHandle, image: types::VkImage, format: types::VkFormat, aspect: types::VkImageAspectFlags) -> Result<types::VkImageView, types::VkResult>
    {
        let components = types::VkComponentMapping
        {
            r: types::VK_COMPONENT_SWIZZLE_IDENTITY,
            g: types::VK_COMPONENT_SWIZZLE_IDENTITY,
            b: types::VK_COMPONENT_SWIZZLE_IDENTITY,
            a: types::VK_COMPONENT_SWIZZLE_IDENTITY
        };

        let subresource_range = types::VkImageSubresourceRange
        {
            aspectMask: aspect,
            baseMipLevel: 0,
            levelCount: 1,
            baseArrayLayer: 0,
            layerCount: 1
        };

        let image_view_create_info = types::VkImageViewCreateInfo
        {
            sType: types::VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
            pNext: std::ptr::null(),
            flags: 0,
            image: image,
            viewType: types::VK_IMAGE_VIEW_TYPE_2D,
            format: format,
            components: components,
            subresourceRange: subresource_range
        };

        Self::create_handle(device.get_handle(), image_view_create_info)
    }

    pub fn new_from_raw_image(device: LogicalDeviceHandle, image_handle: types::VkImage, image_format: types::VkFormat, aspect: types::VkImageAspectFlags) -> Result<Self, types::VkResult>
    {
        match Self::create_image_view(&device, image_handle, image_format, aspect)
        {
            Ok(handle) => Ok(Self {
                handle: handle,
                device: device,
                image: image_handle
            }),
            Err(e) => Err(e)
        }
    }

    pub fn new(device: LogicalDeviceHandle, image: &Image, aspect: types::VkImageAspectFlags) -> Result<Self, types::VkResult>
    {
        match Self::create_image_view(&device, image.get_handle(), image.get_format(), aspect)
        {
            Ok(handle) => Ok(Self {
                handle: handle,
                device: device,
                image: image.get_handle()
            }),
            Err(e) => Err(e)
        }
    }

    pub fn new_from_handle<T: ImageTrait>(image: &guarded::ImageHandle<T>, aspect: types::VkImageAspectFlags) -> Result<Self, types::VkResult>
    {
        match Self::create_image_view(&image.get_device(), image.get_handle(), image.get_format(), aspect)
        {
            Ok(handle) => Ok(Self {
                handle: handle,
                device: image.get_device(),
                image: image.get_handle()
            }),
            Err(e) => Err(e)
        }
    }

    pub fn new_color_image_view_from_raw(device: LogicalDeviceHandle, image: &Image) -> Result<Self, types::VkResult>
    {
        Self::new(device, image, types::VK_IMAGE_ASPECT_COLOR_BIT)
    }

    pub fn new_depth_image_view(device: LogicalDeviceHandle, image: &Image) -> Result<Self, types::VkResult>
    {
        Self::new(device, image, types::VK_IMAGE_ASPECT_DEPTH_BIT)
    }

    pub fn new_image_view(device: LogicalDeviceHandle, image: &Image) -> Result<Self, types::VkResult>
    {
        Self::new_color_image_view_from_raw(device, image)
    }

    pub fn get_handle(&self) -> types::VkImageView
    {
        self.handle
    }
}

impl Drop for ImageView
{
    fn drop(&mut self)
    {
        unsafe { vkDestroyImageView(self.device.get_handle(), self.get_handle(), std::ptr::null()) };
    }
}

#[derive(Debug)]
pub struct ShaderModule
{
    handle: types::VkShaderModule,
    device: Arc<LogicalDevice>
}

impl ShaderModule
{
    pub fn create_shader_module(device: &LogicalDevice, code: &[u8]) -> Result<types::VkShaderModule, types::VkResult>
    {
        let create_info = types::VkShaderModuleCreateInfo
        {
            sType: types::VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
            pNext: std::ptr::null(),
            flags: 0,
            codeSize: code.len(),
            pCode: code.as_ptr() as *const i8
        };

        get_vk_handle(
            |handle: &mut types::VkShaderModule| {
                unsafe { vkCreateShaderModule(device.get_handle(), &create_info, std::ptr::null(), handle) }
            }
        )
    }

    pub fn new(device: Arc<LogicalDevice>, code: &[u8]) -> Result<Self, types::VkResult>
    {
        match Self::create_shader_module(&device, code)
        {
            Ok(handle) => Ok(Self {
                handle: handle,
                device: device
            }),
            Err(e) => Err(e)
        }
    }

    pub fn get_handle(&self) -> types::VkShaderModule
    {
        self.handle
    }

    pub fn get_device(&self) -> Arc<LogicalDevice>
    {
        self.device.clone()
    }
}

impl Drop for ShaderModule
{
    fn drop(&mut self)
    {
        unsafe { vkDestroyShaderModule(self.get_device().get_handle(), self.get_handle(), std::ptr::null()) };
    }
}

#[derive(Debug)]
pub struct MemoryMap
{
    ptr: *mut raw::c_void,
    allocation: types::VkDeviceMemory,
    device: types::VkDevice
}

impl MemoryMap
{
    pub fn new(ptr: *mut raw::c_void, allocation: types::VkDeviceMemory, device: types::VkDevice) -> Self
    {
        Self
        {
            ptr: ptr,
            allocation: allocation,
            device: device
        }
    }
}

impl MemoryMap
{
    pub fn create_handle(device: types::VkDevice, allocation: types::VkDeviceMemory, offset: usize, size: usize) -> Result<*mut raw::c_void, types::VkResult>
    {
        let mut ptr : *mut raw::c_void = std::ptr::null_mut();
        match map_vk_result(unsafe { vkMapMemory(
            device,
            allocation,
            offset as types::VkDeviceSize,
            size as types::VkDeviceSize,
            0,
            &mut ptr
        ) }) {
            Ok(()) => Ok(ptr),
            Err(code) => Err(code)
        }
    }

    pub fn copy_to(&self, data: *const raw::c_void, size: usize)
    {
        unsafe { std::ptr::copy_nonoverlapping(data, self.ptr, size) };
    }

    pub fn copy_from(&self, data: *mut raw::c_void, size: usize)
    {
        unsafe { std::ptr::copy_nonoverlapping(self.ptr, data, size) };
    }
}

impl Drop for MemoryMap
{
    fn drop(&mut self)
    {
        unsafe { vkUnmapMemory(self.device, self.allocation) };
    }
}

#[derive(Debug)]
pub struct DebugUtilsCallback<'a>
{
    handle: types::VkDebugUtilsMessengerEXT,
    instance : &'a VulkanInstance
}

impl DebugUtilsCallback<'_>
{
    pub fn get_handle(&self) -> types::VkDebugUtilsMessengerEXT
    {
        self.handle
    }
}

impl Drop for DebugUtilsCallback<'_>
{
    fn drop(&mut self)
    {
        unsafe { vkDestroyDebugUtilsMessengerEXT(self.instance.get_handle(), self.get_handle(), std::ptr::null()) };
    }
}

#[derive(Debug)]
pub struct CommandPool
{
    handle : types::VkCommandPool,
    device : Arc<LogicalDevice>
}

impl CommandPool
{
    pub fn new(handle: types::VkCommandPool, device: Arc<LogicalDevice>) -> Self {
        Self {
            handle: handle,
            device: device
        }
    }

    pub fn get_handle(&self) -> types::VkCommandPool
    {
        self.handle
    }

    pub fn get_device(&self) -> Arc<LogicalDevice>
    {
        self.device.clone()
    }
}

impl Drop for CommandPool
{
    fn drop(&mut self)
    {
        unsafe { vkDestroyCommandPool(self.device.get_handle(), self.get_handle(), std::ptr::null()) };
    }
}

#[derive(Debug)]
pub struct CommandBuffer
{
    handle : types::VkCommandBuffer,
    pool: guarded::CommandPoolHandle,
    claimed_buffers: Vec<guarded::BufferHandle>,
    claimed_images: Vec<guarded::ImageHandle<Image>>,
    claimed_graphics_pipelines: Vec<guarded::GraphicsPipelineHandle>,
    claimed_descriptor_sets: Vec<DescriptorSetHandle>
}

pub struct CommandBufferRecording<'a>
{
    command_buffer: RwLockWriteGuard<'a, CommandBuffer>,
}

impl<'a> CommandBufferRecording<'a>
{
    fn new(command_buffer: RwLockWriteGuard<'a, CommandBuffer>) -> Self
    {
        Self
        {
            command_buffer: command_buffer,
        }
    }

    pub fn begin_recording(command_buffer: RwLockWriteGuard<'a, CommandBuffer>) -> Self
    {
        let begin_info = CommandBufferRecording::make_begin_info(0);
        map_vk_result(unsafe { vkBeginCommandBuffer(command_buffer.get_handle(), &begin_info) }).unwrap();

        CommandBufferRecording::new(command_buffer)
    }

    pub fn begin_one_time_recording(command_buffer: RwLockWriteGuard<'a, CommandBuffer>) -> Self
    {
        //println!("Command Buffer ({:?}) begin_one_time_recording", command_buffer.get_handle());

        let begin_info = CommandBufferRecording::make_begin_info(types::VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
        map_vk_result(unsafe { vkBeginCommandBuffer(command_buffer.get_handle(), &begin_info) }).unwrap();

        CommandBufferRecording::new(command_buffer)
    }

    fn make_begin_info(usage_flags: types::VkCommandBufferUsageFlags) -> types::VkCommandBufferBeginInfo
    {
        types::VkCommandBufferBeginInfo
        {
            sType : types::VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
            pNext : std::ptr::null(),
            flags : usage_flags,
            pInheritanceInfo : std::ptr::null()
        }
    }

    fn end_recording(command_buffer: types::VkCommandBuffer)
    {
        //println!("Command Buffer ({:?}) end_recording", command_buffer);
        map_vk_result(unsafe { vkEndCommandBuffer(command_buffer) }).unwrap();
    }
}

impl CommandBufferRecording<'_>
{
    pub fn bind_vertex_buffer(&mut self, vertex_buffers: &[guarded::BufferHandle]) -> &mut Self
    {
        //println!("Command Buffer ({:?}) bind_vertex_buffer ({:?})", self.command_buffer.get_handle(), vertex_buffers);

        let vertex_buffer_handles : Vec<types::VkBuffer> = vertex_buffers.iter().map(|vb| vb.get_handle()).collect();
        let offsets : Vec<types::VkDeviceSize> = vertex_buffers.iter().map(|_| 0).collect();

        unsafe { vkCmdBindVertexBuffers(
            self.command_buffer.get_handle(),
            0,
            vertex_buffer_handles.len() as u32,
            vertex_buffer_handles.as_ptr(),
            offsets.as_ptr()
        ) };

        for buffer in vertex_buffers.iter() {
            self.command_buffer.claim_buffer(buffer.clone());
        }

        self
    }

    pub fn bind_index_buffer(&mut self, buffer: &guarded::BufferHandle) -> &mut Self
    {
        //println!("Command Buffer ({:?}) bind_index_buffer ({:?})", self.command_buffer.get_handle(), buffer);

        unsafe { vkCmdBindIndexBuffer(
            self.command_buffer.get_handle(),
            buffer.buffer.lock().unwrap().get_handle(),
            0,
            types::VK_INDEX_TYPE_UINT16
        ) };

        self.command_buffer.claim_buffer(buffer.clone());

        self
    }

    pub fn copy_buffer(&mut self, src: &BufferHandle, dest: &BufferHandle, size: usize) -> &mut Self
    {
        //println!("Command Buffer ({:?}) copy_buffer ({:?}) -> ({:?})", self.command_buffer.get_handle(), src.get_handle(), dest.get_handle());

        let copy_info = types::VkBufferCopy {
            srcOffset : 0,
            dstOffset : 0,
            size: size as u64
        };

        unsafe { vkCmdCopyBuffer(self.command_buffer.get_handle(), src.get_handle(), dest.get_handle(), 1, &copy_info) };

        self.command_buffer.claim_buffer(src.clone());
        self.command_buffer.claim_buffer(dest.clone());

        self
    }

    pub fn dispatch(&mut self, x: u32, y: u32, z: u32) -> &mut Self
    {
        unsafe { vkCmdDispatch(self.command_buffer.get_handle(), x, y, z) };

        self
    }

    pub fn begin_render_pass(&mut self, render_pass: &RenderPass, frame_buffer: &FrameBuffer, extent: &types::VkExtent2D) -> &mut Self
    {
        let clear_values : [types::VkClearValue; 2] = [
            types::VkClearValue {
                color: types::VkClearColorValue { float32: [0.0, 0.0, 0.0, 1.0] }
            },
            types::VkClearValue {
                depthStencil: types::VkClearDepthStencilValue {
                    depth: 1.0,
                    stencil: 0
                }
            }
        ];

        let render_pass_info = types::VkRenderPassBeginInfo
        {
            sType : types::VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
            pNext : std::ptr::null(),
            renderPass : render_pass.get_handle(),
            framebuffer : frame_buffer.get_handle(),
            renderArea : types::VkRect2D {
                offset : types::VkOffset2D { x: 0, y: 0 },
                extent : *extent
            },
            clearValueCount : clear_values.len() as u32,
            pClearValues : clear_values.as_ptr()
        };

        unsafe { vkCmdBeginRenderPass(self.command_buffer.get_handle(), &render_pass_info, types::VK_SUBPASS_CONTENTS_INLINE) };

        self
    }

    pub fn end_render_pass(&self) -> &Self
    {
        unsafe { vkCmdEndRenderPass(self.command_buffer.get_handle()) };

        self
    }

    pub fn bind_graphics_pipeline(&mut self, pipeline: guarded::GraphicsPipelineHandle) -> &mut Self
    {
        //println!("\nCommand Buffer ({:?}) bind_graphics_pipeline ({:?})\n", self.command_buffer.get_handle(), pipeline.get_handle());

        unsafe { vkCmdBindPipeline(self.command_buffer.get_handle(), types::VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.get_handle()) };

        self.command_buffer.claimed_graphics_pipelines.push(pipeline);

        self
    }

    pub fn bind_compute_pipeline(&self, pipeline: &ComputePipeline) -> &Self
    {
        unsafe { vkCmdBindPipeline(self.command_buffer.get_handle(), types::VK_PIPELINE_BIND_POINT_COMPUTE, pipeline.get_handle()) };

        self
    }

    pub fn bind_descriptor_set_for_graphics_pipeline(
        &mut self,
        layout: &guarded::PipelineLayoutHandle,
        descriptor_set: &DescriptorSetHandle,
        location: u32
    ) -> &mut Self {
        //println!("\nCommand Buffer ({:?}) bind_descriptor_set_for_graphics_pipeline ({:#?})", self.command_buffer.get_handle(), descriptor_set);

        {
            let dynamic_offsets = descriptor_set.get_dynamic_offsets();
            unsafe { vkCmdBindDescriptorSets(
                self.command_buffer.get_handle(),
                types::VK_PIPELINE_BIND_POINT_GRAPHICS,
                layout.get_handle(),
                location,
                1,
                &descriptor_set.get_handle(),
                dynamic_offsets.len() as u32,
                dynamic_offsets.as_ptr()
            ) };
        }

        self.command_buffer.claimed_descriptor_sets.push(descriptor_set.clone());

        self
    }

    pub fn bind_descriptor_set_for_compute_pipeline(&self, layout: &PipelineLayout, descriptor_set: &DescriptorSetHandle, location: u32) -> &Self
    {
        //println!("\nCommand Buffer ({:?}) bind_descriptor_set_for_compute_pipeline", self.command_buffer.get_handle());
        //println!("bind_descriptor_set_for_compute_pipeline: layout({:?}) descriptor_set({:?})", layout.get_handle(), descriptor_set.get_handle());
        let dynamic_offsets = descriptor_set.get_dynamic_offsets();
        unsafe { vkCmdBindDescriptorSets(self.command_buffer.get_handle(), types::VK_PIPELINE_BIND_POINT_COMPUTE, layout.get_handle(), location, 1, &descriptor_set.get_handle(), dynamic_offsets.len() as u32, dynamic_offsets.as_ptr()) };

        self
    }

    pub fn push_constants(&self, layout: &PipelineLayout, stage_flags: types::VkShaderStageFlags, offset: u32, size: u32, p_values: *const c_void) -> &Self
    {
        unsafe { vkCmdPushConstants(self.command_buffer.get_handle(), layout.get_handle(), stage_flags, offset, size, p_values) };

        self
    }

    pub fn draw(&self, vertex_count: u32, instance_count: u32, first_vertex: u32, first_instance: u32) -> &Self
    {
        unsafe { vkCmdDraw(self.command_buffer.get_handle(), vertex_count, instance_count, first_vertex, first_instance) };

        self
    }

    pub fn draw_indexed(&self, index_count: u32, instance_count: u32, first_vertex: u32, vertex_offset: i32, first_instance: u32) -> &Self
    {
        unsafe { vkCmdDrawIndexed(self.command_buffer.get_handle(), index_count, instance_count, first_vertex, vertex_offset, first_instance) };

        self
    }

    pub fn pipeline_barrier_to_dst_optimal(&mut self, image: &guarded::ImageHandle<Image>) -> &Self
    {
        //println!("Command Buffer ({:?}) pipeline_barrier_to_dst_optimal", self.command_buffer.get_handle());
        //println!("- Claiming image ({:?})", image.get_handle());

        self.command_buffer.claimed_images.push(image.clone());

        self.pipeline_barrier(
            types::VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
            types::VK_PIPELINE_STAGE_TRANSFER_BIT,
            vec![],
            vec![],
            vec![types::VkImageMemoryBarrier::new_color_aspect(
                image,
                types::VK_IMAGE_LAYOUT_UNDEFINED,
                types::VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                0,
                types::VK_ACCESS_TRANSFER_WRITE_BIT
            )]
        )
    }

    pub fn pipeline_barrier_to_shader_read_only(&mut self, image: &guarded::ImageHandle<Image>) -> &Self
    {
        //println!("Command Buffer ({:?}) pipeline_barrier_to_shader_read_only", self.command_buffer.get_handle());
        //println!("- Claiming image ({:?})", image.get_handle());

        self.command_buffer.claimed_images.push(image.clone());

        self.pipeline_barrier(
            types::VK_PIPELINE_STAGE_TRANSFER_BIT,
            types::VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
            vec![],
            vec![],
            vec![types::VkImageMemoryBarrier::new_color_aspect(
                image,
                types::VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                types::VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                types::VK_ACCESS_TRANSFER_WRITE_BIT,
                types::VK_ACCESS_SHADER_READ_BIT
            )]
        )
    }

    pub fn barrier_buffer_transfer_to_compute(&self, buffer: &Buffer, size: types::VkDeviceSize, src_queue_family: u32, dest_queue_family: u32) -> &Self
    {
        self.pipeline_barrier(
            types::VK_PIPELINE_STAGE_TRANSFER_BIT,
            types::VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
            vec![],
            vec![types::VkBufferMemoryBarrier::new(
                buffer, 0, size,
                types::VK_ACCESS_TRANSFER_WRITE_BIT,
                types::VK_ACCESS_SHADER_READ_BIT,
                src_queue_family, dest_queue_family
            )],
            vec![]
        )
    }

    pub fn barrier_buffer_compute_to_transfer(&self, buffer: &Buffer, size: types::VkDeviceSize, src_queue_family: u32, dest_queue_family: u32) -> &Self
    {
        self.pipeline_barrier(
            types::VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
            types::VK_PIPELINE_STAGE_TRANSFER_BIT,
            vec![],
            vec![types::VkBufferMemoryBarrier::new(
                buffer, 0, size,
                types::VK_ACCESS_SHADER_WRITE_BIT,
                types::VK_ACCESS_TRANSFER_READ_BIT,
                src_queue_family, dest_queue_family
            )],
            vec![]
        );

        self
    }

    fn pipeline_barrier(&self, src_stage: types::VkPipelineStageFlags, dst_stage: types::VkPipelineStageFlags, memory_barriers: Vec<types::VkMemoryBarrier>, buffer_memory_barriers: Vec<types::VkBufferMemoryBarrier>, image_barriers: Vec<types::VkImageMemoryBarrier>) -> &Self
    {
        unsafe {
            vkCmdPipelineBarrier(
                self.command_buffer.get_handle(),
                src_stage, dst_stage,
                0,
                memory_barriers.len() as u32, memory_barriers.as_ptr(),
                buffer_memory_barriers.len() as u32, buffer_memory_barriers.as_ptr(),
                image_barriers.len() as u32, image_barriers.as_ptr()
            )
        };

        self
    }

    pub fn copy_buffer_to_image(&mut self, buffer: &guarded::BufferHandle, image: &guarded::ImageHandle<Image>, x: i32, y: i32, width: u32, height: u32) -> &Self
    {
        //println!("Command Buffer ({:?}) copy buffer to image ({:?}) -> ({:?})", self.command_buffer.get_handle(), buffer.get_handle(), image.get_handle());
        //println!("- Claiming buffer ({:?})", buffer.get_handle());
        //println!("- Claiming Image ({:?})", image.get_handle());

        self.command_buffer.claimed_images.push(image.clone());
        self.command_buffer.claim_buffer(buffer.clone());

        let layers = types::VkImageSubresourceLayers
        {
            aspectMask: types::VK_IMAGE_ASPECT_COLOR_BIT,
            mipLevel: 0,
            baseArrayLayer: 0,
            layerCount: 1
        };

        let offset = types::VkOffset3D
        {
            x: x,
            y: y,
            z: 0
        };

        let extent = types::VkExtent3D
        {
            width: width,
            height: height,
            depth: 1
        };

        let region = types::VkBufferImageCopy
        {
            bufferOffset: 0,
            bufferRowLength: 0,
            bufferImageHeight: 0,
            imageSubresource: layers,
            imageOffset: offset,
            imageExtent: extent
        };

        unsafe {
            vkCmdCopyBufferToImage(
                self.command_buffer.get_handle(),
                buffer.get_handle(),
                image.get_handle(),
                types::VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                1,
                &region
            )
        };

        self
    }
}

impl Drop for CommandBufferRecording<'_>
{
    fn drop(&mut self)
    {
        Self::end_recording(self.command_buffer.get_handle());
        //self.command_buffer.claimed_buffers = std::mem::replace(&mut self.claimed_buffers, Vec::new());
        //self.command_buffer.claimed_images = std::mem::replace(&mut self.claimed_images, Vec::new());
        //self.command_buffer.claimed_graphics_pipelines = std::mem::replace(&mut self.claimed_graphics_pipelines, Vec::new());
        //self.command_buffer.claimed_descriptor_sets = std::mem::replace(&mut self.claimed_descriptor_sets, Vec::new());
    }
}

impl PartialEq for CommandBuffer
{
    fn eq(&self, other: &Self) -> bool
    {
        self.handle == other.handle
    }
}

impl Drop for CommandBuffer {
    fn drop(&mut self) {
        self.reset();
        //println!("Command Buffer ({:?}) Drop", self.get_handle());
    }
}

impl CommandBuffer
{
    pub fn new(pool: guarded::CommandPoolHandle) -> Self
    {
        let create_info = types::VkCommandBufferAllocateInfo
        {
            sType : types::VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
            pNext : std::ptr::null(),
            commandPool : pool.handle.get_handle(),
            level : types::VK_COMMAND_BUFFER_LEVEL_PRIMARY,
            commandBufferCount : 1,
        };

        CommandBuffer
        {
            handle : get_vk_handle(
                |handle: &mut types::VkCommandBuffer| {
                    unsafe { vkAllocateCommandBuffers(pool.handle.get_device().get_handle(), &create_info, handle) }
                }
            ).unwrap(),
            pool: pool,
            claimed_buffers: Vec::new(),
            claimed_images: Vec::new(),
            claimed_graphics_pipelines: Vec::new(),
            claimed_descriptor_sets: Vec::new()
        }
    }

    pub fn get_handle(&self) -> types::VkCommandBuffer
    {
        self.handle
    }

    pub fn claim_buffer(&mut self, buffer: guarded::BufferHandle) {
        if !self.claimed_buffers.contains(&buffer) {
            self.claimed_buffers.push(buffer);
        }
    }

    pub fn unclaim_descriptor_sets(&mut self) -> Vec<DescriptorSetHandle> {
        std::mem::take(&mut self.claimed_descriptor_sets)
    }

    pub fn reset(&mut self) {
        // println!("Command Buffer ({:?}) reset", self.get_handle());
        // for x in self.claimed_buffers.iter() {
        //     println!("- releasing buffer ({:?})", x.get_handle());
        // }
        // for x in self.claimed_images.iter() {
        //     println!("- releasing image ({:?})", x.get_handle());
        // }
        // for x in self.claimed_graphics_pipelines.iter() {
        //     println!("- releasing graphics pipeline ({:?})", x.get_handle());
        // }
        for x in self.claimed_descriptor_sets.iter() {
            //println!("- releasing descriptor set ({:?})", x.get_handle());
            x.release();
        }

        self.claimed_buffers = Vec::new();
        self.claimed_images = Vec::new();
        self.claimed_graphics_pipelines = Vec::new();
        self.claimed_descriptor_sets = Vec::new();
    }
}

impl types::VkBufferMemoryBarrier
{
    fn new(buffer: &Buffer, offset: types::VkDeviceSize, size: types::VkDeviceSize, src_access: types::VkAccessFlags, dest_access: types::VkAccessFlags, src_queue_family: u32, dest_queue_family: u32) -> Self
    {
        types::VkBufferMemoryBarrier
        {
            sType: types::VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER,
            pNext: std::ptr::null(),
            srcAccessMask: src_access,
            dstAccessMask: dest_access,
            srcQueueFamilyIndex: src_queue_family,
            dstQueueFamilyIndex: dest_queue_family,
            buffer: buffer.get_handle(),
            offset: offset,
            size: size
        }
    }
}

impl types::VkImageMemoryBarrier
{
    fn new_color_aspect<T: ImageTrait>(image: &guarded::ImageHandle<T>, old_layout: types::VkImageLayout, new_layout: types::VkImageLayout, src_access_mask: types::VkAccessFlags, dst_access_mask: types::VkAccessFlags) -> Self
    {
        Self::new(image, old_layout, new_layout, src_access_mask, dst_access_mask, types::VK_IMAGE_ASPECT_COLOR_BIT)
    }

    fn new<T: ImageTrait>(image: &guarded::ImageHandle<T>, old_layout: types::VkImageLayout, new_layout: types::VkImageLayout, src_access_mask: types::VkAccessFlags, dst_access_mask: types::VkAccessFlags, aspect: types::VkImageAspectFlags) -> Self
    {
        let image_subresource_range = types::VkImageSubresourceRange
        {
            aspectMask: aspect,
            baseMipLevel: 0,
            levelCount: 1,
            baseArrayLayer: 0,
            layerCount: 1
        };

        Self
        {
            sType: types::VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            pNext: std::ptr::null(),
            srcAccessMask: src_access_mask,
            dstAccessMask: dst_access_mask,
            oldLayout: old_layout,
            newLayout: new_layout,
            srcQueueFamilyIndex: types::VK_QUEUE_FAMILY_IGNORED,
            dstQueueFamilyIndex: types::VK_QUEUE_FAMILY_IGNORED,
            image: image.get_handle(),
            subresourceRange: image_subresource_range
        }
    }
}

#[derive(Debug)]
pub struct Fence
{
    handle : types::VkFence,
    device : Arc<LogicalDevice>
}

impl Fence
{
    pub fn new(handle: types::VkFence, device: Arc<LogicalDevice>) -> Self {
        Self {
            handle: handle,
            device: device
        }
    }

    pub fn get_handle(&self) -> types::VkFence
    {
        self.handle
    }

    pub fn wait(&self) // this should probably be considered mut
    {
        unsafe { vkWaitForFences(self.device.get_handle(), 1, &self.get_handle(), types::VK_TRUE, std::u64::MAX) };
    }

    pub fn reset(&self) // this should probably be considered mut
    {
        unsafe { vkResetFences(self.device.get_handle(), 1, &self.get_handle()) };
    }

    pub fn check(&self) -> types::VkResult {
        unsafe { vkGetFenceStatus(self.device.get_handle(), self.get_handle()) }
    }
}

impl Drop for Fence
{
    fn drop(&mut self)
    {
        unsafe { vkDestroyFence(self.device.get_handle(), self.get_handle(), std::ptr::null()) };
    }
}
