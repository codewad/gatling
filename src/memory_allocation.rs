use allocation_tracker::AllocationBlock;

use crate::memory_type_allocator::MemoryAllocator;
use crate::memory_type_allocator::Tracker;
use crate::types;
use crate::guarded;
use crate::util::map_vk_result;
use crate::image::ImageTrait;

use super::MemoryMap;
use super::vkBindBufferMemory;
use super::vkBindImageMemory;

#[derive(Debug)]
pub struct MemoryAllocation
{
    tracker: Tracker,
    block: AllocationBlock
}

impl MemoryAllocation
{
    pub fn get_local_properties() -> types::VkMemoryPropertyFlags
    {
        types::VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
    }

    pub fn get_mapable_properties() -> types::VkMemoryPropertyFlags
    {
        types::VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
        types::VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
    }

    pub fn get_staging_properties() -> types::VkMemoryPropertyFlags
    {
        Self::get_mapable_properties()
    }

    pub fn new(tracker: Tracker, block: AllocationBlock) -> Self
    {
        Self
        {
            tracker: tracker,
            block: block
        }
    }
}

impl MemoryAllocation
{
    pub fn get_handle(&self) -> types::VkDeviceMemory
    {
        self.tracker.resource_handle()
    }

    pub fn get_device(&self) -> guarded::LogicalDeviceHandle
    {
        self.tracker.resource_device()
    }

    pub fn get_offset(&self) -> types::VkDeviceSize {
        self.block.offset() as types::VkDeviceSize
    }

    pub fn get_block(&self) -> AllocationBlock {
        self.block
    }

    pub fn bind_buffer(&self, buffer: &guarded::BufferHandle) -> Result<(), types::VkResult>
    {
        //println!("bind_buffer {:?}", self.block);
        map_vk_result(unsafe { vkBindBufferMemory(self.get_device().get_handle(), buffer.get_handle(), self.get_handle(), self.block.offset() as u64) })
    }

    pub fn bind_image<T: ImageTrait>(&self, image: &guarded::ImageHandle<T>) -> Result<(), types::VkResult>
    {
        //println!("bind_image {:?}", self.block);
        map_vk_result(unsafe { vkBindImageMemory(
            self.get_device().get_handle(),
            image.get_handle(),
            self.get_handle(),
            self.block.offset() as u64
        ) })
    }

    pub fn get_map(&self, size: usize) -> MemoryMap
    {
        MemoryMap::new(
            MemoryMap::create_handle(self.get_device().get_handle(), self.get_handle(), self.block.offset(), size).unwrap(),
            self.get_handle(),
            self.get_device().get_handle()
        )
    }
}

impl Drop for MemoryAllocation
{
    fn drop(&mut self)
    {
        self.tracker.free(self.block);
    }
}
