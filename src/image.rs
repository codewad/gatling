use crate::guarded;
use crate::types;

use super::{
    vkDestroyImage,
    vkGetImageMemoryRequirements
};

pub trait ImageTrait {
    fn get_handle(&self) -> types::VkImage;
    fn get_device(&self) -> guarded::LogicalDeviceHandle;
    fn get_format(&self) -> types::VkFormat;
    fn get_memory_requirements(&self) -> types::VkMemoryRequirements;
}

#[derive(Debug, Clone)]
pub struct UnownedImage {
    handle: types::VkImage,
    device: guarded::LogicalDeviceHandle,
    format: types::VkFormat,
}

impl UnownedImage {
    pub fn from_raw(device: guarded::LogicalDeviceHandle, handle: types::VkImage, format: types::VkFormat) -> Self
    {
        Self {
            handle: handle,
            device: device,
            format: format,
        }
    }
}

impl ImageTrait for UnownedImage {
    fn get_handle(&self) -> types::VkImage
    {
        self.handle
    }

    fn get_device(&self) -> guarded::LogicalDeviceHandle
    {
        self.device.clone()
    }

    fn get_format(&self) -> types::VkFormat
    {
        self.format
    }

    fn get_memory_requirements(&self) -> types::VkMemoryRequirements
    {
        let mut requirements = types::VkMemoryRequirements::default();
        unsafe { vkGetImageMemoryRequirements(self.get_device().get_handle(), self.get_handle(), &mut requirements) };
        requirements
    }
}

#[derive(Debug, Clone)]
pub struct Image
{
    handle: types::VkImage,
    device: guarded::LogicalDeviceHandle,
    format: types::VkFormat,
}

impl Image
{
    fn create_image(device: &guarded::LogicalDeviceHandle, width: usize, height: usize, usage: types::VkImageUsageFlags, format: types::VkFormat) -> Result<types::VkImage, types::VkResult>
    {
        let extent = types::VkExtent3D
        {
            width: width as u32,
            height: height as u32,
            depth: 1
        };

        let create_info = types::VkImageCreateInfo
        {
            sType: types::VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
            pNext: std::ptr::null(),
            flags: 0,
            imageType: types::VK_IMAGE_TYPE_2D,
            format: format,
            extent: extent,
            mipLevels: 1,
            arrayLayers: 1,
            samples: types::VK_SAMPLE_COUNT_1_BIT,
            tiling: types::VK_IMAGE_TILING_OPTIMAL,
            usage: usage,
            sharingMode: types::VK_SHARING_MODE_EXCLUSIVE,
            queueFamilyIndexCount: 0,
            pQueueFamilyIndices: std::ptr::null(),
            initialLayout: types::VK_IMAGE_LAYOUT_UNDEFINED
        };

        Self::create_handle(device.get_handle(), create_info)
    }

    pub fn from_raw(device: guarded::LogicalDeviceHandle, handle: types::VkImage, format: types::VkFormat) -> Self
    {
        Self {
            handle: handle,
            device: device,
            format: format,
        }
    }

    pub fn new(device: guarded::LogicalDeviceHandle, width: usize, height: usize, usage: types::VkImageUsageFlags, format: types::VkFormat) -> Result<Self, types::VkResult>
    {
        match Self::create_image(&device, width, height, usage, format)
        {
            Ok(handle) => Ok(Self::from_raw(device, handle, format)),
            Err(e) => Err(e)
        }
    }

    pub fn new_depth_image(device: guarded::LogicalDeviceHandle, width: usize, height: usize) -> Result<Self, types::VkResult>
    {
        let depth_format = device.get_device().find_depth_format().unwrap();
        Self::new(device, width, height, types::VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, depth_format)
    }

    pub fn new_texture_image(device: guarded::LogicalDeviceHandle, width: usize, height: usize) -> Result<Self, types::VkResult>
    {
        Self::new_texture_image_with_format(device, width, height, types::VK_FORMAT_R8G8B8A8_SRGB)
        //Self::new(device, width, height, types::VK_IMAGE_USAGE_TRANSFER_DST_BIT | types::VK_IMAGE_USAGE_SAMPLED_BIT, types::VK_FORMAT_R8G8B8A8_SRGB)
    }

    pub fn new_texture_image_with_format(device: guarded::LogicalDeviceHandle, width: usize, height: usize, format: types::VkFormat) -> Result<Self, types::VkResult>
    {
        Self::new(device, width, height, types::VK_IMAGE_USAGE_TRANSFER_DST_BIT | types::VK_IMAGE_USAGE_SAMPLED_BIT, format)
    }
}

impl ImageTrait for Image
{
    fn get_handle(&self) -> types::VkImage
    {
        self.handle
    }

    fn get_device(&self) -> guarded::LogicalDeviceHandle
    {
        self.device.clone()
    }

    fn get_format(&self) -> types::VkFormat
    {
        self.format
    }

    fn get_memory_requirements(&self) -> types::VkMemoryRequirements
    {
        let mut requirements = types::VkMemoryRequirements::default();
        unsafe { vkGetImageMemoryRequirements(self.get_device().get_handle(), self.get_handle(), &mut requirements) };
        requirements
    }
}

impl Drop for Image
{
    fn drop(&mut self)
    {
        unsafe { vkDestroyImage(self.get_device().get_handle(), self.get_handle(), std::ptr::null()) };
    }
}
