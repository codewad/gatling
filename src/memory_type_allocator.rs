use std::sync::Arc;
use std::sync::RwLock;

use allocation_tracker::AllocationBlock;
use allocation_tracker::AllocationError;
use allocation_tracker::AllocationPool;

use crate::guarded;
use crate::types;

use crate::MemoryAllocationPool;
use crate::MemoryAllocation;
use crate::MemoryAllocationHandle;

pub trait MemoryAllocator
{
    fn allocate(&mut self, size: types::VkDeviceSize) -> Result<MemoryAllocationHandle, AllocationError>;
    fn free(&mut self, block: AllocationBlock);
}

#[derive(Clone, Debug)]
pub struct Tracker {
    allocator: Arc<RwLock<AllocationPool>>,
    resource: Arc<RwLock<MemoryAllocationPool>>
}

impl Tracker
{
    fn new(resource: MemoryAllocationPool) -> Self {
        Self {
            allocator: Arc::new(RwLock::new(AllocationPool::new(resource.len() as usize).unwrap())),
            resource: Arc::new(RwLock::new(resource))
        }
    }

    pub fn resource_handle(&self) -> types::VkDeviceMemory {
        self.resource.read().unwrap().get_handle()
    }

    pub fn resource_device(&self) -> guarded::LogicalDeviceHandle {
        self.resource.read().unwrap().get_device()
    }
}

impl MemoryAllocator for Tracker
{
    fn allocate(&mut self, size: types::VkDeviceSize) -> Result<MemoryAllocationHandle, AllocationError>
    {
        match self.allocator.write().unwrap().get_block(size as usize) {
            Ok(block) => {
                Ok(MemoryAllocationHandle::new(
                    MemoryAllocation::new(self.clone(), block)
                ))
            },
            Err(e) => Err(e)
        }
    }

    fn free(&mut self, block: AllocationBlock)
    {
        self.allocator.write().unwrap().free_block(block);
    }
}

#[derive(Debug)]
pub struct MemoryTypeAllocator
{
    size: types::VkDeviceSize,
    memory_type_index: u32,
    trackers: Vec<Tracker>,
}

impl MemoryTypeAllocator
{
    pub fn allocate(&mut self, device: guarded::LogicalDeviceHandle, size: types::VkDeviceSize) -> Result<MemoryAllocationHandle, types::VkResult>
    {
        for tracker in self.trackers.iter_mut() {
            match tracker.allocate(size) {
                Ok(allocation) => {
                    return Ok(allocation);
                }
                Err(_) => {}
            }
        }

        let allocation_size = if size < 65536 { 65536 } else { size };
        let new_resource = MemoryAllocationPool::create(device, self.memory_type_index, allocation_size)?;
        let mut new_tracker = Tracker::new(new_resource);

        let new_allocation_result = new_tracker.allocate(size);
        if new_allocation_result.is_err() {
            return Err(types::VK_ERROR_OUT_OF_DEVICE_MEMORY);
        }

        let new_allocation = new_allocation_result.unwrap();
        self.trackers.push(new_tracker);

        Ok(new_allocation)
    }

    pub fn new(size: types::VkDeviceSize, memory_type_index: u32) -> Self
    {
        Self
        {
            size: size,
            memory_type_index: memory_type_index,
            trackers: Vec::new()
        }
    }
}
