use super::types;
use super::{
    vkAllocateDescriptorSets,
    vkCreateBuffer,
    vkCreateCommandPool,
    vkCreateComputePipelines,
    vkCreateDescriptorPool,
    vkCreateDescriptorSetLayout,
    vkCreateFramebuffer,
    vkCreateImage,
    vkCreateImageView,
    vkCreatePipelineLayout,
    vkCreateSampler,
    vkCreateSemaphore
};

use super::wrappertypes::LogicalDevice;
use super::wrappertypes::ImageView;
use super::wrappertypes::CommandPool;
use super::wrappertypes::FrameBuffer;
use super::wrappertypes::PipelineLayout;

use super::util::{
    get_vk_handle,
    map_vk_result
};

use super::image::Image;
use super::sampler::Sampler;
use super::semaphore::Semaphore;
use super::descriptor_set::DescriptorSet;
use super::descriptor_pool::DescriptorPool;
use super::ComputePipeline;
use super::DescriptorSetLayout;

use crate::Buffer;

#[cfg(any(test,feature = "test"))]
mod test_create_handle
{
    use super::super::types;

    #[allow(non_snake_case, unused_variables, dead_code)] #[inline]
    fn dummy_create_compute_pipelines(
        device: types::VkDevice,
        pipelineCache: types::VkPipelineCache,
        createInfoCount: u32,
        pCreateInfos: *const types::VkComputePipelineCreateInfo,
        pAllocator: *const types::VkAllocationCallbacks,
        pPipelines: *mut types::VkPipeline) -> types::VkResult
    {
        println!("in dummy");
        types::VK_SUCCESS
    }

    macro_rules! set_VkFn
    {
        ($name:ident, $func:ident) => {
            unsafe { super::super::storage::$name = Some($func) };
        }
    }
}

impl ComputePipeline<'_>
{
    pub fn create_handle(device: &LogicalDevice, create_info: types::VkComputePipelineCreateInfo) -> Result<types::VkPipeline, types::VkResult>
    {
        get_vk_handle(|handle: &mut types::VkPipeline| {
            unsafe { vkCreateComputePipelines(device.get_handle(), std::ptr::null(), 1, &create_info, std::ptr::null(), handle) }
        })
    }
}

impl CommandPool
{
    pub fn create_handle(device: &LogicalDevice, create_info: types::VkCommandPoolCreateInfo) -> Result<types::VkCommandPool, types::VkResult>
    {
        get_vk_handle(|handle: &mut types::VkCommandPool| {
            unsafe { vkCreateCommandPool(device.get_handle(), &create_info, std::ptr::null(), handle) }
        })
    }
}

impl DescriptorSetLayout
{
    pub fn create_handle(device_handle: types::VkDevice, create_info: &types::VkDescriptorSetLayoutCreateInfo) -> Result<types::VkDescriptorSetLayout, types::VkResult>
    {
        get_vk_handle(|handle: &mut types::VkDescriptorSetLayout| {
            unsafe { vkCreateDescriptorSetLayout(device_handle, create_info, std::ptr::null(), handle) }
        })
    }
}

impl PipelineLayout
{
    pub fn create_handle(device: &LogicalDevice, create_info: types::VkPipelineLayoutCreateInfo) -> Result<types::VkPipelineLayout, types::VkResult>
    {
        get_vk_handle(|handle: &mut types::VkPipelineLayout| {
            unsafe { vkCreatePipelineLayout(device.get_handle(), &create_info, std::ptr::null_mut(), handle) }
        })
    }
}

impl Buffer
{
    pub fn create_handle(device: &LogicalDevice, create_info: types::VkBufferCreateInfo) -> Result<types::VkBuffer, types::VkResult>
    {
        get_vk_handle(|handle: &mut types::VkBuffer| {
            unsafe { vkCreateBuffer(device.get_handle(), &create_info, std::ptr::null(), handle) }
        })
    }
}

impl FrameBuffer
{
    pub fn create_handle(device: types::VkDevice, create_info: types::VkFramebufferCreateInfo) -> Result<types::VkFramebuffer, types::VkResult>
    {
        get_vk_handle(|handle: &mut types::VkFramebuffer| {
            unsafe { vkCreateFramebuffer(device, &create_info, std::ptr::null(), handle) }
        })
    }
}

impl Sampler
{
    pub fn create_handle(device: &LogicalDevice, create_info: types::VkSamplerCreateInfo) -> Result<types::VkSampler, types::VkResult>
    {
        get_vk_handle(|handle: &mut types::VkSampler| {
            unsafe { vkCreateSampler(device.get_handle(), &create_info, std::ptr::null(), handle) }
        })
    }
}

impl Semaphore
{
    pub fn create_handle(device: &LogicalDevice, create_info: types::VkSemaphoreCreateInfo) -> Result<types::VkSemaphore, types::VkResult>
    {
        get_vk_handle(|handle: &mut types::VkSemaphore| {
            unsafe { vkCreateSemaphore(device.get_handle(), &create_info, std::ptr::null(), handle) }
        })
    }
}

impl ImageView
{
    pub fn create_handle(device: types::VkDevice, create_info: types::VkImageViewCreateInfo) -> Result<types::VkImageView, types::VkResult>
    {
        get_vk_handle(|handle: &mut types::VkImageView| {
            unsafe { vkCreateImageView(device, &create_info, std::ptr::null(), handle) }
        })
    }
}

impl Image
{
    pub fn create_handle(device: types::VkDevice, create_info: types::VkImageCreateInfo) -> Result<types::VkImage, types::VkResult>
    {
        get_vk_handle(|handle: &mut types::VkImage| {
            unsafe { vkCreateImage(device, &create_info, std::ptr::null(), handle) }
        })
    }
}

impl DescriptorPool
{
    pub fn create_handle(device: &LogicalDevice, create_info: types::VkDescriptorPoolCreateInfo) -> Result<types::VkDescriptorPool, types::VkResult>
    {
        get_vk_handle(|handle: &mut types::VkDescriptorPool| {
            unsafe { vkCreateDescriptorPool(device.get_handle(), &create_info, std::ptr::null(), handle) }
        })
    }
}

impl DescriptorSet
{
    pub fn create_handles(device: &LogicalDevice, count: usize, allocate_info: types::VkDescriptorSetAllocateInfo) -> Result<Vec<types::VkDescriptorSet>, types::VkResult>
    {
        let mut handles: Vec<types::VkDescriptorSet> = Vec::new();
        handles.resize(count, std::ptr::null());

        match map_vk_result(unsafe { vkAllocateDescriptorSets(device.get_handle(), &allocate_info, handles.as_mut_ptr()) })
        {
            Ok(()) => Ok(handles),
            Err(code) => Err(code)
        }
    }
}
