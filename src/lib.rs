use std::mem;
use std::os::raw;
use std::ffi::c_void;
use std::ffi::CString;
use libloading::Library;

mod guarded;
pub use guarded::BufferHandle;
pub use guarded::CommandBufferHandle;
pub use guarded::CommandPoolHandle;
pub use guarded::DescriptorSetHandle;
pub use guarded::DescriptorSetLayoutHandle;
pub use guarded::DescriptorPoolHandle;
pub use guarded::FenceHandle;
pub use guarded::GraphicsPipelineHandle;
pub use guarded::LibContextHandle;
pub use guarded::LogicalDeviceHandle;
pub use guarded::ImageHandle;
pub use guarded::ImageViewHandle;
pub use guarded::InstanceHandle;
pub use guarded::MemoryAllocationHandle;
pub use guarded::PhysicalDeviceHandle;
pub use guarded::PipelineLayoutHandle;
pub use guarded::QueueHandle;
pub use guarded::RenderPassHandle;
pub use guarded::SemaphoreHandle;
pub use guarded::SurfaceHandle;
pub use guarded::SwapChainHandle;

mod wrappertypes;
pub use wrappertypes::{
    IntoVertexAttributeDescriptions,

    PhysicalDevice,
    LogicalDevice,
    ImageView,
    VulkanInstance,
    VulkanLibContext,
    CommandBuffer,
    CommandBufferRecording,
    CommandPool,
    Fence,
    FrameBuffer,
    GraphicsPipeline,
    MemoryMap,
    PipelineLayout,
    RenderPass,
    ShaderModule,
    Surface,
    SwapChain,
    Queue
};

pub mod types;
mod compute_pipeline;
mod create_handles;
mod descriptor_set;
pub use descriptor_set::DescriptorSet;
mod descriptor_set_definition;
mod descriptor_set_layout;
pub use descriptor_set_layout::DescriptorSetLayout;
mod descriptor_pool;
pub use descriptor_pool::DescriptorPool;
mod image;
mod memory_type_allocator;
use memory_type_allocator::MemoryTypeAllocator;
mod memory_allocation_pool;
use memory_allocation_pool::MemoryAllocationPool;
mod memory_allocation;
pub use memory_allocation::MemoryAllocation;
pub use image::Image;
mod sampler;
pub use sampler::Sampler;
mod semaphore;
mod util;
mod logical_device_builder;
mod shader_stage;
mod buffer;
pub use buffer::Buffer;

pub use descriptor_set_definition::DescriptorSetDefinition;
pub use descriptor_set_definition::DescriptorType;
use compute_pipeline::ComputePipeline;
pub use logical_device_builder::LogicalDeviceBuilder;
pub use semaphore::Semaphore;

pub use types::{
    win32,

    VkAllocationCallbacks,
    VkApplicationInfo,
    VkBool32,
    VkDeviceCreateInfo,
    VkDevice,
    VkDebugReportCallbackCreateInfoEXT,
    VkDebugReportCallbackEXT,
    VkImage,
    VkImageView,
    VkImageViewCreateInfo,
    VkInstance,
    VkInstanceCreateInfo,
    VkExtensionProperties,
    VkLayerProperties,
    VkPhysicalDevice,
    VkPhysicalDeviceFeatures,
    VkPhysicalDeviceProperties,
    VkPresentModeKHR,
    VkQueue,
    VkQueueFamilyProperties,
    VkResult,
    VkShaderModule,
    VkShaderModuleCreateInfo,
    VkSurfaceCapabilitiesKHR,
    VkSurfaceFormatKHR,
    VkSurfaceKHR,
    VkSwapchainCreateInfoKHR,
    VkSwapchainKHR,

    // VK_EXT_debug_utils extension
    VkDebugUtilsMessengerEXT,
    VkDebugUtilsMessageTypeFlagsEXT,
    VkDebugUtilsMessengerCallbackDataEXT,
    VkDebugUtilsMessengerCreateInfoEXT,
    VkDebugUtilsMessengerCreateFlagsEXT,
    VkDebugUtilsMessageSeverityFlagBitsEXT,

    PFN_vkVoidFunction
};

macro_rules! VkFnType
{
    ($typename:ident, ($( $param_name:ident ),*) , ($( $param_type:ty ),*) => $return:ty) => {
        #[allow(non_snake_case, non_camel_case_types, dead_code)]
        type $typename = fn($( $param_name: $param_type, )*) -> $return;
    }
}

macro_rules! VkFn
{
    ($name:ident, $typename:ident, ($( $param_name:ident ),*) , ($( $param_type:ty ),*) => $return:ty) => {
        #[allow(non_snake_case, unused_variables, dead_code)] #[inline]
        pub unsafe fn $name($( $param_name: $param_type, )*) -> $return
        {
            let result = (storage::$name.unwrap())($( $param_name, )*);
            result
        }
    }
}

mod storage
{
    use super::*;

    macro_rules! VkFnStore
    {
        ($name:ident, $typename:ident) => {
            #[allow(non_upper_case_globals, unused_variables, dead_code)]
            pub static mut $name : Option<$typename> = None;
        }
    }

    VkFnStore!(vkAcquireNextImageKHR, vkAcquireNextImageKHR_type);
    VkFnStore!(vkAllocateCommandBuffers, vkAllocateCommandBuffers_type);
    VkFnStore!(vkBeginCommandBuffer, vkBeginCommandBuffer_type);
    VkFnStore!(vkCmdBeginRenderPass, vkCmdBeginRenderPass_type);
    VkFnStore!(vkCmdBindPipeline, vkCmdBindPipeline_type);
    VkFnStore!(vkCmdDraw, vkCmdDraw_type);
    VkFnStore!(vkCmdEndRenderPass, vkCmdEndRenderPass_type);
    VkFnStore!(vkCreateBuffer, vkCreateBuffer_type);
    VkFnStore!(vkCreateBufferView, vkCreateBufferView_type);
    VkFnStore!(vkCreateCommandPool, vkCreateCommandPool_type);
    VkFnStore!(vkCreateDevice, vkCreateDevice_type);
    VkFnStore!(vkCreateFence, vkCreateFence_type);
    VkFnStore!(vkCreateFramebuffer, vkCreateFramebuffer_type);
    VkFnStore!(vkCreateGraphicsPipelines, vkCreateGraphicsPipelines_type);
    VkFnStore!(vkCreateImageView, vkCreateImageView_type);
    VkFnStore!(vkCreateInstance, vkCreateInstance_type);
    VkFnStore!(vkCreatePipelineLayout, vkCreatePipelineLayout_type);
    VkFnStore!(vkCreateRenderPass, vkCreateRenderPass_type);
    VkFnStore!(vkCreateSemaphore, vkCreateSemaphore_type);
    VkFnStore!(vkCreateShaderModule, vkCreateShaderModule_type);
    VkFnStore!(vkCreateSwapchainKHR, vkCreateSwapchainKHR_type);
    VkFnStore!(vkDestroyBuffer, vkDestroyBuffer_type);
    VkFnStore!(vkDestroyCommandPool, vkDestroyCommandPool_type);
    VkFnStore!(vkDestroyDevice, vkDestroyDevice_type);
    VkFnStore!(vkDestroyFence, vkDestroyFence_type);
    VkFnStore!(vkDestroyFramebuffer, vkDestroyFramebuffer_type);
    VkFnStore!(vkDestroyImageView, vkDestroyImageView_type);
    VkFnStore!(vkDestroyInstance, vkDestroyInstance_type);
    VkFnStore!(vkDestroyPipeline, vkDestroyPipeline_type);
    VkFnStore!(vkDestroyPipelineLayout, vkDestroyPipelineLayout_type);
    VkFnStore!(vkDestroyRenderPass, vkDestroyRenderPass_type);
    VkFnStore!(vkDestroySemaphore, vkDestroySemaphore_type);
    VkFnStore!(vkDestroyShaderModule, vkDestroyShaderModule_type);
    VkFnStore!(vkDestroySurfaceKHR, vkDestroySurfaceKHR_type);
    VkFnStore!(vkDestroySwapchainKHR, vkDestroySwapchainKHR_type);
    VkFnStore!(vkDeviceWaitIdle, vkDeviceWaitIdle_type);
    VkFnStore!(vkEndCommandBuffer, vkEndCommandBuffer_type);
    VkFnStore!(vkEnumerateDeviceExtensionProperties, vkEnumerateDeviceExtensionProperties_type);
    VkFnStore!(vkEnumerateInstanceExtensionProperties, vkEnumerateInstanceExtensionProperties_type);
    VkFnStore!(vkEnumerateInstanceLayerProperties, vkEnumerateInstanceLayerProperties_type);
    VkFnStore!(vkEnumeratePhysicalDevices, vkEnumeratePhysicalDevices_type);
    VkFnStore!(vkGetDeviceQueue, vkGetDeviceQueue_type);
    VkFnStore!(vkGetInstanceProcAddr, vkGetInstanceProcAddr_type);
    VkFnStore!(vkGetPhysicalDeviceFeatures, vkGetPhysicalDeviceFeatures_type);
    VkFnStore!(vkGetPhysicalDeviceProperties, vkGetPhysicalDeviceProperties_type);
    VkFnStore!(vkGetPhysicalDeviceQueueFamilyProperties, vkGetPhysicalDeviceQueueFamilyProperties_type);
    VkFnStore!(vkGetPhysicalDeviceSurfaceCapabilitiesKHR, vkGetPhysicalDeviceSurfaceCapabilitiesKHR_type);
    VkFnStore!(vkGetPhysicalDeviceSurfaceFormatsKHR, vkGetPhysicalDeviceSurfaceFormatsKHR_type);
    VkFnStore!(vkGetPhysicalDeviceSurfacePresentModesKHR, vkGetPhysicalDeviceSurfacePresentModesKHR_type);
    VkFnStore!(vkGetPhysicalDeviceSurfaceSupportKHR, vkGetPhysicalDeviceSurfaceSupportKHR_type);
    VkFnStore!(vkGetSwapchainImagesKHR, vkGetSwapchainImagesKHR_type);
    VkFnStore!(vkQueuePresentKHR, vkQueuePresentKHR_type);
    VkFnStore!(vkQueueSubmit, vkQueueSubmit_type);
    VkFnStore!(vkQueueWaitIdle, vkQueueWaitIdle_type);
    VkFnStore!(vkResetFences, vkResetFences_type);
    VkFnStore!(vkWaitForFences, vkWaitForFences_type);
    VkFnStore!(vkGetBufferMemoryRequirements, vkGetBufferMemoryRequirements_type);
    VkFnStore!(vkGetPhysicalDeviceMemoryProperties, vkGetPhysicalDeviceMemoryProperties_type);
    VkFnStore!(vkAllocateMemory, vkAllocateMemory_type);
    VkFnStore!(vkFreeMemory, vkFreeMemory_type);
    VkFnStore!(vkBindBufferMemory, vkBindBufferMemory_type);
    VkFnStore!(vkMapMemory, vkMapMemory_type);
    VkFnStore!(vkUnmapMemory, vkUnmapMemory_type);
    VkFnStore!(vkCmdBindVertexBuffers, vkCmdBindVertexBuffers_type);
    VkFnStore!(vkCmdCopyBuffer, vkCmdCopyBuffer_type);
    VkFnStore!(vkCmdDrawIndexed, vkCmdDrawIndexed_type);
    VkFnStore!(vkCmdBindIndexBuffer, vkCmdBindIndexBuffer_type);
    VkFnStore!(vkCreateDescriptorSetLayout, vkCreateDescriptorSetLayout_type);
    VkFnStore!(vkDestroyDescriptorSetLayout, vkDestroyDescriptorSetLayout_type);
    VkFnStore!(vkCreateDescriptorPool, vkCreateDescriptorPool_type);
    VkFnStore!(vkDestroyDescriptorPool, vkDestroyDescriptorPool_type);
    VkFnStore!(vkAllocateDescriptorSets, vkAllocateDescriptorSets_type);
    VkFnStore!(vkUpdateDescriptorSets, vkUpdateDescriptorSets_type);
    VkFnStore!(vkCmdBindDescriptorSets, vkCmdBindDescriptorSets_type);
    VkFnStore!(vkCreateImage, vkCreateImage_type);
    VkFnStore!(vkGetImageMemoryRequirements, vkGetImageMemoryRequirements_type);
    VkFnStore!(vkBindImageMemory, vkBindImageMemory_type);
    VkFnStore!(vkCmdPipelineBarrier, vkCmdPipelineBarrier_type);
    VkFnStore!(vkDestroyImage, vkDestroyImage_type);
    VkFnStore!(vkCmdCopyBufferToImage, vkCmdCopyBufferToImage_type);
    VkFnStore!(vkCreateSampler, vkCreateSampler_type);
    VkFnStore!(vkDestroySampler, vkDestroySampler_type);
    VkFnStore!(vkGetPhysicalDeviceFormatProperties, vkGetPhysicalDeviceFormatProperties_type);
    VkFnStore!(vkCreateComputePipelines, vkCreateComputePipelines_type);
    VkFnStore!(vkCmdDispatch, vkCmdDispatch_type);
    VkFnStore!(vkCmdPushConstants, vkCmdPushConstants_type);
    VkFnStore!(vkGetFenceStatus, vkGetFenceStatus_type);
    VkFnStore!(vkGetSemaphoreCounterValue, vkGetSemaphoreCounterValue_type);
    VkFnStore!(vkFreeDescriptorSets, vkFreeDescriptorSets_type);

    // VK_KHR_win32_surface extension
    VkFnStore!(vkCreateWin32SurfaceKHR, vkCreateWin32SurfaceKHR_type);

    // VK_EXT_debug_report extension
    VkFnStore!(vkCreateDebugReportCallbackEXT, vkCreateDebugReportCallbackEXT_type);
    VkFnStore!(vkDestroyDebugReportCallbackEXT, vkDestroyDebugReportCallbackEXT_type);

    // VK_EXT_debug_utils extension
    VkFnStore!(vkCreateDebugUtilsMessengerEXT, vkCreateDebugUtilsMessengerEXT_type);
    VkFnStore!(vkDestroyDebugUtilsMessengerEXT, vkDestroyDebugUtilsMessengerEXT_type);
    VkFnStore!(vkSubmitDebugUtilsMessageEXT, vkSubmitDebugUtilsMessageEXT_type);
}

VkFnType!(vkAcquireNextImageKHR_type, (device, swapchain, timeout, semaphore, fence, pImageIndex), (VkDevice, VkSwapchainKHR, u64, types::VkSemaphore, types::VkFence, *mut u32) => VkResult);
VkFnType!(vkAllocateCommandBuffers_type, (device, pAllocateInfo, pCommandBuffers), (VkDevice, *const types::VkCommandBufferAllocateInfo, *mut types::VkCommandBuffer) => VkResult);
VkFnType!(vkBeginCommandBuffer_type, (commandBuffer, pBeginInfo), (types::VkCommandBuffer, *const types::VkCommandBufferBeginInfo) => VkResult);
VkFnType!(vkCmdBeginRenderPass_type, (commandBuffer, pRenderPassBegin, contents), (types::VkCommandBuffer, *const types::VkRenderPassBeginInfo, types::VkSubpassContents) => ());
VkFnType!(vkCmdBindPipeline_type, (commandBuffer, pipelineBindPoint, pipeline), (types::VkCommandBuffer, types::VkPipelineBindPoint, types::VkPipeline) => ());
VkFnType!(vkCmdDraw_type, (commandBuffer, vertexCount, instanceCount, firstVertex, firstInstance), (types::VkCommandBuffer, u32, u32, u32, u32) => ());
VkFnType!(vkCmdEndRenderPass_type, (commandBuffer), (types::VkCommandBuffer) => ());
VkFnType!(vkCreateBuffer_type, (device, pCreateInfo, pAllocator, pBuffer), (VkDevice, *const types::VkBufferCreateInfo, *const VkAllocationCallbacks, *mut types::VkBuffer) => VkResult);
VkFnType!(vkCreateBufferView_type, (device, pCreateInfo, pAllocator, pView), (VkDevice, *const types::VkBufferViewCreateInfo, *const VkAllocationCallbacks, *mut types::VkBufferView) => VkResult);
VkFnType!(vkCreateCommandPool_type, (device, pCreateInfo, pAllocator, pCommandPool), (VkDevice, *const types::VkCommandPoolCreateInfo, *const VkAllocationCallbacks, *mut types::VkCommandPool) => VkResult);
VkFnType!(vkCreateDevice_type, (physicalDevice, pCreateInfo, pAllocator, pDevice), (VkPhysicalDevice, *const VkDeviceCreateInfo, *const VkAllocationCallbacks, *mut VkDevice) => VkResult);
VkFnType!(vkCreateFence_type, (device, pCreateInfo, pAllocator, pFence), (VkDevice, *const types::VkFenceCreateInfo, *const VkAllocationCallbacks, *mut types::VkFence) => VkResult);
VkFnType!(vkCreateFramebuffer_type, (device, pCreateInfo, pAllocator, pFramebuffer), (VkDevice, *const types::VkFramebufferCreateInfo, *const VkAllocationCallbacks, *mut types::VkFramebuffer) => VkResult);
VkFnType!(vkCreateGraphicsPipelines_type, (device, pipelineCache, createInfoCount, pCreateInfos, pAllocator, pPipelines), (VkDevice, types::VkPipelineCache, u32, *const types::VkGraphicsPipelineCreateInfo, *const VkAllocationCallbacks, *mut types::VkPipeline) => VkResult);
VkFnType!(vkCreateImageView_type, (device, pCreateInfo, pAllocator, pView), (VkDevice, *const VkImageViewCreateInfo, *const VkAllocationCallbacks, *mut VkImageView) => VkResult);
VkFnType!(vkCreateInstance_type, (pCreateInfo, pAllocator, pInstance), (*const VkInstanceCreateInfo, *const VkAllocationCallbacks, *mut VkInstance) => VkResult);
VkFnType!(vkCreatePipelineLayout_type, (device, pCreateInfo, pAllocator, pPipelineLayout), (VkDevice, *const types::VkPipelineLayoutCreateInfo, *const VkAllocationCallbacks, *mut types::VkPipelineLayout) => VkResult);
VkFnType!(vkCreateRenderPass_type, (device, pCreateInfo, pAllocator, pRenderPass), (VkDevice, *const types::VkRenderPassCreateInfo, *const VkAllocationCallbacks, *mut types::VkRenderPass) => VkResult);
VkFnType!(vkCreateSemaphore_type, (device, pCreateInfo, pAllocator, pSemaphore), (VkDevice, *const types::VkSemaphoreCreateInfo, *const VkAllocationCallbacks, *mut types::VkSemaphore) => VkResult);
VkFnType!(vkCreateShaderModule_type, (device, pCreateInfo, pAllocator, pShaderModule), (VkDevice, *const VkShaderModuleCreateInfo, *const VkAllocationCallbacks, *mut VkShaderModule) => VkResult);
VkFnType!(vkCreateSwapchainKHR_type, (device, pCreateInfo, pAllocator, pSwapchain), (VkDevice, *const VkSwapchainCreateInfoKHR, *const VkAllocationCallbacks, *mut VkSwapchainKHR) => VkResult);
VkFnType!(vkDestroyBuffer_type, (device, buffer, pAllocator), (VkDevice, types::VkBuffer, *const VkAllocationCallbacks) => ());
VkFnType!(vkDestroyCommandPool_type, (device, commandPool, pAllocator), (VkDevice, types::VkCommandPool, *const VkAllocationCallbacks) => ());
VkFnType!(vkDestroyDevice_type, (device, pAllocator), (VkDevice, *const VkAllocationCallbacks) => ());
VkFnType!(vkDestroyFence_type, (device, fence, pAllocator), (VkDevice, types::VkFence, *const VkAllocationCallbacks) => ());
VkFnType!(vkDestroyFramebuffer_type, (device, framebuffer, pAllocator), (VkDevice, types::VkFramebuffer, *const VkAllocationCallbacks) => ());
VkFnType!(vkDestroyImageView_type, (device, imageView, pAllocator), (VkDevice, VkImageView, *const VkAllocationCallbacks) => ());
VkFnType!(vkDestroyInstance_type, (instance, pAllocator), (VkInstance, *const VkAllocationCallbacks) => VkResult);
VkFnType!(vkDestroyPipeline_type, (device, pipeline, pAllocator), (VkDevice, types::VkPipeline, *const VkAllocationCallbacks) => ());
VkFnType!(vkDestroyPipelineLayout_type, (device, pipelineLayout, pAllocator), (VkDevice, types::VkPipelineLayout, *const VkAllocationCallbacks) => ());
VkFnType!(vkDestroyRenderPass_type, (device, renderPass, pAllocator), (VkDevice, types::VkRenderPass, *const VkAllocationCallbacks) => ());
VkFnType!(vkDestroySemaphore_type, (device, semaphore, pAllocator), (VkDevice, types::VkSemaphore, *const VkAllocationCallbacks) => ());
VkFnType!(vkDestroyShaderModule_type, (device, shaderModule, pAllocator), (VkDevice, VkShaderModule, *const VkAllocationCallbacks) => ());
VkFnType!(vkDestroySurfaceKHR_type, (instance, surface, pAllocator), (VkInstance, VkSurfaceKHR, *const VkAllocationCallbacks) => ());
VkFnType!(vkDestroySwapchainKHR_type, (device, swapchain, pAllocator), (VkDevice, VkSwapchainKHR, *const VkAllocationCallbacks) => VkResult);
VkFnType!(vkDeviceWaitIdle_type, (device), (VkDevice) => VkResult);
VkFnType!(vkEndCommandBuffer_type, (commandBuffer), (types::VkCommandBuffer) => VkResult);
VkFnType!(vkEnumerateDeviceExtensionProperties_type, (physicalDevice, pLayerName, pPropertyCount, pProperties), (VkPhysicalDevice, *const u8, *mut u32, *mut VkExtensionProperties) => VkResult);
VkFnType!(vkEnumerateInstanceExtensionProperties_type, (layer_name, property_count, properties), (*const u8, *const u32, *mut VkExtensionProperties) => VkResult);
VkFnType!(vkEnumerateInstanceLayerProperties_type, (pPropertyCount, pProperties), (*mut u32, *mut VkLayerProperties) => VkResult);
VkFnType!(vkEnumeratePhysicalDevices_type, (instance, physical_device_count, physical_devices), (VkInstance, *const u32, *mut VkPhysicalDevice) => VkResult);
VkFnType!(vkGetDeviceQueue_type, (device, queueFamilyIndex, queueIndex, pQueue), (VkDevice, u32, u32, *mut VkQueue) => ());
VkFnType!(vkGetInstanceProcAddr_type, (instance, name), (VkInstance, *const u8) => PFN_vkVoidFunction);
VkFnType!(vkGetPhysicalDeviceFeatures_type, (physicalDevice, pFeatures), (VkPhysicalDevice, *mut VkPhysicalDeviceFeatures) => ());
VkFnType!(vkGetPhysicalDeviceProperties_type, (physicalDevice, pProperties), (VkPhysicalDevice, *mut VkPhysicalDeviceProperties) => ());
VkFnType!(vkGetPhysicalDeviceQueueFamilyProperties_type, (physicalDevice, pQueueFamilyPropertyCount, pQueueFamilyProperties), (VkPhysicalDevice, *mut u32, *mut VkQueueFamilyProperties) => ());
VkFnType!(vkGetPhysicalDeviceSurfaceCapabilitiesKHR_type, (physicalDevice, surface, pSurfaceCapabilities), (VkPhysicalDevice, VkSurfaceKHR, *mut VkSurfaceCapabilitiesKHR) => VkResult);
VkFnType!(vkGetPhysicalDeviceSurfaceFormatsKHR_type, (physicalDevice, surface, pSurfaceFormatCount, pSurfaceFormats), (VkPhysicalDevice, VkSurfaceKHR, *mut u32, *mut VkSurfaceFormatKHR) => VkResult);
VkFnType!(vkGetPhysicalDeviceSurfacePresentModesKHR_type, (physicalDevice, surface, pPresentModeCount, pPresentModes), (VkPhysicalDevice, VkSurfaceKHR, *mut u32, *mut VkPresentModeKHR) => VkResult);
VkFnType!(vkGetPhysicalDeviceSurfaceSupportKHR_type, (physicalDevice, queueFamilyIndex, surface, pSupported), (VkPhysicalDevice, u32, VkSurfaceKHR, *mut VkBool32) => VkResult);
VkFnType!(vkGetSwapchainImagesKHR_type, (device, swapchain, pSwapchainImageCount, pSwapchainImages), (VkDevice, VkSwapchainKHR, *mut u32, *mut VkImage) => VkResult);
VkFnType!(vkQueuePresentKHR_type, (queue, pPresentInfo), (VkQueue, *const types::VkPresentInfoKHR) => VkResult);
VkFnType!(vkQueueSubmit_type, (queue, submitCount, pSubmits, fence), (VkQueue, u32, *const types::VkSubmitInfo, types::VkFence) => VkResult);
VkFnType!(vkQueueWaitIdle_type, (queue), (VkQueue) => VkResult);
VkFnType!(vkResetFences_type, (device, fenceCount, pFences), (VkDevice, u32, *const types::VkFence) => VkResult);
VkFnType!(vkWaitForFences_type, (device, fenceCount, pFences, waitAll, timeout), (VkDevice, u32, *const types::VkFence, VkBool32, u64) => VkResult);
VkFnType!(vkGetBufferMemoryRequirements_type, (device, buffer, pMemoryRequirements), (VkDevice, types::VkBuffer, *mut types::VkMemoryRequirements) => ());
VkFnType!(vkGetPhysicalDeviceMemoryProperties_type, (physicalDevice, pMemoryProperties), (types::VkPhysicalDevice, *mut types::VkPhysicalDeviceMemoryProperties) => ());
VkFnType!(vkAllocateMemory_type, (device, pAllocateInfo, pAllocator, pMemory), (VkDevice, *const types::VkMemoryAllocateInfo, *const VkAllocationCallbacks, *mut types::VkDeviceMemory) => VkResult);
VkFnType!(vkFreeMemory_type, (device, memory, pAllocator), (VkDevice, types::VkDeviceMemory, *const VkAllocationCallbacks) => ());
VkFnType!(vkBindBufferMemory_type, (device, buffer, memory, memoryOffset), (VkDevice, types::VkBuffer, types::VkDeviceMemory, types::VkDeviceSize) => VkResult);
VkFnType!(vkMapMemory_type, (device, memory, offset, size, flags, ppData), (VkDevice, types::VkDeviceMemory, types::VkDeviceSize, types::VkDeviceSize, types::VkMemoryMapFlags, *mut *mut raw::c_void) => VkResult);
VkFnType!(vkUnmapMemory_type, (device, memory), (VkDevice, types::VkDeviceMemory) => ());
VkFnType!(vkCmdBindVertexBuffers_type, (commandBuffer, firstBinding, bindingCount, pBuffers, pOffsets), (types::VkCommandBuffer, u32, u32, *const types::VkBuffer, *const types::VkDeviceSize) => ());
VkFnType!(vkCmdCopyBuffer_type, (commandBuffer, srcBuffer, dstBuffer, regionCount, pRegions), (types::VkCommandBuffer, types::VkBuffer, types::VkBuffer, u32, *const types::VkBufferCopy) => ());
VkFnType!(vkCmdDrawIndexed_type, (commandBuffer, indexCount, instanceCount, firstIndex, vertexOffset, firstInstance), (types::VkCommandBuffer, u32, u32, u32, i32, u32) => ());
VkFnType!(vkCmdBindIndexBuffer_type, (commandBuffer, buffer, offset, indexType), (types::VkCommandBuffer, types::VkBuffer, types::VkDeviceSize, types::VkIndexType) => ());
VkFnType!(vkCreateDescriptorSetLayout_type, (device, pCreateInfo, pAllocator, pSetLayout), (VkDevice, *const types::VkDescriptorSetLayoutCreateInfo, *const VkAllocationCallbacks, *mut types::VkDescriptorSetLayout) => VkResult);
VkFnType!(vkDestroyDescriptorSetLayout_type, (device, descriptorSetLayout, pAllocator), (VkDevice, types::VkDescriptorSetLayout, *const VkAllocationCallbacks) => ());
VkFnType!(vkCreateDescriptorPool_type, (device, pCreateInfo, pAllocator, pDescriptorPool), (VkDevice, *const types::VkDescriptorPoolCreateInfo, *const VkAllocationCallbacks, *mut types::VkDescriptorPool) => VkResult);
VkFnType!(vkDestroyDescriptorPool_type, (device, descriptorPool, pAllocator), (VkDevice, types::VkDescriptorPool, *const VkAllocationCallbacks) => ());
VkFnType!(vkAllocateDescriptorSets_type, (device, pAllocateInfo, pDescriptorSets), (VkDevice, *const types::VkDescriptorSetAllocateInfo, *mut types::VkDescriptorSet) => VkResult);
VkFnType!(vkUpdateDescriptorSets_type, (device, descriptorWriteCount, pDescriptorWrites, descriptorCopyCount, pDescriptorCopies), (VkDevice, u32, *const types::VkWriteDescriptorSet, u32, *const types::VkCopyDescriptorSet) => ());
VkFnType!(vkCmdBindDescriptorSets_type, (commandBuffer, pipelineBindPoint, layout, firstSet, descriptorSetCount, pDescriptorSets, dynamicOffsetCount, pDynamicOffsets), (types::VkCommandBuffer, types::VkPipelineBindPoint, types::VkPipelineLayout, u32, u32, *const types::VkDescriptorSet, u32, *const u32) => ());
VkFnType!(vkCreateImage_type, (device, pCreateInfo, pAllocator, pImage), (VkDevice, *const types::VkImageCreateInfo, *const VkAllocationCallbacks, *mut types::VkImage) => VkResult);
VkFnType!(vkGetImageMemoryRequirements_type, (device, image, pMemoryRequirements), (VkDevice, types::VkImage, *mut types::VkMemoryRequirements) => ());
VkFnType!(vkBindImageMemory_type, (device, image, memory, memoryOffset), (VkDevice, types::VkImage, types::VkDeviceMemory, types::VkDeviceSize) => VkResult);
VkFnType!(vkCmdPipelineBarrier_type, (commandBuffer, srcStageMask, dstStageMask, dependencyFlags, memoryBarrierCount, pMemoryBarriers, bufferMemoryBarrierCount, pBufferMemoryBarriers, imageMemoryBarrierCount, pImageMemoryBarriers), (types::VkCommandBuffer, types::VkPipelineStageFlags, types::VkPipelineStageFlags, types::VkDependencyFlags, u32, *const types::VkMemoryBarrier, u32, *const types::VkBufferMemoryBarrier, u32, *const types::VkImageMemoryBarrier) => ());
VkFnType!(vkDestroyImage_type, (device, image, pAllocator), (VkDevice, types::VkImage, *const VkAllocationCallbacks) => ());
VkFnType!(vkCmdCopyBufferToImage_type, (commandBuffer, srcBuffer, dstImage, dstImageLayout, regionCount, pRegions), (types::VkCommandBuffer, types::VkBuffer, types::VkImage, types::VkImageLayout, u32, *const types::VkBufferImageCopy) => ());
VkFnType!(vkCreateSampler_type, (device, pCreateInfo, pAllocator, pSampler), (VkDevice, *const types::VkSamplerCreateInfo, *const VkAllocationCallbacks, *mut types::VkSampler) => VkResult);
VkFnType!(vkDestroySampler_type, (device, sampler, pAllocator), (VkDevice, types::VkSampler, *const types::VkAllocationCallbacks) => ());
VkFnType!(vkGetPhysicalDeviceFormatProperties_type, (physicalDevice, format, pFormatProperties), (VkPhysicalDevice, types::VkFormat, *mut types::VkFormatProperties) => ());
VkFnType!(vkCreateComputePipelines_type, (device, pipelineCache, createInfoCount, pCreateInfos, pAllocator, pPipelines), (VkDevice, types::VkPipelineCache, u32, *const types::VkComputePipelineCreateInfo, *const VkAllocationCallbacks, *mut types::VkPipeline) => VkResult);
VkFnType!(vkCmdDispatch_type, (commandBuffer, groupCountX, groupCountY, groupCountZ), (types::VkCommandBuffer, u32, u32, u32) => ());
VkFnType!(vkCmdPushConstants_type, (commandBuffer, layout, stageFlags, offset, size, pValues), (types::VkCommandBuffer, types::VkPipelineLayout, types::VkShaderStageFlags, u32, u32, *const c_void) => ());
VkFnType!(vkGetFenceStatus_type, (device, fence), (VkDevice, types::VkFence) => VkResult);
VkFnType!(vkGetSemaphoreCounterValue_type, (device, semaphore, value), (VkDevice, types::VkSemaphore, *mut u64) => VkResult);
VkFnType!(vkFreeDescriptorSets_type, (device, descriptorPool, descriptorSetCount, pDescriptorSets), (VkDevice, types::VkDescriptorPool, u32, *const types::VkDescriptorSet) => VkResult);

// VK_KHR_win32_surface extension
VkFnType!(vkCreateWin32SurfaceKHR_type, (instance, pCreateInfo, pAllocator, pSurface), (VkInstance, *const win32::VkWin32SurfaceCreateInfoKHR, *const VkAllocationCallbacks, *mut VkSurfaceKHR) => VkResult);

// VK_EXT_debug_report extension
VkFnType!(vkCreateDebugReportCallbackEXT_type, (instance, pCreateInfo, pAllocator, pCallback), (VkInstance, *const VkDebugReportCallbackCreateInfoEXT, *const VkAllocationCallbacks, *mut VkDebugReportCallbackEXT) => VkResult);
VkFnType!(vkDestroyDebugReportCallbackEXT_type, (instance, callback, pAllocator), (VkInstance, VkDebugReportCallbackEXT, *const VkAllocationCallbacks) => ());

// VK_EXT_debug_utils extension
VkFnType!(vkCreateDebugUtilsMessengerEXT_type, (instance, pCreateInfo, pAllocator, pMessenger), (VkInstance, *const VkDebugUtilsMessengerCreateInfoEXT, *const VkAllocationCallbacks, *mut VkDebugUtilsMessengerEXT) => VkResult);
VkFnType!(vkDestroyDebugUtilsMessengerEXT_type, (instance, messenger, pAllocator), (VkInstance, VkDebugUtilsMessengerEXT, *const VkAllocationCallbacks) => ());
VkFnType!(vkSubmitDebugUtilsMessageEXT_type, (instance, messageSeverity, messageTypes, pCallbackData), (VkInstance, VkDebugUtilsMessageSeverityFlagBitsEXT, VkDebugUtilsMessageTypeFlagsEXT, *const VkDebugUtilsMessengerCallbackDataEXT) => ());

VkFn!(vkAcquireNextImageKHR, vkAcquireNextImageKHR_type, (device, swapchain, timeout, semaphore, fence, pImageIndex), (VkDevice, VkSwapchainKHR, u64, types::VkSemaphore, types::VkFence, *mut u32) => VkResult);
VkFn!(vkAllocateCommandBuffers, vkAllocateCommandBuffers_type, (device, pAllocateInfo, pCommandBuffers), (VkDevice, *const types::VkCommandBufferAllocateInfo, *mut types::VkCommandBuffer) => VkResult);
VkFn!(vkBeginCommandBuffer, vkBeginCommandBuffer_type, (commandBuffer, pBeginInfo), (types::VkCommandBuffer, *const types::VkCommandBufferBeginInfo) => VkResult);
VkFn!(vkCmdBeginRenderPass, vkCmdBeginRenderPass_type, (commandBuffer, pRenderPassBegin, contents), (types::VkCommandBuffer, *const types::VkRenderPassBeginInfo, types::VkSubpassContents) => ());
VkFn!(vkCmdBindPipeline, vkCmdBindPipeline_type, (commandBuffer, pipelineBindPoint, pipeline), (types::VkCommandBuffer, types::VkPipelineBindPoint, types::VkPipeline) => ());
VkFn!(vkCmdDraw, vkCmdDraw_type, (commandBuffer, vertexCount, instanceCount, firstVertex, firstInstance), (types::VkCommandBuffer, u32, u32, u32, u32) => ());
VkFn!(vkCmdEndRenderPass, vkCmdEndRenderPass_type, (commandBuffer), (types::VkCommandBuffer) => ());
VkFn!(vkCreateBuffer, vkCreateBuffer_type, (device, pCreateInfo, pAllocator, pBuffer), (VkDevice, *const types::VkBufferCreateInfo, *const VkAllocationCallbacks, *mut types::VkBuffer) => VkResult);
VkFn!(vkCreateBufferView, vkCreateBufferView_type, (device, pCreateInfo, pAllocator, pView), (VkDevice, *const types::VkBufferViewCreateInfo, *const VkAllocationCallbacks, *mut types::VkBufferView) => VkResult);
VkFn!(vkCreateCommandPool, vkCreateCommandPool_type, (device, pCreateInfo, pAllocator, pCommandPool), (VkDevice, *const types::VkCommandPoolCreateInfo, *const VkAllocationCallbacks, *mut types::VkCommandPool) => VkResult);
VkFn!(vkCreateDevice, vkCreateDevice_type, (physicalDevice, pCreateInfo, pAllocator, pDevice), (VkPhysicalDevice, *const VkDeviceCreateInfo, *const VkAllocationCallbacks, *mut VkDevice) => VkResult);
VkFn!(vkCreateFence, vkCreateFence_type, (device, pCreateInfo, pAllocator, pFence), (VkDevice, *const types::VkFenceCreateInfo, *const VkAllocationCallbacks, *mut types::VkFence) => VkResult);
VkFn!(vkCreateFramebuffer, vkCreateFramebuffer_type, (device, pCreateInfo, pAllocator, pFramebuffer), (VkDevice, *const types::VkFramebufferCreateInfo, *const VkAllocationCallbacks, *mut types::VkFramebuffer) => VkResult);
VkFn!(vkCreateGraphicsPipelines, vkCreateGraphicsPipelines_type, (device, pipelineCache, createInfoCount, pCreateInfos, pAllocator, pPipelines), (VkDevice, types::VkPipelineCache, u32, *const types::VkGraphicsPipelineCreateInfo, *const VkAllocationCallbacks, *mut types::VkPipeline) => VkResult);
VkFn!(vkCreateImageView, vkCreateImageView_type, (device, pCreateInfo, pAllocator, pView), (VkDevice, *const VkImageViewCreateInfo, *const VkAllocationCallbacks, *mut VkImageView) => VkResult);
VkFn!(vkCreateInstance, vkCreateInstance_type, (pCreateInfo, pAllocator, pInstance), (*const VkInstanceCreateInfo, *const VkAllocationCallbacks, *mut VkInstance) => VkResult);
VkFn!(vkCreatePipelineLayout, vkCreatePipelineLayout_type, (device, pCreateInfo, pAllocator, pPipelineLayout), (VkDevice, *const types::VkPipelineLayoutCreateInfo, *const VkAllocationCallbacks, *mut types::VkPipelineLayout) => VkResult);
VkFn!(vkCreateRenderPass, vkCreateRenderPass_type, (device, pCreateInfo, pAllocator, pRenderPass), (VkDevice, *const types::VkRenderPassCreateInfo, *const VkAllocationCallbacks, *mut types::VkRenderPass) => VkResult);
VkFn!(vkCreateSemaphore, vkCreateSemaphore_type, (device, pCreateInfo, pAllocator, pSemaphore), (VkDevice, *const types::VkSemaphoreCreateInfo, *const VkAllocationCallbacks, *mut types::VkSemaphore) => VkResult);
VkFn!(vkCreateShaderModule, vkCreateShaderModule_type, (device, pCreateInfo, pAllocator, pShaderModule), (VkDevice, *const VkShaderModuleCreateInfo, *const VkAllocationCallbacks, *mut VkShaderModule) => VkResult);
VkFn!(vkCreateSwapchainKHR, vkCreateSwapchainKHR_type, (device, pCreateInfo, pAllocator, pSwapchain), (VkDevice, *const VkSwapchainCreateInfoKHR, *const VkAllocationCallbacks, *mut VkSwapchainKHR) => VkResult);
VkFn!(vkDestroyBuffer, vkDestroyBuffer_type, (device, buffer, pAllocator), (VkDevice, types::VkBuffer, *const VkAllocationCallbacks) => ());
VkFn!(vkDestroyCommandPool, vkDestroyCommandPool_type, (device, commandPool, pAllocator), (VkDevice, types::VkCommandPool, *const VkAllocationCallbacks) => ());
VkFn!(vkDestroyDevice, vkDestroyDevice_type, (device, pAllocator), (VkDevice, *const VkAllocationCallbacks) => ());
VkFn!(vkDestroyFence, vkDestroyFence_type, (device, fence, pAllocator), (VkDevice, types::VkFence, *const VkAllocationCallbacks) => ());
VkFn!(vkDestroyFramebuffer, vkDestroyFramebuffer_type, (device, framebuffer, pAllocator), (VkDevice, types::VkFramebuffer, *const VkAllocationCallbacks) => ());
VkFn!(vkDestroyImageView, vkDestroyImageView_type, (device, imageView, pAllocator), (VkDevice, VkImageView, *const VkAllocationCallbacks) => ());
VkFn!(vkDestroyInstance, vkDestroyInstance_type, (instance, pAllocator), (VkInstance, *const VkAllocationCallbacks) => VkResult);
VkFn!(vkDestroyPipeline, vkDestroyPipeline_type, (device, pipeline, pAllocator), (VkDevice, types::VkPipeline, *const VkAllocationCallbacks) => ());
VkFn!(vkDestroyPipelineLayout, vkDestroyPipelineLayout_type, (device, pipelineLayout, pAllocator), (VkDevice, types::VkPipelineLayout, *const VkAllocationCallbacks) => ());
VkFn!(vkDestroyRenderPass, vkDestroyRenderPass_type, (device, renderPass, pAllocator), (VkDevice, types::VkRenderPass, *const VkAllocationCallbacks) => ());
VkFn!(vkDestroySemaphore, vkDestroySemaphore_type, (device, semaphore, pAllocator), (VkDevice, types::VkSemaphore, *const VkAllocationCallbacks) => ());
VkFn!(vkDestroyShaderModule, vkDestroyShaderModule_type, (device, shaderModule, pAllocator), (VkDevice, VkShaderModule, *const VkAllocationCallbacks) => ());
VkFn!(vkDestroySurfaceKHR, vkDestroySurfaceKHR_type, (instance, surface, pAllocator), (VkInstance, VkSurfaceKHR, *const VkAllocationCallbacks) => ());
VkFn!(vkDestroySwapchainKHR, vkDestroySwapchainKHR_type, (device, swapchain, pAllocator), (VkDevice, VkSwapchainKHR, *const VkAllocationCallbacks) => VkResult);
VkFn!(vkDeviceWaitIdle, vkDeviceWaitIdle_type, (device), (VkDevice) => VkResult);
VkFn!(vkEndCommandBuffer, vkEndCommandBuffer_type, (commandBuffer), (types::VkCommandBuffer) => VkResult);
VkFn!(vkEnumerateDeviceExtensionProperties, vkEnumerateDeviceExtensionProperties_type, (physicalDevice, pLayerName, pPropertyCount, pProperties), (VkPhysicalDevice, *const u8, *mut u32, *mut VkExtensionProperties) => VkResult);
VkFn!(vkEnumerateInstanceExtensionProperties, vkEnumerateInstanceExtensionProperties_type, (layer_name, property_count, properties), (*const u8, *const u32, *mut VkExtensionProperties) => VkResult);
VkFn!(vkEnumerateInstanceLayerProperties, vkEnumerateInstanceLayerProperties_type, (pPropertyCount, pProperties), (*mut u32, *mut VkLayerProperties) => VkResult);
VkFn!(vkEnumeratePhysicalDevices, vkEnumeratePhysicalDevices_type, (instance, physical_device_count, physical_devices), (VkInstance, *const u32, *mut VkPhysicalDevice) => VkResult);
VkFn!(vkGetDeviceQueue, vkGetDeviceQueue_type, (device, queueFamilyIndex, queueIndex, pQueue), (VkDevice, u32, u32, *mut VkQueue) => ());
VkFn!(vkGetInstanceProcAddr, vkGetInstanceProcAddr_type, (instance, name), (VkInstance, *const u8) => PFN_vkVoidFunction);
VkFn!(vkGetPhysicalDeviceFeatures, vkGetPhysicalDeviceFeatures_type, (physicalDevice, pFeatures), (VkPhysicalDevice, *mut VkPhysicalDeviceFeatures) => ());
VkFn!(vkGetPhysicalDeviceProperties, vkGetPhysicalDeviceProperties_type, (physicalDevice, pProperties), (VkPhysicalDevice, *mut VkPhysicalDeviceProperties) => ());
VkFn!(vkGetPhysicalDeviceQueueFamilyProperties, vkGetPhysicalDeviceQueueFamilyProperties_type, (physicalDevice, pQueueFamilyPropertyCount, pQueueFamilyProperties), (VkPhysicalDevice, *mut u32, *mut VkQueueFamilyProperties) => ());
VkFn!(vkGetPhysicalDeviceSurfaceCapabilitiesKHR, vkGetPhysicalDeviceSurfaceCapabilitiesKHR_type, (physicalDevice, surface, pSurfaceCapabilities), (VkPhysicalDevice, VkSurfaceKHR, *mut VkSurfaceCapabilitiesKHR) => VkResult);
VkFn!(vkGetPhysicalDeviceSurfaceFormatsKHR, vkGetPhysicalDeviceSurfaceFormatsKHR_type, (physicalDevice, surface, pSurfaceFormatCount, pSurfaceFormats), (VkPhysicalDevice, VkSurfaceKHR, *mut u32, *mut VkSurfaceFormatKHR) => VkResult);
VkFn!(vkGetPhysicalDeviceSurfacePresentModesKHR, vkGetPhysicalDeviceSurfacePresentModesKHR_type, (physicalDevice, surface, pPresentModeCount, pPresentModes), (VkPhysicalDevice, VkSurfaceKHR, *mut u32, *mut VkPresentModeKHR) => VkResult);
VkFn!(vkGetPhysicalDeviceSurfaceSupportKHR, vkGetPhysicalDeviceSurfaceSupportKHR_type, (physicalDevice, queueFamilyIndex, surface, pSupported), (VkPhysicalDevice, u32, VkSurfaceKHR, *mut VkBool32) => VkResult);
VkFn!(vkGetSwapchainImagesKHR, vkGetSwapchainImagesKHR_type, (device, swapchain, pSwapchainImageCount, pSwapchainImages), (VkDevice, VkSwapchainKHR, *mut u32, *mut VkImage) => VkResult);
VkFn!(vkQueuePresentKHR, vkQueuePresentKHR_type, (queue, pPresentInfo), (VkQueue, *const types::VkPresentInfoKHR) => VkResult);
VkFn!(vkQueueSubmit, vkQueueSubmit_type, (queue, submitCount, pSubmits, fence), (VkQueue, u32, *const types::VkSubmitInfo, types::VkFence) => VkResult);
VkFn!(vkQueueWaitIdle, vkQueueWaitIdle_type, (queue), (VkQueue) => VkResult);
VkFn!(vkResetFences, vkResetFences_type, (device, fenceCount, pFences), (VkDevice, u32, *const types::VkFence) => VkResult);
VkFn!(vkWaitForFences, vkWaitForFences_type, (device, fenceCount, pFences, waitAll, timeout), (VkDevice, u32, *const types::VkFence, VkBool32, u64) => VkResult);
VkFn!(vkGetBufferMemoryRequirements, vkGetBufferMemoryRequirements_type, (device, buffer, pMemoryRequirements), (VkDevice, types::VkBuffer, *mut types::VkMemoryRequirements) => ());
VkFn!(vkGetPhysicalDeviceMemoryProperties, vkGetPhysicalDeviceMemoryProperties_type, (physicalDevice, pMemoryProperties), (types::VkPhysicalDevice, *mut types::VkPhysicalDeviceMemoryProperties) => ());
VkFn!(vkAllocateMemory, vkAllocateMemory_type, (device, pAllocateInfo, pAllocator, pMemory), (VkDevice, *const types::VkMemoryAllocateInfo, *const VkAllocationCallbacks, *mut types::VkDeviceMemory) => VkResult);
VkFn!(vkFreeMemory, vkFreeMemory_type, (device, memory, pAllocator), (VkDevice, types::VkDeviceMemory, *const VkAllocationCallbacks) => ());
VkFn!(vkBindBufferMemory, vkBindBufferMemory_type, (device, buffer, memory, memoryOffset), (VkDevice, types::VkBuffer, types::VkDeviceMemory, types::VkDeviceSize) => VkResult);
VkFn!(vkMapMemory, vkMapMemory_type, (device, memory, offset, size, flags, ppData), (VkDevice, types::VkDeviceMemory, types::VkDeviceSize, types::VkDeviceSize, types::VkMemoryMapFlags, *mut *mut raw::c_void) => VkResult);
VkFn!(vkUnmapMemory, vkUnmapMemory_type, (device, memory), (VkDevice, types::VkDeviceMemory) => ());
VkFn!(vkCmdBindVertexBuffers, vkCmdBindVertexBuffers_type, (commandBuffer, firstBinding, bindingCount, pBuffers, pOffsets), (types::VkCommandBuffer, u32, u32, *const types::VkBuffer, *const types::VkDeviceSize) => ());
VkFn!(vkCmdCopyBuffer, vkCmdCopyBuffer_type, (commandBuffer, srcBuffer, dstBuffer, regionCount, pRegions), (types::VkCommandBuffer, types::VkBuffer, types::VkBuffer, u32, *const types::VkBufferCopy) => ());
VkFn!(vkCmdDrawIndexed, vkCmdDrawIndexed_type, (commandBuffer, indexCount, instanceCount, firstIndex, vertexOffset, firstInstance), (types::VkCommandBuffer, u32, u32, u32, i32, u32) => ());
VkFn!(vkCmdBindIndexBuffer, vkCmdBindIndexBuffer_type, (commandBuffer, buffer, offset, indexType), (types::VkCommandBuffer, types::VkBuffer, types::VkDeviceSize, types::VkIndexType) => ());
VkFn!(vkCreateDescriptorSetLayout, vkCreateDescriptorSetLayout_type, (device, pCreateInfo, pAllocator, pSetLayout), (VkDevice, *const types::VkDescriptorSetLayoutCreateInfo, *const VkAllocationCallbacks, *mut types::VkDescriptorSetLayout) => VkResult);
VkFn!(vkDestroyDescriptorSetLayout, vkDestroyDescriptorSetLayout_type, (device, descriptorSetLayout, pAllocator), (VkDevice, types::VkDescriptorSetLayout, *const VkAllocationCallbacks) => ());
VkFn!(vkCreateDescriptorPool, vkCreateDescriptorPool_type, (device, pCreateInfo, pAllocator, pDescriptorPool), (VkDevice, *const types::VkDescriptorPoolCreateInfo, *const VkAllocationCallbacks, *mut types::VkDescriptorPool) => VkResult);
VkFn!(vkDestroyDescriptorPool, vkDestroyDescriptorPool_type, (device, descriptorPool, pAllocator), (VkDevice, types::VkDescriptorPool, *const VkAllocationCallbacks) => ());
VkFn!(vkAllocateDescriptorSets, vkAllocateDescriptorSets_type, (device, pAllocateInfo, pDescriptorSets), (VkDevice, *const types::VkDescriptorSetAllocateInfo, *mut types::VkDescriptorSet) => VkResult);
VkFn!(vkUpdateDescriptorSets, vkUpdateDescriptorSets_type, (device, descriptorWriteCount, pDescriptorWrites, descriptorCopyCount, pDescriptorCopies), (VkDevice, u32, *const types::VkWriteDescriptorSet, u32, *const types::VkCopyDescriptorSet) => ());
VkFn!(vkCmdBindDescriptorSets, vkCmdBindDescriptorSets_type, (commandBuffer, pipelineBindPoint, layout, firstSet, descriptorSetCount, pDescriptorSets, dynamicOffsetCount, pDynamicOffsets), (types::VkCommandBuffer, types::VkPipelineBindPoint, types::VkPipelineLayout, u32, u32, *const types::VkDescriptorSet, u32, *const u32) => ());
VkFn!(vkCreateImage, vkCreateImage_type, (device, pCreateInfo, pAllocator, pImage), (VkDevice, *const types::VkImageCreateInfo, *const VkAllocationCallbacks, *mut types::VkImage) => VkResult);
VkFn!(vkGetImageMemoryRequirements, vkGetImageMemoryRequirements_type, (device, image, pMemoryRequirements), (VkDevice, types::VkImage, *mut types::VkMemoryRequirements) => ());
VkFn!(vkBindImageMemory, vkBindImageMemory_type, (device, image, memory, memoryOffset), (VkDevice, types::VkImage, types::VkDeviceMemory, types::VkDeviceSize) => VkResult);
VkFn!(vkCmdPipelineBarrier, vkCmdPipelineBarrier_type, (commandBuffer, srcStageMask, dstStageMask, dependencyFlags, memoryBarrierCount, pMemoryBarriers, bufferMemoryBarrierCount, pBufferMemoryBarriers, imageMemoryBarrierCount, pImageMemoryBarriers), (types::VkCommandBuffer, types::VkPipelineStageFlags, types::VkPipelineStageFlags, types::VkDependencyFlags, u32, *const types::VkMemoryBarrier, u32, *const types::VkBufferMemoryBarrier, u32, *const types::VkImageMemoryBarrier) => ());
VkFn!(vkDestroyImage, vkDestroyImage_type, (device, image, pAllocator), (VkDevice, types::VkImage, *const VkAllocationCallbacks) => ());
VkFn!(vkCmdCopyBufferToImage, vkCmdCopyBufferToImage_type, (commandBuffer, srcBuffer, dstImage, dstImageLayout, regionCount, pRegions), (types::VkCommandBuffer, types::VkBuffer, types::VkImage, types::VkImageLayout, u32, *const types::VkBufferImageCopy) => ());
VkFn!(vkCreateSampler, vkCreateSampler_type, (device, pCreateInfo, pAllocator, pSampler), (VkDevice, *const types::VkSamplerCreateInfo, *const VkAllocationCallbacks, *mut types::VkSampler) => VkResult);
VkFn!(vkDestroySampler, vkDestroySampler_type, (device, sampler, pAllocator), (VkDevice, types::VkSampler, *const types::VkAllocationCallbacks) => ());
VkFn!(vkGetPhysicalDeviceFormatProperties, vkGetPhysicalDeviceFormatProperties_type, (physicalDevice, format, pFormatProperties), (VkPhysicalDevice, types::VkFormat, *mut types::VkFormatProperties) => ());
VkFn!(vkCreateComputePipelines, vkCreateComputePipelines_type, (device, pipelineCache, createInfoCount, pCreateInfos, pAllocator, pPipelines), (VkDevice, types::VkPipelineCache, u32, *const types::VkComputePipelineCreateInfo, *const VkAllocationCallbacks, *mut types::VkPipeline) => VkResult);
VkFn!(vkCmdDispatch, vkCmdDispatch_type, (commandBuffer, groupCountX, groupCountY, groupCountZ), (types::VkCommandBuffer, u32, u32, u32) => ());
VkFn!(vkCmdPushConstants, vkCmdPushConstants_type, (commandBuffer, layout, stageFlags, offset, size, pValues), (types::VkCommandBuffer, types::VkPipelineLayout, types::VkShaderStageFlags, u32, u32, *const c_void) => ());
VkFn!(vkGetFenceStatus, vkGetFenceStatus_type, (device, fence), (VkDevice, types::VkFence) => VkResult);
VkFn!(vkGetSemaphoreCounterValue, vkGetSemaphoreCounterValue_type, (device, semaphore, value), (VkDevice, types::VkSemaphore, *mut u64) => VkResult);
VkFn!(vkFreeDescriptorSets, vkFreeDescriptorSets_type, (device, descriptorPool, descriptorSetCount, pDescriptorSets), (VkDevice, types::VkDescriptorPool, u32, *const types::VkDescriptorSet) => VkResult);

// VK_KHR_win32_surface extension
VkFn!(vkCreateWin32SurfaceKHR, vkCreateWin32SurfaceKHR_type, (instance, pCreateInfo, pAllocator, pSurface), (VkInstance, *const win32::VkWin32SurfaceCreateInfoKHR, *const VkAllocationCallbacks, *mut VkSurfaceKHR) => VkResult);

// VK_EXT_debug_report extension
VkFn!(vkCreateDebugReportCallbackEXT, vkCreateDebugReportCallbackEXT_type, (instance, pCreateInfo, pAllocator, pCallback), (VkInstance, *const VkDebugReportCallbackCreateInfoEXT, *const VkAllocationCallbacks, *mut VkDebugReportCallbackEXT) => VkResult);
VkFn!(vkDestroyDebugReportCallbackEXT, vkDestroyDebugReportCallbackEXT_type, (instance, callback, pAllocator), (VkInstance, VkDebugReportCallbackEXT, *const VkAllocationCallbacks) => ());

// VK_EXT_debug_utils extension
VkFn!(vkCreateDebugUtilsMessengerEXT, vkCreateDebugUtilsMessengerEXT_type, (instance, pCreateInfo, pAllocator, pMessenger), (VkInstance, *const VkDebugUtilsMessengerCreateInfoEXT, *const VkAllocationCallbacks, *mut VkDebugUtilsMessengerEXT) => VkResult);
VkFn!(vkDestroyDebugUtilsMessengerEXT, vkDestroyDebugUtilsMessengerEXT_type, (instance, messenger, pAllocator), (VkInstance, VkDebugUtilsMessengerEXT, *const VkAllocationCallbacks) => ());
VkFn!(vkSubmitDebugUtilsMessageEXT, vkSubmitDebugUtilsMessageEXT_type, (instance, messageSeverity, messageTypes, pCallbackData), (VkInstance, VkDebugUtilsMessageSeverityFlagBitsEXT, VkDebugUtilsMessageTypeFlagsEXT, *const VkDebugUtilsMessengerCallbackDataEXT) => ());

pub fn to_raw_string(s: &str) -> *const u8
{
    CString::new(s).unwrap().into_raw() as *const u8
}

fn load_proc(instance: VkInstance, name: &str) -> Option<PFN_vkVoidFunction>
{
    let raw_name = to_raw_string(name);

    let proc_addr = unsafe { vkGetInstanceProcAddr(instance, raw_name) };

    if proc_addr == unsafe { mem::transmute::<*const u32, PFN_vkVoidFunction>(std::ptr::null()) }
    {
        println!("Failed to load vk function: {}", name);
        None
    }
    else
    {
        Some(proc_addr)
    }
}

pub fn get_library() -> wrappertypes::VulkanLibContext
{
    wrappertypes::VulkanLibContext::new(unsafe { Library::new("vulkan-1.dll") }.unwrap())
}
