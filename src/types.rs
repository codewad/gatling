#![allow(non_upper_case_globals, unused_parens)]
use std::os::raw;

macro_rules! VK_HANDLE
{
    ($name:ident, $raw:ident) => {
        #[allow(non_upper_case_globals, unused_variables, dead_code)]
        pub enum $raw {}
        pub type $name = *const $raw;
    }
}

pub use win32::*;

VK_HANDLE!(VkBuffer, VkBuffer__);
VK_HANDLE!(VkBufferView, VkBufferView__);
VK_HANDLE!(VkCommandBuffer, VkCommandBuffer__);
VK_HANDLE!(VkCommandPool, VkCommandPool__);
VK_HANDLE!(VkDebugReportCallbackEXT, VkDebugReportCallbackEXT__);
VK_HANDLE!(VkDebugUtilsMessengerEXT, VkDebugUtilsMessengerEXT__);
VK_HANDLE!(VkDescriptorSetLayout, VkDescriptorSetLayout__);
VK_HANDLE!(VkDevice, VkDevice__);
VK_HANDLE!(VkFence, VkFence__);
VK_HANDLE!(VkFramebuffer, VkFramebuffer__);
VK_HANDLE!(VkImage, VkImage__);
VK_HANDLE!(VkImageView, VkImageView__);
VK_HANDLE!(VkInstance, VkInstance__);
VK_HANDLE!(VkPhysicalDevice, VkPhysicalDevice__);
VK_HANDLE!(VkPipeline, VkPipeline__);
VK_HANDLE!(VkPipelineCache, VkPipelineCache__);
VK_HANDLE!(VkPipelineLayout, VkPipelineLayout__);
VK_HANDLE!(VkQueue, VkQueue__);
VK_HANDLE!(VkRenderPass, VkRenderPass__);
VK_HANDLE!(VkSemaphore, VkSemaphore__);
VK_HANDLE!(VkShaderModule, VkShaderModule__);
VK_HANDLE!(VkSurfaceKHR, VkSurfaceKHR__);
VK_HANDLE!(VkSwapchainKHR, VkSwapchainKHR__);
VK_HANDLE!(VkDeviceMemory, VkDeviceMemory__);
VK_HANDLE!(VkSampler, VkSampler__);
VK_HANDLE!(VkDescriptorPool, VkDescriptorPool__);
VK_HANDLE!(VkDescriptorSet, VkDescriptorSet__);

pub type VkMemoryMapFlags = VkFlags;
pub type VkAccessFlags = VkFlags;
pub type VkAttachmentDescriptionFlags = VkFlags;
pub type VkBool32 = u32;
pub type VkBufferCreateFlags = VkFlags;
pub type VkBufferUsageFlags = VkFlags;
pub type VkBufferViewCreateFlags = VkFlags;
pub type VkColorComponentFlags = VkFlags;
pub type VkCommandBufferResetFlags = VkFlags;
pub type VkCommandBufferUsageFlags = VkFlags;
pub type VkCommandPoolCreateFlags = VkFlags;
pub type VkCompositeAlphaFlagsKHR = VkFlags;
pub type VkCullModeFlags = VkFlags;
pub type VkDebugReportFlagsEXT = VkFlags;
pub type VkDebugUtilsMessageSeverityFlagsEXT = VkFlags;
pub type VkDebugUtilsMessageTypeFlagsEXT = VkFlags;
pub type VkDebugUtilsMessengerCallbackDataFlagsEXT = VkFlags;
pub type VkDebugUtilsMessengerCreateFlagsEXT = VkFlags;
pub type VkDependencyFlags = VkFlags;
pub type VkDeviceCreateFlags = VkFlags;
pub type VkDeviceQueueCreateFlags = VkFlags;
pub type VkDeviceSize = u64;
pub type VkFenceCreateFlags = VkFlags;
pub type VkFlags = u32;
pub type VkFramebufferCreateFlags = VkFlags;
pub type VkImageAspectFlags = VkFlags;
pub type VkImageUsageFlags = VkFlags;
pub type VkImageViewCreateFlags = VkFlags;
pub type VkInstanceCreateFlags = VkFlags;
pub type VkPipelineCacheCreateFlags = VkFlags;
pub type VkPipelineColorBlendStateCreateFlags = VkFlags;
pub type VkPipelineCreateFlags = VkFlags;
pub type VkPipelineDepthStencilStateCreateFlags = VkFlags;
pub type VkPipelineDynamicStateCreateFlags = VkFlags;
pub type VkPipelineInputAssemblyStateCreateFlags = VkFlags;
pub type VkPipelineLayoutCreateFlags = VkFlags;
pub type VkPipelineMultisampleStateCreateFlags = VkFlags;
pub type VkPipelineRasterizationStateCreateFlags = VkFlags;
pub type VkPipelineShaderStageCreateFlags = VkFlags;
pub type VkPipelineStageFlags = VkFlags;
pub type VkPipelineTessellationStateCreateFlags = VkFlags;
pub type VkPipelineVertexInputStateCreateFlags = VkFlags;
pub type VkPipelineViewportStateCreateFlags = VkFlags;
pub type VkQueryControlFlags = VkFlags;
pub type VkQueryPipelineStatisticFlags = VkFlags;
pub type VkQueueFlags = VkFlags;
pub type VkRenderPassCreateFlags = VkFlags;
pub type VkSampleCountFlags = VkFlags;
pub type VkSampleMask = u32;
pub type VkSemaphoreCreateFlags = VkFlags;
pub type VkShaderModuleCreateFlags = VkFlags;
pub type VkShaderStageFlags = VkFlags;
pub type VkSubpassDescriptionFlags = VkFlags;
pub type VkSurfaceTransformFlagsKHR = VkFlags;
pub type VkSwapchainCreateFlagsKHR = VkFlags;
pub type VkMemoryPropertyFlags = VkFlags;
pub type VkMemoryHeapFlags = VkFlags;
pub type VkDescriptorSetLayoutCreateFlags = VkFlags;
pub type VkDescriptorPoolCreateFlags = VkFlags;
pub type VkImageCreateFlags = VkFlags;
pub type VkSamplerCreateFlags = VkFlags;
pub type VkFormatFeatureFlags = VkFlags;

pub type VkFormatFeatureFlagBits = raw::c_uint;
pub type VkBorderColor = raw::c_uint;
pub type VkSamplerMipmapMode = raw::c_uint;
pub type VkSamplerCreateFlagBits = raw::c_uint;
pub type VkImageType = raw::c_uint;
pub type VkImageCreateFlagBits = raw::c_uint;
pub type VkDescriptorPoolCreateFlagBits = raw::c_uint;
pub type VkDescriptorType = raw::c_uint;
pub type VkDescriptorSetLayoutCreateFlagBits = raw::c_uint;
pub type VkIndexType = raw::c_uint;
pub type VkMemoryHeapFlagBits = raw::c_uint;
pub type VkMemoryPropertyFlagBits = raw::c_uint;
pub type VkBufferUsageFlagBits = raw::c_uint;
pub type VkAccessFlagBits = raw::c_uint;
pub type VkAttachmentDescriptionFlagBits = raw::c_int;
pub type VkAttachmentLoadOp = raw::c_int;
pub type VkAttachmentStoreOp = raw::c_int;
pub type VkBlendFactor = raw::c_int;
pub type VkBlendOp = raw::c_int;
pub type VkColorComponentFlagBits = raw::c_uint;
pub type VkColorSpaceKHR = raw::c_int;
pub type VkCommandBufferLevel = raw::c_int;
pub type VkCommandBufferResetFlagBits = raw::c_int;
pub type VkCommandBufferUsageFlagBits = raw::c_uint;
pub type VkCommandPoolCreateFlagBits = VkFlags;
pub type VkCompareOp = raw::c_int;
pub type VkComponentSwizzle = raw::c_uint;
pub type VkCompositeAlphaFlagBitsKHR = raw::c_int;
pub type VkCullModeFlagBits = raw::c_uint;
pub type VkDebugReportFlagBitsEXT = raw::c_int;
pub type VkDebugReportObjectTypeEXT = raw::c_int;
pub type VkDebugUtilsMessageSeverityFlagBitsEXT = raw::c_uint;
pub type VkDebugUtilsMessageTypeFlagBitsEXT = raw::c_uint;
pub type VkDependencyFlagBits = raw::c_int;
pub type VkDynamicState = raw::c_int;
pub type VkFenceCreateFlagBits = raw::c_uint;
pub type VkFormat = raw::c_int;
pub type VkFramebufferCreateFlagBits = raw::c_int;
pub type VkFrontFace = raw::c_int;
pub type VkImageAspectFlagBits = raw::c_uint;
pub type VkImageLayout = raw::c_uint;
pub type VkImageUsageFlagBits = raw::c_uint;
pub type VkImageViewType = raw::c_uint;
pub type VkLogicOp = raw::c_int;
pub type VkObjectType = raw::c_int;
pub type VkPhysicalDeviceType = raw::c_int;
pub type VkPipelineBindPoint = raw::c_int;
pub type VkPipelineCreateFlagBits = raw::c_int;
pub type VkPipelineShaderStageCreateFlagBits = raw::c_uint;
pub type VkPipelineStageFlagBits = raw::c_uint;
pub type VkPolygonMode = raw::c_int;
pub type VkPresentModeKHR = raw::c_int;
pub type VkPrimitiveTopology = raw::c_int;
pub type VkQueryControlFlagBits = raw::c_int;
pub type VkQueryPipelineStatisticFlagBits = raw::c_int;
pub type VkQueueFlagBits = raw::c_uint;
pub type VkRenderPassCreateFlagBits = raw::c_int;
pub type VkResult = raw::c_int;
pub type VkSampleCountFlagBits = raw::c_uint;
pub type VkShaderStageFlagBits = raw::c_uint;
pub type VkSharingMode = raw::c_int;
pub type VkStencilOp = raw::c_int;
pub type VkStructureType = raw::c_int;
pub type VkSubpassContents = raw::c_int;
pub type VkSubpassDescriptionFlagBits = raw::c_int;
pub type VkSurfaceTransformFlagBitsKHR = raw::c_int;
pub type VkVertexInputRate = raw::c_int;
pub type VkImageTiling = raw::c_uint;
pub type VkFilter = raw::c_uint;
pub type VkSamplerAddressMode = raw::c_uint;

pub const VK_MAX_DESCRIPTION_SIZE : usize = 256;
pub const VK_MAX_EXTENSION_NAME_SIZE : usize = 256;
pub const VK_MAX_PHYSICAL_DEVICE_NAME_SIZE : usize = 256;
pub const VK_UUID_SIZE : usize = 16;
pub const VK_MAX_MEMORY_TYPES : usize = 32;
pub const VK_MAX_MEMORY_HEAPS : usize = 16;
pub const VK_QUEUE_FAMILY_IGNORED : raw::c_uint = 0;

pub const VK_TRUE : raw::c_uint = 1;
pub const VK_FALSE : raw::c_uint = 0;

pub const VK_NULL_HANDLE : raw::c_uint = 0;
pub const VK_SUBPASS_EXTERNAL : raw::c_uint = !0;

pub const VK_ACCESS_INDIRECT_COMMAND_READ_BIT : VkAccessFlagBits = 0x00000001;
pub const VK_ACCESS_INDEX_READ_BIT : VkAccessFlagBits = 0x00000002;
pub const VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT : VkAccessFlagBits = 0x00000004;
pub const VK_ACCESS_UNIFORM_READ_BIT : VkAccessFlagBits = 0x00000008;
pub const VK_ACCESS_INPUT_ATTACHMENT_READ_BIT : VkAccessFlagBits = 0x00000010;
pub const VK_ACCESS_SHADER_READ_BIT : VkAccessFlagBits = 0x00000020;
pub const VK_ACCESS_SHADER_WRITE_BIT : VkAccessFlagBits = 0x00000040;
pub const VK_ACCESS_COLOR_ATTACHMENT_READ_BIT : VkAccessFlagBits = 0x00000080;
pub const VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT : VkAccessFlagBits = 0x00000100;
pub const VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT : VkAccessFlagBits = 0x00000200;
pub const VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT : VkAccessFlagBits = 0x00000400;
pub const VK_ACCESS_TRANSFER_READ_BIT : VkAccessFlagBits = 0x00000800;
pub const VK_ACCESS_TRANSFER_WRITE_BIT : VkAccessFlagBits = 0x00001000;
pub const VK_ACCESS_HOST_READ_BIT : VkAccessFlagBits = 0x00002000;
pub const VK_ACCESS_HOST_WRITE_BIT : VkAccessFlagBits = 0x00004000;
pub const VK_ACCESS_MEMORY_READ_BIT : VkAccessFlagBits = 0x00008000;
pub const VK_ACCESS_MEMORY_WRITE_BIT : VkAccessFlagBits = 0x00010000;
pub const VK_ACCESS_TRANSFORM_FEEDBACK_WRITE_BIT_EXT : VkAccessFlagBits = 0x02000000;
pub const VK_ACCESS_TRANSFORM_FEEDBACK_COUNTER_READ_BIT_EXT : VkAccessFlagBits = 0x04000000;
pub const VK_ACCESS_TRANSFORM_FEEDBACK_COUNTER_WRITE_BIT_EXT : VkAccessFlagBits = 0x08000000;
pub const VK_ACCESS_CONDITIONAL_RENDERING_READ_BIT_EXT : VkAccessFlagBits = 0x00100000;
pub const VK_ACCESS_COMMAND_PROCESS_READ_BIT_NVX : VkAccessFlagBits = 0x00020000;
pub const VK_ACCESS_COMMAND_PROCESS_WRITE_BIT_NVX : VkAccessFlagBits = 0x00040000;
pub const VK_ACCESS_COLOR_ATTACHMENT_READ_NONCOHERENT_BIT_EXT : VkAccessFlagBits = 0x00080000;
pub const VK_ACCESS_SHADING_RATE_IMAGE_READ_BIT_NV : VkAccessFlagBits = 0x00800000;
pub const VK_ACCESS_ACCELERATION_STRUCTURE_READ_BIT_NV : VkAccessFlagBits = 0x00200000;
pub const VK_ACCESS_ACCELERATION_STRUCTURE_WRITE_BIT_NV : VkAccessFlagBits = 0x00400000;
pub const VK_ACCESS_FRAGMENT_DENSITY_MAP_READ_BIT_EXT : VkAccessFlagBits = 0x01000000;
pub const VK_ACCESS_FLAG_BITS_MAX_ENUM : VkAccessFlagBits = 0x7FFFFFFF;

pub const VK_BUFFER_USAGE_TRANSFER_SRC_BIT : VkBufferUsageFlagBits = 0x00000001;
pub const VK_BUFFER_USAGE_TRANSFER_DST_BIT : VkBufferUsageFlagBits = 0x00000002;
pub const VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT : VkBufferUsageFlagBits = 0x00000004;
pub const VK_BUFFER_USAGE_STORAGE_TEXEL_BUFFER_BIT : VkBufferUsageFlagBits = 0x00000008;
pub const VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT : VkBufferUsageFlagBits = 0x00000010;
pub const VK_BUFFER_USAGE_STORAGE_BUFFER_BIT : VkBufferUsageFlagBits = 0x00000020;
pub const VK_BUFFER_USAGE_INDEX_BUFFER_BIT : VkBufferUsageFlagBits = 0x00000040;
pub const VK_BUFFER_USAGE_VERTEX_BUFFER_BIT : VkBufferUsageFlagBits = 0x00000080;
pub const VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT : VkBufferUsageFlagBits = 0x00000100;
pub const VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT : VkBufferUsageFlagBits = 0x00020000;
pub const VK_BUFFER_USAGE_TRANSFORM_FEEDBACK_BUFFER_BIT_EXT : VkBufferUsageFlagBits = 0x00000800;
pub const VK_BUFFER_USAGE_TRANSFORM_FEEDBACK_COUNTER_BUFFER_BIT_EXT : VkBufferUsageFlagBits = 0x00001000;
pub const VK_BUFFER_USAGE_CONDITIONAL_RENDERING_BIT_EXT : VkBufferUsageFlagBits = 0x00000200;
pub const VK_BUFFER_USAGE_RAY_TRACING_BIT_NV : VkBufferUsageFlagBits = 0x00000400;
pub const VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT_EXT : VkBufferUsageFlagBits = VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;
pub const VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT_KHR : VkBufferUsageFlagBits = VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;
pub const VK_BUFFER_USAGE_FLAG_BITS_MAX_ENUM : VkBufferUsageFlagBits = 0x7FFFFFFF;

pub const VK_FRAMEBUFFER_CREATE_IMAGELESS_BIT : VkFramebufferCreateFlagBits = 0x00000001;
pub const VK_FRAMEBUFFER_CREATE_IMAGELESS_BIT_KHR : VkFramebufferCreateFlagBits = VK_FRAMEBUFFER_CREATE_IMAGELESS_BIT;
pub const VK_FRAMEBUFFER_CREATE_FLAG_BITS_MAX_ENUM : VkFramebufferCreateFlagBits = 0x7FFFFFFF;

pub const VK_FILTER_NEAREST : VkFilter = 0;
pub const VK_FILTER_LINEAR : VkFilter = 1;
pub const VK_FILTER_CUBIC_IMG : VkFilter = 1000015000;
pub const VK_FILTER_CUBIC_EXT : VkFilter = VK_FILTER_CUBIC_IMG;
pub const VK_FILTER_BEGIN_RANGE : VkFilter = VK_FILTER_NEAREST;
pub const VK_FILTER_END_RANGE : VkFilter = VK_FILTER_LINEAR;
pub const VK_FILTER_RANGE_SIZE : VkFilter = (VK_FILTER_LINEAR - VK_FILTER_NEAREST + 1);
pub const VK_FILTER_MAX_ENUM : VkFilter = 0x7FFFFFFF;

pub const VK_DESCRIPTOR_TYPE_SAMPLER : VkDescriptorType = 0;
pub const VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER : VkDescriptorType = 1;
pub const VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE : VkDescriptorType = 2;
pub const VK_DESCRIPTOR_TYPE_STORAGE_IMAGE : VkDescriptorType = 3;
pub const VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER : VkDescriptorType = 4;
pub const VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER : VkDescriptorType = 5;
pub const VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER : VkDescriptorType = 6;
pub const VK_DESCRIPTOR_TYPE_STORAGE_BUFFER : VkDescriptorType = 7;
pub const VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC : VkDescriptorType = 8;
pub const VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC : VkDescriptorType = 9;
pub const VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT : VkDescriptorType = 10;
pub const VK_DESCRIPTOR_TYPE_INLINE_UNIFORM_BLOCK_EXT : VkDescriptorType = 1000138000;
pub const VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_NV : VkDescriptorType = 1000165000;
pub const VK_DESCRIPTOR_TYPE_BEGIN_RANGE : VkDescriptorType = VK_DESCRIPTOR_TYPE_SAMPLER;
pub const VK_DESCRIPTOR_TYPE_END_RANGE : VkDescriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
pub const VK_DESCRIPTOR_TYPE_RANGE_SIZE : VkDescriptorType = (VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT - VK_DESCRIPTOR_TYPE_SAMPLER + 1);
pub const VK_DESCRIPTOR_TYPE_MAX_ENUM : VkDescriptorType = 0x7FFFFFFF;

pub const VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT : VkDescriptorPoolCreateFlagBits = 0x00000001;
pub const VK_DESCRIPTOR_POOL_CREATE_UPDATE_AFTER_BIND_BIT : VkDescriptorPoolCreateFlagBits = 0x00000002;
pub const VK_DESCRIPTOR_POOL_CREATE_UPDATE_AFTER_BIND_BIT_EXT : VkDescriptorPoolCreateFlagBits = VK_DESCRIPTOR_POOL_CREATE_UPDATE_AFTER_BIND_BIT;
pub const VK_DESCRIPTOR_POOL_CREATE_FLAG_BITS_MAX_ENUM : VkDescriptorPoolCreateFlagBits = 0x7FFFFFFF;

pub const VK_IMAGE_CREATE_SPARSE_BINDING_BIT : VkImageCreateFlagBits = 0x00000001;
pub const VK_IMAGE_CREATE_SPARSE_RESIDENCY_BIT : VkImageCreateFlagBits = 0x00000002;
pub const VK_IMAGE_CREATE_SPARSE_ALIASED_BIT : VkImageCreateFlagBits = 0x00000004;
pub const VK_IMAGE_CREATE_MUTABLE_FORMAT_BIT : VkImageCreateFlagBits = 0x00000008;
pub const VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT : VkImageCreateFlagBits = 0x00000010;
pub const VK_IMAGE_CREATE_ALIAS_BIT : VkImageCreateFlagBits = 0x00000400;
pub const VK_IMAGE_CREATE_SPLIT_INSTANCE_BIND_REGIONS_BIT : VkImageCreateFlagBits = 0x00000040;
pub const VK_IMAGE_CREATE_2D_ARRAY_COMPATIBLE_BIT : VkImageCreateFlagBits = 0x00000020;
pub const VK_IMAGE_CREATE_BLOCK_TEXEL_VIEW_COMPATIBLE_BIT : VkImageCreateFlagBits = 0x00000080;
pub const VK_IMAGE_CREATE_EXTENDED_USAGE_BIT : VkImageCreateFlagBits = 0x00000100;
pub const VK_IMAGE_CREATE_PROTECTED_BIT : VkImageCreateFlagBits = 0x00000800;
pub const VK_IMAGE_CREATE_DISJOINT_BIT : VkImageCreateFlagBits = 0x00000200;
pub const VK_IMAGE_CREATE_CORNER_SAMPLED_BIT_NV : VkImageCreateFlagBits = 0x00002000;
pub const VK_IMAGE_CREATE_SAMPLE_LOCATIONS_COMPATIBLE_DEPTH_BIT_EXT : VkImageCreateFlagBits = 0x00001000;
pub const VK_IMAGE_CREATE_SUBSAMPLED_BIT_EXT : VkImageCreateFlagBits = 0x00004000;
pub const VK_IMAGE_CREATE_SPLIT_INSTANCE_BIND_REGIONS_BIT_KHR : VkImageCreateFlagBits = VK_IMAGE_CREATE_SPLIT_INSTANCE_BIND_REGIONS_BIT;
pub const VK_IMAGE_CREATE_2D_ARRAY_COMPATIBLE_BIT_KHR : VkImageCreateFlagBits = VK_IMAGE_CREATE_2D_ARRAY_COMPATIBLE_BIT;
pub const VK_IMAGE_CREATE_BLOCK_TEXEL_VIEW_COMPATIBLE_BIT_KHR : VkImageCreateFlagBits = VK_IMAGE_CREATE_BLOCK_TEXEL_VIEW_COMPATIBLE_BIT;
pub const VK_IMAGE_CREATE_EXTENDED_USAGE_BIT_KHR : VkImageCreateFlagBits = VK_IMAGE_CREATE_EXTENDED_USAGE_BIT;
pub const VK_IMAGE_CREATE_DISJOINT_BIT_KHR : VkImageCreateFlagBits = VK_IMAGE_CREATE_DISJOINT_BIT;
pub const VK_IMAGE_CREATE_ALIAS_BIT_KHR : VkImageCreateFlagBits = VK_IMAGE_CREATE_ALIAS_BIT;
pub const VK_IMAGE_CREATE_FLAG_BITS_MAX_ENUM : VkImageCreateFlagBits = 0x7FFFFFFF;

pub const VK_IMAGE_TILING_OPTIMAL : VkImageTiling = 0;
pub const VK_IMAGE_TILING_LINEAR : VkImageTiling = 1;
pub const VK_IMAGE_TILING_DRM_FORMAT_MODIFIER_EXT : VkImageTiling = 1000158000;
pub const VK_IMAGE_TILING_BEGIN_RANGE : VkImageTiling = VK_IMAGE_TILING_OPTIMAL;
pub const VK_IMAGE_TILING_END_RANGE : VkImageTiling = VK_IMAGE_TILING_LINEAR;
pub const VK_IMAGE_TILING_RANGE_SIZE : VkImageTiling = (VK_IMAGE_TILING_LINEAR - VK_IMAGE_TILING_OPTIMAL + 1);
pub const VK_IMAGE_TILING_MAX_ENUM : VkImageTiling = 0x7FFFFFFF;

pub const VK_IMAGE_TYPE_1D : VkImageType = 0;
pub const VK_IMAGE_TYPE_2D : VkImageType = 1;
pub const VK_IMAGE_TYPE_3D : VkImageType = 2;
pub const VK_IMAGE_TYPE_BEGIN_RANGE : VkImageType = VK_IMAGE_TYPE_1D;
pub const VK_IMAGE_TYPE_END_RANGE : VkImageType = VK_IMAGE_TYPE_3D;
pub const VK_IMAGE_TYPE_RANGE_SIZE : VkImageType = (VK_IMAGE_TYPE_3D - VK_IMAGE_TYPE_1D + 1);
pub const VK_IMAGE_TYPE_MAX_ENUM : VkImageType = 0x7FFFFFFF;

pub const VK_SUBPASS_CONTENTS_INLINE : VkSubpassContents = 0;
pub const VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS : VkSubpassContents = 1;
pub const VK_SUBPASS_CONTENTS_BEGIN_RANGE : VkSubpassContents = VK_SUBPASS_CONTENTS_INLINE;
pub const VK_SUBPASS_CONTENTS_END_RANGE : VkSubpassContents = VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS;
pub const VK_SUBPASS_CONTENTS_RANGE_SIZE : VkSubpassContents = (VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS - VK_SUBPASS_CONTENTS_INLINE + 1);
pub const VK_SUBPASS_CONTENTS_MAX_ENUM : VkSubpassContents = 0x7FFFFFFF;

pub const VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK : VkBorderColor = 0;
pub const VK_BORDER_COLOR_INT_TRANSPARENT_BLACK : VkBorderColor = 1;
pub const VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK : VkBorderColor = 2;
pub const VK_BORDER_COLOR_INT_OPAQUE_BLACK : VkBorderColor = 3;
pub const VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE : VkBorderColor = 4;
pub const VK_BORDER_COLOR_INT_OPAQUE_WHITE : VkBorderColor = 5;
pub const VK_BORDER_COLOR_BEGIN_RANGE : VkBorderColor = VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK;
pub const VK_BORDER_COLOR_END_RANGE : VkBorderColor = VK_BORDER_COLOR_INT_OPAQUE_WHITE;
pub const VK_BORDER_COLOR_RANGE_SIZE : VkBorderColor = (VK_BORDER_COLOR_INT_OPAQUE_WHITE - VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK + 1);
pub const VK_BORDER_COLOR_MAX_ENUM : VkBorderColor = 0x7FFFFFFF;

pub const VK_SAMPLER_ADDRESS_MODE_REPEAT : VkSamplerAddressMode = 0;
pub const VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT : VkSamplerAddressMode = 1;
pub const VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE : VkSamplerAddressMode = 2;
pub const VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER : VkSamplerAddressMode = 3;
pub const VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE : VkSamplerAddressMode = 4;
pub const VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE_KHR : VkSamplerAddressMode = VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE;
pub const VK_SAMPLER_ADDRESS_MODE_BEGIN_RANGE : VkSamplerAddressMode = VK_SAMPLER_ADDRESS_MODE_REPEAT;
pub const VK_SAMPLER_ADDRESS_MODE_END_RANGE : VkSamplerAddressMode = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
pub const VK_SAMPLER_ADDRESS_MODE_RANGE_SIZE : VkSamplerAddressMode = (VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER - VK_SAMPLER_ADDRESS_MODE_REPEAT + 1);
pub const VK_SAMPLER_ADDRESS_MODE_MAX_ENUM : VkSamplerAddressMode = 0x7FFFFFFF;

pub const VK_SAMPLER_CREATE_SUBSAMPLED_BIT_EXT : VkSamplerCreateFlagBits = 0x00000001;
pub const VK_SAMPLER_CREATE_SUBSAMPLED_COARSE_RECONSTRUCTION_BIT_EXT : VkSamplerCreateFlagBits = 0x00000002;
pub const VK_SAMPLER_CREATE_FLAG_BITS_MAX_ENUM : VkSamplerCreateFlagBits = 0x7FFFFFFF;

pub const VK_FORMAT_FEATURE_SAMPLED_IMAGE_BIT : VkFormatFeatureFlagBits = 0x00000001;
pub const VK_FORMAT_FEATURE_STORAGE_IMAGE_BIT : VkFormatFeatureFlagBits = 0x00000002;
pub const VK_FORMAT_FEATURE_STORAGE_IMAGE_ATOMIC_BIT : VkFormatFeatureFlagBits = 0x00000004;
pub const VK_FORMAT_FEATURE_UNIFORM_TEXEL_BUFFER_BIT : VkFormatFeatureFlagBits = 0x00000008;
pub const VK_FORMAT_FEATURE_STORAGE_TEXEL_BUFFER_BIT : VkFormatFeatureFlagBits = 0x00000010;
pub const VK_FORMAT_FEATURE_STORAGE_TEXEL_BUFFER_ATOMIC_BIT : VkFormatFeatureFlagBits = 0x00000020;
pub const VK_FORMAT_FEATURE_VERTEX_BUFFER_BIT : VkFormatFeatureFlagBits = 0x00000040;
pub const VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BIT : VkFormatFeatureFlagBits = 0x00000080;
pub const VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BLEND_BIT : VkFormatFeatureFlagBits = 0x00000100;
pub const VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT : VkFormatFeatureFlagBits = 0x00000200;
pub const VK_FORMAT_FEATURE_BLIT_SRC_BIT : VkFormatFeatureFlagBits = 0x00000400;
pub const VK_FORMAT_FEATURE_BLIT_DST_BIT : VkFormatFeatureFlagBits = 0x00000800;
pub const VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT : VkFormatFeatureFlagBits = 0x00001000;
pub const VK_FORMAT_FEATURE_TRANSFER_SRC_BIT : VkFormatFeatureFlagBits = 0x00004000;
pub const VK_FORMAT_FEATURE_TRANSFER_DST_BIT : VkFormatFeatureFlagBits = 0x00008000;
pub const VK_FORMAT_FEATURE_MIDPOINT_CHROMA_SAMPLES_BIT : VkFormatFeatureFlagBits = 0x00020000;
pub const VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_LINEAR_FILTER_BIT : VkFormatFeatureFlagBits = 0x00040000;
pub const VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_SEPARATE_RECONSTRUCTION_FILTER_BIT : VkFormatFeatureFlagBits = 0x00080000;
pub const VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_BIT : VkFormatFeatureFlagBits = 0x00100000;
pub const VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_FORCEABLE_BIT : VkFormatFeatureFlagBits = 0x00200000;
pub const VK_FORMAT_FEATURE_DISJOINT_BIT : VkFormatFeatureFlagBits = 0x00400000;
pub const VK_FORMAT_FEATURE_COSITED_CHROMA_SAMPLES_BIT : VkFormatFeatureFlagBits = 0x00800000;
pub const VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_MINMAX_BIT : VkFormatFeatureFlagBits = 0x00010000;
pub const VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_CUBIC_BIT_IMG : VkFormatFeatureFlagBits = 0x00002000;
pub const VK_FORMAT_FEATURE_FRAGMENT_DENSITY_MAP_BIT_EXT : VkFormatFeatureFlagBits = 0x01000000;
pub const VK_FORMAT_FEATURE_TRANSFER_SRC_BIT_KHR : VkFormatFeatureFlagBits = VK_FORMAT_FEATURE_TRANSFER_SRC_BIT;
pub const VK_FORMAT_FEATURE_TRANSFER_DST_BIT_KHR : VkFormatFeatureFlagBits = VK_FORMAT_FEATURE_TRANSFER_DST_BIT;
pub const VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_MINMAX_BIT_EXT : VkFormatFeatureFlagBits = VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_MINMAX_BIT;
pub const VK_FORMAT_FEATURE_MIDPOINT_CHROMA_SAMPLES_BIT_KHR : VkFormatFeatureFlagBits = VK_FORMAT_FEATURE_MIDPOINT_CHROMA_SAMPLES_BIT;
pub const VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_LINEAR_FILTER_BIT_KHR : VkFormatFeatureFlagBits = VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_LINEAR_FILTER_BIT;
pub const VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_SEPARATE_RECONSTRUCTION_FILTER_BIT_KHR : VkFormatFeatureFlagBits = VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_SEPARATE_RECONSTRUCTION_FILTER_BIT;
pub const VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_BIT_KHR : VkFormatFeatureFlagBits = VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_BIT;
pub const VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_FORCEABLE_BIT_KHR : VkFormatFeatureFlagBits = VK_FORMAT_FEATURE_SAMPLED_IMAGE_YCBCR_CONVERSION_CHROMA_RECONSTRUCTION_EXPLICIT_FORCEABLE_BIT;
pub const VK_FORMAT_FEATURE_DISJOINT_BIT_KHR : VkFormatFeatureFlagBits = VK_FORMAT_FEATURE_DISJOINT_BIT;
pub const VK_FORMAT_FEATURE_COSITED_CHROMA_SAMPLES_BIT_KHR : VkFormatFeatureFlagBits = VK_FORMAT_FEATURE_COSITED_CHROMA_SAMPLES_BIT;
pub const VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_CUBIC_BIT_EXT : VkFormatFeatureFlagBits = VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_CUBIC_BIT_IMG;
pub const VK_FORMAT_FEATURE_FLAG_BITS_MAX_ENUM : VkFormatFeatureFlagBits = 0x7FFFFFFF;

pub const VK_SAMPLER_MIPMAP_MODE_NEAREST : VkSamplerMipmapMode = 0;
pub const VK_SAMPLER_MIPMAP_MODE_LINEAR : VkSamplerMipmapMode = 1;
pub const VK_SAMPLER_MIPMAP_MODE_BEGIN_RANGE : VkSamplerMipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
pub const VK_SAMPLER_MIPMAP_MODE_END_RANGE : VkSamplerMipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
pub const VK_SAMPLER_MIPMAP_MODE_RANGE_SIZE : VkSamplerMipmapMode = (VK_SAMPLER_MIPMAP_MODE_LINEAR - VK_SAMPLER_MIPMAP_MODE_NEAREST + 1);
pub const VK_SAMPLER_MIPMAP_MODE_MAX_ENUM : VkSamplerMipmapMode = 0x7FFFFFFF;

pub const VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT : VkDescriptorSetLayoutCreateFlagBits = 0x00000002;
pub const VK_DESCRIPTOR_SET_LAYOUT_CREATE_PUSH_DESCRIPTOR_BIT_KHR : VkDescriptorSetLayoutCreateFlagBits = 0x00000001;
pub const VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT_EXT : VkDescriptorSetLayoutCreateFlagBits = VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT;
pub const VK_DESCRIPTOR_SET_LAYOUT_CREATE_FLAG_BITS_MAX_ENUM : VkDescriptorSetLayoutCreateFlagBits = 0x7FFFFFFF;

pub const VK_DYNAMIC_STATE_VIEWPORT : VkDynamicState = 0;
pub const VK_DYNAMIC_STATE_SCISSOR : VkDynamicState = 1;
pub const VK_DYNAMIC_STATE_LINE_WIDTH : VkDynamicState = 2;
pub const VK_DYNAMIC_STATE_DEPTH_BIAS : VkDynamicState = 3;
pub const VK_DYNAMIC_STATE_BLEND_CONSTANTS : VkDynamicState = 4;
pub const VK_DYNAMIC_STATE_DEPTH_BOUNDS : VkDynamicState = 5;
pub const VK_DYNAMIC_STATE_STENCIL_COMPARE_MASK : VkDynamicState = 6;
pub const VK_DYNAMIC_STATE_STENCIL_WRITE_MASK : VkDynamicState = 7;
pub const VK_DYNAMIC_STATE_STENCIL_REFERENCE : VkDynamicState = 8;
pub const VK_DYNAMIC_STATE_VIEWPORT_W_SCALING_NV : VkDynamicState = 1000087000;
pub const VK_DYNAMIC_STATE_DISCARD_RECTANGLE_EXT : VkDynamicState = 1000099000;
pub const VK_DYNAMIC_STATE_SAMPLE_LOCATIONS_EXT : VkDynamicState = 1000143000;
pub const VK_DYNAMIC_STATE_VIEWPORT_SHADING_RATE_PALETTE_NV : VkDynamicState = 1000164004;
pub const VK_DYNAMIC_STATE_VIEWPORT_COARSE_SAMPLE_ORDER_NV : VkDynamicState = 1000164006;
pub const VK_DYNAMIC_STATE_EXCLUSIVE_SCISSOR_NV : VkDynamicState = 1000205001;
pub const VK_DYNAMIC_STATE_LINE_STIPPLE_EXT : VkDynamicState = 1000259000;
pub const VK_DYNAMIC_STATE_BEGIN_RANGE : VkDynamicState = VK_DYNAMIC_STATE_VIEWPORT;
pub const VK_DYNAMIC_STATE_END_RANGE : VkDynamicState = VK_DYNAMIC_STATE_STENCIL_REFERENCE;
pub const VK_DYNAMIC_STATE_RANGE_SIZE : VkDynamicState = (VK_DYNAMIC_STATE_STENCIL_REFERENCE - VK_DYNAMIC_STATE_VIEWPORT + 1);
pub const VK_DYNAMIC_STATE_MAX_ENUM : VkDynamicState = 0x7FFFFFFF;

pub const VK_FENCE_CREATE_SIGNALED_BIT : VkFenceCreateFlagBits = 0x00000001;
pub const VK_FENCE_CREATE_FLAG_BITS_MAX_ENUM : VkFenceCreateFlagBits = 0x7FFFFFFF;

pub const VK_INDEX_TYPE_UINT16 : VkIndexType = 0;
pub const VK_INDEX_TYPE_UINT32 : VkIndexType = 1;
pub const VK_INDEX_TYPE_NONE_NV : VkIndexType = 1000165000;
pub const VK_INDEX_TYPE_UINT8_EXT : VkIndexType = 1000265000;
pub const VK_INDEX_TYPE_BEGIN_RANGE : VkIndexType = VK_INDEX_TYPE_UINT16;
pub const VK_INDEX_TYPE_END_RANGE : VkIndexType = VK_INDEX_TYPE_UINT32;
pub const VK_INDEX_TYPE_RANGE_SIZE : VkIndexType = (VK_INDEX_TYPE_UINT32 - VK_INDEX_TYPE_UINT16 + 1);
pub const VK_INDEX_TYPE_MAX_ENUM : VkIndexType = 0x7FFFFFFF;

pub const VK_COMPARE_OP_NEVER : VkCompareOp = 0;
pub const VK_COMPARE_OP_LESS : VkCompareOp = 1;
pub const VK_COMPARE_OP_EQUAL : VkCompareOp = 2;
pub const VK_COMPARE_OP_LESS_OR_EQUAL : VkCompareOp = 3;
pub const VK_COMPARE_OP_GREATER : VkCompareOp = 4;
pub const VK_COMPARE_OP_NOT_EQUAL : VkCompareOp = 5;
pub const VK_COMPARE_OP_GREATER_OR_EQUAL : VkCompareOp = 6;
pub const VK_COMPARE_OP_ALWAYS : VkCompareOp = 7;
pub const VK_COMPARE_OP_BEGIN_RANGE : VkCompareOp = VK_COMPARE_OP_NEVER;
pub const VK_COMPARE_OP_END_RANGE : VkCompareOp = VK_COMPARE_OP_ALWAYS;
pub const VK_COMPARE_OP_RANGE_SIZE : VkCompareOp = (VK_COMPARE_OP_ALWAYS - VK_COMPARE_OP_NEVER + 1);
pub const VK_COMPARE_OP_MAX_ENUM : VkCompareOp = 0x7FFFFFFF;

pub const VK_MEMORY_HEAP_DEVICE_LOCAL_BIT : VkMemoryHeapFlagBits = 0x00000001;
pub const VK_MEMORY_HEAP_MULTI_INSTANCE_BIT : VkMemoryHeapFlagBits = 0x00000002;
pub const VK_MEMORY_HEAP_MULTI_INSTANCE_BIT_KHR : VkMemoryHeapFlagBits = VK_MEMORY_HEAP_MULTI_INSTANCE_BIT;
pub const VK_MEMORY_HEAP_FLAG_BITS_MAX_ENUM : VkMemoryHeapFlagBits = 0x7FFFFFFF;

pub const VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT : VkMemoryPropertyFlagBits = 0x00000001;
pub const VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT : VkMemoryPropertyFlagBits = 0x00000002;
pub const VK_MEMORY_PROPERTY_HOST_COHERENT_BIT : VkMemoryPropertyFlagBits = 0x00000004;
pub const VK_MEMORY_PROPERTY_HOST_CACHED_BIT : VkMemoryPropertyFlagBits = 0x00000008;
pub const VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT : VkMemoryPropertyFlagBits = 0x00000010;
pub const VK_MEMORY_PROPERTY_PROTECTED_BIT : VkMemoryPropertyFlagBits = 0x00000020;
pub const VK_MEMORY_PROPERTY_DEVICE_COHERENT_BIT_AMD : VkMemoryPropertyFlagBits = 0x00000040;
pub const VK_MEMORY_PROPERTY_DEVICE_UNCACHED_BIT_AMD : VkMemoryPropertyFlagBits = 0x00000080;
pub const VK_MEMORY_PROPERTY_FLAG_BITS_MAX_ENUM : VkMemoryPropertyFlagBits = 0x7FFFFFFF;

pub const VK_STENCIL_OP_KEEP : VkStencilOp = 0;
pub const VK_STENCIL_OP_ZERO : VkStencilOp = 1;
pub const VK_STENCIL_OP_REPLACE : VkStencilOp = 2;
pub const VK_STENCIL_OP_INCREMENT_AND_CLAMP : VkStencilOp = 3;
pub const VK_STENCIL_OP_DECREMENT_AND_CLAMP : VkStencilOp = 4;
pub const VK_STENCIL_OP_INVERT : VkStencilOp = 5;
pub const VK_STENCIL_OP_INCREMENT_AND_WRAP : VkStencilOp = 6;
pub const VK_STENCIL_OP_DECREMENT_AND_WRAP : VkStencilOp = 7;
pub const VK_STENCIL_OP_BEGIN_RANGE : VkStencilOp = VK_STENCIL_OP_KEEP;
pub const VK_STENCIL_OP_END_RANGE : VkStencilOp = VK_STENCIL_OP_DECREMENT_AND_WRAP;
pub const VK_STENCIL_OP_RANGE_SIZE : VkStencilOp = (VK_STENCIL_OP_DECREMENT_AND_WRAP - VK_STENCIL_OP_KEEP + 1);
pub const VK_STENCIL_OP_MAX_ENUM : VkStencilOp = 0x7FFFFFFF;

pub const VK_PRIMITIVE_TOPOLOGY_POINT_LIST : VkPrimitiveTopology = 0;
pub const VK_PRIMITIVE_TOPOLOGY_LINE_LIST : VkPrimitiveTopology = 1;
pub const VK_PRIMITIVE_TOPOLOGY_LINE_STRIP : VkPrimitiveTopology = 2;
pub const VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST : VkPrimitiveTopology = 3;
pub const VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP : VkPrimitiveTopology = 4;
pub const VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN : VkPrimitiveTopology = 5;
pub const VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY : VkPrimitiveTopology = 6;
pub const VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY : VkPrimitiveTopology = 7;
pub const VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY : VkPrimitiveTopology = 8;
pub const VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY : VkPrimitiveTopology = 9;
pub const VK_PRIMITIVE_TOPOLOGY_PATCH_LIST : VkPrimitiveTopology = 10;
pub const VK_PRIMITIVE_TOPOLOGY_BEGIN_RANGE : VkPrimitiveTopology = VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
pub const VK_PRIMITIVE_TOPOLOGY_END_RANGE : VkPrimitiveTopology = VK_PRIMITIVE_TOPOLOGY_PATCH_LIST;
pub const VK_PRIMITIVE_TOPOLOGY_RANGE_SIZE : VkPrimitiveTopology = (VK_PRIMITIVE_TOPOLOGY_PATCH_LIST - VK_PRIMITIVE_TOPOLOGY_POINT_LIST + 1);
pub const VK_PRIMITIVE_TOPOLOGY_MAX_ENUM : VkPrimitiveTopology = 0x7FFFFFFF;

pub const VK_PIPELINE_CREATE_DISABLE_OPTIMIZATION_BIT : VkPipelineCreateFlagBits = 0x00000001;
pub const VK_PIPELINE_CREATE_ALLOW_DERIVATIVES_BIT : VkPipelineCreateFlagBits = 0x00000002;
pub const VK_PIPELINE_CREATE_DERIVATIVE_BIT : VkPipelineCreateFlagBits = 0x00000004;
pub const VK_PIPELINE_CREATE_VIEW_INDEX_FROM_DEVICE_INDEX_BIT : VkPipelineCreateFlagBits = 0x00000008;
pub const VK_PIPELINE_CREATE_DISPATCH_BASE_BIT : VkPipelineCreateFlagBits = 0x00000010;
pub const VK_PIPELINE_CREATE_DEFER_COMPILE_BIT_NV : VkPipelineCreateFlagBits = 0x00000020;
pub const VK_PIPELINE_CREATE_CAPTURE_STATISTICS_BIT_KHR : VkPipelineCreateFlagBits = 0x00000040;
pub const VK_PIPELINE_CREATE_CAPTURE_INTERNAL_REPRESENTATIONS_BIT_KHR : VkPipelineCreateFlagBits = 0x00000080;
pub const VK_PIPELINE_CREATE_DISPATCH_BASE : VkPipelineCreateFlagBits = VK_PIPELINE_CREATE_DISPATCH_BASE_BIT;
pub const VK_PIPELINE_CREATE_VIEW_INDEX_FROM_DEVICE_INDEX_BIT_KHR : VkPipelineCreateFlagBits = VK_PIPELINE_CREATE_VIEW_INDEX_FROM_DEVICE_INDEX_BIT;
pub const VK_PIPELINE_CREATE_DISPATCH_BASE_KHR : VkPipelineCreateFlagBits = VK_PIPELINE_CREATE_DISPATCH_BASE;
pub const VK_PIPELINE_CREATE_FLAG_BITS_MAX_ENUM : VkPipelineCreateFlagBits = 0x7FFFFFFF;

pub const VK_SUCCESS : VkResult = 0;
pub const VK_NOT_READY : VkResult  = 1;
pub const VK_TIMEOUT : VkResult  = 2;
pub const VK_EVENT_SET : VkResult  = 3;
pub const VK_EVENT_RESET : VkResult  = 4;
pub const VK_INCOMPLETE : VkResult  = 5;
pub const VK_ERROR_OUT_OF_HOST_MEMORY : VkResult  = -1;
pub const VK_ERROR_OUT_OF_DEVICE_MEMORY : VkResult  = -2;
pub const VK_ERROR_INITIALIZATION_FAILED : VkResult  = -3;
pub const VK_ERROR_DEVICE_LOST : VkResult  = -4;
pub const VK_ERROR_MEMORY_MAP_FAILED : VkResult  = -5;
pub const VK_ERROR_LAYER_NOT_PRESENT : VkResult  = -6;
pub const VK_ERROR_EXTENSION_NOT_PRESENT : VkResult  = -7;
pub const VK_ERROR_FEATURE_NOT_PRESENT : VkResult  = -8;
pub const VK_ERROR_INCOMPATIBLE_DRIVER : VkResult  = -9;
pub const VK_ERROR_TOO_MANY_OBJECTS : VkResult  = -10;
pub const VK_ERROR_FORMAT_NOT_SUPPORTED : VkResult  = -11;
pub const VK_ERROR_FRAGMENTED_POOL : VkResult  = -12;
pub const VK_ERROR_UNKNOWN : VkResult  = -13;
pub const VK_ERROR_OUT_OF_POOL_MEMORY : VkResult  = -1000069000;
pub const VK_ERROR_INVALID_EXTERNAL_HANDLE : VkResult  = -1000072003;
pub const VK_ERROR_FRAGMENTATION : VkResult  = -1000161000;
pub const VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS : VkResult  = -1000257000;
pub const VK_ERROR_SURFACE_LOST_KHR : VkResult  = -1000000000;
pub const VK_ERROR_NATIVE_WINDOW_IN_USE_KHR : VkResult  = -1000000001;
pub const VK_SUBOPTIMAL_KHR : VkResult  = 1000001003;
pub const VK_ERROR_OUT_OF_DATE_KHR : VkResult  = -1000001004;
pub const VK_ERROR_INCOMPATIBLE_DISPLAY_KHR : VkResult  = -1000003001;
pub const VK_ERROR_VALIDATION_FAILED_EXT : VkResult  = -1000011001;
pub const VK_ERROR_INVALID_SHADER_NV : VkResult  = -1000012000;
pub const VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT : VkResult  = -1000158000;
pub const VK_ERROR_NOT_PERMITTED_EXT : VkResult  = -1000174001;
pub const VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT : VkResult  = -1000255000;
pub const VK_ERROR_OUT_OF_POOL_MEMORY_KHR : VkResult  = VK_ERROR_OUT_OF_POOL_MEMORY;
pub const VK_ERROR_INVALID_EXTERNAL_HANDLE_KHR : VkResult  = VK_ERROR_INVALID_EXTERNAL_HANDLE;
pub const VK_ERROR_FRAGMENTATION_EXT : VkResult  = VK_ERROR_FRAGMENTATION;
pub const VK_ERROR_INVALID_DEVICE_ADDRESS_EXT : VkResult  = VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS;
pub const VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS_KHR : VkResult  = VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS;
pub const VK_RESULT_BEGIN_RANGE : VkResult  = VK_ERROR_UNKNOWN;
pub const VK_RESULT_END_RANGE : VkResult = VK_INCOMPLETE;
pub const VK_RESULT_RANGE_SIZE : VkResult = (VK_INCOMPLETE - VK_ERROR_UNKNOWN + 1);
pub const VK_RESULT_MAX_ENUM : VkResult = 0x7FFFFFFF;

pub const VK_POLYGON_MODE_FILL : VkPolygonMode = 0;
pub const VK_POLYGON_MODE_LINE : VkPolygonMode = 1;
pub const VK_POLYGON_MODE_POINT : VkPolygonMode = 2;
pub const VK_POLYGON_MODE_FILL_RECTANGLE_NV : VkPolygonMode = 1000153000;
pub const VK_POLYGON_MODE_BEGIN_RANGE : VkPolygonMode = VK_POLYGON_MODE_FILL;
pub const VK_POLYGON_MODE_END_RANGE : VkPolygonMode = VK_POLYGON_MODE_POINT;
pub const VK_POLYGON_MODE_RANGE_SIZE : VkPolygonMode = (VK_POLYGON_MODE_POINT - VK_POLYGON_MODE_FILL + 1);
pub const VK_POLYGON_MODE_MAX_ENUM : VkPolygonMode = 0x7FFFFFFF;

pub const VK_ATTACHMENT_DESCRIPTION_MAY_ALIAS_BIT : VkAttachmentDescriptionFlagBits = 0x00000001;
pub const VK_ATTACHMENT_DESCRIPTION_FLAG_BITS_MAX_ENUM : VkAttachmentDescriptionFlagBits = 0x7FFFFFFF;

pub const VK_CULL_MODE_NONE : VkCullModeFlagBits = 0;
pub const VK_CULL_MODE_FRONT_BIT : VkCullModeFlagBits = 0x00000001;
pub const VK_CULL_MODE_BACK_BIT : VkCullModeFlagBits = 0x00000002;
pub const VK_CULL_MODE_FRONT_AND_BACK : VkCullModeFlagBits = 0x00000003;
pub const VK_CULL_MODE_FLAG_BITS_MAX_ENUM : VkCullModeFlagBits = 0x7FFFFFFF;

pub const VK_FRONT_FACE_COUNTER_CLOCKWISE : VkFrontFace = 0;
pub const VK_FRONT_FACE_CLOCKWISE : VkFrontFace = 1;
pub const VK_FRONT_FACE_BEGIN_RANGE : VkFrontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
pub const VK_FRONT_FACE_END_RANGE : VkFrontFace = VK_FRONT_FACE_CLOCKWISE;
pub const VK_FRONT_FACE_RANGE_SIZE : VkFrontFace = (VK_FRONT_FACE_CLOCKWISE - VK_FRONT_FACE_COUNTER_CLOCKWISE + 1);
pub const VK_FRONT_FACE_MAX_ENUM : VkFrontFace = 0x7FFFFFFF;

pub const VK_ATTACHMENT_STORE_OP_STORE : VkAttachmentStoreOp = 0;
pub const VK_ATTACHMENT_STORE_OP_DONT_CARE : VkAttachmentStoreOp = 1;
pub const VK_ATTACHMENT_STORE_OP_BEGIN_RANGE : VkAttachmentStoreOp = VK_ATTACHMENT_STORE_OP_STORE;
pub const VK_ATTACHMENT_STORE_OP_END_RANGE : VkAttachmentStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
pub const VK_ATTACHMENT_STORE_OP_RANGE_SIZE : VkAttachmentStoreOp = (VK_ATTACHMENT_STORE_OP_DONT_CARE - VK_ATTACHMENT_STORE_OP_STORE + 1);
pub const VK_ATTACHMENT_STORE_OP_MAX_ENUM : VkAttachmentStoreOp = 0x7FFFFFFF;

pub const VK_ATTACHMENT_LOAD_OP_LOAD : VkAttachmentLoadOp = 0;
pub const VK_ATTACHMENT_LOAD_OP_CLEAR : VkAttachmentLoadOp = 1;
pub const VK_ATTACHMENT_LOAD_OP_DONT_CARE : VkAttachmentLoadOp = 2;
pub const VK_ATTACHMENT_LOAD_OP_BEGIN_RANGE : VkAttachmentLoadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
pub const VK_ATTACHMENT_LOAD_OP_END_RANGE : VkAttachmentLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
pub const VK_ATTACHMENT_LOAD_OP_RANGE_SIZE : VkAttachmentLoadOp = (VK_ATTACHMENT_LOAD_OP_DONT_CARE - VK_ATTACHMENT_LOAD_OP_LOAD + 1);
pub const VK_ATTACHMENT_LOAD_OP_MAX_ENUM : VkAttachmentLoadOp = 0x7FFFFFFF;

pub const VK_COLOR_COMPONENT_R_BIT : VkColorComponentFlagBits = 0x00000001;
pub const VK_COLOR_COMPONENT_G_BIT : VkColorComponentFlagBits = 0x00000002;
pub const VK_COLOR_COMPONENT_B_BIT : VkColorComponentFlagBits = 0x00000004;
pub const VK_COLOR_COMPONENT_A_BIT : VkColorComponentFlagBits = 0x00000008;
pub const VK_COLOR_COMPONENT_FLAG_BITS_MAX_ENUM : VkColorComponentFlagBits = 0x7FFFFFFF;

pub const VK_COMMAND_POOL_CREATE_TRANSIENT_BIT : VkCommandPoolCreateFlagBits = 0x00000001;
pub const VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT : VkCommandPoolCreateFlagBits = 0x00000002;
pub const VK_COMMAND_POOL_CREATE_PROTECTED_BIT : VkCommandPoolCreateFlagBits = 0x00000004;
pub const VK_COMMAND_POOL_CREATE_FLAG_BITS_MAX_ENUM : VkCommandPoolCreateFlagBits = 0x7FFFFFFF;

pub const VK_COMMAND_BUFFER_LEVEL_PRIMARY : VkCommandBufferLevel = 0;
pub const VK_COMMAND_BUFFER_LEVEL_SECONDARY : VkCommandBufferLevel = 1;
pub const VK_COMMAND_BUFFER_LEVEL_BEGIN_RANGE : VkCommandBufferLevel = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
pub const VK_COMMAND_BUFFER_LEVEL_END_RANGE : VkCommandBufferLevel = VK_COMMAND_BUFFER_LEVEL_SECONDARY;
pub const VK_COMMAND_BUFFER_LEVEL_RANGE_SIZE : VkCommandBufferLevel = (VK_COMMAND_BUFFER_LEVEL_SECONDARY - VK_COMMAND_BUFFER_LEVEL_PRIMARY + 1);
pub const VK_COMMAND_BUFFER_LEVEL_MAX_ENUM : VkCommandBufferLevel = 0x7FFFFFFF;

pub const VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT : VkCommandBufferUsageFlagBits = 0x00000001;
pub const VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT : VkCommandBufferUsageFlagBits = 0x00000002;
pub const VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT : VkCommandBufferUsageFlagBits = 0x00000004;
pub const VK_COMMAND_BUFFER_USAGE_FLAG_BITS_MAX_ENUM : VkCommandBufferUsageFlagBits = 0x7FFFFFFF;

pub const VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT : VkCommandBufferResetFlagBits = 0x00000001;
pub const VK_COMMAND_BUFFER_RESET_FLAG_BITS_MAX_ENUM : VkCommandBufferResetFlagBits = 0x7FFFFFFF;

pub const VK_QUERY_CONTROL_PRECISE_BIT : VkQueryControlFlagBits = 0x00000001;
pub const VK_QUERY_CONTROL_FLAG_BITS_MAX_ENUM : VkQueryControlFlagBits = 0x7FFFFFFF;

pub const VK_QUERY_PIPELINE_STATISTIC_INPUT_ASSEMBLY_VERTICES_BIT : VkQueryPipelineStatisticFlagBits = 0x00000001;
pub const VK_QUERY_PIPELINE_STATISTIC_INPUT_ASSEMBLY_PRIMITIVES_BIT : VkQueryPipelineStatisticFlagBits = 0x00000002;
pub const VK_QUERY_PIPELINE_STATISTIC_VERTEX_SHADER_INVOCATIONS_BIT : VkQueryPipelineStatisticFlagBits = 0x00000004;
pub const VK_QUERY_PIPELINE_STATISTIC_GEOMETRY_SHADER_INVOCATIONS_BIT : VkQueryPipelineStatisticFlagBits = 0x00000008;
pub const VK_QUERY_PIPELINE_STATISTIC_GEOMETRY_SHADER_PRIMITIVES_BIT : VkQueryPipelineStatisticFlagBits = 0x00000010;
pub const VK_QUERY_PIPELINE_STATISTIC_CLIPPING_INVOCATIONS_BIT : VkQueryPipelineStatisticFlagBits = 0x00000020;
pub const VK_QUERY_PIPELINE_STATISTIC_CLIPPING_PRIMITIVES_BIT : VkQueryPipelineStatisticFlagBits = 0x00000040;
pub const VK_QUERY_PIPELINE_STATISTIC_FRAGMENT_SHADER_INVOCATIONS_BIT : VkQueryPipelineStatisticFlagBits = 0x00000080;
pub const VK_QUERY_PIPELINE_STATISTIC_TESSELLATION_CONTROL_SHADER_PATCHES_BIT : VkQueryPipelineStatisticFlagBits = 0x00000100;
pub const VK_QUERY_PIPELINE_STATISTIC_TESSELLATION_EVALUATION_SHADER_INVOCATIONS_BIT : VkQueryPipelineStatisticFlagBits = 0x00000200;
pub const VK_QUERY_PIPELINE_STATISTIC_COMPUTE_SHADER_INVOCATIONS_BIT : VkQueryPipelineStatisticFlagBits = 0x00000400;
pub const VK_QUERY_PIPELINE_STATISTIC_FLAG_BITS_MAX_ENUM : VkQueryPipelineStatisticFlagBits = 0x7FFFFFFF;

pub const VK_PIPELINE_BIND_POINT_GRAPHICS : VkPipelineBindPoint = 0;
pub const VK_PIPELINE_BIND_POINT_COMPUTE : VkPipelineBindPoint = 1;
pub const VK_PIPELINE_BIND_POINT_RAY_TRACING_NV : VkPipelineBindPoint = 1000165000;
pub const VK_PIPELINE_BIND_POINT_BEGIN_RANGE : VkPipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
pub const VK_PIPELINE_BIND_POINT_END_RANGE : VkPipelineBindPoint = VK_PIPELINE_BIND_POINT_COMPUTE;
pub const VK_PIPELINE_BIND_POINT_RANGE_SIZE : VkPipelineBindPoint = (VK_PIPELINE_BIND_POINT_COMPUTE - VK_PIPELINE_BIND_POINT_GRAPHICS + 1);
pub const VK_PIPELINE_BIND_POINT_MAX_ENUM : VkPipelineBindPoint = 0x7FFFFFFF;

pub const VK_DEPENDENCY_BY_REGION_BIT : VkDependencyFlagBits = 0x00000001;
pub const VK_DEPENDENCY_DEVICE_GROUP_BIT : VkDependencyFlagBits = 0x00000004;
pub const VK_DEPENDENCY_VIEW_LOCAL_BIT : VkDependencyFlagBits = 0x00000002;
pub const VK_DEPENDENCY_VIEW_LOCAL_BIT_KHR : VkDependencyFlagBits = VK_DEPENDENCY_VIEW_LOCAL_BIT;
pub const VK_DEPENDENCY_DEVICE_GROUP_BIT_KHR : VkDependencyFlagBits = VK_DEPENDENCY_DEVICE_GROUP_BIT;
pub const VK_DEPENDENCY_FLAG_BITS_MAX_ENUM : VkDependencyFlagBits = 0x7FFFFFFF;

pub const VK_SUBPASS_DESCRIPTION_PER_VIEW_ATTRIBUTES_BIT_NVX : VkSubpassDescriptionFlagBits = 0x00000001;
pub const VK_SUBPASS_DESCRIPTION_PER_VIEW_POSITION_X_ONLY_BIT_NVX : VkSubpassDescriptionFlagBits = 0x00000002;
pub const VK_SUBPASS_DESCRIPTION_FLAG_BITS_MAX_ENUM : VkSubpassDescriptionFlagBits = 0x7FFFFFFF;

pub const VK_LOGIC_OP_CLEAR : VkLogicOp = 0;
pub const VK_LOGIC_OP_AND : VkLogicOp = 1;
pub const VK_LOGIC_OP_AND_REVERSE : VkLogicOp = 2;
pub const VK_LOGIC_OP_COPY : VkLogicOp = 3;
pub const VK_LOGIC_OP_AND_INVERTED : VkLogicOp = 4;
pub const VK_LOGIC_OP_NO_OP : VkLogicOp = 5;
pub const VK_LOGIC_OP_XOR : VkLogicOp = 6;
pub const VK_LOGIC_OP_OR : VkLogicOp = 7;
pub const VK_LOGIC_OP_NOR : VkLogicOp = 8;
pub const VK_LOGIC_OP_EQUIVALENT : VkLogicOp = 9;
pub const VK_LOGIC_OP_INVERT : VkLogicOp = 10;
pub const VK_LOGIC_OP_OR_REVERSE : VkLogicOp = 11;
pub const VK_LOGIC_OP_COPY_INVERTED : VkLogicOp = 12;
pub const VK_LOGIC_OP_OR_INVERTED : VkLogicOp = 13;
pub const VK_LOGIC_OP_NAND : VkLogicOp = 14;
pub const VK_LOGIC_OP_SET : VkLogicOp = 15;
pub const VK_LOGIC_OP_BEGIN_RANGE : VkLogicOp = VK_LOGIC_OP_CLEAR;
pub const VK_LOGIC_OP_END_RANGE : VkLogicOp = VK_LOGIC_OP_SET;
pub const VK_LOGIC_OP_RANGE_SIZE : VkLogicOp = (VK_LOGIC_OP_SET - VK_LOGIC_OP_CLEAR + 1);
pub const VK_LOGIC_OP_MAX_ENUM : VkLogicOp = 0x7FFFFFFF;

pub const VK_IMAGE_LAYOUT_UNDEFINED : VkImageLayout = 0;
pub const VK_IMAGE_LAYOUT_GENERAL : VkImageLayout = 1;
pub const VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL : VkImageLayout = 2;
pub const VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL : VkImageLayout = 3;
pub const VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL : VkImageLayout = 4;
pub const VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL : VkImageLayout = 5;
pub const VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL : VkImageLayout = 6;
pub const VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL : VkImageLayout = 7;
pub const VK_IMAGE_LAYOUT_PREINITIALIZED : VkImageLayout = 8;
pub const VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_STENCIL_ATTACHMENT_OPTIMAL : VkImageLayout = 1000117000;
pub const VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_STENCIL_READ_ONLY_OPTIMAL : VkImageLayout = 1000117001;
pub const VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL : VkImageLayout = 1000241000;
pub const VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_OPTIMAL : VkImageLayout = 1000241001;
pub const VK_IMAGE_LAYOUT_STENCIL_ATTACHMENT_OPTIMAL : VkImageLayout = 1000241002;
pub const VK_IMAGE_LAYOUT_STENCIL_READ_ONLY_OPTIMAL : VkImageLayout = 1000241003;
pub const VK_IMAGE_LAYOUT_PRESENT_SRC_KHR : VkImageLayout = 1000001002;
pub const VK_IMAGE_LAYOUT_SHARED_PRESENT_KHR : VkImageLayout = 1000111000;
pub const VK_IMAGE_LAYOUT_SHADING_RATE_OPTIMAL_NV : VkImageLayout = 1000164003;
pub const VK_IMAGE_LAYOUT_FRAGMENT_DENSITY_MAP_OPTIMAL_EXT : VkImageLayout = 1000218000;
pub const VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_STENCIL_ATTACHMENT_OPTIMAL_KHR : VkImageLayout = VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_STENCIL_ATTACHMENT_OPTIMAL;
pub const VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_STENCIL_READ_ONLY_OPTIMAL_KHR : VkImageLayout = VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_STENCIL_READ_ONLY_OPTIMAL;
pub const VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL_KHR : VkImageLayout = VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL;
pub const VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_OPTIMAL_KHR : VkImageLayout = VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_OPTIMAL;
pub const VK_IMAGE_LAYOUT_STENCIL_ATTACHMENT_OPTIMAL_KHR : VkImageLayout = VK_IMAGE_LAYOUT_STENCIL_ATTACHMENT_OPTIMAL;
pub const VK_IMAGE_LAYOUT_STENCIL_READ_ONLY_OPTIMAL_KHR : VkImageLayout = VK_IMAGE_LAYOUT_STENCIL_READ_ONLY_OPTIMAL;
pub const VK_IMAGE_LAYOUT_BEGIN_RANGE : VkImageLayout = VK_IMAGE_LAYOUT_UNDEFINED;
pub const VK_IMAGE_LAYOUT_END_RANGE : VkImageLayout = VK_IMAGE_LAYOUT_PREINITIALIZED;
pub const VK_IMAGE_LAYOUT_RANGE_SIZE : VkImageLayout = (VK_IMAGE_LAYOUT_PREINITIALIZED - VK_IMAGE_LAYOUT_UNDEFINED + 1);
pub const VK_IMAGE_LAYOUT_MAX_ENUM : VkImageLayout = 0x7FFFFFFF;

pub const VK_RENDER_PASS_CREATE_FLAG_BITS_MAX_ENUM : VkRenderPassCreateFlagBits = 0x7FFFFFFF;

pub const VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT : VkPipelineStageFlagBits = 0x00000001;
pub const VK_PIPELINE_STAGE_DRAW_INDIRECT_BIT : VkPipelineStageFlagBits = 0x00000002;
pub const VK_PIPELINE_STAGE_VERTEX_INPUT_BIT : VkPipelineStageFlagBits = 0x00000004;
pub const VK_PIPELINE_STAGE_VERTEX_SHADER_BIT : VkPipelineStageFlagBits = 0x00000008;
pub const VK_PIPELINE_STAGE_TESSELLATION_CONTROL_SHADER_BIT : VkPipelineStageFlagBits = 0x00000010;
pub const VK_PIPELINE_STAGE_TESSELLATION_EVALUATION_SHADER_BIT : VkPipelineStageFlagBits = 0x00000020;
pub const VK_PIPELINE_STAGE_GEOMETRY_SHADER_BIT : VkPipelineStageFlagBits = 0x00000040;
pub const VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT : VkPipelineStageFlagBits = 0x00000080;
pub const VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT : VkPipelineStageFlagBits = 0x00000100;
pub const VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT : VkPipelineStageFlagBits = 0x00000200;
pub const VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT : VkPipelineStageFlagBits = 0x00000400;
pub const VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT : VkPipelineStageFlagBits = 0x00000800;
pub const VK_PIPELINE_STAGE_TRANSFER_BIT : VkPipelineStageFlagBits = 0x00001000;
pub const VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT : VkPipelineStageFlagBits = 0x00002000;
pub const VK_PIPELINE_STAGE_HOST_BIT : VkPipelineStageFlagBits = 0x00004000;
pub const VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT : VkPipelineStageFlagBits = 0x00008000;
pub const VK_PIPELINE_STAGE_ALL_COMMANDS_BIT : VkPipelineStageFlagBits = 0x00010000;
pub const VK_PIPELINE_STAGE_TRANSFORM_FEEDBACK_BIT_EXT : VkPipelineStageFlagBits = 0x01000000;
pub const VK_PIPELINE_STAGE_CONDITIONAL_RENDERING_BIT_EXT : VkPipelineStageFlagBits = 0x00040000;
pub const VK_PIPELINE_STAGE_COMMAND_PROCESS_BIT_NVX : VkPipelineStageFlagBits = 0x00020000;
pub const VK_PIPELINE_STAGE_SHADING_RATE_IMAGE_BIT_NV : VkPipelineStageFlagBits = 0x00400000;
pub const VK_PIPELINE_STAGE_RAY_TRACING_SHADER_BIT_NV : VkPipelineStageFlagBits = 0x00200000;
pub const VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_NV : VkPipelineStageFlagBits = 0x02000000;
pub const VK_PIPELINE_STAGE_TASK_SHADER_BIT_NV : VkPipelineStageFlagBits = 0x00080000;
pub const VK_PIPELINE_STAGE_MESH_SHADER_BIT_NV : VkPipelineStageFlagBits = 0x00100000;
pub const VK_PIPELINE_STAGE_FRAGMENT_DENSITY_PROCESS_BIT_EXT : VkPipelineStageFlagBits = 0x00800000;
pub const VK_PIPELINE_STAGE_FLAG_BITS_MAX_ENUM : VkPipelineStageFlagBits = 0x7FFFFFFF;

pub const VK_STRUCTURE_TYPE_APPLICATION_INFO : VkStructureType = 0;
pub const VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO : VkStructureType = 1;
pub const VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO : VkStructureType = 2;
pub const VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO : VkStructureType = 3;
pub const VK_STRUCTURE_TYPE_SUBMIT_INFO : VkStructureType = 4;
pub const VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO : VkStructureType = 5;
pub const VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE : VkStructureType = 6;
pub const VK_STRUCTURE_TYPE_BIND_SPARSE_INFO : VkStructureType = 7;
pub const VK_STRUCTURE_TYPE_FENCE_CREATE_INFO : VkStructureType = 8;
pub const VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO : VkStructureType = 9;
pub const VK_STRUCTURE_TYPE_EVENT_CREATE_INFO : VkStructureType = 10;
pub const VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO : VkStructureType = 11;
pub const VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO : VkStructureType = 12;
pub const VK_STRUCTURE_TYPE_BUFFER_VIEW_CREATE_INFO : VkStructureType = 13;
pub const VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO : VkStructureType = 14;
pub const VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO : VkStructureType = 15;
pub const VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO : VkStructureType = 16;
pub const VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO : VkStructureType = 17;
pub const VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO : VkStructureType = 18;
pub const VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO : VkStructureType = 19;
pub const VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO : VkStructureType = 20;
pub const VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO : VkStructureType = 21;
pub const VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO : VkStructureType = 22;
pub const VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO : VkStructureType = 23;
pub const VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO : VkStructureType = 24;
pub const VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO : VkStructureType = 25;
pub const VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO : VkStructureType = 26;
pub const VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO : VkStructureType = 27;
pub const VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO : VkStructureType = 28;
pub const VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO : VkStructureType = 29;
pub const VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO : VkStructureType = 30;
pub const VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO : VkStructureType = 31;
pub const VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO : VkStructureType = 32;
pub const VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO : VkStructureType = 33;
pub const VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO : VkStructureType = 34;
pub const VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET : VkStructureType = 35;
pub const VK_STRUCTURE_TYPE_COPY_DESCRIPTOR_SET : VkStructureType = 36;
pub const VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO : VkStructureType = 37;
pub const VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO : VkStructureType = 38;
pub const VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO : VkStructureType = 39;
pub const VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO : VkStructureType = 40;
pub const VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO : VkStructureType = 41;
pub const VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO : VkStructureType = 42;
pub const VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO : VkStructureType = 43;
pub const VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER : VkStructureType = 44;
pub const VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER : VkStructureType = 45;
pub const VK_STRUCTURE_TYPE_MEMORY_BARRIER : VkStructureType = 46;
pub const VK_STRUCTURE_TYPE_LOADER_INSTANCE_CREATE_INFO : VkStructureType = 47;
pub const VK_STRUCTURE_TYPE_LOADER_DEVICE_CREATE_INFO : VkStructureType = 48;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBGROUP_PROPERTIES : VkStructureType = 1000094000;
pub const VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_INFO : VkStructureType = 1000157000;
pub const VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_INFO : VkStructureType = 1000157001;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_16BIT_STORAGE_FEATURES : VkStructureType = 1000083000;
pub const VK_STRUCTURE_TYPE_MEMORY_DEDICATED_REQUIREMENTS : VkStructureType = 1000127000;
pub const VK_STRUCTURE_TYPE_MEMORY_DEDICATED_ALLOCATE_INFO : VkStructureType = 1000127001;
pub const VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO : VkStructureType = 1000060000;
pub const VK_STRUCTURE_TYPE_DEVICE_GROUP_RENDER_PASS_BEGIN_INFO : VkStructureType = 1000060003;
pub const VK_STRUCTURE_TYPE_DEVICE_GROUP_COMMAND_BUFFER_BEGIN_INFO : VkStructureType = 1000060004;
pub const VK_STRUCTURE_TYPE_DEVICE_GROUP_SUBMIT_INFO : VkStructureType = 1000060005;
pub const VK_STRUCTURE_TYPE_DEVICE_GROUP_BIND_SPARSE_INFO : VkStructureType = 1000060006;
pub const VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_DEVICE_GROUP_INFO : VkStructureType = 1000060013;
pub const VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_DEVICE_GROUP_INFO : VkStructureType = 1000060014;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_GROUP_PROPERTIES : VkStructureType = 1000070000;
pub const VK_STRUCTURE_TYPE_DEVICE_GROUP_DEVICE_CREATE_INFO : VkStructureType = 1000070001;
pub const VK_STRUCTURE_TYPE_BUFFER_MEMORY_REQUIREMENTS_INFO_2 : VkStructureType = 1000146000;
pub const VK_STRUCTURE_TYPE_IMAGE_MEMORY_REQUIREMENTS_INFO_2 : VkStructureType = 1000146001;
pub const VK_STRUCTURE_TYPE_IMAGE_SPARSE_MEMORY_REQUIREMENTS_INFO_2 : VkStructureType = 1000146002;
pub const VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2 : VkStructureType = 1000146003;
pub const VK_STRUCTURE_TYPE_SPARSE_IMAGE_MEMORY_REQUIREMENTS_2 : VkStructureType = 1000146004;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2 : VkStructureType = 1000059000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2 : VkStructureType = 1000059001;
pub const VK_STRUCTURE_TYPE_FORMAT_PROPERTIES_2 : VkStructureType = 1000059002;
pub const VK_STRUCTURE_TYPE_IMAGE_FORMAT_PROPERTIES_2 : VkStructureType = 1000059003;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_FORMAT_INFO_2 : VkStructureType = 1000059004;
pub const VK_STRUCTURE_TYPE_QUEUE_FAMILY_PROPERTIES_2 : VkStructureType = 1000059005;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PROPERTIES_2 : VkStructureType = 1000059006;
pub const VK_STRUCTURE_TYPE_SPARSE_IMAGE_FORMAT_PROPERTIES_2 : VkStructureType = 1000059007;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SPARSE_IMAGE_FORMAT_INFO_2 : VkStructureType = 1000059008;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_POINT_CLIPPING_PROPERTIES : VkStructureType = 1000117000;
pub const VK_STRUCTURE_TYPE_RENDER_PASS_INPUT_ATTACHMENT_ASPECT_CREATE_INFO : VkStructureType = 1000117001;
pub const VK_STRUCTURE_TYPE_IMAGE_VIEW_USAGE_CREATE_INFO : VkStructureType = 1000117002;
pub const VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_DOMAIN_ORIGIN_STATE_CREATE_INFO : VkStructureType = 1000117003;
pub const VK_STRUCTURE_TYPE_RENDER_PASS_MULTIVIEW_CREATE_INFO : VkStructureType = 1000053000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_FEATURES : VkStructureType = 1000053001;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_PROPERTIES : VkStructureType = 1000053002;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VARIABLE_POINTERS_FEATURES : VkStructureType = 1000120000;
pub const VK_STRUCTURE_TYPE_PROTECTED_SUBMIT_INFO : VkStructureType = 1000145000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROTECTED_MEMORY_FEATURES : VkStructureType = 1000145001;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROTECTED_MEMORY_PROPERTIES : VkStructureType = 1000145002;
pub const VK_STRUCTURE_TYPE_DEVICE_QUEUE_INFO_2 : VkStructureType = 1000145003;
pub const VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_CREATE_INFO : VkStructureType = 1000156000;
pub const VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_INFO : VkStructureType = 1000156001;
pub const VK_STRUCTURE_TYPE_BIND_IMAGE_PLANE_MEMORY_INFO : VkStructureType = 1000156002;
pub const VK_STRUCTURE_TYPE_IMAGE_PLANE_MEMORY_REQUIREMENTS_INFO : VkStructureType = 1000156003;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_YCBCR_CONVERSION_FEATURES : VkStructureType = 1000156004;
pub const VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_IMAGE_FORMAT_PROPERTIES : VkStructureType = 1000156005;
pub const VK_STRUCTURE_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_CREATE_INFO : VkStructureType = 1000085000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_IMAGE_FORMAT_INFO : VkStructureType = 1000071000;
pub const VK_STRUCTURE_TYPE_EXTERNAL_IMAGE_FORMAT_PROPERTIES : VkStructureType = 1000071001;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_BUFFER_INFO : VkStructureType = 1000071002;
pub const VK_STRUCTURE_TYPE_EXTERNAL_BUFFER_PROPERTIES : VkStructureType = 1000071003;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ID_PROPERTIES : VkStructureType = 1000071004;
pub const VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_BUFFER_CREATE_INFO : VkStructureType = 1000072000;
pub const VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_IMAGE_CREATE_INFO : VkStructureType = 1000072001;
pub const VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO : VkStructureType = 1000072002;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_FENCE_INFO : VkStructureType = 1000112000;
pub const VK_STRUCTURE_TYPE_EXTERNAL_FENCE_PROPERTIES : VkStructureType = 1000112001;
pub const VK_STRUCTURE_TYPE_EXPORT_FENCE_CREATE_INFO : VkStructureType = 1000113000;
pub const VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_CREATE_INFO : VkStructureType = 1000077000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_SEMAPHORE_INFO : VkStructureType = 1000076000;
pub const VK_STRUCTURE_TYPE_EXTERNAL_SEMAPHORE_PROPERTIES : VkStructureType = 1000076001;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_3_PROPERTIES : VkStructureType = 1000168000;
pub const VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_SUPPORT : VkStructureType = 1000168001;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_DRAW_PARAMETERS_FEATURES : VkStructureType = 1000063000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_FEATURES : VkStructureType = 49;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_PROPERTIES : VkStructureType = 50;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES : VkStructureType = 51;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_PROPERTIES : VkStructureType = 52;
pub const VK_STRUCTURE_TYPE_IMAGE_FORMAT_LIST_CREATE_INFO : VkStructureType = 1000147000;
pub const VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_2 : VkStructureType = 1000109000;
pub const VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2 : VkStructureType = 1000109001;
pub const VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_2 : VkStructureType = 1000109002;
pub const VK_STRUCTURE_TYPE_SUBPASS_DEPENDENCY_2 : VkStructureType = 1000109003;
pub const VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2 : VkStructureType = 1000109004;
pub const VK_STRUCTURE_TYPE_SUBPASS_BEGIN_INFO : VkStructureType = 1000109005;
pub const VK_STRUCTURE_TYPE_SUBPASS_END_INFO : VkStructureType = 1000109006;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_8BIT_STORAGE_FEATURES : VkStructureType = 1000177000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DRIVER_PROPERTIES : VkStructureType = 1000196000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_INT64_FEATURES : VkStructureType = 1000180000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_FLOAT16_INT8_FEATURES : VkStructureType = 1000082000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FLOAT_CONTROLS_PROPERTIES : VkStructureType = 1000197000;
pub const VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO : VkStructureType = 1000161000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES : VkStructureType = 1000161001;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_PROPERTIES : VkStructureType = 1000161002;
pub const VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_ALLOCATE_INFO : VkStructureType = 1000161003;
pub const VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_LAYOUT_SUPPORT : VkStructureType = 1000161004;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEPTH_STENCIL_RESOLVE_PROPERTIES : VkStructureType = 1000199000;
pub const VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_DEPTH_STENCIL_RESOLVE : VkStructureType = 1000199001;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SCALAR_BLOCK_LAYOUT_FEATURES : VkStructureType = 1000221000;
pub const VK_STRUCTURE_TYPE_IMAGE_STENCIL_USAGE_CREATE_INFO : VkStructureType = 1000246000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_FILTER_MINMAX_PROPERTIES : VkStructureType = 1000130000;
pub const VK_STRUCTURE_TYPE_SAMPLER_REDUCTION_MODE_CREATE_INFO : VkStructureType = 1000130001;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_MEMORY_MODEL_FEATURES : VkStructureType = 1000211000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGELESS_FRAMEBUFFER_FEATURES : VkStructureType = 1000108000;
pub const VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENTS_CREATE_INFO : VkStructureType = 1000108001;
pub const VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENT_IMAGE_INFO : VkStructureType = 1000108002;
pub const VK_STRUCTURE_TYPE_RENDER_PASS_ATTACHMENT_BEGIN_INFO : VkStructureType = 1000108003;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_UNIFORM_BUFFER_STANDARD_LAYOUT_FEATURES : VkStructureType = 1000253000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SUBGROUP_EXTENDED_TYPES_FEATURES : VkStructureType = 1000175000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SEPARATE_DEPTH_STENCIL_LAYOUTS_FEATURES : VkStructureType = 1000241000;
pub const VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_STENCIL_LAYOUT : VkStructureType = 1000241001;
pub const VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_STENCIL_LAYOUT : VkStructureType = 1000241002;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_HOST_QUERY_RESET_FEATURES : VkStructureType = 1000261000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_FEATURES : VkStructureType = 1000207000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_PROPERTIES : VkStructureType = 1000207001;
pub const VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO : VkStructureType = 1000207002;
pub const VK_STRUCTURE_TYPE_TIMELINE_SEMAPHORE_SUBMIT_INFO : VkStructureType = 1000207003;
pub const VK_STRUCTURE_TYPE_SEMAPHORE_WAIT_INFO : VkStructureType = 1000207004;
pub const VK_STRUCTURE_TYPE_SEMAPHORE_SIGNAL_INFO : VkStructureType = 1000207005;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES : VkStructureType = 1000257000;
pub const VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO : VkStructureType = 1000244001;
pub const VK_STRUCTURE_TYPE_BUFFER_OPAQUE_CAPTURE_ADDRESS_CREATE_INFO : VkStructureType = 1000257002;
pub const VK_STRUCTURE_TYPE_MEMORY_OPAQUE_CAPTURE_ADDRESS_ALLOCATE_INFO : VkStructureType = 1000257003;
pub const VK_STRUCTURE_TYPE_DEVICE_MEMORY_OPAQUE_CAPTURE_ADDRESS_INFO : VkStructureType = 1000257004;
pub const VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR : VkStructureType = 1000001000;
pub const VK_STRUCTURE_TYPE_PRESENT_INFO_KHR : VkStructureType = 1000001001;
pub const VK_STRUCTURE_TYPE_DEVICE_GROUP_PRESENT_CAPABILITIES_KHR : VkStructureType = 1000060007;
pub const VK_STRUCTURE_TYPE_IMAGE_SWAPCHAIN_CREATE_INFO_KHR : VkStructureType = 1000060008;
pub const VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_SWAPCHAIN_INFO_KHR : VkStructureType = 1000060009;
pub const VK_STRUCTURE_TYPE_ACQUIRE_NEXT_IMAGE_INFO_KHR : VkStructureType = 1000060010;
pub const VK_STRUCTURE_TYPE_DEVICE_GROUP_PRESENT_INFO_KHR : VkStructureType = 1000060011;
pub const VK_STRUCTURE_TYPE_DEVICE_GROUP_SWAPCHAIN_CREATE_INFO_KHR : VkStructureType = 1000060012;
pub const VK_STRUCTURE_TYPE_DISPLAY_MODE_CREATE_INFO_KHR : VkStructureType = 1000002000;
pub const VK_STRUCTURE_TYPE_DISPLAY_SURFACE_CREATE_INFO_KHR : VkStructureType = 1000002001;
pub const VK_STRUCTURE_TYPE_DISPLAY_PRESENT_INFO_KHR : VkStructureType = 1000003000;
pub const VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR : VkStructureType = 1000004000;
pub const VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR : VkStructureType = 1000005000;
pub const VK_STRUCTURE_TYPE_WAYLAND_SURFACE_CREATE_INFO_KHR : VkStructureType = 1000006000;
pub const VK_STRUCTURE_TYPE_ANDROID_SURFACE_CREATE_INFO_KHR : VkStructureType = 1000008000;
pub const VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR : VkStructureType = 1000009000;
pub const VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT : VkStructureType = 1000011000;
pub const VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_RASTERIZATION_ORDER_AMD : VkStructureType = 1000018000;
pub const VK_STRUCTURE_TYPE_DEBUG_MARKER_OBJECT_NAME_INFO_EXT : VkStructureType = 1000022000;
pub const VK_STRUCTURE_TYPE_DEBUG_MARKER_OBJECT_TAG_INFO_EXT : VkStructureType = 1000022001;
pub const VK_STRUCTURE_TYPE_DEBUG_MARKER_MARKER_INFO_EXT : VkStructureType = 1000022002;
pub const VK_STRUCTURE_TYPE_DEDICATED_ALLOCATION_IMAGE_CREATE_INFO_NV : VkStructureType = 1000026000;
pub const VK_STRUCTURE_TYPE_DEDICATED_ALLOCATION_BUFFER_CREATE_INFO_NV : VkStructureType = 1000026001;
pub const VK_STRUCTURE_TYPE_DEDICATED_ALLOCATION_MEMORY_ALLOCATE_INFO_NV : VkStructureType = 1000026002;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TRANSFORM_FEEDBACK_FEATURES_EXT : VkStructureType = 1000028000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TRANSFORM_FEEDBACK_PROPERTIES_EXT : VkStructureType = 1000028001;
pub const VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_STREAM_CREATE_INFO_EXT : VkStructureType = 1000028002;
pub const VK_STRUCTURE_TYPE_IMAGE_VIEW_HANDLE_INFO_NVX : VkStructureType = 1000030000;
pub const VK_STRUCTURE_TYPE_TEXTURE_LOD_GATHER_FORMAT_PROPERTIES_AMD : VkStructureType = 1000041000;
pub const VK_STRUCTURE_TYPE_STREAM_DESCRIPTOR_SURFACE_CREATE_INFO_GGP : VkStructureType = 1000049000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CORNER_SAMPLED_IMAGE_FEATURES_NV : VkStructureType = 1000050000;
pub const VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_IMAGE_CREATE_INFO_NV : VkStructureType = 1000056000;
pub const VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO_NV : VkStructureType = 1000056001;
pub const VK_STRUCTURE_TYPE_IMPORT_MEMORY_WIN32_HANDLE_INFO_NV : VkStructureType = 1000057000;
pub const VK_STRUCTURE_TYPE_EXPORT_MEMORY_WIN32_HANDLE_INFO_NV : VkStructureType = 1000057001;
pub const VK_STRUCTURE_TYPE_WIN32_KEYED_MUTEX_ACQUIRE_RELEASE_INFO_NV : VkStructureType = 1000058000;
pub const VK_STRUCTURE_TYPE_VALIDATION_FLAGS_EXT : VkStructureType = 1000061000;
pub const VK_STRUCTURE_TYPE_VI_SURFACE_CREATE_INFO_NN : VkStructureType = 1000062000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TEXTURE_COMPRESSION_ASTC_HDR_FEATURES_EXT : VkStructureType = 1000066000;
pub const VK_STRUCTURE_TYPE_IMAGE_VIEW_ASTC_DECODE_MODE_EXT : VkStructureType = 1000067000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ASTC_DECODE_FEATURES_EXT : VkStructureType = 1000067001;
pub const VK_STRUCTURE_TYPE_IMPORT_MEMORY_WIN32_HANDLE_INFO_KHR : VkStructureType = 1000073000;
pub const VK_STRUCTURE_TYPE_EXPORT_MEMORY_WIN32_HANDLE_INFO_KHR : VkStructureType = 1000073001;
pub const VK_STRUCTURE_TYPE_MEMORY_WIN32_HANDLE_PROPERTIES_KHR : VkStructureType = 1000073002;
pub const VK_STRUCTURE_TYPE_MEMORY_GET_WIN32_HANDLE_INFO_KHR : VkStructureType = 1000073003;
pub const VK_STRUCTURE_TYPE_IMPORT_MEMORY_FD_INFO_KHR : VkStructureType = 1000074000;
pub const VK_STRUCTURE_TYPE_MEMORY_FD_PROPERTIES_KHR : VkStructureType = 1000074001;
pub const VK_STRUCTURE_TYPE_MEMORY_GET_FD_INFO_KHR : VkStructureType = 1000074002;
pub const VK_STRUCTURE_TYPE_WIN32_KEYED_MUTEX_ACQUIRE_RELEASE_INFO_KHR : VkStructureType = 1000075000;
pub const VK_STRUCTURE_TYPE_IMPORT_SEMAPHORE_WIN32_HANDLE_INFO_KHR : VkStructureType = 1000078000;
pub const VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_WIN32_HANDLE_INFO_KHR : VkStructureType = 1000078001;
pub const VK_STRUCTURE_TYPE_D3D12_FENCE_SUBMIT_INFO_KHR : VkStructureType = 1000078002;
pub const VK_STRUCTURE_TYPE_SEMAPHORE_GET_WIN32_HANDLE_INFO_KHR : VkStructureType = 1000078003;
pub const VK_STRUCTURE_TYPE_IMPORT_SEMAPHORE_FD_INFO_KHR : VkStructureType = 1000079000;
pub const VK_STRUCTURE_TYPE_SEMAPHORE_GET_FD_INFO_KHR : VkStructureType = 1000079001;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PUSH_DESCRIPTOR_PROPERTIES_KHR : VkStructureType = 1000080000;
pub const VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_CONDITIONAL_RENDERING_INFO_EXT : VkStructureType = 1000081000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CONDITIONAL_RENDERING_FEATURES_EXT : VkStructureType = 1000081001;
pub const VK_STRUCTURE_TYPE_CONDITIONAL_RENDERING_BEGIN_INFO_EXT : VkStructureType = 1000081002;
pub const VK_STRUCTURE_TYPE_PRESENT_REGIONS_KHR : VkStructureType = 1000084000;
pub const VK_STRUCTURE_TYPE_OBJECT_TABLE_CREATE_INFO_NVX : VkStructureType = 1000086000;
pub const VK_STRUCTURE_TYPE_INDIRECT_COMMANDS_LAYOUT_CREATE_INFO_NVX : VkStructureType = 1000086001;
pub const VK_STRUCTURE_TYPE_CMD_PROCESS_COMMANDS_INFO_NVX : VkStructureType = 1000086002;
pub const VK_STRUCTURE_TYPE_CMD_RESERVE_SPACE_FOR_COMMANDS_INFO_NVX : VkStructureType = 1000086003;
pub const VK_STRUCTURE_TYPE_DEVICE_GENERATED_COMMANDS_LIMITS_NVX : VkStructureType = 1000086004;
pub const VK_STRUCTURE_TYPE_DEVICE_GENERATED_COMMANDS_FEATURES_NVX : VkStructureType = 1000086005;
pub const VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_W_SCALING_STATE_CREATE_INFO_NV : VkStructureType = 1000087000;
pub const VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_2_EXT : VkStructureType = 1000090000;
pub const VK_STRUCTURE_TYPE_DISPLAY_POWER_INFO_EXT : VkStructureType = 1000091000;
pub const VK_STRUCTURE_TYPE_DEVICE_EVENT_INFO_EXT : VkStructureType = 1000091001;
pub const VK_STRUCTURE_TYPE_DISPLAY_EVENT_INFO_EXT : VkStructureType = 1000091002;
pub const VK_STRUCTURE_TYPE_SWAPCHAIN_COUNTER_CREATE_INFO_EXT : VkStructureType = 1000091003;
pub const VK_STRUCTURE_TYPE_PRESENT_TIMES_INFO_GOOGLE : VkStructureType = 1000092000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_PER_VIEW_ATTRIBUTES_PROPERTIES_NVX : VkStructureType = 1000097000;
pub const VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_SWIZZLE_STATE_CREATE_INFO_NV : VkStructureType = 1000098000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DISCARD_RECTANGLE_PROPERTIES_EXT : VkStructureType = 1000099000;
pub const VK_STRUCTURE_TYPE_PIPELINE_DISCARD_RECTANGLE_STATE_CREATE_INFO_EXT : VkStructureType = 1000099001;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_CONSERVATIVE_RASTERIZATION_PROPERTIES_EXT : VkStructureType = 1000101000;
pub const VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_CONSERVATIVE_STATE_CREATE_INFO_EXT : VkStructureType = 1000101001;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEPTH_CLIP_ENABLE_FEATURES_EXT : VkStructureType = 1000102000;
pub const VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_DEPTH_CLIP_STATE_CREATE_INFO_EXT : VkStructureType = 1000102001;
pub const VK_STRUCTURE_TYPE_HDR_METADATA_EXT : VkStructureType = 1000105000;
pub const VK_STRUCTURE_TYPE_SHARED_PRESENT_SURFACE_CAPABILITIES_KHR : VkStructureType = 1000111000;
pub const VK_STRUCTURE_TYPE_IMPORT_FENCE_WIN32_HANDLE_INFO_KHR : VkStructureType = 1000114000;
pub const VK_STRUCTURE_TYPE_EXPORT_FENCE_WIN32_HANDLE_INFO_KHR : VkStructureType = 1000114001;
pub const VK_STRUCTURE_TYPE_FENCE_GET_WIN32_HANDLE_INFO_KHR : VkStructureType = 1000114002;
pub const VK_STRUCTURE_TYPE_IMPORT_FENCE_FD_INFO_KHR : VkStructureType = 1000115000;
pub const VK_STRUCTURE_TYPE_FENCE_GET_FD_INFO_KHR : VkStructureType = 1000115001;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PERFORMANCE_QUERY_FEATURES_KHR : VkStructureType = 1000116000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PERFORMANCE_QUERY_PROPERTIES_KHR : VkStructureType = 1000116001;
pub const VK_STRUCTURE_TYPE_QUERY_POOL_PERFORMANCE_CREATE_INFO_KHR : VkStructureType = 1000116002;
pub const VK_STRUCTURE_TYPE_PERFORMANCE_QUERY_SUBMIT_INFO_KHR : VkStructureType = 1000116003;
pub const VK_STRUCTURE_TYPE_ACQUIRE_PROFILING_LOCK_INFO_KHR : VkStructureType = 1000116004;
pub const VK_STRUCTURE_TYPE_PERFORMANCE_COUNTER_KHR : VkStructureType = 1000116005;
pub const VK_STRUCTURE_TYPE_PERFORMANCE_COUNTER_DESCRIPTION_KHR : VkStructureType = 1000116006;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SURFACE_INFO_2_KHR : VkStructureType = 1000119000;
pub const VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_2_KHR : VkStructureType = 1000119001;
pub const VK_STRUCTURE_TYPE_SURFACE_FORMAT_2_KHR : VkStructureType = 1000119002;
pub const VK_STRUCTURE_TYPE_DISPLAY_PROPERTIES_2_KHR : VkStructureType = 1000121000;
pub const VK_STRUCTURE_TYPE_DISPLAY_PLANE_PROPERTIES_2_KHR : VkStructureType = 1000121001;
pub const VK_STRUCTURE_TYPE_DISPLAY_MODE_PROPERTIES_2_KHR : VkStructureType = 1000121002;
pub const VK_STRUCTURE_TYPE_DISPLAY_PLANE_INFO_2_KHR : VkStructureType = 1000121003;
pub const VK_STRUCTURE_TYPE_DISPLAY_PLANE_CAPABILITIES_2_KHR : VkStructureType = 1000121004;
pub const VK_STRUCTURE_TYPE_IOS_SURFACE_CREATE_INFO_MVK : VkStructureType = 1000122000;
pub const VK_STRUCTURE_TYPE_MACOS_SURFACE_CREATE_INFO_MVK : VkStructureType = 1000123000;
pub const VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT : VkStructureType = 1000128000;
pub const VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_TAG_INFO_EXT : VkStructureType = 1000128001;
pub const VK_STRUCTURE_TYPE_DEBUG_UTILS_LABEL_EXT : VkStructureType = 1000128002;
pub const VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CALLBACK_DATA_EXT : VkStructureType = 1000128003;
pub const VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT : VkStructureType = 1000128004;
pub const VK_STRUCTURE_TYPE_ANDROID_HARDWARE_BUFFER_USAGE_ANDROID : VkStructureType = 1000129000;
pub const VK_STRUCTURE_TYPE_ANDROID_HARDWARE_BUFFER_PROPERTIES_ANDROID : VkStructureType = 1000129001;
pub const VK_STRUCTURE_TYPE_ANDROID_HARDWARE_BUFFER_FORMAT_PROPERTIES_ANDROID : VkStructureType = 1000129002;
pub const VK_STRUCTURE_TYPE_IMPORT_ANDROID_HARDWARE_BUFFER_INFO_ANDROID : VkStructureType = 1000129003;
pub const VK_STRUCTURE_TYPE_MEMORY_GET_ANDROID_HARDWARE_BUFFER_INFO_ANDROID : VkStructureType = 1000129004;
pub const VK_STRUCTURE_TYPE_EXTERNAL_FORMAT_ANDROID : VkStructureType = 1000129005;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INLINE_UNIFORM_BLOCK_FEATURES_EXT : VkStructureType = 1000138000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INLINE_UNIFORM_BLOCK_PROPERTIES_EXT : VkStructureType = 1000138001;
pub const VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_INLINE_UNIFORM_BLOCK_EXT : VkStructureType = 1000138002;
pub const VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_INLINE_UNIFORM_BLOCK_CREATE_INFO_EXT : VkStructureType = 1000138003;
pub const VK_STRUCTURE_TYPE_SAMPLE_LOCATIONS_INFO_EXT : VkStructureType = 1000143000;
pub const VK_STRUCTURE_TYPE_RENDER_PASS_SAMPLE_LOCATIONS_BEGIN_INFO_EXT : VkStructureType = 1000143001;
pub const VK_STRUCTURE_TYPE_PIPELINE_SAMPLE_LOCATIONS_STATE_CREATE_INFO_EXT : VkStructureType = 1000143002;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLE_LOCATIONS_PROPERTIES_EXT : VkStructureType = 1000143003;
pub const VK_STRUCTURE_TYPE_MULTISAMPLE_PROPERTIES_EXT : VkStructureType = 1000143004;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BLEND_OPERATION_ADVANCED_FEATURES_EXT : VkStructureType = 1000148000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BLEND_OPERATION_ADVANCED_PROPERTIES_EXT : VkStructureType = 1000148001;
pub const VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_ADVANCED_STATE_CREATE_INFO_EXT : VkStructureType = 1000148002;
pub const VK_STRUCTURE_TYPE_PIPELINE_COVERAGE_TO_COLOR_STATE_CREATE_INFO_NV : VkStructureType = 1000149000;
pub const VK_STRUCTURE_TYPE_PIPELINE_COVERAGE_MODULATION_STATE_CREATE_INFO_NV : VkStructureType = 1000152000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SM_BUILTINS_FEATURES_NV : VkStructureType = 1000154000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SM_BUILTINS_PROPERTIES_NV : VkStructureType = 1000154001;
pub const VK_STRUCTURE_TYPE_DRM_FORMAT_MODIFIER_PROPERTIES_LIST_EXT : VkStructureType = 1000158000;
pub const VK_STRUCTURE_TYPE_DRM_FORMAT_MODIFIER_PROPERTIES_EXT : VkStructureType = 1000158001;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_DRM_FORMAT_MODIFIER_INFO_EXT : VkStructureType = 1000158002;
pub const VK_STRUCTURE_TYPE_IMAGE_DRM_FORMAT_MODIFIER_LIST_CREATE_INFO_EXT : VkStructureType = 1000158003;
pub const VK_STRUCTURE_TYPE_IMAGE_DRM_FORMAT_MODIFIER_EXPLICIT_CREATE_INFO_EXT : VkStructureType = 1000158004;
pub const VK_STRUCTURE_TYPE_IMAGE_DRM_FORMAT_MODIFIER_PROPERTIES_EXT : VkStructureType = 1000158005;
pub const VK_STRUCTURE_TYPE_VALIDATION_CACHE_CREATE_INFO_EXT : VkStructureType = 1000160000;
pub const VK_STRUCTURE_TYPE_SHADER_MODULE_VALIDATION_CACHE_CREATE_INFO_EXT : VkStructureType = 1000160001;
pub const VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_SHADING_RATE_IMAGE_STATE_CREATE_INFO_NV : VkStructureType = 1000164000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADING_RATE_IMAGE_FEATURES_NV : VkStructureType = 1000164001;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADING_RATE_IMAGE_PROPERTIES_NV : VkStructureType = 1000164002;
pub const VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_COARSE_SAMPLE_ORDER_STATE_CREATE_INFO_NV : VkStructureType = 1000164005;
pub const VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_CREATE_INFO_NV : VkStructureType = 1000165000;
pub const VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_NV : VkStructureType = 1000165001;
pub const VK_STRUCTURE_TYPE_GEOMETRY_NV : VkStructureType = 1000165003;
pub const VK_STRUCTURE_TYPE_GEOMETRY_TRIANGLES_NV : VkStructureType = 1000165004;
pub const VK_STRUCTURE_TYPE_GEOMETRY_AABB_NV : VkStructureType = 1000165005;
pub const VK_STRUCTURE_TYPE_BIND_ACCELERATION_STRUCTURE_MEMORY_INFO_NV : VkStructureType = 1000165006;
pub const VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_NV : VkStructureType = 1000165007;
pub const VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_INFO_NV : VkStructureType = 1000165008;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PROPERTIES_NV : VkStructureType = 1000165009;
pub const VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_NV : VkStructureType = 1000165011;
pub const VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_INFO_NV : VkStructureType = 1000165012;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_REPRESENTATIVE_FRAGMENT_TEST_FEATURES_NV : VkStructureType = 1000166000;
pub const VK_STRUCTURE_TYPE_PIPELINE_REPRESENTATIVE_FRAGMENT_TEST_STATE_CREATE_INFO_NV : VkStructureType = 1000166001;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_VIEW_IMAGE_FORMAT_INFO_EXT : VkStructureType = 1000170000;
pub const VK_STRUCTURE_TYPE_FILTER_CUBIC_IMAGE_VIEW_IMAGE_FORMAT_PROPERTIES_EXT : VkStructureType = 1000170001;
pub const VK_STRUCTURE_TYPE_DEVICE_QUEUE_GLOBAL_PRIORITY_CREATE_INFO_EXT : VkStructureType = 1000174000;
pub const VK_STRUCTURE_TYPE_IMPORT_MEMORY_HOST_POINTER_INFO_EXT : VkStructureType = 1000178000;
pub const VK_STRUCTURE_TYPE_MEMORY_HOST_POINTER_PROPERTIES_EXT : VkStructureType = 1000178001;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_MEMORY_HOST_PROPERTIES_EXT : VkStructureType = 1000178002;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_CLOCK_FEATURES_KHR : VkStructureType = 1000181000;
pub const VK_STRUCTURE_TYPE_PIPELINE_COMPILER_CONTROL_CREATE_INFO_AMD : VkStructureType = 1000183000;
pub const VK_STRUCTURE_TYPE_CALIBRATED_TIMESTAMP_INFO_EXT : VkStructureType = 1000184000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_CORE_PROPERTIES_AMD : VkStructureType = 1000185000;
pub const VK_STRUCTURE_TYPE_DEVICE_MEMORY_OVERALLOCATION_CREATE_INFO_AMD : VkStructureType = 1000189000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VERTEX_ATTRIBUTE_DIVISOR_PROPERTIES_EXT : VkStructureType = 1000190000;
pub const VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_DIVISOR_STATE_CREATE_INFO_EXT : VkStructureType = 1000190001;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VERTEX_ATTRIBUTE_DIVISOR_FEATURES_EXT : VkStructureType = 1000190002;
pub const VK_STRUCTURE_TYPE_PRESENT_FRAME_TOKEN_GGP : VkStructureType = 1000191000;
pub const VK_STRUCTURE_TYPE_PIPELINE_CREATION_FEEDBACK_CREATE_INFO_EXT : VkStructureType = 1000192000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COMPUTE_SHADER_DERIVATIVES_FEATURES_NV : VkStructureType = 1000201000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MESH_SHADER_FEATURES_NV : VkStructureType = 1000202000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MESH_SHADER_PROPERTIES_NV : VkStructureType = 1000202001;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADER_BARYCENTRIC_FEATURES_NV : VkStructureType = 1000203000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_IMAGE_FOOTPRINT_FEATURES_NV : VkStructureType = 1000204000;
pub const VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_EXCLUSIVE_SCISSOR_STATE_CREATE_INFO_NV : VkStructureType = 1000205000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXCLUSIVE_SCISSOR_FEATURES_NV : VkStructureType = 1000205002;
pub const VK_STRUCTURE_TYPE_CHECKPOINT_DATA_NV : VkStructureType = 1000206000;
pub const VK_STRUCTURE_TYPE_QUEUE_FAMILY_CHECKPOINT_PROPERTIES_NV : VkStructureType = 1000206001;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_INTEGER_FUNCTIONS_2_FEATURES_INTEL : VkStructureType = 1000209000;
pub const VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO_INTEL : VkStructureType = 1000210000;
pub const VK_STRUCTURE_TYPE_INITIALIZE_PERFORMANCE_API_INFO_INTEL : VkStructureType = 1000210001;
pub const VK_STRUCTURE_TYPE_PERFORMANCE_MARKER_INFO_INTEL : VkStructureType = 1000210002;
pub const VK_STRUCTURE_TYPE_PERFORMANCE_STREAM_MARKER_INFO_INTEL : VkStructureType = 1000210003;
pub const VK_STRUCTURE_TYPE_PERFORMANCE_OVERRIDE_INFO_INTEL : VkStructureType = 1000210004;
pub const VK_STRUCTURE_TYPE_PERFORMANCE_CONFIGURATION_ACQUIRE_INFO_INTEL : VkStructureType = 1000210005;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PCI_BUS_INFO_PROPERTIES_EXT : VkStructureType = 1000212000;
pub const VK_STRUCTURE_TYPE_DISPLAY_NATIVE_HDR_SURFACE_CAPABILITIES_AMD : VkStructureType = 1000213000;
pub const VK_STRUCTURE_TYPE_SWAPCHAIN_DISPLAY_NATIVE_HDR_CREATE_INFO_AMD : VkStructureType = 1000213001;
pub const VK_STRUCTURE_TYPE_IMAGEPIPE_SURFACE_CREATE_INFO_FUCHSIA : VkStructureType = 1000214000;
pub const VK_STRUCTURE_TYPE_METAL_SURFACE_CREATE_INFO_EXT : VkStructureType = 1000217000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_FEATURES_EXT : VkStructureType = 1000218000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_DENSITY_MAP_PROPERTIES_EXT : VkStructureType = 1000218001;
pub const VK_STRUCTURE_TYPE_RENDER_PASS_FRAGMENT_DENSITY_MAP_CREATE_INFO_EXT : VkStructureType = 1000218002;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBGROUP_SIZE_CONTROL_PROPERTIES_EXT : VkStructureType = 1000225000;
pub const VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_REQUIRED_SUBGROUP_SIZE_CREATE_INFO_EXT : VkStructureType = 1000225001;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBGROUP_SIZE_CONTROL_FEATURES_EXT : VkStructureType = 1000225002;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_CORE_PROPERTIES_2_AMD : VkStructureType = 1000227000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COHERENT_MEMORY_FEATURES_AMD : VkStructureType = 1000229000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_BUDGET_PROPERTIES_EXT : VkStructureType = 1000237000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PRIORITY_FEATURES_EXT : VkStructureType = 1000238000;
pub const VK_STRUCTURE_TYPE_MEMORY_PRIORITY_ALLOCATE_INFO_EXT : VkStructureType = 1000238001;
pub const VK_STRUCTURE_TYPE_SURFACE_PROTECTED_CAPABILITIES_KHR : VkStructureType = 1000239000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEDICATED_ALLOCATION_IMAGE_ALIASING_FEATURES_NV : VkStructureType = 1000240000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES_EXT : VkStructureType = 1000244000;
pub const VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_CREATE_INFO_EXT : VkStructureType = 1000244002;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TOOL_PROPERTIES_EXT : VkStructureType = 1000245000;
pub const VK_STRUCTURE_TYPE_VALIDATION_FEATURES_EXT : VkStructureType = 1000247000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COOPERATIVE_MATRIX_FEATURES_NV : VkStructureType = 1000249000;
pub const VK_STRUCTURE_TYPE_COOPERATIVE_MATRIX_PROPERTIES_NV : VkStructureType = 1000249001;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COOPERATIVE_MATRIX_PROPERTIES_NV : VkStructureType = 1000249002;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COVERAGE_REDUCTION_MODE_FEATURES_NV : VkStructureType = 1000250000;
pub const VK_STRUCTURE_TYPE_PIPELINE_COVERAGE_REDUCTION_STATE_CREATE_INFO_NV : VkStructureType = 1000250001;
pub const VK_STRUCTURE_TYPE_FRAMEBUFFER_MIXED_SAMPLES_COMBINATION_NV : VkStructureType = 1000250002;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FRAGMENT_SHADER_INTERLOCK_FEATURES_EXT : VkStructureType = 1000251000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_YCBCR_IMAGE_ARRAYS_FEATURES_EXT : VkStructureType = 1000252000;
pub const VK_STRUCTURE_TYPE_SURFACE_FULL_SCREEN_EXCLUSIVE_INFO_EXT : VkStructureType = 1000255000;
pub const VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_FULL_SCREEN_EXCLUSIVE_EXT : VkStructureType = 1000255002;
pub const VK_STRUCTURE_TYPE_SURFACE_FULL_SCREEN_EXCLUSIVE_WIN32_INFO_EXT : VkStructureType = 1000255001;
pub const VK_STRUCTURE_TYPE_HEADLESS_SURFACE_CREATE_INFO_EXT : VkStructureType = 1000256000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_LINE_RASTERIZATION_FEATURES_EXT : VkStructureType = 1000259000;
pub const VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_LINE_STATE_CREATE_INFO_EXT : VkStructureType = 1000259001;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_LINE_RASTERIZATION_PROPERTIES_EXT : VkStructureType = 1000259002;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_INDEX_TYPE_UINT8_FEATURES_EXT : VkStructureType = 1000265000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PIPELINE_EXECUTABLE_PROPERTIES_FEATURES_KHR : VkStructureType = 1000269000;
pub const VK_STRUCTURE_TYPE_PIPELINE_INFO_KHR : VkStructureType = 1000269001;
pub const VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_PROPERTIES_KHR : VkStructureType = 1000269002;
pub const VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_INFO_KHR : VkStructureType = 1000269003;
pub const VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_STATISTIC_KHR : VkStructureType = 1000269004;
pub const VK_STRUCTURE_TYPE_PIPELINE_EXECUTABLE_INTERNAL_REPRESENTATION_KHR : VkStructureType = 1000269005;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_DEMOTE_TO_HELPER_INVOCATION_FEATURES_EXT : VkStructureType = 1000276000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TEXEL_BUFFER_ALIGNMENT_FEATURES_EXT : VkStructureType = 1000281000;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TEXEL_BUFFER_ALIGNMENT_PROPERTIES_EXT : VkStructureType = 1000281001;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VARIABLE_POINTER_FEATURES : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VARIABLE_POINTERS_FEATURES;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_DRAW_PARAMETER_FEATURES : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_DRAW_PARAMETERS_FEATURES;
pub const VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT : VkStructureType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
pub const VK_STRUCTURE_TYPE_RENDER_PASS_MULTIVIEW_CREATE_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_RENDER_PASS_MULTIVIEW_CREATE_INFO;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_FEATURES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_FEATURES;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_PROPERTIES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_PROPERTIES;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
pub const VK_STRUCTURE_TYPE_FORMAT_PROPERTIES_2_KHR : VkStructureType = VK_STRUCTURE_TYPE_FORMAT_PROPERTIES_2;
pub const VK_STRUCTURE_TYPE_IMAGE_FORMAT_PROPERTIES_2_KHR : VkStructureType = VK_STRUCTURE_TYPE_IMAGE_FORMAT_PROPERTIES_2;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_FORMAT_INFO_2_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_FORMAT_INFO_2;
pub const VK_STRUCTURE_TYPE_QUEUE_FAMILY_PROPERTIES_2_KHR : VkStructureType = VK_STRUCTURE_TYPE_QUEUE_FAMILY_PROPERTIES_2;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PROPERTIES_2_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PROPERTIES_2;
pub const VK_STRUCTURE_TYPE_SPARSE_IMAGE_FORMAT_PROPERTIES_2_KHR : VkStructureType = VK_STRUCTURE_TYPE_SPARSE_IMAGE_FORMAT_PROPERTIES_2;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SPARSE_IMAGE_FORMAT_INFO_2_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SPARSE_IMAGE_FORMAT_INFO_2;
pub const VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO;
pub const VK_STRUCTURE_TYPE_DEVICE_GROUP_RENDER_PASS_BEGIN_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_DEVICE_GROUP_RENDER_PASS_BEGIN_INFO;
pub const VK_STRUCTURE_TYPE_DEVICE_GROUP_COMMAND_BUFFER_BEGIN_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_DEVICE_GROUP_COMMAND_BUFFER_BEGIN_INFO;
pub const VK_STRUCTURE_TYPE_DEVICE_GROUP_SUBMIT_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_DEVICE_GROUP_SUBMIT_INFO;
pub const VK_STRUCTURE_TYPE_DEVICE_GROUP_BIND_SPARSE_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_DEVICE_GROUP_BIND_SPARSE_INFO;
pub const VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_DEVICE_GROUP_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_DEVICE_GROUP_INFO;
pub const VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_DEVICE_GROUP_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_DEVICE_GROUP_INFO;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_GROUP_PROPERTIES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_GROUP_PROPERTIES;
pub const VK_STRUCTURE_TYPE_DEVICE_GROUP_DEVICE_CREATE_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_DEVICE_GROUP_DEVICE_CREATE_INFO;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_IMAGE_FORMAT_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_IMAGE_FORMAT_INFO;
pub const VK_STRUCTURE_TYPE_EXTERNAL_IMAGE_FORMAT_PROPERTIES_KHR : VkStructureType = VK_STRUCTURE_TYPE_EXTERNAL_IMAGE_FORMAT_PROPERTIES;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_BUFFER_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_BUFFER_INFO;
pub const VK_STRUCTURE_TYPE_EXTERNAL_BUFFER_PROPERTIES_KHR : VkStructureType = VK_STRUCTURE_TYPE_EXTERNAL_BUFFER_PROPERTIES;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ID_PROPERTIES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ID_PROPERTIES;
pub const VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_BUFFER_CREATE_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_BUFFER_CREATE_INFO;
pub const VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_IMAGE_CREATE_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_IMAGE_CREATE_INFO;
pub const VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_SEMAPHORE_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_SEMAPHORE_INFO;
pub const VK_STRUCTURE_TYPE_EXTERNAL_SEMAPHORE_PROPERTIES_KHR : VkStructureType = VK_STRUCTURE_TYPE_EXTERNAL_SEMAPHORE_PROPERTIES;
pub const VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_CREATE_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_CREATE_INFO;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_FLOAT16_INT8_FEATURES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_FLOAT16_INT8_FEATURES;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FLOAT16_INT8_FEATURES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_FLOAT16_INT8_FEATURES;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_16BIT_STORAGE_FEATURES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_16BIT_STORAGE_FEATURES;
pub const VK_STRUCTURE_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_CREATE_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_CREATE_INFO;
pub const VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES2_EXT : VkStructureType = VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_2_EXT;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGELESS_FRAMEBUFFER_FEATURES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGELESS_FRAMEBUFFER_FEATURES;
pub const VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENTS_CREATE_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENTS_CREATE_INFO;
pub const VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENT_IMAGE_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENT_IMAGE_INFO;
pub const VK_STRUCTURE_TYPE_RENDER_PASS_ATTACHMENT_BEGIN_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_RENDER_PASS_ATTACHMENT_BEGIN_INFO;
pub const VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_2_KHR : VkStructureType = VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_2;
pub const VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2_KHR : VkStructureType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2;
pub const VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_2_KHR : VkStructureType = VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_2;
pub const VK_STRUCTURE_TYPE_SUBPASS_DEPENDENCY_2_KHR : VkStructureType = VK_STRUCTURE_TYPE_SUBPASS_DEPENDENCY_2;
pub const VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2_KHR : VkStructureType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2;
pub const VK_STRUCTURE_TYPE_SUBPASS_BEGIN_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_SUBPASS_BEGIN_INFO;
pub const VK_STRUCTURE_TYPE_SUBPASS_END_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_SUBPASS_END_INFO;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_FENCE_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_FENCE_INFO;
pub const VK_STRUCTURE_TYPE_EXTERNAL_FENCE_PROPERTIES_KHR : VkStructureType = VK_STRUCTURE_TYPE_EXTERNAL_FENCE_PROPERTIES;
pub const VK_STRUCTURE_TYPE_EXPORT_FENCE_CREATE_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_EXPORT_FENCE_CREATE_INFO;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_POINT_CLIPPING_PROPERTIES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_POINT_CLIPPING_PROPERTIES;
pub const VK_STRUCTURE_TYPE_RENDER_PASS_INPUT_ATTACHMENT_ASPECT_CREATE_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_RENDER_PASS_INPUT_ATTACHMENT_ASPECT_CREATE_INFO;
pub const VK_STRUCTURE_TYPE_IMAGE_VIEW_USAGE_CREATE_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_IMAGE_VIEW_USAGE_CREATE_INFO;
pub const VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_DOMAIN_ORIGIN_STATE_CREATE_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_DOMAIN_ORIGIN_STATE_CREATE_INFO;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VARIABLE_POINTER_FEATURES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VARIABLE_POINTER_FEATURES;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VARIABLE_POINTERS_FEATURES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VARIABLE_POINTER_FEATURES;
pub const VK_STRUCTURE_TYPE_MEMORY_DEDICATED_REQUIREMENTS_KHR : VkStructureType = VK_STRUCTURE_TYPE_MEMORY_DEDICATED_REQUIREMENTS;
pub const VK_STRUCTURE_TYPE_MEMORY_DEDICATED_ALLOCATE_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_MEMORY_DEDICATED_ALLOCATE_INFO;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_FILTER_MINMAX_PROPERTIES_EXT : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_FILTER_MINMAX_PROPERTIES;
pub const VK_STRUCTURE_TYPE_SAMPLER_REDUCTION_MODE_CREATE_INFO_EXT : VkStructureType = VK_STRUCTURE_TYPE_SAMPLER_REDUCTION_MODE_CREATE_INFO;
pub const VK_STRUCTURE_TYPE_BUFFER_MEMORY_REQUIREMENTS_INFO_2_KHR : VkStructureType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_REQUIREMENTS_INFO_2;
pub const VK_STRUCTURE_TYPE_IMAGE_MEMORY_REQUIREMENTS_INFO_2_KHR : VkStructureType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_REQUIREMENTS_INFO_2;
pub const VK_STRUCTURE_TYPE_IMAGE_SPARSE_MEMORY_REQUIREMENTS_INFO_2_KHR : VkStructureType = VK_STRUCTURE_TYPE_IMAGE_SPARSE_MEMORY_REQUIREMENTS_INFO_2;
pub const VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2_KHR : VkStructureType = VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2;
pub const VK_STRUCTURE_TYPE_SPARSE_IMAGE_MEMORY_REQUIREMENTS_2_KHR : VkStructureType = VK_STRUCTURE_TYPE_SPARSE_IMAGE_MEMORY_REQUIREMENTS_2;
pub const VK_STRUCTURE_TYPE_IMAGE_FORMAT_LIST_CREATE_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_IMAGE_FORMAT_LIST_CREATE_INFO;
pub const VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_CREATE_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_CREATE_INFO;
pub const VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_INFO;
pub const VK_STRUCTURE_TYPE_BIND_IMAGE_PLANE_MEMORY_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_BIND_IMAGE_PLANE_MEMORY_INFO;
pub const VK_STRUCTURE_TYPE_IMAGE_PLANE_MEMORY_REQUIREMENTS_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_IMAGE_PLANE_MEMORY_REQUIREMENTS_INFO;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_YCBCR_CONVERSION_FEATURES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SAMPLER_YCBCR_CONVERSION_FEATURES;
pub const VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_IMAGE_FORMAT_PROPERTIES_KHR : VkStructureType = VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_IMAGE_FORMAT_PROPERTIES;
pub const VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_INFO;
pub const VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_BIND_IMAGE_MEMORY_INFO;
pub const VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO_EXT : VkStructureType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES_EXT : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_PROPERTIES_EXT : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_PROPERTIES;
pub const VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_ALLOCATE_INFO_EXT : VkStructureType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_ALLOCATE_INFO;
pub const VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_LAYOUT_SUPPORT_EXT : VkStructureType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_VARIABLE_DESCRIPTOR_COUNT_LAYOUT_SUPPORT;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_3_PROPERTIES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MAINTENANCE_3_PROPERTIES;
pub const VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_SUPPORT_KHR : VkStructureType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_SUPPORT;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SUBGROUP_EXTENDED_TYPES_FEATURES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_SUBGROUP_EXTENDED_TYPES_FEATURES;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_8BIT_STORAGE_FEATURES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_8BIT_STORAGE_FEATURES;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_INT64_FEATURES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_INT64_FEATURES;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DRIVER_PROPERTIES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DRIVER_PROPERTIES;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FLOAT_CONTROLS_PROPERTIES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FLOAT_CONTROLS_PROPERTIES;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEPTH_STENCIL_RESOLVE_PROPERTIES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DEPTH_STENCIL_RESOLVE_PROPERTIES;
pub const VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_DEPTH_STENCIL_RESOLVE_KHR : VkStructureType = VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_DEPTH_STENCIL_RESOLVE;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_FEATURES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_FEATURES;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_PROPERTIES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_PROPERTIES;
pub const VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO;
pub const VK_STRUCTURE_TYPE_TIMELINE_SEMAPHORE_SUBMIT_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_TIMELINE_SEMAPHORE_SUBMIT_INFO;
pub const VK_STRUCTURE_TYPE_SEMAPHORE_WAIT_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_SEMAPHORE_WAIT_INFO;
pub const VK_STRUCTURE_TYPE_SEMAPHORE_SIGNAL_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_SEMAPHORE_SIGNAL_INFO;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_MEMORY_MODEL_FEATURES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_MEMORY_MODEL_FEATURES;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SCALAR_BLOCK_LAYOUT_FEATURES_EXT : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SCALAR_BLOCK_LAYOUT_FEATURES;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SEPARATE_DEPTH_STENCIL_LAYOUTS_FEATURES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SEPARATE_DEPTH_STENCIL_LAYOUTS_FEATURES;
pub const VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_STENCIL_LAYOUT_KHR : VkStructureType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_STENCIL_LAYOUT;
pub const VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_STENCIL_LAYOUT_KHR : VkStructureType = VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_STENCIL_LAYOUT;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_ADDRESS_FEATURES_EXT : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES_EXT;
pub const VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO_EXT : VkStructureType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
pub const VK_STRUCTURE_TYPE_IMAGE_STENCIL_USAGE_CREATE_INFO_EXT : VkStructureType = VK_STRUCTURE_TYPE_IMAGE_STENCIL_USAGE_CREATE_INFO;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_UNIFORM_BUFFER_STANDARD_LAYOUT_FEATURES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_UNIFORM_BUFFER_STANDARD_LAYOUT_FEATURES;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES_KHR : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES;
pub const VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
pub const VK_STRUCTURE_TYPE_BUFFER_OPAQUE_CAPTURE_ADDRESS_CREATE_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_BUFFER_OPAQUE_CAPTURE_ADDRESS_CREATE_INFO;
pub const VK_STRUCTURE_TYPE_MEMORY_OPAQUE_CAPTURE_ADDRESS_ALLOCATE_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_MEMORY_OPAQUE_CAPTURE_ADDRESS_ALLOCATE_INFO;
pub const VK_STRUCTURE_TYPE_DEVICE_MEMORY_OPAQUE_CAPTURE_ADDRESS_INFO_KHR : VkStructureType = VK_STRUCTURE_TYPE_DEVICE_MEMORY_OPAQUE_CAPTURE_ADDRESS_INFO;
pub const VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_HOST_QUERY_RESET_FEATURES_EXT : VkStructureType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_HOST_QUERY_RESET_FEATURES;
pub const VK_STRUCTURE_TYPE_BEGIN_RANGE : VkStructureType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
pub const VK_STRUCTURE_TYPE_END_RANGE : VkStructureType = VK_STRUCTURE_TYPE_LOADER_DEVICE_CREATE_INFO;
pub const VK_STRUCTURE_TYPE_RANGE_SIZE : VkStructureType = (VK_STRUCTURE_TYPE_LOADER_DEVICE_CREATE_INFO - VK_STRUCTURE_TYPE_APPLICATION_INFO + 1);
pub const VK_STRUCTURE_TYPE_MAX_ENUM : VkStructureType = 0x7FFFFFFF;

pub const VK_PHYSICAL_DEVICE_TYPE_OTHER : VkPhysicalDeviceType = 0;
pub const VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU : VkPhysicalDeviceType = 1;
pub const VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU : VkPhysicalDeviceType = 2;
pub const VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU : VkPhysicalDeviceType = 3;
pub const VK_PHYSICAL_DEVICE_TYPE_CPU : VkPhysicalDeviceType = 4;
pub const VK_PHYSICAL_DEVICE_TYPE_BEGIN_RANGE : VkPhysicalDeviceType = VK_PHYSICAL_DEVICE_TYPE_OTHER;
pub const VK_PHYSICAL_DEVICE_TYPE_END_RANGE : VkPhysicalDeviceType = VK_PHYSICAL_DEVICE_TYPE_CPU;
pub const VK_PHYSICAL_DEVICE_TYPE_RANGE_SIZE : VkPhysicalDeviceType = (VK_PHYSICAL_DEVICE_TYPE_CPU - VK_PHYSICAL_DEVICE_TYPE_OTHER + 1);
pub const VK_PHYSICAL_DEVICE_TYPE_MAX_ENUM : VkPhysicalDeviceType = 0x7FFFFFFF;

pub const VK_QUEUE_GRAPHICS_BIT : VkQueueFlagBits = 0x00000001;
pub const VK_QUEUE_COMPUTE_BIT : VkQueueFlagBits = 0x00000002;
pub const VK_QUEUE_TRANSFER_BIT : VkQueueFlagBits = 0x00000004;
pub const VK_QUEUE_SPARSE_BINDING_BIT : VkQueueFlagBits = 0x00000008;
pub const VK_QUEUE_PROTECTED_BIT : VkQueueFlagBits = 0x00000010;
pub const VK_QUEUE_FLAG_BITS_MAX_ENUM : VkQueueFlagBits = 0x7FFFFFFF;

pub const VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR : VkSurfaceTransformFlagBitsKHR = 0x00000001;
pub const VK_SURFACE_TRANSFORM_ROTATE_90_BIT_KHR : VkSurfaceTransformFlagBitsKHR = 0x00000002;
pub const VK_SURFACE_TRANSFORM_ROTATE_180_BIT_KHR : VkSurfaceTransformFlagBitsKHR = 0x00000004;
pub const VK_SURFACE_TRANSFORM_ROTATE_270_BIT_KHR : VkSurfaceTransformFlagBitsKHR = 0x00000008;
pub const VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_BIT_KHR : VkSurfaceTransformFlagBitsKHR = 0x00000010;
pub const VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_90_BIT_KHR : VkSurfaceTransformFlagBitsKHR = 0x00000020;
pub const VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_180_BIT_KHR : VkSurfaceTransformFlagBitsKHR = 0x00000040;
pub const VK_SURFACE_TRANSFORM_HORIZONTAL_MIRROR_ROTATE_270_BIT_KHR : VkSurfaceTransformFlagBitsKHR = 0x00000080;
pub const VK_SURFACE_TRANSFORM_INHERIT_BIT_KHR : VkSurfaceTransformFlagBitsKHR = 0x00000100;
pub const VK_SURFACE_TRANSFORM_FLAG_BITS_MAX_ENUM_KHR : VkSurfaceTransformFlagBitsKHR = 0x7FFFFFFF;

pub const VK_BLEND_FACTOR_ZERO : VkBlendFactor = 0;
pub const VK_BLEND_FACTOR_ONE : VkBlendFactor = 1;
pub const VK_BLEND_FACTOR_SRC_COLOR : VkBlendFactor = 2;
pub const VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR : VkBlendFactor = 3;
pub const VK_BLEND_FACTOR_DST_COLOR : VkBlendFactor = 4;
pub const VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR : VkBlendFactor = 5;
pub const VK_BLEND_FACTOR_SRC_ALPHA : VkBlendFactor = 6;
pub const VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA : VkBlendFactor = 7;
pub const VK_BLEND_FACTOR_DST_ALPHA : VkBlendFactor = 8;
pub const VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA : VkBlendFactor = 9;
pub const VK_BLEND_FACTOR_CONSTANT_COLOR : VkBlendFactor = 10;
pub const VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR : VkBlendFactor = 11;
pub const VK_BLEND_FACTOR_CONSTANT_ALPHA : VkBlendFactor = 12;
pub const VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA : VkBlendFactor = 13;
pub const VK_BLEND_FACTOR_SRC_ALPHA_SATURATE : VkBlendFactor = 14;
pub const VK_BLEND_FACTOR_SRC1_COLOR : VkBlendFactor = 15;
pub const VK_BLEND_FACTOR_ONE_MINUS_SRC1_COLOR : VkBlendFactor = 16;
pub const VK_BLEND_FACTOR_SRC1_ALPHA : VkBlendFactor = 17;
pub const VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA : VkBlendFactor = 18;
pub const VK_BLEND_FACTOR_BEGIN_RANGE : VkBlendFactor = VK_BLEND_FACTOR_ZERO;
pub const VK_BLEND_FACTOR_END_RANGE : VkBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA;
pub const VK_BLEND_FACTOR_RANGE_SIZE : VkBlendFactor = (VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA - VK_BLEND_FACTOR_ZERO + 1);
pub const VK_BLEND_FACTOR_MAX_ENUM : VkBlendFactor = 0x7FFFFFFF;

pub const VK_BLEND_OP_ADD : VkBlendOp = 0;
pub const VK_BLEND_OP_SUBTRACT : VkBlendOp = 1;
pub const VK_BLEND_OP_REVERSE_SUBTRACT : VkBlendOp = 2;
pub const VK_BLEND_OP_MIN : VkBlendOp = 3;
pub const VK_BLEND_OP_MAX : VkBlendOp = 4;
pub const VK_BLEND_OP_ZERO_EXT : VkBlendOp = 1000148000;
pub const VK_BLEND_OP_SRC_EXT : VkBlendOp = 1000148001;
pub const VK_BLEND_OP_DST_EXT : VkBlendOp = 1000148002;
pub const VK_BLEND_OP_SRC_OVER_EXT : VkBlendOp = 1000148003;
pub const VK_BLEND_OP_DST_OVER_EXT : VkBlendOp = 1000148004;
pub const VK_BLEND_OP_SRC_IN_EXT : VkBlendOp = 1000148005;
pub const VK_BLEND_OP_DST_IN_EXT : VkBlendOp = 1000148006;
pub const VK_BLEND_OP_SRC_OUT_EXT : VkBlendOp = 1000148007;
pub const VK_BLEND_OP_DST_OUT_EXT : VkBlendOp = 1000148008;
pub const VK_BLEND_OP_SRC_ATOP_EXT : VkBlendOp = 1000148009;
pub const VK_BLEND_OP_DST_ATOP_EXT : VkBlendOp = 1000148010;
pub const VK_BLEND_OP_XOR_EXT : VkBlendOp = 1000148011;
pub const VK_BLEND_OP_MULTIPLY_EXT : VkBlendOp = 1000148012;
pub const VK_BLEND_OP_SCREEN_EXT : VkBlendOp = 1000148013;
pub const VK_BLEND_OP_OVERLAY_EXT : VkBlendOp = 1000148014;
pub const VK_BLEND_OP_DARKEN_EXT : VkBlendOp = 1000148015;
pub const VK_BLEND_OP_LIGHTEN_EXT : VkBlendOp = 1000148016;
pub const VK_BLEND_OP_COLORDODGE_EXT : VkBlendOp = 1000148017;
pub const VK_BLEND_OP_COLORBURN_EXT : VkBlendOp = 1000148018;
pub const VK_BLEND_OP_HARDLIGHT_EXT : VkBlendOp = 1000148019;
pub const VK_BLEND_OP_SOFTLIGHT_EXT : VkBlendOp = 1000148020;
pub const VK_BLEND_OP_DIFFERENCE_EXT : VkBlendOp = 1000148021;
pub const VK_BLEND_OP_EXCLUSION_EXT : VkBlendOp = 1000148022;
pub const VK_BLEND_OP_INVERT_EXT : VkBlendOp = 1000148023;
pub const VK_BLEND_OP_INVERT_RGB_EXT : VkBlendOp = 1000148024;
pub const VK_BLEND_OP_LINEARDODGE_EXT : VkBlendOp = 1000148025;
pub const VK_BLEND_OP_LINEARBURN_EXT : VkBlendOp = 1000148026;
pub const VK_BLEND_OP_VIVIDLIGHT_EXT : VkBlendOp = 1000148027;
pub const VK_BLEND_OP_LINEARLIGHT_EXT : VkBlendOp = 1000148028;
pub const VK_BLEND_OP_PINLIGHT_EXT : VkBlendOp = 1000148029;
pub const VK_BLEND_OP_HARDMIX_EXT : VkBlendOp = 1000148030;
pub const VK_BLEND_OP_HSL_HUE_EXT : VkBlendOp = 1000148031;
pub const VK_BLEND_OP_HSL_SATURATION_EXT : VkBlendOp = 1000148032;
pub const VK_BLEND_OP_HSL_COLOR_EXT : VkBlendOp = 1000148033;
pub const VK_BLEND_OP_HSL_LUMINOSITY_EXT : VkBlendOp = 1000148034;
pub const VK_BLEND_OP_PLUS_EXT : VkBlendOp = 1000148035;
pub const VK_BLEND_OP_PLUS_CLAMPED_EXT : VkBlendOp = 1000148036;
pub const VK_BLEND_OP_PLUS_CLAMPED_ALPHA_EXT : VkBlendOp = 1000148037;
pub const VK_BLEND_OP_PLUS_DARKER_EXT : VkBlendOp = 1000148038;
pub const VK_BLEND_OP_MINUS_EXT : VkBlendOp = 1000148039;
pub const VK_BLEND_OP_MINUS_CLAMPED_EXT : VkBlendOp = 1000148040;
pub const VK_BLEND_OP_CONTRAST_EXT : VkBlendOp = 1000148041;
pub const VK_BLEND_OP_INVERT_OVG_EXT : VkBlendOp = 1000148042;
pub const VK_BLEND_OP_RED_EXT : VkBlendOp = 1000148043;
pub const VK_BLEND_OP_GREEN_EXT : VkBlendOp = 1000148044;
pub const VK_BLEND_OP_BLUE_EXT : VkBlendOp = 1000148045;
pub const VK_BLEND_OP_BEGIN_RANGE : VkBlendOp = VK_BLEND_OP_ADD;
pub const VK_BLEND_OP_END_RANGE : VkBlendOp = VK_BLEND_OP_MAX;
pub const VK_BLEND_OP_RANGE_SIZE : VkBlendOp = (VK_BLEND_OP_MAX - VK_BLEND_OP_ADD + 1);
pub const VK_BLEND_OP_MAX_ENUM : VkBlendOp = 0x7FFFFFFF;

pub const VK_FORMAT_UNDEFINED : VkFormat = 0;
pub const VK_FORMAT_R4G4_UNORM_PACK8 : VkFormat = 1;
pub const VK_FORMAT_R4G4B4A4_UNORM_PACK16 : VkFormat = 2;
pub const VK_FORMAT_B4G4R4A4_UNORM_PACK16 : VkFormat = 3;
pub const VK_FORMAT_R5G6B5_UNORM_PACK16 : VkFormat = 4;
pub const VK_FORMAT_B5G6R5_UNORM_PACK16 : VkFormat = 5;
pub const VK_FORMAT_R5G5B5A1_UNORM_PACK16 : VkFormat = 6;
pub const VK_FORMAT_B5G5R5A1_UNORM_PACK16 : VkFormat = 7;
pub const VK_FORMAT_A1R5G5B5_UNORM_PACK16 : VkFormat = 8;
pub const VK_FORMAT_R8_UNORM : VkFormat = 9;
pub const VK_FORMAT_R8_SNORM : VkFormat = 10;
pub const VK_FORMAT_R8_USCALED : VkFormat = 11;
pub const VK_FORMAT_R8_SSCALED : VkFormat = 12;
pub const VK_FORMAT_R8_UINT : VkFormat = 13;
pub const VK_FORMAT_R8_SINT : VkFormat = 14;
pub const VK_FORMAT_R8_SRGB : VkFormat = 15;
pub const VK_FORMAT_R8G8_UNORM : VkFormat = 16;
pub const VK_FORMAT_R8G8_SNORM : VkFormat = 17;
pub const VK_FORMAT_R8G8_USCALED : VkFormat = 18;
pub const VK_FORMAT_R8G8_SSCALED : VkFormat = 19;
pub const VK_FORMAT_R8G8_UINT : VkFormat = 20;
pub const VK_FORMAT_R8G8_SINT : VkFormat = 21;
pub const VK_FORMAT_R8G8_SRGB : VkFormat = 22;
pub const VK_FORMAT_R8G8B8_UNORM : VkFormat = 23;
pub const VK_FORMAT_R8G8B8_SNORM : VkFormat = 24;
pub const VK_FORMAT_R8G8B8_USCALED : VkFormat = 25;
pub const VK_FORMAT_R8G8B8_SSCALED : VkFormat = 26;
pub const VK_FORMAT_R8G8B8_UINT : VkFormat = 27;
pub const VK_FORMAT_R8G8B8_SINT : VkFormat = 28;
pub const VK_FORMAT_R8G8B8_SRGB : VkFormat = 29;
pub const VK_FORMAT_B8G8R8_UNORM : VkFormat = 30;
pub const VK_FORMAT_B8G8R8_SNORM : VkFormat = 31;
pub const VK_FORMAT_B8G8R8_USCALED : VkFormat = 32;
pub const VK_FORMAT_B8G8R8_SSCALED : VkFormat = 33;
pub const VK_FORMAT_B8G8R8_UINT : VkFormat = 34;
pub const VK_FORMAT_B8G8R8_SINT : VkFormat = 35;
pub const VK_FORMAT_B8G8R8_SRGB : VkFormat = 36;
pub const VK_FORMAT_R8G8B8A8_UNORM : VkFormat = 37;
pub const VK_FORMAT_R8G8B8A8_SNORM : VkFormat = 38;
pub const VK_FORMAT_R8G8B8A8_USCALED : VkFormat = 39;
pub const VK_FORMAT_R8G8B8A8_SSCALED : VkFormat = 40;
pub const VK_FORMAT_R8G8B8A8_UINT : VkFormat = 41;
pub const VK_FORMAT_R8G8B8A8_SINT : VkFormat = 42;
pub const VK_FORMAT_R8G8B8A8_SRGB : VkFormat = 43;
pub const VK_FORMAT_B8G8R8A8_UNORM : VkFormat = 44;
pub const VK_FORMAT_B8G8R8A8_SNORM : VkFormat = 45;
pub const VK_FORMAT_B8G8R8A8_USCALED : VkFormat = 46;
pub const VK_FORMAT_B8G8R8A8_SSCALED : VkFormat = 47;
pub const VK_FORMAT_B8G8R8A8_UINT : VkFormat = 48;
pub const VK_FORMAT_B8G8R8A8_SINT : VkFormat = 49;
pub const VK_FORMAT_B8G8R8A8_SRGB : VkFormat = 50;
pub const VK_FORMAT_A8B8G8R8_UNORM_PACK32 : VkFormat = 51;
pub const VK_FORMAT_A8B8G8R8_SNORM_PACK32 : VkFormat = 52;
pub const VK_FORMAT_A8B8G8R8_USCALED_PACK32 : VkFormat = 53;
pub const VK_FORMAT_A8B8G8R8_SSCALED_PACK32 : VkFormat = 54;
pub const VK_FORMAT_A8B8G8R8_UINT_PACK32 : VkFormat = 55;
pub const VK_FORMAT_A8B8G8R8_SINT_PACK32 : VkFormat = 56;
pub const VK_FORMAT_A8B8G8R8_SRGB_PACK32 : VkFormat = 57;
pub const VK_FORMAT_A2R10G10B10_UNORM_PACK32 : VkFormat = 58;
pub const VK_FORMAT_A2R10G10B10_SNORM_PACK32 : VkFormat = 59;
pub const VK_FORMAT_A2R10G10B10_USCALED_PACK32 : VkFormat = 60;
pub const VK_FORMAT_A2R10G10B10_SSCALED_PACK32 : VkFormat = 61;
pub const VK_FORMAT_A2R10G10B10_UINT_PACK32 : VkFormat = 62;
pub const VK_FORMAT_A2R10G10B10_SINT_PACK32 : VkFormat = 63;
pub const VK_FORMAT_A2B10G10R10_UNORM_PACK32 : VkFormat = 64;
pub const VK_FORMAT_A2B10G10R10_SNORM_PACK32 : VkFormat = 65;
pub const VK_FORMAT_A2B10G10R10_USCALED_PACK32 : VkFormat = 66;
pub const VK_FORMAT_A2B10G10R10_SSCALED_PACK32 : VkFormat = 67;
pub const VK_FORMAT_A2B10G10R10_UINT_PACK32 : VkFormat = 68;
pub const VK_FORMAT_A2B10G10R10_SINT_PACK32 : VkFormat = 69;
pub const VK_FORMAT_R16_UNORM : VkFormat = 70;
pub const VK_FORMAT_R16_SNORM : VkFormat = 71;
pub const VK_FORMAT_R16_USCALED : VkFormat = 72;
pub const VK_FORMAT_R16_SSCALED : VkFormat = 73;
pub const VK_FORMAT_R16_UINT : VkFormat = 74;
pub const VK_FORMAT_R16_SINT : VkFormat = 75;
pub const VK_FORMAT_R16_SFLOAT : VkFormat = 76;
pub const VK_FORMAT_R16G16_UNORM : VkFormat = 77;
pub const VK_FORMAT_R16G16_SNORM : VkFormat = 78;
pub const VK_FORMAT_R16G16_USCALED : VkFormat = 79;
pub const VK_FORMAT_R16G16_SSCALED : VkFormat = 80;
pub const VK_FORMAT_R16G16_UINT : VkFormat = 81;
pub const VK_FORMAT_R16G16_SINT : VkFormat = 82;
pub const VK_FORMAT_R16G16_SFLOAT : VkFormat = 83;
pub const VK_FORMAT_R16G16B16_UNORM : VkFormat = 84;
pub const VK_FORMAT_R16G16B16_SNORM : VkFormat = 85;
pub const VK_FORMAT_R16G16B16_USCALED : VkFormat = 86;
pub const VK_FORMAT_R16G16B16_SSCALED : VkFormat = 87;
pub const VK_FORMAT_R16G16B16_UINT : VkFormat = 88;
pub const VK_FORMAT_R16G16B16_SINT : VkFormat = 89;
pub const VK_FORMAT_R16G16B16_SFLOAT : VkFormat = 90;
pub const VK_FORMAT_R16G16B16A16_UNORM : VkFormat = 91;
pub const VK_FORMAT_R16G16B16A16_SNORM : VkFormat = 92;
pub const VK_FORMAT_R16G16B16A16_USCALED : VkFormat = 93;
pub const VK_FORMAT_R16G16B16A16_SSCALED : VkFormat = 94;
pub const VK_FORMAT_R16G16B16A16_UINT : VkFormat = 95;
pub const VK_FORMAT_R16G16B16A16_SINT : VkFormat = 96;
pub const VK_FORMAT_R16G16B16A16_SFLOAT : VkFormat = 97;
pub const VK_FORMAT_R32_UINT : VkFormat = 98;
pub const VK_FORMAT_R32_SINT : VkFormat = 99;
pub const VK_FORMAT_R32_SFLOAT : VkFormat = 100;
pub const VK_FORMAT_R32G32_UINT : VkFormat = 101;
pub const VK_FORMAT_R32G32_SINT : VkFormat = 102;
pub const VK_FORMAT_R32G32_SFLOAT : VkFormat = 103;
pub const VK_FORMAT_R32G32B32_UINT : VkFormat = 104;
pub const VK_FORMAT_R32G32B32_SINT : VkFormat = 105;
pub const VK_FORMAT_R32G32B32_SFLOAT : VkFormat = 106;
pub const VK_FORMAT_R32G32B32A32_UINT : VkFormat = 107;
pub const VK_FORMAT_R32G32B32A32_SINT : VkFormat = 108;
pub const VK_FORMAT_R32G32B32A32_SFLOAT : VkFormat = 109;
pub const VK_FORMAT_R64_UINT : VkFormat = 110;
pub const VK_FORMAT_R64_SINT : VkFormat = 111;
pub const VK_FORMAT_R64_SFLOAT : VkFormat = 112;
pub const VK_FORMAT_R64G64_UINT : VkFormat = 113;
pub const VK_FORMAT_R64G64_SINT : VkFormat = 114;
pub const VK_FORMAT_R64G64_SFLOAT : VkFormat = 115;
pub const VK_FORMAT_R64G64B64_UINT : VkFormat = 116;
pub const VK_FORMAT_R64G64B64_SINT : VkFormat = 117;
pub const VK_FORMAT_R64G64B64_SFLOAT : VkFormat = 118;
pub const VK_FORMAT_R64G64B64A64_UINT : VkFormat = 119;
pub const VK_FORMAT_R64G64B64A64_SINT : VkFormat = 120;
pub const VK_FORMAT_R64G64B64A64_SFLOAT : VkFormat = 121;
pub const VK_FORMAT_B10G11R11_UFLOAT_PACK32 : VkFormat = 122;
pub const VK_FORMAT_E5B9G9R9_UFLOAT_PACK32 : VkFormat = 123;
pub const VK_FORMAT_D16_UNORM : VkFormat = 124;
pub const VK_FORMAT_X8_D24_UNORM_PACK32 : VkFormat = 125;
pub const VK_FORMAT_D32_SFLOAT : VkFormat = 126;
pub const VK_FORMAT_S8_UINT : VkFormat = 127;
pub const VK_FORMAT_D16_UNORM_S8_UINT : VkFormat = 128;
pub const VK_FORMAT_D24_UNORM_S8_UINT : VkFormat = 129;
pub const VK_FORMAT_D32_SFLOAT_S8_UINT : VkFormat = 130;
pub const VK_FORMAT_BC1_RGB_UNORM_BLOCK : VkFormat = 131;
pub const VK_FORMAT_BC1_RGB_SRGB_BLOCK : VkFormat = 132;
pub const VK_FORMAT_BC1_RGBA_UNORM_BLOCK : VkFormat = 133;
pub const VK_FORMAT_BC1_RGBA_SRGB_BLOCK : VkFormat = 134;
pub const VK_FORMAT_BC2_UNORM_BLOCK : VkFormat = 135;
pub const VK_FORMAT_BC2_SRGB_BLOCK : VkFormat = 136;
pub const VK_FORMAT_BC3_UNORM_BLOCK : VkFormat = 137;
pub const VK_FORMAT_BC3_SRGB_BLOCK : VkFormat = 138;
pub const VK_FORMAT_BC4_UNORM_BLOCK : VkFormat = 139;
pub const VK_FORMAT_BC4_SNORM_BLOCK : VkFormat = 140;
pub const VK_FORMAT_BC5_UNORM_BLOCK : VkFormat = 141;
pub const VK_FORMAT_BC5_SNORM_BLOCK : VkFormat = 142;
pub const VK_FORMAT_BC6H_UFLOAT_BLOCK : VkFormat = 143;
pub const VK_FORMAT_BC6H_SFLOAT_BLOCK : VkFormat = 144;
pub const VK_FORMAT_BC7_UNORM_BLOCK : VkFormat = 145;
pub const VK_FORMAT_BC7_SRGB_BLOCK : VkFormat = 146;
pub const VK_FORMAT_ETC2_R8G8B8_UNORM_BLOCK : VkFormat = 147;
pub const VK_FORMAT_ETC2_R8G8B8_SRGB_BLOCK : VkFormat = 148;
pub const VK_FORMAT_ETC2_R8G8B8A1_UNORM_BLOCK : VkFormat = 149;
pub const VK_FORMAT_ETC2_R8G8B8A1_SRGB_BLOCK : VkFormat = 150;
pub const VK_FORMAT_ETC2_R8G8B8A8_UNORM_BLOCK : VkFormat = 151;
pub const VK_FORMAT_ETC2_R8G8B8A8_SRGB_BLOCK : VkFormat = 152;
pub const VK_FORMAT_EAC_R11_UNORM_BLOCK : VkFormat = 153;
pub const VK_FORMAT_EAC_R11_SNORM_BLOCK : VkFormat = 154;
pub const VK_FORMAT_EAC_R11G11_UNORM_BLOCK : VkFormat = 155;
pub const VK_FORMAT_EAC_R11G11_SNORM_BLOCK : VkFormat = 156;
pub const VK_FORMAT_ASTC_4x4_UNORM_BLOCK : VkFormat = 157;
pub const VK_FORMAT_ASTC_4x4_SRGB_BLOCK : VkFormat = 158;
pub const VK_FORMAT_ASTC_5x4_UNORM_BLOCK : VkFormat = 159;
pub const VK_FORMAT_ASTC_5x4_SRGB_BLOCK : VkFormat = 160;
pub const VK_FORMAT_ASTC_5x5_UNORM_BLOCK : VkFormat = 161;
pub const VK_FORMAT_ASTC_5x5_SRGB_BLOCK : VkFormat = 162;
pub const VK_FORMAT_ASTC_6x5_UNORM_BLOCK : VkFormat = 163;
pub const VK_FORMAT_ASTC_6x5_SRGB_BLOCK : VkFormat = 164;
pub const VK_FORMAT_ASTC_6x6_UNORM_BLOCK : VkFormat = 165;
pub const VK_FORMAT_ASTC_6x6_SRGB_BLOCK : VkFormat = 166;
pub const VK_FORMAT_ASTC_8x5_UNORM_BLOCK : VkFormat = 167;
pub const VK_FORMAT_ASTC_8x5_SRGB_BLOCK : VkFormat = 168;
pub const VK_FORMAT_ASTC_8x6_UNORM_BLOCK : VkFormat = 169;
pub const VK_FORMAT_ASTC_8x6_SRGB_BLOCK : VkFormat = 170;
pub const VK_FORMAT_ASTC_8x8_UNORM_BLOCK : VkFormat = 171;
pub const VK_FORMAT_ASTC_8x8_SRGB_BLOCK : VkFormat = 172;
pub const VK_FORMAT_ASTC_10x5_UNORM_BLOCK : VkFormat = 173;
pub const VK_FORMAT_ASTC_10x5_SRGB_BLOCK : VkFormat = 174;
pub const VK_FORMAT_ASTC_10x6_UNORM_BLOCK : VkFormat = 175;
pub const VK_FORMAT_ASTC_10x6_SRGB_BLOCK : VkFormat = 176;
pub const VK_FORMAT_ASTC_10x8_UNORM_BLOCK : VkFormat = 177;
pub const VK_FORMAT_ASTC_10x8_SRGB_BLOCK : VkFormat = 178;
pub const VK_FORMAT_ASTC_10x10_UNORM_BLOCK : VkFormat = 179;
pub const VK_FORMAT_ASTC_10x10_SRGB_BLOCK : VkFormat = 180;
pub const VK_FORMAT_ASTC_12x10_UNORM_BLOCK : VkFormat = 181;
pub const VK_FORMAT_ASTC_12x10_SRGB_BLOCK : VkFormat = 182;
pub const VK_FORMAT_ASTC_12x12_UNORM_BLOCK : VkFormat = 183;
pub const VK_FORMAT_ASTC_12x12_SRGB_BLOCK : VkFormat = 184;
pub const VK_FORMAT_G8B8G8R8_422_UNORM : VkFormat = 1000156000;
pub const VK_FORMAT_B8G8R8G8_422_UNORM : VkFormat = 1000156001;
pub const VK_FORMAT_G8_B8_R8_3PLANE_420_UNORM : VkFormat = 1000156002;
pub const VK_FORMAT_G8_B8R8_2PLANE_420_UNORM : VkFormat = 1000156003;
pub const VK_FORMAT_G8_B8_R8_3PLANE_422_UNORM : VkFormat = 1000156004;
pub const VK_FORMAT_G8_B8R8_2PLANE_422_UNORM : VkFormat = 1000156005;
pub const VK_FORMAT_G8_B8_R8_3PLANE_444_UNORM : VkFormat = 1000156006;
pub const VK_FORMAT_R10X6_UNORM_PACK16 : VkFormat = 1000156007;
pub const VK_FORMAT_R10X6G10X6_UNORM_2PACK16 : VkFormat = 1000156008;
pub const VK_FORMAT_R10X6G10X6B10X6A10X6_UNORM_4PACK16 : VkFormat = 1000156009;
pub const VK_FORMAT_G10X6B10X6G10X6R10X6_422_UNORM_4PACK16 : VkFormat = 1000156010;
pub const VK_FORMAT_B10X6G10X6R10X6G10X6_422_UNORM_4PACK16 : VkFormat = 1000156011;
pub const VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_420_UNORM_3PACK16 : VkFormat = 1000156012;
pub const VK_FORMAT_G10X6_B10X6R10X6_2PLANE_420_UNORM_3PACK16 : VkFormat = 1000156013;
pub const VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_422_UNORM_3PACK16 : VkFormat = 1000156014;
pub const VK_FORMAT_G10X6_B10X6R10X6_2PLANE_422_UNORM_3PACK16 : VkFormat = 1000156015;
pub const VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_444_UNORM_3PACK16 : VkFormat = 1000156016;
pub const VK_FORMAT_R12X4_UNORM_PACK16 : VkFormat = 1000156017;
pub const VK_FORMAT_R12X4G12X4_UNORM_2PACK16 : VkFormat = 1000156018;
pub const VK_FORMAT_R12X4G12X4B12X4A12X4_UNORM_4PACK16 : VkFormat = 1000156019;
pub const VK_FORMAT_G12X4B12X4G12X4R12X4_422_UNORM_4PACK16 : VkFormat = 1000156020;
pub const VK_FORMAT_B12X4G12X4R12X4G12X4_422_UNORM_4PACK16 : VkFormat = 1000156021;
pub const VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_420_UNORM_3PACK16 : VkFormat = 1000156022;
pub const VK_FORMAT_G12X4_B12X4R12X4_2PLANE_420_UNORM_3PACK16 : VkFormat = 1000156023;
pub const VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_422_UNORM_3PACK16 : VkFormat = 1000156024;
pub const VK_FORMAT_G12X4_B12X4R12X4_2PLANE_422_UNORM_3PACK16 : VkFormat = 1000156025;
pub const VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_444_UNORM_3PACK16 : VkFormat = 1000156026;
pub const VK_FORMAT_G16B16G16R16_422_UNORM : VkFormat = 1000156027;
pub const VK_FORMAT_B16G16R16G16_422_UNORM : VkFormat = 1000156028;
pub const VK_FORMAT_G16_B16_R16_3PLANE_420_UNORM : VkFormat = 1000156029;
pub const VK_FORMAT_G16_B16R16_2PLANE_420_UNORM : VkFormat = 1000156030;
pub const VK_FORMAT_G16_B16_R16_3PLANE_422_UNORM : VkFormat = 1000156031;
pub const VK_FORMAT_G16_B16R16_2PLANE_422_UNORM : VkFormat = 1000156032;
pub const VK_FORMAT_G16_B16_R16_3PLANE_444_UNORM : VkFormat = 1000156033;
pub const VK_FORMAT_PVRTC1_2BPP_UNORM_BLOCK_IMG : VkFormat = 1000054000;
pub const VK_FORMAT_PVRTC1_4BPP_UNORM_BLOCK_IMG : VkFormat = 1000054001;
pub const VK_FORMAT_PVRTC2_2BPP_UNORM_BLOCK_IMG : VkFormat = 1000054002;
pub const VK_FORMAT_PVRTC2_4BPP_UNORM_BLOCK_IMG : VkFormat = 1000054003;
pub const VK_FORMAT_PVRTC1_2BPP_SRGB_BLOCK_IMG : VkFormat = 1000054004;
pub const VK_FORMAT_PVRTC1_4BPP_SRGB_BLOCK_IMG : VkFormat = 1000054005;
pub const VK_FORMAT_PVRTC2_2BPP_SRGB_BLOCK_IMG : VkFormat = 1000054006;
pub const VK_FORMAT_PVRTC2_4BPP_SRGB_BLOCK_IMG : VkFormat = 1000054007;
pub const VK_FORMAT_ASTC_4x4_SFLOAT_BLOCK_EXT : VkFormat = 1000066000;
pub const VK_FORMAT_ASTC_5x4_SFLOAT_BLOCK_EXT : VkFormat = 1000066001;
pub const VK_FORMAT_ASTC_5x5_SFLOAT_BLOCK_EXT : VkFormat = 1000066002;
pub const VK_FORMAT_ASTC_6x5_SFLOAT_BLOCK_EXT : VkFormat = 1000066003;
pub const VK_FORMAT_ASTC_6x6_SFLOAT_BLOCK_EXT : VkFormat = 1000066004;
pub const VK_FORMAT_ASTC_8x5_SFLOAT_BLOCK_EXT : VkFormat = 1000066005;
pub const VK_FORMAT_ASTC_8x6_SFLOAT_BLOCK_EXT : VkFormat = 1000066006;
pub const VK_FORMAT_ASTC_8x8_SFLOAT_BLOCK_EXT : VkFormat = 1000066007;
pub const VK_FORMAT_ASTC_10x5_SFLOAT_BLOCK_EXT : VkFormat = 1000066008;
pub const VK_FORMAT_ASTC_10x6_SFLOAT_BLOCK_EXT : VkFormat = 1000066009;
pub const VK_FORMAT_ASTC_10x8_SFLOAT_BLOCK_EXT : VkFormat = 1000066010;
pub const VK_FORMAT_ASTC_10x10_SFLOAT_BLOCK_EXT : VkFormat = 1000066011;
pub const VK_FORMAT_ASTC_12x10_SFLOAT_BLOCK_EXT : VkFormat = 1000066012;
pub const VK_FORMAT_ASTC_12x12_SFLOAT_BLOCK_EXT : VkFormat = 1000066013;
pub const VK_FORMAT_G8B8G8R8_422_UNORM_KHR : VkFormat = VK_FORMAT_G8B8G8R8_422_UNORM;
pub const VK_FORMAT_B8G8R8G8_422_UNORM_KHR : VkFormat = VK_FORMAT_B8G8R8G8_422_UNORM;
pub const VK_FORMAT_G8_B8_R8_3PLANE_420_UNORM_KHR : VkFormat = VK_FORMAT_G8_B8_R8_3PLANE_420_UNORM;
pub const VK_FORMAT_G8_B8R8_2PLANE_420_UNORM_KHR : VkFormat = VK_FORMAT_G8_B8R8_2PLANE_420_UNORM;
pub const VK_FORMAT_G8_B8_R8_3PLANE_422_UNORM_KHR : VkFormat = VK_FORMAT_G8_B8_R8_3PLANE_422_UNORM;
pub const VK_FORMAT_G8_B8R8_2PLANE_422_UNORM_KHR : VkFormat = VK_FORMAT_G8_B8R8_2PLANE_422_UNORM;
pub const VK_FORMAT_G8_B8_R8_3PLANE_444_UNORM_KHR : VkFormat = VK_FORMAT_G8_B8_R8_3PLANE_444_UNORM;
pub const VK_FORMAT_R10X6_UNORM_PACK16_KHR : VkFormat = VK_FORMAT_R10X6_UNORM_PACK16;
pub const VK_FORMAT_R10X6G10X6_UNORM_2PACK16_KHR : VkFormat = VK_FORMAT_R10X6G10X6_UNORM_2PACK16;
pub const VK_FORMAT_R10X6G10X6B10X6A10X6_UNORM_4PACK16_KHR : VkFormat = VK_FORMAT_R10X6G10X6B10X6A10X6_UNORM_4PACK16;
pub const VK_FORMAT_G10X6B10X6G10X6R10X6_422_UNORM_4PACK16_KHR : VkFormat = VK_FORMAT_G10X6B10X6G10X6R10X6_422_UNORM_4PACK16;
pub const VK_FORMAT_B10X6G10X6R10X6G10X6_422_UNORM_4PACK16_KHR : VkFormat = VK_FORMAT_B10X6G10X6R10X6G10X6_422_UNORM_4PACK16;
pub const VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_420_UNORM_3PACK16_KHR : VkFormat = VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_420_UNORM_3PACK16;
pub const VK_FORMAT_G10X6_B10X6R10X6_2PLANE_420_UNORM_3PACK16_KHR : VkFormat = VK_FORMAT_G10X6_B10X6R10X6_2PLANE_420_UNORM_3PACK16;
pub const VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_422_UNORM_3PACK16_KHR : VkFormat = VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_422_UNORM_3PACK16;
pub const VK_FORMAT_G10X6_B10X6R10X6_2PLANE_422_UNORM_3PACK16_KHR : VkFormat = VK_FORMAT_G10X6_B10X6R10X6_2PLANE_422_UNORM_3PACK16;
pub const VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_444_UNORM_3PACK16_KHR : VkFormat = VK_FORMAT_G10X6_B10X6_R10X6_3PLANE_444_UNORM_3PACK16;
pub const VK_FORMAT_R12X4_UNORM_PACK16_KHR : VkFormat = VK_FORMAT_R12X4_UNORM_PACK16;
pub const VK_FORMAT_R12X4G12X4_UNORM_2PACK16_KHR : VkFormat = VK_FORMAT_R12X4G12X4_UNORM_2PACK16;
pub const VK_FORMAT_R12X4G12X4B12X4A12X4_UNORM_4PACK16_KHR : VkFormat = VK_FORMAT_R12X4G12X4B12X4A12X4_UNORM_4PACK16;
pub const VK_FORMAT_G12X4B12X4G12X4R12X4_422_UNORM_4PACK16_KHR : VkFormat = VK_FORMAT_G12X4B12X4G12X4R12X4_422_UNORM_4PACK16;
pub const VK_FORMAT_B12X4G12X4R12X4G12X4_422_UNORM_4PACK16_KHR : VkFormat = VK_FORMAT_B12X4G12X4R12X4G12X4_422_UNORM_4PACK16;
pub const VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_420_UNORM_3PACK16_KHR : VkFormat = VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_420_UNORM_3PACK16;
pub const VK_FORMAT_G12X4_B12X4R12X4_2PLANE_420_UNORM_3PACK16_KHR : VkFormat = VK_FORMAT_G12X4_B12X4R12X4_2PLANE_420_UNORM_3PACK16;
pub const VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_422_UNORM_3PACK16_KHR : VkFormat = VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_422_UNORM_3PACK16;
pub const VK_FORMAT_G12X4_B12X4R12X4_2PLANE_422_UNORM_3PACK16_KHR : VkFormat = VK_FORMAT_G12X4_B12X4R12X4_2PLANE_422_UNORM_3PACK16;
pub const VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_444_UNORM_3PACK16_KHR : VkFormat = VK_FORMAT_G12X4_B12X4_R12X4_3PLANE_444_UNORM_3PACK16;
pub const VK_FORMAT_G16B16G16R16_422_UNORM_KHR : VkFormat = VK_FORMAT_G16B16G16R16_422_UNORM;
pub const VK_FORMAT_B16G16R16G16_422_UNORM_KHR : VkFormat = VK_FORMAT_B16G16R16G16_422_UNORM;
pub const VK_FORMAT_G16_B16_R16_3PLANE_420_UNORM_KHR : VkFormat = VK_FORMAT_G16_B16_R16_3PLANE_420_UNORM;
pub const VK_FORMAT_G16_B16R16_2PLANE_420_UNORM_KHR : VkFormat = VK_FORMAT_G16_B16R16_2PLANE_420_UNORM;
pub const VK_FORMAT_G16_B16_R16_3PLANE_422_UNORM_KHR : VkFormat = VK_FORMAT_G16_B16_R16_3PLANE_422_UNORM;
pub const VK_FORMAT_G16_B16R16_2PLANE_422_UNORM_KHR : VkFormat = VK_FORMAT_G16_B16R16_2PLANE_422_UNORM;
pub const VK_FORMAT_G16_B16_R16_3PLANE_444_UNORM_KHR : VkFormat = VK_FORMAT_G16_B16_R16_3PLANE_444_UNORM;
pub const VK_FORMAT_BEGIN_RANGE : VkFormat = VK_FORMAT_UNDEFINED;
pub const VK_FORMAT_END_RANGE : VkFormat = VK_FORMAT_ASTC_12x12_SRGB_BLOCK;
pub const VK_FORMAT_RANGE_SIZE : VkFormat = (VK_FORMAT_ASTC_12x12_SRGB_BLOCK - VK_FORMAT_UNDEFINED + 1);
pub const VK_FORMAT_MAX_ENUM : VkFormat = 0x7FFFFFFF;

pub const VK_COLOR_SPACE_SRGB_NONLINEAR_KHR : VkColorSpaceKHR = 0;
pub const VK_COLOR_SPACE_DISPLAY_P3_NONLINEAR_EXT : VkColorSpaceKHR = 1000104001;
pub const VK_COLOR_SPACE_EXTENDED_SRGB_LINEAR_EXT : VkColorSpaceKHR = 1000104002;
pub const VK_COLOR_SPACE_DISPLAY_P3_LINEAR_EXT : VkColorSpaceKHR = 1000104003;
pub const VK_COLOR_SPACE_DCI_P3_NONLINEAR_EXT : VkColorSpaceKHR = 1000104004;
pub const VK_COLOR_SPACE_BT709_LINEAR_EXT : VkColorSpaceKHR = 1000104005;
pub const VK_COLOR_SPACE_BT709_NONLINEAR_EXT : VkColorSpaceKHR = 1000104006;
pub const VK_COLOR_SPACE_BT2020_LINEAR_EXT : VkColorSpaceKHR = 1000104007;
pub const VK_COLOR_SPACE_HDR10_ST2084_EXT : VkColorSpaceKHR = 1000104008;
pub const VK_COLOR_SPACE_DOLBYVISION_EXT : VkColorSpaceKHR = 1000104009;
pub const VK_COLOR_SPACE_HDR10_HLG_EXT : VkColorSpaceKHR = 1000104010;
pub const VK_COLOR_SPACE_ADOBERGB_LINEAR_EXT : VkColorSpaceKHR = 1000104011;
pub const VK_COLOR_SPACE_ADOBERGB_NONLINEAR_EXT : VkColorSpaceKHR = 1000104012;
pub const VK_COLOR_SPACE_PASS_THROUGH_EXT : VkColorSpaceKHR = 1000104013;
pub const VK_COLOR_SPACE_EXTENDED_SRGB_NONLINEAR_EXT : VkColorSpaceKHR = 1000104014;
pub const VK_COLOR_SPACE_DISPLAY_NATIVE_AMD : VkColorSpaceKHR = 1000213000;
pub const VK_COLORSPACE_SRGB_NONLINEAR_KHR : VkColorSpaceKHR = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
pub const VK_COLOR_SPACE_DCI_P3_LINEAR_EXT : VkColorSpaceKHR = VK_COLOR_SPACE_DISPLAY_P3_LINEAR_EXT;
pub const VK_COLOR_SPACE_BEGIN_RANGE_KHR : VkColorSpaceKHR = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
pub const VK_COLOR_SPACE_END_RANGE_KHR : VkColorSpaceKHR = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
pub const VK_COLOR_SPACE_RANGE_SIZE_KHR : VkColorSpaceKHR = (VK_COLOR_SPACE_SRGB_NONLINEAR_KHR - VK_COLOR_SPACE_SRGB_NONLINEAR_KHR + 1);
pub const VK_COLOR_SPACE_MAX_ENUM_KHR : VkColorSpaceKHR = 0x7FFFFFFF;

pub const VK_PRESENT_MODE_IMMEDIATE_KHR : VkPresentModeKHR = 0;
pub const VK_PRESENT_MODE_MAILBOX_KHR : VkPresentModeKHR = 1;
pub const VK_PRESENT_MODE_FIFO_KHR : VkPresentModeKHR = 2;
pub const VK_PRESENT_MODE_FIFO_RELAXED_KHR : VkPresentModeKHR = 3;
pub const VK_PRESENT_MODE_SHARED_DEMAND_REFRESH_KHR : VkPresentModeKHR = 1000111000;
pub const VK_PRESENT_MODE_SHARED_CONTINUOUS_REFRESH_KHR : VkPresentModeKHR = 1000111001;
pub const VK_PRESENT_MODE_BEGIN_RANGE_KHR : VkPresentModeKHR = VK_PRESENT_MODE_IMMEDIATE_KHR;
pub const VK_PRESENT_MODE_END_RANGE_KHR : VkPresentModeKHR = VK_PRESENT_MODE_FIFO_RELAXED_KHR;
pub const VK_PRESENT_MODE_RANGE_SIZE_KHR : VkPresentModeKHR = (VK_PRESENT_MODE_FIFO_RELAXED_KHR - VK_PRESENT_MODE_IMMEDIATE_KHR + 1);
pub const VK_PRESENT_MODE_MAX_ENUM_KHR : VkPresentModeKHR = 0x7FFFFFFF;

pub const VK_SHADER_STAGE_VERTEX_BIT : VkShaderStageFlagBits = 0x00000001;
pub const VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT : VkShaderStageFlagBits = 0x00000002;
pub const VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT : VkShaderStageFlagBits = 0x00000004;
pub const VK_SHADER_STAGE_GEOMETRY_BIT : VkShaderStageFlagBits = 0x00000008;
pub const VK_SHADER_STAGE_FRAGMENT_BIT : VkShaderStageFlagBits = 0x00000010;
pub const VK_SHADER_STAGE_COMPUTE_BIT : VkShaderStageFlagBits = 0x00000020;
pub const VK_SHADER_STAGE_ALL_GRAPHICS : VkShaderStageFlagBits = 0x0000001F;
pub const VK_SHADER_STAGE_ALL : VkShaderStageFlagBits = 0x7FFFFFFF;
pub const VK_SHADER_STAGE_RAYGEN_BIT_NV : VkShaderStageFlagBits = 0x00000100;
pub const VK_SHADER_STAGE_ANY_HIT_BIT_NV : VkShaderStageFlagBits = 0x00000200;
pub const VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV : VkShaderStageFlagBits = 0x00000400;
pub const VK_SHADER_STAGE_MISS_BIT_NV : VkShaderStageFlagBits = 0x00000800;
pub const VK_SHADER_STAGE_INTERSECTION_BIT_NV : VkShaderStageFlagBits = 0x00001000;
pub const VK_SHADER_STAGE_CALLABLE_BIT_NV : VkShaderStageFlagBits = 0x00002000;
pub const VK_SHADER_STAGE_TASK_BIT_NV : VkShaderStageFlagBits = 0x00000040;
pub const VK_SHADER_STAGE_MESH_BIT_NV : VkShaderStageFlagBits = 0x00000080;
pub const VK_SHADER_STAGE_FLAG_BITS_MAX_ENUM : VkShaderStageFlagBits = 0x7FFFFFFF;

pub const VK_SHARING_MODE_EXCLUSIVE : VkSharingMode = 0;
pub const VK_SHARING_MODE_CONCURRENT : VkSharingMode = 1;
pub const VK_SHARING_MODE_BEGIN_RANGE : VkSharingMode = VK_SHARING_MODE_EXCLUSIVE;
pub const VK_SHARING_MODE_END_RANGE : VkSharingMode = VK_SHARING_MODE_CONCURRENT;
pub const VK_SHARING_MODE_RANGE_SIZE : VkSharingMode = (VK_SHARING_MODE_CONCURRENT - VK_SHARING_MODE_EXCLUSIVE + 1);
pub const VK_SHARING_MODE_MAX_ENUM : VkSharingMode = 0x7FFFFFFF;

pub const VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR : VkCompositeAlphaFlagBitsKHR = 0x00000001;
pub const VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR : VkCompositeAlphaFlagBitsKHR = 0x00000002;
pub const VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR : VkCompositeAlphaFlagBitsKHR = 0x00000004;
pub const VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR : VkCompositeAlphaFlagBitsKHR = 0x00000008;
pub const VK_COMPOSITE_ALPHA_FLAG_BITS_MAX_ENUM_KHR : VkCompositeAlphaFlagBitsKHR = 0x7FFFFFFF;

pub const VK_IMAGE_USAGE_TRANSFER_SRC_BIT : VkImageUsageFlagBits = 0x00000001;
pub const VK_IMAGE_USAGE_TRANSFER_DST_BIT : VkImageUsageFlagBits = 0x00000002;
pub const VK_IMAGE_USAGE_SAMPLED_BIT : VkImageUsageFlagBits = 0x00000004;
pub const VK_IMAGE_USAGE_STORAGE_BIT : VkImageUsageFlagBits = 0x00000008;
pub const VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT : VkImageUsageFlagBits = 0x00000010;
pub const VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT : VkImageUsageFlagBits = 0x00000020;
pub const VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT : VkImageUsageFlagBits = 0x00000040;
pub const VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT : VkImageUsageFlagBits = 0x00000080;
pub const VK_IMAGE_USAGE_SHADING_RATE_IMAGE_BIT_NV : VkImageUsageFlagBits = 0x00000100;
pub const VK_IMAGE_USAGE_FRAGMENT_DENSITY_MAP_BIT_EXT : VkImageUsageFlagBits = 0x00000200;
pub const VK_IMAGE_USAGE_FLAG_BITS_MAX_ENUM : VkImageUsageFlagBits = 0x7FFFFFFF;

pub const VK_COMPONENT_SWIZZLE_IDENTITY : VkComponentSwizzle = 0;
pub const VK_COMPONENT_SWIZZLE_ZERO : VkComponentSwizzle = 1;
pub const VK_COMPONENT_SWIZZLE_ONE : VkComponentSwizzle = 2;
pub const VK_COMPONENT_SWIZZLE_R : VkComponentSwizzle = 3;
pub const VK_COMPONENT_SWIZZLE_G : VkComponentSwizzle = 4;
pub const VK_COMPONENT_SWIZZLE_B : VkComponentSwizzle = 5;
pub const VK_COMPONENT_SWIZZLE_A : VkComponentSwizzle = 6;
pub const VK_COMPONENT_SWIZZLE_BEGIN_RANGE : VkComponentSwizzle = VK_COMPONENT_SWIZZLE_IDENTITY;
pub const VK_COMPONENT_SWIZZLE_END_RANGE : VkComponentSwizzle = VK_COMPONENT_SWIZZLE_A;
pub const VK_COMPONENT_SWIZZLE_RANGE_SIZE : VkComponentSwizzle = (VK_COMPONENT_SWIZZLE_A - VK_COMPONENT_SWIZZLE_IDENTITY + 1);
pub const VK_COMPONENT_SWIZZLE_MAX_ENUM : VkComponentSwizzle = 0x7FFFFFFF;

pub const VK_IMAGE_VIEW_TYPE_1D : VkImageViewType = 0;
pub const VK_IMAGE_VIEW_TYPE_2D : VkImageViewType = 1;
pub const VK_IMAGE_VIEW_TYPE_3D : VkImageViewType = 2;
pub const VK_IMAGE_VIEW_TYPE_CUBE : VkImageViewType = 3;
pub const VK_IMAGE_VIEW_TYPE_1D_ARRAY : VkImageViewType = 4;
pub const VK_IMAGE_VIEW_TYPE_2D_ARRAY : VkImageViewType = 5;
pub const VK_IMAGE_VIEW_TYPE_CUBE_ARRAY : VkImageViewType = 6;
pub const VK_IMAGE_VIEW_TYPE_BEGIN_RANGE : VkImageViewType = VK_IMAGE_VIEW_TYPE_1D;
pub const VK_IMAGE_VIEW_TYPE_END_RANGE : VkImageViewType = VK_IMAGE_VIEW_TYPE_CUBE_ARRAY;
pub const VK_IMAGE_VIEW_TYPE_RANGE_SIZE : VkImageViewType = (VK_IMAGE_VIEW_TYPE_CUBE_ARRAY - VK_IMAGE_VIEW_TYPE_1D + 1);
pub const VK_IMAGE_VIEW_TYPE_MAX_ENUM : VkImageViewType = 0x7FFFFFFF;

pub const VK_IMAGE_ASPECT_COLOR_BIT : VkImageAspectFlagBits = 0x00000001;
pub const VK_IMAGE_ASPECT_DEPTH_BIT : VkImageAspectFlagBits = 0x00000002;
pub const VK_IMAGE_ASPECT_STENCIL_BIT : VkImageAspectFlagBits = 0x00000004;
pub const VK_IMAGE_ASPECT_METADATA_BIT : VkImageAspectFlagBits = 0x00000008;
pub const VK_IMAGE_ASPECT_PLANE_0_BIT : VkImageAspectFlagBits = 0x00000010;
pub const VK_IMAGE_ASPECT_PLANE_1_BIT : VkImageAspectFlagBits = 0x00000020;
pub const VK_IMAGE_ASPECT_PLANE_2_BIT : VkImageAspectFlagBits = 0x00000040;
pub const VK_IMAGE_ASPECT_MEMORY_PLANE_0_BIT_EXT : VkImageAspectFlagBits = 0x00000080;
pub const VK_IMAGE_ASPECT_MEMORY_PLANE_1_BIT_EXT : VkImageAspectFlagBits = 0x00000100;
pub const VK_IMAGE_ASPECT_MEMORY_PLANE_2_BIT_EXT : VkImageAspectFlagBits = 0x00000200;
pub const VK_IMAGE_ASPECT_MEMORY_PLANE_3_BIT_EXT : VkImageAspectFlagBits = 0x00000400;
pub const VK_IMAGE_ASPECT_PLANE_0_BIT_KHR : VkImageAspectFlagBits = VK_IMAGE_ASPECT_PLANE_0_BIT;
pub const VK_IMAGE_ASPECT_PLANE_1_BIT_KHR : VkImageAspectFlagBits = VK_IMAGE_ASPECT_PLANE_1_BIT;
pub const VK_IMAGE_ASPECT_PLANE_2_BIT_KHR : VkImageAspectFlagBits = VK_IMAGE_ASPECT_PLANE_2_BIT;
pub const VK_IMAGE_ASPECT_FLAG_BITS_MAX_ENUM : VkImageAspectFlagBits = 0x7FFFFFFF;

pub const VK_SAMPLE_COUNT_1_BIT : VkSampleCountFlagBits = 0x00000001;
pub const VK_SAMPLE_COUNT_2_BIT : VkSampleCountFlagBits = 0x00000002;
pub const VK_SAMPLE_COUNT_4_BIT : VkSampleCountFlagBits = 0x00000004;
pub const VK_SAMPLE_COUNT_8_BIT : VkSampleCountFlagBits = 0x00000008;
pub const VK_SAMPLE_COUNT_16_BIT : VkSampleCountFlagBits = 0x00000010;
pub const VK_SAMPLE_COUNT_32_BIT : VkSampleCountFlagBits = 0x00000020;
pub const VK_SAMPLE_COUNT_64_BIT : VkSampleCountFlagBits = 0x00000040;
pub const VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM : VkSampleCountFlagBits = 0x7FFFFFFF;

pub const VK_VERTEX_INPUT_RATE_VERTEX : VkVertexInputRate = 0;
pub const VK_VERTEX_INPUT_RATE_INSTANCE : VkVertexInputRate = 1;
pub const VK_VERTEX_INPUT_RATE_BEGIN_RANGE : VkVertexInputRate = VK_VERTEX_INPUT_RATE_VERTEX;
pub const VK_VERTEX_INPUT_RATE_END_RANGE : VkVertexInputRate = VK_VERTEX_INPUT_RATE_INSTANCE;
pub const VK_VERTEX_INPUT_RATE_RANGE_SIZE : VkVertexInputRate = (VK_VERTEX_INPUT_RATE_INSTANCE - VK_VERTEX_INPUT_RATE_VERTEX + 1);
pub const VK_VERTEX_INPUT_RATE_MAX_ENUM : VkVertexInputRate = 0x7FFFFFFF;

pub const VK_DEBUG_REPORT_INFORMATION_BIT_EXT : VkDebugReportFlagBitsEXT = 0x00000001;
pub const VK_DEBUG_REPORT_WARNING_BIT_EXT : VkDebugReportFlagBitsEXT = 0x00000002;
pub const VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT : VkDebugReportFlagBitsEXT = 0x00000004;
pub const VK_DEBUG_REPORT_ERROR_BIT_EXT : VkDebugReportFlagBitsEXT = 0x00000008;
pub const VK_DEBUG_REPORT_DEBUG_BIT_EXT : VkDebugReportFlagBitsEXT = 0x00000010;
pub const VK_DEBUG_REPORT_FLAG_BITS_MAX_ENUM_EXT : VkDebugReportFlagBitsEXT = 0x7FFFFFFF;

pub const VK_DEBUG_REPORT_OBJECT_TYPE_UNKNOWN_EXT : VkDebugReportObjectTypeEXT = 0;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_INSTANCE_EXT : VkDebugReportObjectTypeEXT = 1;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_PHYSICAL_DEVICE_EXT : VkDebugReportObjectTypeEXT = 2;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_EXT : VkDebugReportObjectTypeEXT = 3;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_QUEUE_EXT : VkDebugReportObjectTypeEXT = 4;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_SEMAPHORE_EXT : VkDebugReportObjectTypeEXT = 5;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_BUFFER_EXT : VkDebugReportObjectTypeEXT = 6;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_FENCE_EXT : VkDebugReportObjectTypeEXT = 7;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_MEMORY_EXT : VkDebugReportObjectTypeEXT = 8;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT : VkDebugReportObjectTypeEXT = 9;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_EXT : VkDebugReportObjectTypeEXT = 10;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_EVENT_EXT : VkDebugReportObjectTypeEXT = 11;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_QUERY_POOL_EXT : VkDebugReportObjectTypeEXT = 12;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_VIEW_EXT : VkDebugReportObjectTypeEXT = 13;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_VIEW_EXT : VkDebugReportObjectTypeEXT = 14;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_SHADER_MODULE_EXT : VkDebugReportObjectTypeEXT = 15;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_CACHE_EXT : VkDebugReportObjectTypeEXT = 16;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_LAYOUT_EXT : VkDebugReportObjectTypeEXT = 17;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_RENDER_PASS_EXT : VkDebugReportObjectTypeEXT = 18;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_EXT : VkDebugReportObjectTypeEXT = 19;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT_EXT : VkDebugReportObjectTypeEXT = 20;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_EXT : VkDebugReportObjectTypeEXT = 21;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_POOL_EXT : VkDebugReportObjectTypeEXT = 22;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_EXT : VkDebugReportObjectTypeEXT = 23;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_FRAMEBUFFER_EXT : VkDebugReportObjectTypeEXT = 24;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_POOL_EXT : VkDebugReportObjectTypeEXT = 25;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_SURFACE_KHR_EXT : VkDebugReportObjectTypeEXT = 26;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_SWAPCHAIN_KHR_EXT : VkDebugReportObjectTypeEXT = 27;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_DEBUG_REPORT_CALLBACK_EXT_EXT : VkDebugReportObjectTypeEXT = 28;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_DISPLAY_KHR_EXT : VkDebugReportObjectTypeEXT = 29;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_DISPLAY_MODE_KHR_EXT : VkDebugReportObjectTypeEXT = 30;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_VALIDATION_CACHE_EXT_EXT : VkDebugReportObjectTypeEXT = 33;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_YCBCR_CONVERSION_EXT : VkDebugReportObjectTypeEXT = 1000156000;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_EXT : VkDebugReportObjectTypeEXT = 1000085000;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_ACCELERATION_STRUCTURE_KHR_EXT : VkDebugReportObjectTypeEXT = 1000165000;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_DEBUG_REPORT_EXT : VkDebugReportObjectTypeEXT = VK_DEBUG_REPORT_OBJECT_TYPE_DEBUG_REPORT_CALLBACK_EXT_EXT;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_VALIDATION_CACHE_EXT : VkDebugReportObjectTypeEXT = VK_DEBUG_REPORT_OBJECT_TYPE_VALIDATION_CACHE_EXT_EXT;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_KHR_EXT : VkDebugReportObjectTypeEXT = VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_UPDATE_TEMPLATE_EXT;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_YCBCR_CONVERSION_KHR_EXT : VkDebugReportObjectTypeEXT = VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_YCBCR_CONVERSION_EXT;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_ACCELERATION_STRUCTURE_NV_EXT : VkDebugReportObjectTypeEXT = VK_DEBUG_REPORT_OBJECT_TYPE_ACCELERATION_STRUCTURE_KHR_EXT;
pub const VK_DEBUG_REPORT_OBJECT_TYPE_MAX_ENUM_EXT : VkDebugReportObjectTypeEXT = 0x7FFFFFFF;

pub const VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT : VkDebugUtilsMessageTypeFlagBitsEXT = 0x00000001;
pub const VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT : VkDebugUtilsMessageTypeFlagBitsEXT = 0x00000002;
pub const VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT : VkDebugUtilsMessageTypeFlagBitsEXT = 0x00000004;
pub const VK_DEBUG_UTILS_MESSAGE_TYPE_FLAG_BITS_MAX_ENUM_EXT : VkDebugUtilsMessageTypeFlagBitsEXT = 0x7FFFFFFF;

pub const VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT : VkDebugUtilsMessageSeverityFlagBitsEXT = 0x00000001;
pub const VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT : VkDebugUtilsMessageSeverityFlagBitsEXT = 0x00000010;
pub const VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT : VkDebugUtilsMessageSeverityFlagBitsEXT = 0x00000100;
pub const VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT : VkDebugUtilsMessageSeverityFlagBitsEXT = 0x00001000;
pub const VK_DEBUG_UTILS_MESSAGE_SEVERITY_FLAG_BITS_MAX_ENUM_EXT : VkDebugUtilsMessageSeverityFlagBitsEXT = 0x7FFFFFFF;

pub const VK_PIPELINE_SHADER_STAGE_CREATE_ALLOW_VARYING_SUBGROUP_SIZE_BIT_EXT : VkPipelineShaderStageCreateFlagBits = 0x00000001;
pub const VK_PIPELINE_SHADER_STAGE_CREATE_REQUIRE_FULL_SUBGROUPS_BIT_EXT : VkPipelineShaderStageCreateFlagBits = 0x00000002;
pub const VK_PIPELINE_SHADER_STAGE_CREATE_FLAG_BITS_MAX_ENUM : VkPipelineShaderStageCreateFlagBits = 0x7FFFFFFF;

#[allow(non_snake_case)]
#[repr(C)]
pub struct VkExtensionProperties {
    pub extensionName : [raw::c_char; VK_MAX_EXTENSION_NAME_SIZE],
    pub specVersion : u32
}

#[allow(non_snake_case)]
#[repr(C)]
pub struct VkLayerProperties {
    pub layerName : [raw::c_char; VK_MAX_EXTENSION_NAME_SIZE],
    pub specVersion : u32,
    pub implementationVersion : u32,
    pub description : [raw::c_char; VK_MAX_DESCRIPTION_SIZE]
}

#[allow(non_snake_case)]
#[repr(C)]
pub struct VkApplicationInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub pApplicationName : *const u8,
    pub applicationVersion : u32,
    pub pEngineName : *const u8,
    pub engineVersion : u32,
    pub apiVersion : u32
}


#[allow(non_snake_case)]
#[repr(C)]
pub struct VkInstanceCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkInstanceCreateFlags,
    pub pApplicationInfo : *const VkApplicationInfo,
    pub enabledLayerCount : u32,
    pub ppEnabledLayerNames : *const *const raw::c_char,
    pub enabledExtensionCount : u32,
    pub ppEnabledExtensionNames : *const *const raw::c_char
}

#[allow(non_snake_case)]
#[repr(C)]
pub struct VkAllocationCallbacks {
    pub pUserData : *const raw::c_void,
    pub pfnAllocation : PFN_vkAllocationFunction,
    pub pfnReallocation : PFN_vkReallocationFunction,
    pub pfnFree : PFN_vkFreeFunction,
    pub pfnInternalAllocation : PFN_vkInternalAllocationNotification,
    pub pfnInternalFree : PFN_vkInternalFreeNotification
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkPhysicalDeviceProperties {
    pub apiVersion : u32,
    pub driverVersion : u32,
    pub vendorID : u32,
    pub deviceID : u32,
    pub deviceType : VkPhysicalDeviceType,
    pub deviceName : [i8; VK_MAX_PHYSICAL_DEVICE_NAME_SIZE],
    pub pipelineCacheUUID : [u8; VK_UUID_SIZE],
    pub limits : VkPhysicalDeviceLimits,
    pub sparseProperties : VkPhysicalDeviceSparseProperties
}

impl Default for VkPhysicalDeviceProperties
{
    fn default() -> VkPhysicalDeviceProperties
    {
        VkPhysicalDeviceProperties
        {
            apiVersion : 0,
            driverVersion : 0,
            vendorID : 0,
            deviceID : 0,
            deviceType : VK_PHYSICAL_DEVICE_TYPE_OTHER,
            deviceName : [0; VK_MAX_PHYSICAL_DEVICE_NAME_SIZE],
            pipelineCacheUUID : [0; VK_UUID_SIZE],
            limits : VkPhysicalDeviceLimits::default(),
            sparseProperties : VkPhysicalDeviceSparseProperties::default()
        }
    }
}

#[derive(Default, Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkQueueFamilyProperties {
    pub queueFlags : VkQueueFlags,
    pub queueCount : u32,
    pub timestampValidBits : u32,
    pub minImageTransferGranularity : VkExtent3D
}

#[derive(Default, Debug, Copy, Clone)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkExtent2D {
    pub width : u32,
    pub height : u32
}

#[derive(Default, Debug, Copy, Clone)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkExtent3D {
    pub width : u32,
    pub height : u32,
    pub depth : u32
}

#[derive(Default, Debug, Copy, Clone)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkOffset3D {
    pub x : i32,
    pub y : i32,
    pub z : i32
}

#[derive(Default, Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkPhysicalDeviceSparseProperties {
    pub residencyStandard2DBlockShape : VkBool32,
    pub residencyStandard2DMultisampleBlockShape : VkBool32,
    pub residencyStandard3DBlockShape : VkBool32,
    pub residencyAlignedMipSize : VkBool32,
    pub residencyNonResidentStrict : VkBool32
}

#[derive(Default, Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkPhysicalDeviceFeatures {
    pub robustBufferAccess : VkBool32,
    pub fullDrawIndexUint32 : VkBool32,
    pub imageCubeArray : VkBool32,
    pub independentBlend : VkBool32,
    pub geometryShader : VkBool32,
    pub tessellationShader : VkBool32,
    pub sampleRateShading : VkBool32,
    pub dualSrcBlend : VkBool32,
    pub logicOp : VkBool32,
    pub multiDrawIndirect : VkBool32,
    pub drawIndirectFirstInstance : VkBool32,
    pub depthClamp : VkBool32,
    pub depthBiasClamp : VkBool32,
    pub fillModeNonSolid : VkBool32,
    pub depthBounds : VkBool32,
    pub wideLines : VkBool32,
    pub largePoints : VkBool32,
    pub alphaToOne : VkBool32,
    pub multiViewport : VkBool32,
    pub samplerAnisotropy : VkBool32,
    pub textureCompressionETC2 : VkBool32,
    pub textureCompressionASTC_LDR : VkBool32,
    pub textureCompressionBC : VkBool32,
    pub occlusionQueryPrecise : VkBool32,
    pub pipelineStatisticsQuery : VkBool32,
    pub vertexPipelineStoresAndAtomics : VkBool32,
    pub fragmentStoresAndAtomics : VkBool32,
    pub shaderTessellationAndGeometryPointSize : VkBool32,
    pub shaderImageGatherExtended : VkBool32,
    pub shaderStorageImageExtendedFormats : VkBool32,
    pub shaderStorageImageMultisample : VkBool32,
    pub shaderStorageImageReadWithoutFormat : VkBool32,
    pub shaderStorageImageWriteWithoutFormat : VkBool32,
    pub shaderUniformBufferArrayDynamicIndexing : VkBool32,
    pub shaderSampledImageArrayDynamicIndexing : VkBool32,
    pub shaderStorageBufferArrayDynamicIndexing : VkBool32,
    pub shaderStorageImageArrayDynamicIndexing : VkBool32,
    pub shaderClipDistance : VkBool32,
    pub shaderCullDistance : VkBool32,
    pub shaderFloat64 : VkBool32,
    pub shaderInt64 : VkBool32,
    pub shaderInt16 : VkBool32,
    pub shaderResourceResidency : VkBool32,
    pub shaderResourceMinLod : VkBool32,
    pub sparseBinding : VkBool32,
    pub sparseResidencyBuffer : VkBool32,
    pub sparseResidencyImage2D : VkBool32,
    pub sparseResidencyImage3D : VkBool32,
    pub sparseResidency2Samples : VkBool32,
    pub sparseResidency4Samples : VkBool32,
    pub sparseResidency8Samples : VkBool32,
    pub sparseResidency16Samples : VkBool32,
    pub sparseResidencyAliased : VkBool32,
    pub variableMultisampleRate : VkBool32,
    pub inheritedQueries : VkBool32
}

#[derive(Default, Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkPhysicalDeviceLimits {
    pub maxImageDimension1D : u32,
    pub maxImageDimension2D : u32,
    pub maxImageDimension3D : u32,
    pub maxImageDimensionCube : u32,
    pub maxImageArrayLayers : u32,
    pub maxTexelBufferElements : u32,
    pub maxUniformBufferRange : u32,
    pub maxStorageBufferRange : u32,
    pub maxPushConstantsSize : u32,
    pub maxMemoryAllocationCount : u32,
    pub maxSamplerAllocationCount : u32,
    pub bufferImageGranularity : VkDeviceSize,
    pub sparseAddressSpaceSize : VkDeviceSize,
    pub maxBoundDescriptorSets : u32,
    pub maxPerStageDescriptorSamplers : u32,
    pub maxPerStageDescriptorUniformBuffers : u32,
    pub maxPerStageDescriptorStorageBuffers : u32,
    pub maxPerStageDescriptorSampledImages : u32,
    pub maxPerStageDescriptorStorageImages : u32,
    pub maxPerStageDescriptorInputAttachments : u32,
    pub maxPerStageResources : u32,
    pub maxDescriptorSetSamplers : u32,
    pub maxDescriptorSetUniformBuffers : u32,
    pub maxDescriptorSetUniformBuffersDynamic : u32,
    pub maxDescriptorSetStorageBuffers : u32,
    pub maxDescriptorSetStorageBuffersDynamic : u32,
    pub maxDescriptorSetSampledImages : u32,
    pub maxDescriptorSetStorageImages : u32,
    pub maxDescriptorSetInputAttachments : u32,
    pub maxVertexInputAttributes : u32,
    pub maxVertexInputBindings : u32,
    pub maxVertexInputAttributeOffset : u32,
    pub maxVertexInputBindingStride : u32,
    pub maxVertexOutputComponents : u32,
    pub maxTessellationGenerationLevel : u32,
    pub maxTessellationPatchSize : u32,
    pub maxTessellationControlPerVertexInputComponents : u32,
    pub maxTessellationControlPerVertexOutputComponents : u32,
    pub maxTessellationControlPerPatchOutputComponents : u32,
    pub maxTessellationControlTotalOutputComponents : u32,
    pub maxTessellationEvaluationInputComponents : u32,
    pub maxTessellationEvaluationOutputComponents : u32,
    pub maxGeometryShaderInvocations : u32,
    pub maxGeometryInputComponents : u32,
    pub maxGeometryOutputComponents : u32,
    pub maxGeometryOutputVertices : u32,
    pub maxGeometryTotalOutputComponents : u32,
    pub maxFragmentInputComponents : u32,
    pub maxFragmentOutputAttachments : u32,
    pub maxFragmentDualSrcAttachments : u32,
    pub maxFragmentCombinedOutputResources : u32,
    pub maxComputeSharedMemorySize : u32,
    pub maxComputeWorkGroupCount : [u32; 3],
    pub maxComputeWorkGroupInvocations : u32,
    pub maxComputeWorkGroupSize : [u32; 3],
    pub subPixelPrecisionBits : u32,
    pub subTexelPrecisionBits : u32,
    pub mipmapPrecisionBits : u32,
    pub maxDrawIndexedIndexValue : u32,
    pub maxDrawIndirectCount : u32,
    pub maxSamplerLodBias : f32,
    pub maxSamplerAnisotropy : f32,
    pub maxViewports : u32,
    pub maxViewportDimensions : [u32; 2],
    pub viewportBoundsRange : [f32; 2],
    pub viewportSubPixelBits : u32,
    pub minMemoryMapAlignment : usize,
    pub minTexelBufferOffsetAlignment : VkDeviceSize,
    pub minUniformBufferOffsetAlignment : VkDeviceSize,
    pub minStorageBufferOffsetAlignment : VkDeviceSize,
    pub minTexelOffset : i32,
    pub maxTexelOffset : u32,
    pub minTexelGatherOffset : i32,
    pub maxTexelGatherOffset : u32,
    pub minInterpolationOffset : f32,
    pub maxInterpolationOffset : f32,
    pub subPixelInterpolationOffsetBits : u32,
    pub maxFramebufferWidth : u32,
    pub maxFramebufferHeight : u32,
    pub maxFramebufferLayers : u32,
    pub framebufferColorSampleCounts : VkSampleCountFlags,
    pub framebufferDepthSampleCounts : VkSampleCountFlags,
    pub framebufferStencilSampleCounts : VkSampleCountFlags,
    pub framebufferNoAttachmentsSampleCounts : VkSampleCountFlags,
    pub maxColorAttachments : u32,
    pub sampledImageColorSampleCounts : VkSampleCountFlags,
    pub sampledImageIntegerSampleCounts : VkSampleCountFlags,
    pub sampledImageDepthSampleCounts : VkSampleCountFlags,
    pub sampledImageStencilSampleCounts : VkSampleCountFlags,
    pub storageImageSampleCounts : VkSampleCountFlags,
    pub maxSampleMaskWords : u32,
    pub timestampComputeAndGraphics : VkBool32,
    pub timestampPeriod : f32,
    pub maxClipDistances : u32,
    pub maxCullDistances : u32,
    pub maxCombinedClipAndCullDistances : u32,
    pub discreteQueuePriorities : u32,
    pub pointSizeRange : [f32; 2],
    pub lineWidthRange : [f32; 2],
    pub pointSizeGranularity : f32,
    pub lineWidthGranularity : f32,
    pub strictLines : VkBool32,
    pub standardSampleLocations : VkBool32,
    pub optimalBufferCopyOffsetAlignment : VkDeviceSize,
    pub optimalBufferCopyRowPitchAlignment : VkDeviceSize,
    pub nonCoherentAtomSize : VkDeviceSize
}

#[allow(non_snake_case)]
#[repr(C)]
pub struct VkDeviceQueueCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_int,
    pub flags : VkDeviceQueueCreateFlags,
    pub queueFamilyIndex : u32,
    pub queueCount : u32,
    pub pQueuePriorities : *const f32
}

#[allow(non_snake_case)]
#[repr(C)]
pub struct VkDeviceCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkDeviceCreateFlags,
    pub queueCreateInfoCount : u32,
    pub pQueueCreateInfos : *const VkDeviceQueueCreateInfo,
    pub enabledLayerCount : u32,
    pub ppEnabledLayerNames : *const *const u8,
    pub enabledExtensionCount : u32,
    pub ppEnabledExtensionNames : *const *const u8,
    pub pEnabledFeatures : *const VkPhysicalDeviceFeatures
}

#[derive(Default, Debug, Copy, Clone)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkSurfaceCapabilitiesKHR {
    pub minImageCount : u32,
    pub maxImageCount : u32,
    pub currentExtent : VkExtent2D,
    pub minImageExtent : VkExtent2D,
    pub maxImageExtent : VkExtent2D,
    pub maxImageArrayLayers : u32,
    pub supportedTransforms : VkSurfaceTransformFlagsKHR,
    pub currentTransform : VkSurfaceTransformFlagBitsKHR,
    pub supportedCompositeAlpha : VkCompositeAlphaFlagsKHR,
    pub supportedUsageFlags : VkImageUsageFlags
}

#[derive(Default, Debug, Clone, Copy)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkSurfaceFormatKHR {
    pub format : VkFormat,
    pub colorSpace : VkColorSpaceKHR
}

#[allow(non_snake_case)]
#[repr(C)]
pub struct VkSwapchainCreateInfoKHR {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkSwapchainCreateFlagsKHR,
    pub surface : VkSurfaceKHR,
    pub minImageCount : u32,
    pub imageFormat : VkFormat,
    pub imageColorSpace : VkColorSpaceKHR,
    pub imageExtent : VkExtent2D,
    pub imageArrayLayers : u32,
    pub imageUsage : VkImageUsageFlags,
    pub imageSharingMode : VkSharingMode,
    pub queueFamilyIndexCount : u32,
    pub pQueueFamilyIndices : *const u32,
    pub preTransform : VkSurfaceTransformFlagBitsKHR,
    pub compositeAlpha : VkCompositeAlphaFlagBitsKHR,
    pub presentMode : VkPresentModeKHR,
    pub clipped : VkBool32,
    pub oldSwapchain : VkSwapchainKHR
}

#[allow(non_snake_case)]
#[repr(C)]
pub struct VkImageViewCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkImageViewCreateFlags,
    pub image : VkImage,
    pub viewType : VkImageViewType,
    pub format : VkFormat,
    pub components : VkComponentMapping,
    pub subresourceRange : VkImageSubresourceRange
}

#[derive(Default, Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkComponentMapping {
    pub r : VkComponentSwizzle,
    pub g : VkComponentSwizzle,
    pub b : VkComponentSwizzle,
    pub a : VkComponentSwizzle,
}

#[derive(Default, Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkImageSubresourceRange {
    pub aspectMask : VkImageAspectFlags,
    pub baseMipLevel : u32,
    pub levelCount : u32,
    pub baseArrayLayer : u32,
    pub layerCount : u32,
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkShaderModuleCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkShaderModuleCreateFlags,
    pub codeSize : usize,
    pub pCode : *const raw::c_char
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkDebugReportCallbackCreateInfoEXT {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkDebugReportFlagsEXT,
    pub pfnCallback : PFN_vkDebugReportCallbackEXT,
    pub pUserData : *mut raw::c_void
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkDebugUtilsMessengerCreateInfoEXT {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkDebugUtilsMessengerCreateFlagsEXT,
    pub messageSeverity : VkDebugUtilsMessageSeverityFlagsEXT,
    pub messageType : VkDebugUtilsMessageTypeFlagsEXT,
    pub pfnUserCallback : PFN_vkDebugUtilsMessengerCallbackEXT,
    pub pUserData : *mut raw::c_void
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkDebugUtilsMessengerCallbackDataEXT {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkDebugUtilsMessengerCallbackDataFlagsEXT,
    pub pMessageIdName : *const u8,
    pub messageIdNumber : u32,
    pub pMessage : *const u8,
    pub queueLabelCount : u32,
    pub pQueueLabels : *const VkDebugUtilsLabelEXT,
    pub cmdBufLabelCount : u32,
    pub pCmdBufLabels : *const VkDebugUtilsLabelEXT,
    pub objectCount : u32,
    pub pObjects : *const VkDebugUtilsObjectNameInfoEXT
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkDebugUtilsLabelEXT {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub pLabelName : *const u8,
    pub color : [f32; 4],
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkDebugUtilsObjectNameInfoEXT {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub objectType : VkObjectType,
    pub objectHandle : u64,
    pub pObjectName : *const u8
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkPipelineShaderStageCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkPipelineShaderStageCreateFlags,
    pub stage : VkShaderStageFlagBits,
    pub module : VkShaderModule,
    pub pName : *const raw::c_char,
    pub pSpecializationInfo : *const VkSpecializationInfo
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkSpecializationInfo {
    pub mapEntryCount : u32,
    pub pMapEntries : *const VkSpecializationMapEntry,
    pub dataSize : usize,
    pub pData : *const raw::c_void
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkSpecializationMapEntry {
    pub constantID : u32,
    pub offset : u32,
    pub size : usize
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkPipelineVertexInputStateCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkPipelineVertexInputStateCreateFlags,
    pub vertexBindingDescriptionCount : u32,
    pub pVertexBindingDescriptions : *const VkVertexInputBindingDescription,
    pub vertexAttributeDescriptionCount : u32,
    pub pVertexAttributeDescriptions : *const VkVertexInputAttributeDescription
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkVertexInputBindingDescription {
    pub binding : u32,
    pub stride : u32,
    pub inputRate : VkVertexInputRate
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkVertexInputAttributeDescription {
    pub location : u32,
    pub binding : u32,
    pub format : VkFormat,
    pub offset : u32
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkPipelineInputAssemblyStateCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkPipelineInputAssemblyStateCreateFlags,
    pub topology : VkPrimitiveTopology,
    pub primitiveRestartEnable : VkBool32,
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkViewport {
    pub x : f32,
    pub y : f32,
    pub width : f32,
    pub height : f32,
    pub minDepth : f32,
    pub maxDepth : f32
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkRect2D {
    pub offset : VkOffset2D,
    pub extent : VkExtent2D
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkOffset2D {
    pub x : i32,
    pub y : i32
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkPipelineViewportStateCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkPipelineViewportStateCreateFlags,
    pub viewportCount : u32,
    pub pViewports : *const VkViewport,
    pub scissorCount : u32,
    pub pScissors : *const VkRect2D
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkPipelineRasterizationStateCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkPipelineRasterizationStateCreateFlags,
    pub depthClampEnable : VkBool32,
    pub rasterizerDiscardEnable : VkBool32,
    pub polygonMode : VkPolygonMode,
    pub cullMode : VkCullModeFlags,
    pub frontFace : VkFrontFace,
    pub depthBiasEnable : VkBool32,
    pub depthBiasConstantFactor : f32,
    pub depthBiasClamp : f32,
    pub depthBiasSlopeFactor : f32,
    pub lineWidth : f32
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkPipelineMultisampleStateCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkPipelineMultisampleStateCreateFlags,
    pub rasterizationSamples : VkSampleCountFlagBits,
    pub sampleShadingEnable : VkBool32,
    pub minSampleShading : f32,
    pub pSampleMask : *const VkSampleMask,
    pub alphaToCoverageEnable : VkBool32,
    pub alphaToOneEnable : VkBool32
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkPipelineColorBlendAttachmentState {
    pub blendEnable : VkBool32,
    pub srcColorBlendFactor : VkBlendFactor,
    pub dstColorBlendFactor : VkBlendFactor,
    pub colorBlendOp : VkBlendOp,
    pub srcAlphaBlendFactor : VkBlendFactor,
    pub dstAlphaBlendFactor : VkBlendFactor,
    pub alphaBlendOp : VkBlendOp,
    pub colorWriteMask : VkColorComponentFlags,
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkPipelineColorBlendStateCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkPipelineColorBlendStateCreateFlags,
    pub logicOpEnable : VkBool32,
    pub logicOp : VkLogicOp,
    pub attachmentCount : u32,
    pub pAttachments : *const VkPipelineColorBlendAttachmentState,
    pub blendConstants : [f32; 4]
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkPipelineLayoutCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkPipelineLayoutCreateFlags,
    pub setLayoutCount : u32,
    pub pSetLayouts : *const VkDescriptorSetLayout,
    pub pushConstantRangeCount : u32,
    pub pPushConstantRanges : *const VkPushConstantRange
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkPushConstantRange {
    pub stageFlags : VkShaderStageFlags,
    pub offset : u32,
    pub size : u32,
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkRenderPassCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkRenderPassCreateFlags,
    pub attachmentCount : u32,
    pub pAttachments : *const VkAttachmentDescription,
    pub subpassCount : u32,
    pub pSubpasses : *const VkSubpassDescription,
    pub dependencyCount : u32,
    pub pDependencies : *const VkSubpassDependency
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkSubpassDependency {
    pub srcSubpass : u32,
    pub dstSubpass : i32,
    pub srcStageMask : VkPipelineStageFlags,
    pub dstStageMask : VkPipelineStageFlags,
    pub srcAccessMask : VkAccessFlags,
    pub dstAccessMask : VkAccessFlags,
    pub dependencyFlags : VkDependencyFlags
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkSubpassDescription {
    pub flags : VkSubpassDescriptionFlags,
    pub pipelineBindPoint : VkPipelineBindPoint,
    pub inputAttachmentCount : u32,
    pub pInputAttachments : *const VkAttachmentReference,
    pub colorAttachmentCount : u32,
    pub pColorAttachments : *const VkAttachmentReference,
    pub pResolveAttachments : *const VkAttachmentReference,
    pub pDepthStencilAttachment : *const VkAttachmentReference,
    pub preserveAttachmentCount : u32,
    pub pPreserveAttachments : *const u32
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkAttachmentReference {
    pub attachment : u32,
    pub layout : VkImageLayout
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkAttachmentDescription {
    pub flags : VkAttachmentDescriptionFlags,
    pub format : VkFormat,
    pub samples : VkSampleCountFlagBits,
    pub loadOp : VkAttachmentLoadOp,
    pub storeOp : VkAttachmentStoreOp,
    pub stencilLoadOp : VkAttachmentLoadOp,
    pub stencilStoreOp : VkAttachmentStoreOp,
    pub initialLayout : VkImageLayout,
    pub finalLayout : VkImageLayout
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkPipelineCacheCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkPipelineCacheCreateFlags,
    pub initialDataSize : usize,
    pub pInitialData : *const raw::c_void
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkGraphicsPipelineCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkPipelineCreateFlags,
    pub stageCount : u32,
    pub pStages : *const VkPipelineShaderStageCreateInfo,
    pub pVertexInputState : *const VkPipelineVertexInputStateCreateInfo,
    pub pInputAssemblyState : *const VkPipelineInputAssemblyStateCreateInfo,
    pub pTessellationState : *const VkPipelineTessellationStateCreateInfo,
    pub pViewportState : *const VkPipelineViewportStateCreateInfo,
    pub pRasterizationState : *const VkPipelineRasterizationStateCreateInfo,
    pub pMultisampleState : *const VkPipelineMultisampleStateCreateInfo,
    pub pDepthStencilState : *const VkPipelineDepthStencilStateCreateInfo,
    pub pColorBlendState : *const VkPipelineColorBlendStateCreateInfo,
    pub pDynamicState : *const VkPipelineDynamicStateCreateInfo,
    pub layout : VkPipelineLayout,
    pub renderPass : VkRenderPass,
    pub subpass : u32,
    pub basePipelineHandle : VkPipeline,
    pub basePipelineIndex : i32
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkPipelineDynamicStateCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkPipelineDynamicStateCreateFlags,
    pub dynamicStateCount : u32,
    pub pDynamicStates : *const VkDynamicState,
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkPipelineDepthStencilStateCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkPipelineDepthStencilStateCreateFlags,
    pub depthTestEnable : VkBool32,
    pub depthWriteEnable : VkBool32,
    pub depthCompareOp : VkCompareOp,
    pub depthBoundsTestEnable : VkBool32,
    pub stencilTestEnable : VkBool32,
    pub front : VkStencilOpState,
    pub back : VkStencilOpState,
    pub minDepthBounds : f32,
    pub maxDepthBounds : f32
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkPipelineTessellationStateCreateInfo {
    sType : VkStructureType,
    pNext : *const raw::c_void,
    flags : VkPipelineTessellationStateCreateFlags,
    patchControlPoints : u32
}

#[derive(Default, Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkStencilOpState {
    pub failOp : VkStencilOp,
    pub passOp : VkStencilOp,
    pub depthFailOp : VkStencilOp,
    pub compareOp : VkCompareOp,
    pub compareMask : u32,
    pub writeMask : u32,
    pub reference : u32
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkFramebufferCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkFramebufferCreateFlags,
    pub renderPass : VkRenderPass,
    pub attachmentCount : u32,
    pub pAttachments : *const VkImageView,
    pub width : u32,
    pub height : u32,
    pub layers : u32
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkCommandPoolCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkCommandPoolCreateFlags,
    pub queueFamilyIndex : u32
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkCommandBufferAllocateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub commandPool : VkCommandPool,
    pub level : VkCommandBufferLevel,
    pub commandBufferCount : u32
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkCommandBufferBeginInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkCommandBufferUsageFlags,
    pub pInheritanceInfo : *const VkCommandBufferInheritanceInfo
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkCommandBufferInheritanceInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub renderPass : VkRenderPass,
    pub subpass : u32,
    pub framebuffer : VkFramebuffer,
    pub occlusionQueryEnable : VkBool32,
    pub queryFlags : VkQueryControlFlags,
    pub pipelineStatistics : VkQueryPipelineStatisticFlags
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkRenderPassBeginInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub renderPass : VkRenderPass,
    pub framebuffer : VkFramebuffer,
    pub renderArea : VkRect2D,
    pub clearValueCount : u32,
    pub pClearValues : *const VkClearValue
}

#[derive(Clone, Copy)]
#[allow(non_snake_case)]
#[repr(C)]
pub union VkClearColorValue {
    pub float32 : [f32; 4],
    pub int32 : [i32; 4],
    pub uint32 : [u32; 4],
}

#[derive(Clone, Copy)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkClearDepthStencilValue {
    pub depth : f32,
    pub stencil : u32
}

#[allow(non_snake_case)]
#[repr(C)]
pub union VkClearValue {
    pub color : VkClearColorValue,
    pub depthStencil : VkClearDepthStencilValue
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkSemaphoreCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkSemaphoreCreateFlags
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkFenceCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkFenceCreateFlags
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkSubmitInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub waitSemaphoreCount : u32,
    pub pWaitSemaphores : *const VkSemaphore,
    pub pWaitDstStageMask : *const VkPipelineStageFlags,
    pub commandBufferCount : u32,
    pub pCommandBuffers : *const VkCommandBuffer,
    pub signalSemaphoreCount : u32,
    pub pSignalSemaphores : *const VkSemaphore
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkPresentInfoKHR {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub waitSemaphoreCount : u32,
    pub pWaitSemaphores : *const VkSemaphore,
    pub swapchainCount : u32,
    pub pSwapchains : *const VkSwapchainKHR,
    pub pImageIndices : *const u32,
    pub pResults : *mut VkResult
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkBufferCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkBufferCreateFlags,
    pub size : VkDeviceSize,
    pub usage : VkBufferUsageFlags,
    pub sharingMode : VkSharingMode,
    pub queueFamilyIndexCount : u32,
    pub pQueueFamilyIndices : *const u32
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkBufferViewCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkBufferViewCreateFlags,
    pub buffer : VkBuffer,
    pub format : VkFormat,
    pub offset : VkDeviceSize,
    pub range : VkDeviceSize
}

#[derive(Debug, Default)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkMemoryRequirements {
    pub size : VkDeviceSize,
    pub alignment : VkDeviceSize,
    pub memoryTypeBits : u32
}

#[derive(Debug, Default)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkPhysicalDeviceMemoryProperties {
    pub memoryTypeCount : u32,
    pub memoryTypes : [VkMemoryType; VK_MAX_MEMORY_TYPES],
    pub memoryHeapCount : u32,
    pub memoryHeaps : [VkMemoryHeap; VK_MAX_MEMORY_HEAPS]
}

#[derive(Debug, Default)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkMemoryType {
    pub propertyFlags : VkMemoryPropertyFlags,
    pub heapIndex : u32
}

#[derive(Debug, Default)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkMemoryHeap {
    pub size : VkDeviceSize,
    pub flags : VkMemoryHeapFlags,
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkMemoryAllocateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub allocationSize : VkDeviceSize,
    pub memoryTypeIndex : u32
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkBufferCopy {
    pub srcOffset : VkDeviceSize,
    pub dstOffset : VkDeviceSize,
    pub size : VkDeviceSize
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkDescriptorSetLayoutCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkDescriptorSetLayoutCreateFlags,
    pub bindingCount : u32,
    pub pBindings : *const VkDescriptorSetLayoutBinding
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkDescriptorSetLayoutBinding {
    pub binding : u32,
    pub descriptorType : VkDescriptorType,
    pub descriptorCount : u32,
    pub stageFlags : VkShaderStageFlags,
    pub pImmutableSamplers : *const VkSampler
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkDescriptorPoolSize {
    pub r#type : VkDescriptorType,
    pub descriptorCount : u32
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkDescriptorPoolCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkDescriptorPoolCreateFlags,
    pub maxSets : u32,
    pub poolSizeCount : u32,
    pub pPoolSizes : *const VkDescriptorPoolSize
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkCopyDescriptorSet {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub srcSet : VkDescriptorSet,
    pub srcBinding : u32,
    pub srcArrayElement : u32,
    pub dstSet : VkDescriptorSet,
    pub dstBinding : u32,
    pub dstArrayElement : u32,
    pub descriptorCount : u32
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkWriteDescriptorSet {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub dstSet : VkDescriptorSet,
    pub dstBinding : u32,
    pub dstArrayElement : u32,
    pub descriptorCount : u32,
    pub descriptorType : VkDescriptorType,
    pub pImageInfo : *const VkDescriptorImageInfo,
    pub pBufferInfo : *const VkDescriptorBufferInfo,
    pub pTexelBufferView : *const VkBufferView
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkDescriptorSetAllocateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub descriptorPool : VkDescriptorPool,
    pub descriptorSetCount : u32,
    pub pSetLayouts : *const VkDescriptorSetLayout
}

#[derive(Debug, Clone, Copy)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkDescriptorBufferInfo {
    pub buffer : VkBuffer,
    pub offset : VkDeviceSize,
    pub range : VkDeviceSize
}

#[derive(Debug, Clone, Copy)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkDescriptorImageInfo {
    pub sampler : VkSampler,
    pub imageView : VkImageView,
    pub imageLayout : VkImageLayout
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkImageCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkImageCreateFlags,
    pub imageType : VkImageType,
    pub format : VkFormat,
    pub extent : VkExtent3D,
    pub mipLevels : u32,
    pub arrayLayers : u32,
    pub samples : u32,
    pub tiling : u32,
    pub usage : VkImageUsageFlags,
    pub sharingMode : VkSharingMode,
    pub queueFamilyIndexCount : u32,
    pub pQueueFamilyIndices : *const u32,
    pub initialLayout : VkImageLayout
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkImageMemoryBarrier {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub srcAccessMask : VkAccessFlags,
    pub dstAccessMask : VkAccessFlags,
    pub oldLayout : VkImageLayout,
    pub newLayout : VkImageLayout,
    pub srcQueueFamilyIndex : u32,
    pub dstQueueFamilyIndex : u32,
    pub image : VkImage,
    pub subresourceRange : VkImageSubresourceRange
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkBufferMemoryBarrier {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub srcAccessMask : VkAccessFlags,
    pub dstAccessMask : VkAccessFlags,
    pub srcQueueFamilyIndex : u32,
    pub dstQueueFamilyIndex : u32,
    pub buffer : VkBuffer,
    pub offset : VkDeviceSize,
    pub size : VkDeviceSize
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkMemoryBarrier {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub srcAccessMask : VkAccessFlags,
    pub dstAccessMask : VkAccessFlags
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkBufferImageCopy {
    pub bufferOffset : VkDeviceSize,
    pub bufferRowLength : u32,
    pub bufferImageHeight : u32,
    pub imageSubresource : VkImageSubresourceLayers,
    pub imageOffset : VkOffset3D,
    pub imageExtent : VkExtent3D
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkImageSubresourceLayers {
    pub aspectMask : VkImageAspectFlags,
    pub mipLevel : u32,
    pub baseArrayLayer : u32,
    pub layerCount : u32
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkSamplerCreateInfo {
    pub sType : VkStructureType,
    pub pNext : *const raw::c_void,
    pub flags : VkSamplerCreateFlags,
    pub magFilter : VkFilter,
    pub minFilter : VkFilter,
    pub mipmapMode : VkSamplerMipmapMode,
    pub addressModeU : VkSamplerAddressMode,
    pub addressModeV : VkSamplerAddressMode,
    pub addressModeW : VkSamplerAddressMode,
    pub mipLodBias : f32,
    pub anisotropyEnable : VkBool32,
    pub maxAnisotropy : f32,
    pub compareEnable : VkBool32,
    pub compareOp : VkCompareOp,
    pub minLod : f32,
    pub maxLod : f32,
    pub borderColor : VkBorderColor,
    pub unnormalizedCoordinates : VkBool32
}

#[derive(Default, Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkFormatProperties {
    pub linearTilingFeatures : VkFormatFeatureFlags,
    pub optimalTilingFeatures : VkFormatFeatureFlags,
    pub bufferFeatures : VkFormatFeatureFlags
}

#[derive(Debug)]
#[allow(non_snake_case)]
#[repr(C)]
pub struct VkComputePipelineCreateInfo {
    pub sType: VkStructureType,
    pub pNext: *const raw::c_void,
    pub flags: VkPipelineCreateFlags,
    pub stage: VkPipelineShaderStageCreateInfo,
    pub layout: VkPipelineLayout,
    pub basePipelineHandle: VkPipeline,
    pub basePipelineIndex: i32
}

pub struct VkSystemAllocationScope;
pub struct VkInternalAllocationType;

pub mod win32
{
    use core::ffi::c_void;

    pub type VkWin32SurfaceCreateFlagsKHR = super::VkFlags;

    #[allow(non_snake_case)]
    #[repr(C)]
    pub struct VkWin32SurfaceCreateInfoKHR {
        pub sType : super::VkStructureType,
        pub pNext : *const super::raw::c_void,
        pub flags : VkWin32SurfaceCreateFlagsKHR,
        pub hinstance : *const c_void,
        pub hwnd : *const c_void
    }
}

macro_rules! VkPfn
{
    ($name:ident, ($( $param_name:ident ),*) , ($( $param_type:ty ),*) => $return:ty) => {
        #[allow(non_camel_case_types, unused_variables, dead_code)]
        pub type $name = fn($( $param_name: $param_type, )*) -> $return;
    }
}

#[allow(non_camel_case_types, unused_variables, dead_code)]
pub type PFN_vkVoidFunction = fn() -> ();

VkPfn!(PFN_vkAllocationFunction,
       (pUserData, size, alignment, allocationScope),
       (*const raw::c_void, usize, usize, VkSystemAllocationScope)
       => *const raw::c_void);

VkPfn!(PFN_vkReallocationFunction,
       (pUserData, pOriginal, size, alignment, allocationScope),
       (*const raw::c_void, *const raw::c_void, usize, usize, VkSystemAllocationScope)
       => *const raw::c_void );

VkPfn!(PFN_vkFreeFunction,
       (pUserData, pMemory),
       (*const raw::c_void, *const raw::c_void)
       => ());

VkPfn!(PFN_vkInternalAllocationNotification,
       (pUserData, size, allocationType, allocationScope),
       (*const raw::c_void, usize, VkInternalAllocationType, VkSystemAllocationScope)
       => ());

VkPfn!(PFN_vkInternalFreeNotification,
       (pUserData, size, allocationType, allocationScope),
       (*const raw::c_void, usize, VkInternalAllocationType, VkSystemAllocationScope)
       => ());

VkPfn!(PFN_vkDebugReportCallbackEXT,
       (flags, objectType, object, location, messageCode, pLayerPrefix, pMessage, pUserData),
       (VkDebugReportFlagsEXT, VkDebugReportObjectTypeEXT, u64, usize, i32, *const u8, *const u8, *mut raw::c_void)
       => VkBool32);

VkPfn!(PFN_vkDebugUtilsMessengerCallbackEXT,
       (messageSeverity, messageTypes, pCallbackData, pUserData),
       (VkDebugUtilsMessageSeverityFlagBitsEXT, VkDebugUtilsMessageTypeFlagsEXT, *const VkDebugUtilsMessengerCallbackDataEXT, *mut raw::c_void)
       => VkBool32);
