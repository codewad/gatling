use super::types;
use super::LogicalDevice;
use super::{
    vkDestroyPipeline
};

pub struct ComputePipeline<'d>
{
    handle: types::VkPipeline,
    device: &'d LogicalDevice
}

impl Drop for ComputePipeline<'_>
{
    fn drop(&mut self)
    {
        unsafe { vkDestroyPipeline(self.device.get_handle(), self.get_handle(), std::ptr::null()); };
    }
}

impl<'d> ComputePipeline<'d>
{
    pub fn new(handle: types::VkPipeline, device: &'d LogicalDevice) -> Self
    {
        Self
        {
            handle: handle,
            device: device
        }
    }
}

impl ComputePipeline<'_>
{
    pub fn get_handle(&self) -> types::VkPipeline
    {
        self.handle
    }
}
