extern crate cc;

fn main()
{
    if std::env::var("CARGO_CFG_TARGET_FAMILY").unwrap() == "windows"
    {
        cc::Build::new()
            .file("src/vulkan.c")
            .compile("cvulk");

        //println!("cargo:rustc-link-lib=gdi32");
    }
}
